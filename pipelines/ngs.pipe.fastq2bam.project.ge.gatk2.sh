#!/bin/bash
## 
## DESCRIPTION:   Run alignment pipeline on each sample directory
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2014.01.21
## MODIFIED BY:   Jin Kim jjinking(at)gmail(dot)com
##
## USAGE:         ngs.pipe.fastq2bam.project.ge.gatk2.sh
##                                                       batchname                 # Batch name for this run
##                                                       (HG|B3x)                  # Resource type
##                                                       (WGS|target_region.bed)   # If not WGS, then target region bed or intervals file
##                                                       Sample_X
##                                                       [Sample_Y [...]]
##
## OUTPUT:        Alignment files in each sample directory
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 4 $# $0

# Process input params
BAT=$1; shift
RCT=$1; shift
TGT=$1; shift
SMD=$@

# Set resource vars
if [ $RCT = 'HG' ]; then
  REF=$HG_REF_GATK2
  HAP=$HG_HAPMAP_VCF_GATK2
  OMN=$HG_OMNI1000_VCF_GATK2
  TGS=$HG_1000G_SNPS_PHASE1_HC_VCF_GATK2
  DBS=$HG_DBSNP_VCF_GATK2
  MIL=$HG_MILLS_INDELS_VCF_GATK2
  IND=$HG_1000G_INDELS_PHASE1_VCF_GATK2
else
  REF=$B3x_REF_GATK2
  HAP=$B3x_HAPMAP_VCF_GATK2
  OMN=$B3x_OMNI1000_VCF_GATK2
  TGS=$B3x_1000G_SNPS_PHASE1_HC_VCF_GATK2
  DBS=$B3x_DBSNP_VCF_GATK2
  MIL=$B3x_MILLS_INDELS_VCF_GATK2
  IND=$B3x_1000G_INDELS_PHASE1_VCF_GATK2
fi

# Submit each sample directory to the grid engine
for sampledir in $SMD; do
  sampledir=`echo $sampledir | sed 's/\/$//'`
  SAMPLENAME=`echo $sampledir | cut -f2- -d'_'`
  qsub_wrapper.sh                                                         \
    $SAMPLENAME.fastq2bam                                                 \
    $Q_LOW                                                                \
    1                                                                     \
    none                                                                  \
    n                                                                     \
    $NGS_ANALYSIS_DIR/pipelines/ngs.pipe.fastq2bam.sample.ge.gatk2.sh     \
      $sampledir                                                          \
      $REF                                                                \
      $DBS                                                                \
      $MIL                                                                \
      $IND                                                                \
      $TGT
done

exit
# Are the rest really necessary?


# Wait 1 hour
sleep 3600

# Wait for bam file to be created
BAM=''
for sampledir in $SMD; do
  sampledir=`echo $sampledir | sed 's/\/$//'`
  SAMPLENAME=`echo $sampledir | cut -f2- -d'_'`
  qsub_wrapper.sh                \
    $BAT.wait4bam                \
    $Q_LOW                       \
    1                            \
    $SAMPLENAME.processbam       \
    n                            \
    hello_world.sh
  BAM="$BAM $sampledir/*recal.bam"
done

# Run aggregate bam QC
qsub_wrapper.sh                                                     \
  $BAT.bam.QC                                                       \
  $Q_HIGH                                                           \
  1                                                                 \
  $BAT.wait4bam                                                     \
  n                                                                 \
  ngs.pipe.qc.bam.ge.gatk2.sh                                       \
    AGG                                                             \
    $BAT                                                            \
    $REF                                                            \
    $TGT                                                            \
    $BAM
