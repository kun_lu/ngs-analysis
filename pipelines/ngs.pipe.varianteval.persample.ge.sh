#!/bin/bash
## 
## DESCRIPTION:   Run GATK VariantEval for each sample in a vcf file
##                Use grid engine using qsub
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         ngs.pipe.varianteval.persample.ge.sh
##                                                     ref.fa
##                                                     in.vcf
##                                                     outprefix
##
## OUTPUT:        VariantEval output for each sample in vcf file
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 3 $# $0

# Process input parameters
REF=$1
VCF=$2
OUT=$3

# Make sure input files exist
assert_file_exists_w_content $REF
assert_file_exists_w_content $VCF

QSUB=$NGS_ANALYSIS_DIR/modules/util/qsub_wrapper.sh
TOOL=$NGS_ANALYSIS_DIR/modules/variant/gatk2.varianteval.sh

# Get sample names within vcf file
for sn in `python_ngs.sh vcf_tool.py sample_names $VCF`; do
  sn_out=$OUT.`echo $sn | sed 's/\//_/g'`
  $QSUB $sn_out                              \
        $Q_HIGH                              \
        1                                    \
        none                                 \
        n                                    \
        $TOOL $REF $sn_out "-sn $sn" $VCF
done
