#!/bin/bash
## 
## DESCRIPTION:   Run bam-readcount on a list of bamfiles
##                Use grid engine using qsub
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2014.01.21
## MODIFIED BY:   Jin Kim jjinking(at)gmail(dot)com
##
## USAGE:         ngs.pipe.bam_readcount.ge.sh
##                                             grid_engine_job_name
##                                             bamfileslist
##                                             ref.fa
##                                             chrom_type            # chr | num ('chr' if chr1, chr2,..,chrY; 'num' if 1,2,..,chrY)
##                                             min_base_quality
##                                             min_mapping_quality
##                                             [target_region]
##
## OUTPUT:        mpileups for the bamfiles listed in bamlist
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 6 $# $0

# Process input parameters
JOBNAME=$1; shift
BAMLIST=$1; shift
REFEREN=$1; shift
CHROM_T=$1; shift
MIN_B_Q=$1; shift
MIN_M_Q=$1; shift
TARGETI=$1; shift

# Qsub wrapper script path
QSUB=$NGS_ANALYSIS_DIR/modules/util/qsub_wrapper.sh

# Run tool
for bamfile in `cat $BAMLIST`; do
  $QSUB $JOBNAME                                                \
        $Q_MID                                                  \
        4                                                       \
        none                                                    \
        n                                                       \
        $NGS_ANALYSIS_DIR/modules/align/bam_readcount.sh        \
          $bamfile                                              \
          $REFEREN                                              \
          $CHROM_T                                              \
          $MIN_B_Q                                              \
          $MIN_M_Q                                              \
          $TARGETI
done
