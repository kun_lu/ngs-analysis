#!/bin/bash
## 
## DESCRIPTION:   Run samtools mpileup for each bamfile in a list of bamfiles
##                Use grid engine using qsub
##                Bamlist is a single column list of the paths to the bamfiles
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         ngs.pipe.mpileup.ge.sh
##                                       grid_engine_job_name
##                                       ref.fa
##                                       "samtools mpileup options"
##                                       in1.bam
##                                       [in2.bam [...]]
##                                       
##
## OUTPUT:        mpileups for the bamfiles listed in bamlist
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 4 $# $0

# Process input parameters
JOBNAME=$1; shift
REFEREN=$1; shift
OPTIONS=$1; shift
BAMLIST=$@;

# Qsub wrapper script path
QSUB=$NGS_ANALYSIS_DIR/modules/util/qsub_wrapper.sh

# Run samtools mpileup
for bamfile in $BAMLIST; do
  $QSUB $JOBNAME                                                                        \
        $Q_HIGH                                                                         \
        1                                                                               \
        none                                                                            \
        n                                                                               \
        samtools.mpileup.sh $bamfile $REFEREN "$OPTIONS"
done
