#!/bin/bash
## 
## DESCRIPTION:   Run GATK SomaticIndelDetector
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2013.11.08
## LAST MODIFIED: 2014.01.21
## MODIFIED BY:   Jin Kim jjinking(at)gmail(dot)com
##
## USAGE:         ngs.pipe.somatic.somaticindel.ge.sh
##                                                    sample
##                                                    normal.bam
##                                                    tumor.bam
##                                                    (hg|b3x)
##                                                    output_dir
##                                                    [target_interval]
##
## OUTPUT:        SomaticIndelDetector output files
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 5 $# $0

# Process input parameters
SAMPLEN=$1; shift
BAMNORM=$1; shift
BAMTUMR=$1; shift
RESOURC=$1; shift
OUT_DIR=`echo $1 | sed 's/\///'`; shift
TARGETI=$1; shift

# Make sure input files exist
assert_file_exists_w_content $BAMNORM
assert_file_exists_w_content $BAMTUMR

# Set resource vars
if [ $RESOURC = 'hg' ]; then
  REF=$HG_REF_GATK2
  HAP=$HG_HAPMAP_VCF_GATK2
  OMN=$HG_OMNI1000_VCF_GATK2
  TGS=$HG_1000G_SNPS_PHASE1_HC_VCF_GATK2
  DBS=$HG_DBSNP_VCF_GATK2
  MIL=$HG_MILLS_INDELS_VCF_GATK2
  IND=$HG_1000G_INDELS_PHASE1_VCF_GATK2
else
  REF=$B3x_REF_GATK2
  HAP=$B3x_HAPMAP_VCF_GATK2
  OMN=$B3x_OMNI1000_VCF_GATK2
  TGS=$B3x_1000G_SNPS_PHASE1_HC_VCF_GATK2
  DBS=$B3x_DBSNP_VCF_GATK2
  MIL=$B3x_MILLS_INDELS_VCF_GATK2
  IND=$B3x_1000G_INDELS_PHASE1_VCF_GATK2
fi


QSUB=$NGS_ANALYSIS_DIR/modules/util/qsub_wrapper.sh
#------------------------------------------------------------------------
# SomaticIndelDetector

# Set up output directory
create_dir $OUT_DIR

# Run GATK SomaticIndelDetector
$QSUB $OUT_DIR.$SAMPLEN                                                               \
      $Q_HIGH                                                                         \
      1                                                                               \
      none                                                                            \
      n                                                                               \
      gatk.somaticindeldetector.sh                                                    \
        $BAMNORM                                                                      \
        $BAMTUMR                                                                      \
        $OUT_DIR/$SAMPLEN                                                             \
        $REF

# Extract somatic variant
$QSUB $OUT_DIR.extractsom.$SAMPLEN                                                    \
      $Q_MID                                                                          \
      1                                                                               \
      $OUT_DIR.$SAMPLEN                                                               \
      n                                                                               \
      python_ngs.sh vcf_filter.py                                                     \
        --col-info SOMATIC                                                            \
        $OUT_DIR/$SAMPLEN.somaticindel.vcf                                            \
        -o $OUT_DIR/$SAMPLEN.somaticindel.somatic.vcf

# Remove old dbSNP
$QSUB $OUT_DIR.removersid.$SAMPLEN                                                    \
      $Q_MID                                                                          \
      1                                                                               \
      $OUT_DIR.extractsom.$SAMPLEN                                                    \
      n                                                                               \
      python_ngs.sh vcf_tool.py                                                       \
        remove_id                                                                     \
        $OUT_DIR/$SAMPLEN.somaticindel.somatic.vcf                                    \
        -o $OUT_DIR/$SAMPLEN.somaticindel.somatic.norsid.vcf

# Annotate with latest dbSNP
$QSUB $OUT_DIR.addrsid.$SAMPLEN                                                       \
      $Q_MID                                                                          \
      1                                                                               \
      $OUT_DIR.removersid.$SAMPLEN                                                    \
      n                                                                               \
      vcf_add_rsid.sh                                                                 \
        $OUT_DIR/$SAMPLEN.somaticindel.somatic.norsid.vcf                             \
        $DBS                                                                          \
        $OUT_DIR/$SAMPLEN.somaticindel.somatic.rsid.vcf

# Annotate vcf
SAMPLENAME_N=`basename $BAMNORM | cut -f1 -d .`
SAMPLENAME_T=`basename $BAMTUMR | cut -f1 -d .`
#if [ ! -s $OUT_DIR.samplenames.$SAMPLEN ]; then
#  echo $SAMPLEN $SAMPLENAME_N $SAMPLENAME_T | sed 's/ /\t/g' > $OUT_DIR.samplenames.$SAMPLEN
#fi
$QSUB $OUT_DIR.annot.$SAMPLEN                                                         \
      $Q_HIGH                                                                         \
      1                                                                               \
      $OUT_DIR.addrsid.$SAMPLEN                                                       \
      n                                                                               \
      ngs.pipe.snpeff.snpsift.sh $OUT_DIR/$SAMPLEN.somaticindel.somatic.rsid.vcf      \
                                 "-cancer"
#                                 "-cancer -cancerSample $OUT_DIR.samplenames.$SAMPLEN"

# Convert annotated vcf to maf
VCF=$OUT_DIR/$SAMPLEN.somaticindel.somatic.rsid.snpeff.vcf
$QSUB $OUT_DIR.vcf2maf.$SAMPLEN                                                       \
      $Q_MID                                                                          \
      1                                                                               \
      $OUT_DIR.annot.$SAMPLEN                                                         \
      n                                                                               \
      python_ngs.sh vcf2maf.py $VCF                                                   \
                               $SAMPLEN                                               \
                               $SAMPLENAME_N                                          \
                               $SAMPLENAME_T                                          \
                               $NGS_ANALYSIS_DIR/resources/gene2entrezid              \
                               -o $OUT_DIR/$SAMPLEN.maf                               \
                               -e
