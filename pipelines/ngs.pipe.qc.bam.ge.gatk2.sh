#!/bin/bash
##
## DESCRIPTION:   Run some qc tools on bam files
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2014.01.21
## MODIFIED BY:   Jin Kim jjinking(at)gmail(dot)com
## 
## USAGE:         ngs.pipe.qc.bam.ge.gatk2.sh
##                                            (AGG|SINGLE|BOTH)        # AGG: Aggregate only, SINGLE: Single sample only, BOTH: AGG + SINGLE
##                                            outprefix                # Output prefix
##                                            ref.fa                   # Reference genome fasta file
##                                            (WGS|target_region.bed)  # If not WGS, then target region bed or intervals file
##                                            in1.bam                  # Input bam files
##                                            [in2.bam [...]]
##
## OUTPUT:        QC outputs for each bam file inputted
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 5 $# $0

# Process input params
MOD=$1; shift
OUT=$1; shift
REF=$1; shift
TGT=$1; shift
BAM=$@

# Check to make sure reference file exists
assert_file_exists_w_content $REF

# Check if target region is specified
if [ ! $TGT = 'WGS' ]; then
  assert_file_exists_w_content $TGT
fi

QSUB=$NGS_ANALYSIS_DIR/modules/util/qsub_wrapper.sh

# Set parameters for depthofcov total
DEPTHOFCOV_TOTAL_OPT=""
DEPTHOFCOV_TOTAL_NT=1
if [ ! $TGT = 'WGS' ]; then
  DEPTHOFCOV_TOTAL_NT=6
  DEPTHOFCOV_TOTAL_OPT="-omitLocusTable -omitIntervals -omitBaseOutput -nt $DEPTHOFCOV_TOTAL_NT"
fi

# Run aggregate bam QC
if [ $MOD = 'AGG' ] || [ $MOD = 'BOTH' ]; then
  # Target aligned depth of coverage for all samples together
  if [ ! $TGT = 'WGS' ]; then
    $QSUB $OUT.depthofcov.target.all                                                 \
          $Q_HIGH                                                                    \
          4                                                                          \
          none                                                                       \
          n                                                                          \
          $NGS_ANALYSIS_DIR/modules/align/gatk2.depthofcoverage.sh                   \
            $REF                                                                     \
            $OUT.target                                                              \
            "-L $TGT"                                                                \
            $BAM
  fi
  
  # Total aligned depth of coverage for all samples together
  $QSUB $OUT.depthofcov.total.all                                                    \
        $Q_HIGH                                                                      \
        $DEPTHOFCOV_TOTAL_NT                                                         \
        none                                                                         \
        n                                                                            \
        $NGS_ANALYSIS_DIR/modules/align/gatk2.depthofcoverage.sh                     \
          $REF                                                                       \
          $OUT.total                                                                 \
          "$DEPTHOFCOV_TOTAL_OPT"                                                    \
  	$BAM
fi

# Run QC for each sample bam file
if [ $MOD = 'SINGLE' ] || [ $MOD = 'BOTH' ]; then
  
  for bamfile in $BAM; do
    if [ ! $TGT = 'WGS' ]; then
      # Target aligned depth of coverage
      $QSUB $OUT.depthofcov.target                                                   \
            $Q_HIGH                                                                  \
            4                                                                        \
            none                                                                     \
            n                                                                        \
            $NGS_ANALYSIS_DIR/modules/align/gatk2.depthofcoverage.sh                 \
              $REF                                                                   \
              $bamfile.target                                                        \
              "-L $TGT"                                                              \
              $bamfile
  
      # Target number of reads mapped
      $QSUB $OUT.countreads.target                                                   \
            $Q_HIGH                                                                  \
            4                                                                        \
            none                                                                     \
            n                                                                        \
            $NGS_ANALYSIS_DIR/modules/align/gatk2.countreads.sh                      \
              $REF                                                                   \
              $bamfile.target                                                        \
              "-nct 4 -L $TGT"                                                       \
              $bamfile
    fi
  
    # Total aligned depth of coverage
    $QSUB $OUT.depthofcov.total                                                      \
          $Q_HIGH                                                                    \
          $DEPTHOFCOV_TOTAL_NT                                                       \
          none                                                                       \
          n                                                                          \
          $NGS_ANALYSIS_DIR/modules/align/gatk2.depthofcoverage.sh                   \
            $REF                                                                     \
            $bamfile.total                                                           \
            "$DEPTHOFCOV_TOTAL_OPT"                                                  \
    	  $bamfile
  
    # Total number of reads mapped
    $QSUB $OUT.countreads.total                                                      \
          $Q_HIGH                                                                    \
          4                                                                          \
          none                                                                       \
          n                                                                          \
          $NGS_ANALYSIS_DIR/modules/align/gatk2.countreads.sh                        \
            $REF                                                                     \
            $bamfile.total                                                           \
            "-nct 4"                                                                 \
            $bamfile
  
    # QualityScoreDistribution
    $QSUB $OUT.qscoredist                                                            \
          $Q_MID                                                                     \
          1                                                                          \
          none                                                                       \
          n                                                                          \
          $NGS_ANALYSIS_DIR/modules/align/picard.qualityscoredistribution.sh         \
  	  $bamfile
  
    # CollectGcBiasMetrics
    $QSUB $OUT.gcbias                                                                \
          $Q_MID                                                                     \
          1                                                                          \
          none                                                                       \
          n                                                                          \
          $NGS_ANALYSIS_DIR/modules/align/picard.collectgcbiasmetrics.sh             \
  	    $bamfile                                                                 \
            $REF
  
    # CollectInsertSizeMetrics
    $QSUB $OUT.insertsize                                                            \
          $Q_MID                                                                     \
          1                                                                          \
          none                                                                       \
          n                                                                          \
          $NGS_ANALYSIS_DIR/modules/align/picard.collectinsertsizemetrics.sh         \
  	  $bamfile
  
    # MeanQualityByCycle
    $QSUB $OUT.meanqbycycle                                                          \
          $Q_MID                                                                     \
          1                                                                          \
          none                                                                       \
          n                                                                          \
          $NGS_ANALYSIS_DIR/modules/align/picard.meanqualitybycycle.sh               \
  	  $bamfile
  
    # CollectAlignmentSummaryMetrics
    $QSUB $OUT.alignsummetric                                                        \
          $Q_MID                                                                     \
          1                                                                          \
          none                                                                       \
          n                                                                          \
          $NGS_ANALYSIS_DIR/modules/align/picard.collectalignmentsummarymetrics.sh   \
  	  $bamfile
  
    # Samtools idxstats
    $QSUB $OUT.idxstats                                                              \
          $Q_MID                                                                     \
          1                                                                          \
          none                                                                       \
          n                                                                          \
          samtools.idxstats.sh $bamfile
  
    # Samtools flagstat
    $QSUB $OUT.flagstat                                                              \
          $Q_MID                                                                     \
          1                                                                          \
          none                                                                       \
          n                                                                          \
          samtools.flagstat.sh $bamfile
  done
fi