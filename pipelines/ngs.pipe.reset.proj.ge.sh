#!/bin/bash
## 
## DESCRIPTION:   Remove all non-original fastq.gz files within multiple
##                sample directories within a Hiseq project directory
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2014.01.21
## MODIFIED BY:   Jin Kim jjinking(at)gmail(dot)com
##
## USAGE:         ngs.pipe.reset.proj.ge.sh Sample_X [Sample_Y [...]]
##
## OUTPUT:        None
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 1 $# $0

# Submit each sample directory to the grid engine
SAMPLEDIRS=$@
for sampledir in $SAMPLEDIRS; do
  sampledir=`echo $sampledir | sed 's/\/$//'`
  SAMPLENAME=`echo $sampledir | cut -f2- -d'_'`
  qsub_wrapper.sh                                                   \
    $SAMPLENAME.reset                                               \
    $Q_MID                                                          \
    4                                                               \
    none                                                            \
    n                                                               \
    $NGS_ANALYSIS_DIR/modules/util/rm_non_orig_fastq.sh             \
      $sampledir                                                    \
      4
done