#!/bin/bash
## 
## DESCRIPTION:   Pipeline for calling somatic variants from a list of bam files using VarScan
##                Uses grid engine using qsub
##                Bamlist should be tab-delimited file given in the format specified
##                by Genome MuSiC, created by Washington Univ in St Louis.
##                Bamlist columns: sample_id
##                                 path/to/normal.bam
##                                 path/to/tumor.bam
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         ngs.pipe.somatic.varscan.multiple.ge.sh
##                                                        bamlist               # 3 column file (sample_id, normal.bam, tumor.bam)
##                                                        ref.fa                # Reference genome file used in alignment
##                                                        "mpileup_options"     # i.e. "-Q 30 -q 23 -L target_region.bed"
##                                                        varscan_output_dir    # Name of directory for varscan outputs, i.e. varscan
##                                                        somatic-p-value       # (VarScan somatic) P-value threshold to call a somatic site [0.05]
##                                                        normal-purity         # (VarScan somatic) Estimated purity (non-tumor content) of normal sample [1.00]
##                                                        tumor-purity          # (VarScan somatic) Estimated purity (tumor content) of tumor sample [1.00]
##                                                        min-coverage-normal   # (VarScan somatic) Minimum coverage in normal to call somatic [8]
##                                                        min-coverage-tumor    # (VarScan somatic) Minimum coverage in tumor to call somatic [6]
##                                                        min-var-freq          # (VarScan somatic) Minimum variant frequency to call a heterozygote [0.10]
##                                                        min-freq-for-hom      # (VarScan somatic) Minimum frequency to call homozygote [0.75]
##                                                        chrom_type            # (bam-readcount) chr | num ('chr' if chr1, chr2,..,chrY; 'num' if 1,2,..,Y)
##                                                        min_base_quality      # (bam-readcount) don't include reads where the base quality is less than INT [0]
##                                                        min_mapping_quality   # (bam-readcount) filtering reads with mapping quality less than INT [0]
##                                                        p-value (somatic)     # (VarScan somaticFilter) Default p-value threshold for calling variants [5e-02]
##                                                        min-coverage          # (VarScan somaticFilter) Minimum read depth at a position to make a call [10]
##                                                        min-var-freq          # (VarScan somaticFilter) Minimum variant allele frequency threshold [0.20]
##                                                        min-reads2            # (VarScan somaticFilter) Minimum supporting reads at a position to call variants [4]
##                                                        min-strands2          # (VarScan somaticFilter) Minimum # of strands on which variant observed (1 or 2) [1]
##                                                        min-tumor-freq        # (VarScan processSomatic) Minimum variant allele frequency in tumor [0.10]
##                                                        max-normal-freq       # (VarScan processSomatic) Maximum variant allele frequency in normal [0.05]
##                                                        p-value (somatic)     # (VarScan processSomatic) P-value for high-confidence calling [0.07]
##
## OUTPUT:        varscan/ directory containing annotated VarScan output and intermediate files
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 22 $# $0

# Process input parameters
BAMLIST=$1; shift

# Make sure the input file exists
assert_file_exists_w_content $BAMLIST

# Run varscan pipeline
for bamfiles in `sed 's/\t/:/g' $BAMLIST`; do
  SAMPL=`echo $bamfiles | cut -f1 -d':'`
  BAM_N=`echo $bamfiles | cut -f2 -d':'`
  BAM_T=`echo $bamfiles | cut -f3 -d':'`
  qsub_wrapper.sh                                             \
    varscan_pipe                                              \
    $Q_LOW                                                    \
    1                                                         \
    none                                                      \
    n                                                         \
    ngs.pipe.somatic.varscan.ge.sh $SAMPL $BAM_N $BAM_T "$@"
done
