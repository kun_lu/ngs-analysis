#!/bin/bash
##
## DESCRIPTION:   Run rare variant filtering on a vcf file
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         ngs.pipe.vcf.filter.rare_variant.sh
##                                                    ref.fa                                      # Reference genome
##                                                    (ALL|AFR|EUR|AMR|ASN)                       # 1000 Genomes population type
##                                                    in.snpeff.dbNSFP.vartype.gwas.opl.vcf       # Output of ngs.pipe.snpeff.snpsift.sh
##                                                    control.vcf                                 # Control set vcf file
##
## OUTPUT:        
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 4 $# $0

# Process input params
REF=$1; shift
TGP=$1; shift
VCF=$1; shift
CTR=$1; shift

# Make sure input file is not empty
assert_file_exists_w_content $VCF
assert_file_exists_w_content $CTR

# Set up env vars
OUT=`filter_ext $VCF 1`
NSAMPLES=`python_ngs.sh vcf_tool.py num_samples $VCF`

# Set 1000 Genomes population to filter by
if [ $TGP = "AFR" ] ; then
  TGP_OPT=dbNSFP_1000Gp1_AFR_AF
elif [ $TGP = "EUR" ] ; then
  TGP_OPT=dbNSFP_1000Gp1_EUR_AF
elif [ $TGP = "AMR" ] ; then
  TGP_OPT=dbNSFP_1000Gp1_AMR_AF
elif [ $TGP = "ASN" ] ; then
  TGP_OPT=dbNSFP_1000Gp1_ASN_AF
else
  TGP_OPT=dbNSFP_1000Gp1_AF
fi

# Counts summary files
TABLE_PREFIX='rare_variants'
numeric_cols=''
for((n=1; n<=$NSAMPLES; n=n+1)); do
  numeric_cols="$numeric_cols\\t$n"
done
echo -e "Step$numeric_cols" > $TABLE_PREFIX.cumul_table.genes                         # Cumulative sample counts for filtered genes
echo -e "Step$numeric_cols" > $TABLE_PREFIX.cumul_table.transcripts                   # Cumulative sample counts for filtered transcripts
echo -e "Step\tN_Variants\tN_Genes\tN_Transcripts" > $TABLE_PREFIX.filter_counts      # Variant and gene, transcript counts

# Function to generate reports for each vcf and output counts
report_counts() {
  V_IN=$1;
  step=$2;
  ngs.pipe.vcf.report.sh $V_IN
  num_vars=`python_ngs.sh vcf_tool.py count $V_IN -u -n`
  num_genes=`sed 1d $V_IN.gene_summary.txt | cut -f1 | sort -u | wc -l`
  num_transcripts=`sed 1d $V_IN.gene_summary.txt | wc -l`
  echo -e "$step\t$num_vars\t$num_genes\t$num_transcripts" >>  $TABLE_PREFIX.filter_counts
  cumul_counts_genes=''
  cumul_counts_transcripts=''
  for((n=1; n<=$NSAMPLES; n=n+1)); do
    n_genes=`sed 1d $V_IN.gene_summary.txt | python_ngs.sh data_numeric_filter.py -k 5 -g $n -e $n | cut -f1 | sort -u | wc -l`
    n_transcripts=`sed 1d $V_IN.gene_summary.txt | python_ngs.sh data_numeric_filter.py -k 5 -g $n -e $n | cut -f1,2 | sort -u | wc -l`
    cumul_counts_genes="$cumul_counts_genes\\t$n_genes"
    cumul_counts_transcripts="$cumul_counts_transcripts\\t$n_transcripts"
  done
  echo -e "$step$cumul_counts_genes"       >> $TABLE_PREFIX.cumul_table.genes
  echo -e "$step$cumul_counts_transcripts" >> $TABLE_PREFIX.cumul_table.transcripts
}


#-----------------------------------------------------------------------------------------
# Num variants before filtering

# Generate report
report_counts $VCF "Called"

#-----------------------------------------------------------------------------------------
# Select snpeff high and moderate impact variants
VIN=$OUT.vcf
VOU=$OUT.impact.vcf
cat $VIN | `javajar 2g` $SNPSIFT filter "( EFF[*].IMPACT = 'HIGH' ) | (EFF[*].IMPACT = 'MODERATE')" > $VOU

# Generate report
report_counts $VOU "NS/SS/I"

#-----------------------------------------------------------------------------------------
# Filter by 1000 Genomes
VIN=$OUT.impact.vcf
VOU=$OUT.impact.1000g.vcf
cat $VIN | `javajar 2g` $SNPSIFT filter "!( exists $TGP_OPT ) | ( $TGP_OPT <= 0.05 )" > $VOU

# Generate report
report_counts $VOU "1000Genomes maf <= 0.05"

#-----------------------------------------------------------------------------------------
# Filter by control

# Remove variants where control set had maf >= 0.05
VIN=$CTR
VOU=`filter_ext $CTR 1`.maf.vcf
`javajar 4g` $GATK2                \
  -T SelectVariants                \
  -R $REF                          \
  --variant $VIN                   \
  -select "AF > 0.05"              \
  --restrictAllelesTo BIALLELIC    \
  -o  $VOU                         \
  1> $VOU.log                      \
  2> $VOU.err

# Filter by control group's high maf variants
VIN=$OUT.impact.vcf
VMI=`filter_ext $CTR 1`.maf.vcf
VOU=$OUT.impact.ctrl.vcf
python_ngs.sh vcf_tool.py subtract $VIN $VMI -o $VOU

# VIN=$OUT.impact.vcf
# VMI=`filter_ext $CTR 1`.maf.vcf
# COM=$OUT.impact.ctrl_combined.vcf
# VOU=$OUT.impact.ctrl.vcf
# # Combine
# gatk2.combinevariants.sh $REF $COM "--variant:case $VIN --variant:control $VMI"
# # Extract
# samples=`python_ngs.sh vcf_tool.py sample_names $VIN`
# samples_option=''
# for sample in $samples; do
#   samples_option=$samples_option' -sn '$sample
# done
# gatk2.selectvariants.sh  $REF $COM $VOU -select 'set=="case";' $samples_option --excludeNonVariants
# # set=="Intersection"

# Generate report
report_counts $VOU "Control maf <= 0.05"

#-----------------------------------------------------------------------------------------
# Filter by both 1000 Genomes and control

# Filter by control group's high maf variants
VIN=$OUT.impact.1000g.vcf
VMI=`filter_ext $CTR 1`.maf.vcf
VOU=$OUT.impact.1000g.ctrl.vcf
python_ngs.sh vcf_tool.py subtract $VIN $VMI -o $VOU

# VIN=$OUT.impact.1000g.vcf
# VMI=`filter_ext $CTR 1`.maf.vcf
# COM=$OUT.impact.1000g.ctrl_combined.vcf
# VOU=$OUT.impact.1000g.ctrl.vcf
# # Combine
# gatk2.combinevariants.sh $REF $COM "--variant:case $VIN --variant:control $VMI"
# # Extract
# gatk2.selectvariants.sh  $REF $COM $VOU -select 'set=="case";' $samples_option --excludeNonVariants

# Generate report
report_counts $VOU "1000Genomes and control maf <= 0.05"

#-----------------------------------------------------------------------------------------
# Loss-of-function (LOF) and Nonsense mediated decay (NMD) (START_LOST, STOP_GAINED, FRAME_SHIFT)

THRESH=0.9
VIN=$OUT.impact.1000g.ctrl.vcf
VOU=$OUT.impact.1000g.ctrl.lof.vcf
# cat $VIN | `javajar 2g` $SNPSIFT filter "( (exists LOF[*].PERC) & (LOF[*].PERC \> $THRESH) ) | ( (exists NMD[*].PERC) & (NMD[*].PERC \> $THRESH) )" > $VOU
cat <(grep ^# $VIN) \
    <(grep -v ^# $VIN | $PYTHON -c "import re,sys;
pass_lof = lambda infostr: [True]==[float(m.group(1)) > $THRESH for m in [re.search(r';?LOF=\(.+\|.+\|.+\|([\.0-9]+)\)',infostr)] if m];
pass_nmd = lambda infostr: [True]==[float(m.group(1)) > $THRESH for m in [re.search(r';?NMD=\(.+\|.+\|.+\|([\.0-9]+)\)',infostr)] if m];
[sys.stdout.write(line) for line in sys.stdin if [True] == [pass_lof(infostr) or pass_nmd(infostr) for infostr in [line.split()[7]]]];") > $VOU

# Generate report
report_counts $VOU "Loss of function"

#-----------------------------------------------------------------------------------------
# Generate excel file with the counts summaries and each gene summary

python_ngs.sh excel_tool.py insert                                                                                              \
  $TABLE_PREFIX.filter_counts                                                                                                   \
  $TABLE_PREFIX.cumul_table.genes                                                                                               \
  $TABLE_PREFIX.cumul_table.transcripts                                                                                         \
  $OUT.vcf.gene_summary.txt                                                                                                     \
  $OUT.impact.vcf.gene_summary.txt                                                                                              \
  $OUT.impact.1000g.vcf.gene_summary.txt                                                                                        \
  $OUT.impact.ctrl.vcf.gene_summary.txt                                                                                         \
  $OUT.impact.1000g.ctrl.vcf.gene_summary.txt                                                                                   \
  $OUT.impact.1000g.ctrl.lof.vcf.gene_summary.txt                                                                               \
  $TABLE_PREFIX.report                                                                                                          \
  -n FilterCountsSummary GeneCumulAffected TranscriptCumulAffected Called NS_SS_I 1000Genomes Control 1000Genomes+Control LOF
