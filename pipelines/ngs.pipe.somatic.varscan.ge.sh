#!/bin/bash
## 
## DESCRIPTION:   Pipeline for generating somatic variants, filtering the results, and annotating from tumor/normal bam files
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         ngs.pipe.somatic.varscan.ge.sh
##                                               sample                # Sample name, used as output prefix
##                                               normal.bam
##                                               tumor.bam
##                                               ref.fa                # Reference genome file used in alignment
##                                               "mpileup_options"     # i.e. "-Q 30 -q 23 -L target_region.bed"
##                                               varscan_output_dir    # Name of directory for varscan outputs, i.e. varscan
##                                               somatic-p-value       # (VarScan somatic) P-value threshold to call a somatic site [0.05]
##                                               normal-purity         # (VarScan somatic) Estimated purity (non-tumor content) of normal sample [1.00]
##                                               tumor-purity          # (VarScan somatic) Estimated purity (tumor content) of tumor sample [1.00]
##                                               min-coverage-normal   # (VarScan somatic) Minimum coverage in normal to call somatic [8]
##                                               min-coverage-tumor    # (VarScan somatic) Minimum coverage in tumor to call somatic [6]
##                                               min-var-freq          # (VarScan somatic) Minimum variant frequency to call a heterozygote [0.10]
##                                               min-freq-for-hom      # (VarScan somatic) Minimum frequency to call homozygote [0.75]
##                                               chrom_type            # (bam-readcount) chr | num ('chr' if chr1, chr2,..,chrY; 'num' if 1,2,..,Y)
##                                               min_base_quality      # (bam-readcount) don't include reads where the base quality is less than INT [0]
##                                               min_mapping_quality   # (bam-readcount) filtering reads with mapping quality less than INT [0]
##                                               p-value (somatic)     # (VarScan somaticFilter) Default p-value threshold for calling variants [5e-02]
##                                               min-coverage          # (VarScan somaticFilter) Minimum read depth at a position to make a call [10]
##                                               min-var-freq          # (VarScan somaticFilter) Minimum variant allele frequency threshold [0.20]
##                                               min-reads2            # (VarScan somaticFilter) Minimum supporting reads at a position to call variants [4]
##                                               min-strands2          # (VarScan somaticFilter) Minimum # of strands on which variant observed (1 or 2) [1]
##                                               min-tumor-freq        # (VarScan processSomatic) Minimum variant allele frequency in tumor [0.10]
##                                               max-normal-freq       # (VarScan processSomatic) Maximum variant allele frequency in normal [0.05]
##                                               p-value (somatic)     # (VarScan processSomatic) P-value for high-confidence calling [0.07]
##
## OUTPUT:        varscan_output_dir/ directory containing annotated VarScan output and intermediate files
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 24 $# $0

# Process input parameters
SAMPL=$1;   shift
BAM_N=$1;   shift
BAM_T=$1;   shift
REFEREN=$1; shift
MPILOPT=$1; shift
VARSCND=$1; shift
# VarScan somatic
SOMPVAL=$1; shift
NPURITY=$1; shift
TPURITY=$1; shift
MINCOVN=$1; shift
MINCOVT=$1; shift
MINVARF=$1; shift
MINFHOM=$1; shift
# bam-readcount
RDCNT_CHROMTYPE=$1; shift
RDCNT_MIN_BQUAL=$1; shift
RDCNT_MIN_MQUAL=$1; shift
# VarScan somaticFilter
P_VALUE_FILTER=$1; shift
MINCOVR_FILTER=$1; shift
MINVARF_FILTER=$1; shift
MINRDS2_FILTER=$1; shift
MINSTR2_FILTER=$1; shift
# VarScan processSomatic
PROCSOM_MIN_T_F=$1; shift
PROCSOM_MAX_N_F=$1; shift
PROCSOM_P_VALUE=$1; shift

# Make sure input files exists
assert_file_exists_w_content $BAM_N
assert_file_exists_w_content $BAM_T
assert_file_exists_w_content $REFEREN

#------------------------------------------------------------------------
# samtools mpileup

# Normal
qsub_wrapper.sh                                                           \
  mpileup.$SAMPL                                                          \
  $Q_HIGH                                                                 \
  1                                                                       \
  none                                                                    \
  n                                                                       \
  samtools.mpileup.sh $BAM_N $REFEREN "$MPILOPT"

# Tumor
qsub_wrapper.sh                                                           \
  mpileup.$SAMPL                                                          \
  $Q_HIGH                                                                 \
  1                                                                       \
  none                                                                    \
  n                                                                       \
  samtools.mpileup.sh $BAM_T $REFEREN "$MPILOPT"

#------------------------------------------------------------------------
# VarScan

create_dir $VARSCND

# Txt output
qsub_wrapper.sh                                                           \
  varscan.$SAMPL                                                          \
  $Q_HIGH                                                                 \
  1                                                                       \
  mpileup.$SAMPL                                                          \
  n                                                                       \
  varscan.somatic.sh                                                      \
    $BAM_N.mpileup                                                        \
    $BAM_T.mpileup                                                        \
    $VARSCND/$SAMPL                                                       \
    $SOMPVAL                                                              \
    $NPURITY                                                              \
    $TPURITY                                                              \
    $MINCOVN                                                              \
    $MINCOVT                                                              \
    $MINVARF                                                              \
    $MINFHOM                                                              \
    txt

# Dummy job to wait for varscan to end
qsub_wrapper.sh                                                           \
  varscanwait.$SAMPL                                                      \
  $Q_HIGH                                                                 \
  1                                                                       \
  varscan.$SAMPL                                                          \
  n                                                                       \
  hello_world.sh

#------------------------------------------------------------------------
# fpfilter

# bam-readcounts on tumor bam file
qsub_wrapper.sh                                                           \
  bam_readcnt.$SAMPL                                                      \
  $Q_HIGH                                                                 \
  24                                                                      \
  none                                                                    \
  n                                                                       \
  bam_readcount.sh                                                        \
    $BAM_T                                                                \
    $REFEREN                                                              \
    $RDCNT_CHROMTYPE                                                      \
    $RDCNT_MIN_BQUAL                                                      \
    $RDCNT_MIN_MQUAL

# Dummy job to wait for varscan to end
qsub_wrapper.sh                                                           \
  varscanwait.$SAMPL                                                      \
  $Q_HIGH                                                                 \
  1                                                                       \
  bam_readcnt.$SAMPL                                                      \
  n                                                                       \
  hello_world.sh

# fpfilter on snps
qsub_wrapper.sh                                                           \
  fpfilter.$SAMPL                                                         \
  $Q_HIGH                                                                 \
  24                                                                      \
  varscanwait.$SAMPL                                                      \
  n                                                                       \
  varscan.fpfilter.sh                                                     \
    $VARSCND/$SAMPL.varscan.snp                                           \
    $BAM_T.readcount

# fpfilter on indels
qsub_wrapper.sh                                                           \
  fpfilter.$SAMPL                                                         \
  $Q_HIGH                                                                 \
  24                                                                      \
  varscanwait.$SAMPL                                                      \
  n                                                                       \
  varscan.fpfilter.sh                                                     \
    $VARSCND/$SAMPL.varscan.indel                                         \
    $BAM_T.readcount

#------------------------------------------------------------------------
# somaticFilter

qsub_wrapper.sh                                                           \
  somaticFilter.$SAMPL                                                    \
  $Q_HIGH                                                                 \
  1                                                                       \
  fpfilter.$SAMPL                                                         \
  n                                                                       \
  varscan.somaticfilter.sh                                                \
    $VARSCND/$SAMPL.varscan.snp.fpfilter.pass                             \
    $VARSCND/$SAMPL.varscan.indel.fpfilter.pass                           \
    $P_VALUE_FILTER                                                       \
    $MINCOVR_FILTER                                                       \
    $MINVARF_FILTER                                                       \
    $MINRDS2_FILTER                                                       \
    $MINSTR2_FILTER

#------------------------------------------------------------------------
# processSomatic

# snps
qsub_wrapper.sh                                                           \
  procsomatic.snp.$SAMPL                                                  \
  $Q_HIGH                                                                 \
  1                                                                       \
  somaticFilter.$SAMPL                                                    \
  n                                                                       \
  varscan.procsomatic.sh                                                  \
    $VARSCND/$SAMPL.varscan.snp.fpfilter.pass.somaticfilter               \
    $PROCSOM_MIN_T_F                                                      \
    $PROCSOM_MAX_N_F                                                      \
    $PROCSOM_P_VALUE

# indels
qsub_wrapper.sh                                                           \
  procsomatic.indel.$SAMPL                                                \
  $Q_HIGH                                                                 \
  1                                                                       \
  fpfilter.$SAMPL                                                         \
  n                                                                       \
  varscan.procsomatic.sh                                                  \
    $VARSCND/$SAMPL.varscan.indel.fpfilter.pass                           \
    $PROCSOM_MIN_T_F                                                      \
    $PROCSOM_MAX_N_F                                                      \
    $PROCSOM_P_VALUE


#------------------------------------------------------------------------
# VarScan vcf file

# Run VarScan for vcf output
qsub_wrapper.sh                                                           \
  varscan.vcf.$SAMPL                                                      \
  $Q_HIGH                                                                 \
  1                                                                       \
  mpileup.$SAMPL                                                          \
  n                                                                       \
  varscan.somatic.sh                                                      \
    $BAM_N.mpileup                                                        \
    $BAM_T.mpileup                                                        \
    $VARSCND/$SAMPL                                                       \
    $SOMPVAL                                                              \
    $NPURITY                                                              \
    $TPURITY                                                              \
    $MINCOVN                                                              \
    $MINCOVT                                                              \
    $MINVARF                                                              \
    $MINFHOM                                                              \
    vcf

# Fix format of indel vcf files
TODO



# # Filter somatic
# qsub_wrapper.s
#   varscan.vcf.filtr.somatic.$SAMPL                                        \
#   $Q_HIGH                                                                 \
#   1                                                                       \
#   varscan.vcf.$SAMPL                                                      \
#   n                                                                       \
#   python_ngs.sh vcf_somatic_filter.py                                     \
#     $VARSCND/$SAMPL.varscan.snp.vcf                                       \
#     -t somatic                                                            \
#     -o $VARSCND/$SAMPL.varscan.snp.somatic.vcf

# # Filter somatic
# qsub_wrapper.s
#   varscan.vcf.filtr.somatic.$SAMPL                                        \
#   $Q_HIGH                                                                 \
#   1                                                                       \
#   varscan.vcf.$SAMPL                                                      \
#   n                                                                       \
#   python_ngs.sh vcf_somatic_filter.py                                     \
#     $VARSCND/$SAMPL.varscan.indel.vcf                                     \
#     -t somatic                                                            \
#     -o $VARSCND/$SAMPL.varscan.indel.somatic.vcf
# # Filter VarScan results based on the qc filtering steps

