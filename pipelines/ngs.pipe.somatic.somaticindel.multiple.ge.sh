#!/bin/bash
## 
## DESCRIPTION:   Run GATK SomaticIndelDetector for several normal/tumor pairs using grid engine
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2013.11.08
## LAST MODIFIED: 2014.01.21
## MODIFIED BY:   Jin Kim jjinking(at)gmail(dot)com
##
## USAGE:         ngs.pipe.somatic.somaticindel.multiple.ge.sh
##                                                             bamlist
##                                                             (hg|b3x)
##                                                             output_dir
##                                                             [target_interval]
##
## OUTPUT:        SomaticIndelDetector output files
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 3 $# $0

# Process input parameters
BAMLIST=$1; shift
RESOURC=$1; shift
OUT_DIR=`echo $1 | sed 's/\///'`; shift
TARGETI=$1; shift

# Make sure input files exist
assert_file_exists_w_content $BAMLIST

# Set reference file
if [ $RESOURC = 'hg' ]; then
  REF_FA=$HG_REF_GATK2
else
  REF_FA=$B3x_REF_GATK2
fi

# Run GATK SomaticIndelDetector
QSUB=$NGS_ANALYSIS_DIR/modules/util/qsub_wrapper.sh
for bamfiles in `sed 's/\t/:/g' $BAMLIST`; do
  SAMPL=`echo $bamfiles | cut -f1 -d':'`
  BAM_N=`echo $bamfiles | cut -f2 -d':'`
  BAM_T=`echo $bamfiles | cut -f3 -d':'`
  $QSUB gatksomindel.pipe.$SAMPL                                                        \
        $Q_MID                                                                          \
        1                                                                               \
        none                                                                            \
        n                                                                               \
        ngs.pipe.somatic.somaticindel.ge.sh                                             \
          $SAMPL                                                                        \
          $BAM_N                                                                        \
          $BAM_T                                                                        \
          $RESOURC                                                                      \
          $OUT_DIR                                                                      \
	  $TARGETI
done
