#!/bin/bash
## 
## DESCRIPTION:   Run MuTect for several normal/tumor pairs using grid engine
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2014.01.21
## MODIFIED BY:   Jin Kim jjinking(at)gmail(dot)com
##
## USAGE:         ngs.pipe.somatic.mutect.multiple.ge.sh
##                                                       bamlist           # 3-column tsv file containing sample name, normal bam, and tumor bam file paths
##                                                       (hg|b3x)          # Resource type to use, depends on what was used during alignment and GATK processing of the bam files
##                                                       output_dir        # Output directory, also serves as batch id
##                                                       [target_interval]
##
## OUTPUT:        MuTect output files
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 3 $# $0

# Process input parameters
BAMLIST=$1; shift
RESOURC=$1; shift
OUT_DIR=`echo $1 | sed 's/\///'`; shift
TARGETI=$1; shift

# Make sure input files exist
assert_file_exists_w_content $BAMLIST

# Set reference file
if [ $RESOURC = 'hg' ]; then
  REF_FA=$HG_REF_GATK2
else
  REF_FA=$B3x_REF_GATK2
fi

# Run mutect
QSUB=$NGS_ANALYSIS_DIR/modules/util/qsub_wrapper.sh
for bamfiles in `sed 's/\t/:/g' $BAMLIST`; do
  SAMPL=`echo $bamfiles | cut -f1 -d':'`
  BAM_N=`echo $bamfiles | cut -f2 -d':'`
  BAM_T=`echo $bamfiles | cut -f3 -d':'`
  $QSUB mutect.pipe.$SAMPL                                                              \
        $Q_MID                                                                          \
        1                                                                               \
        none                                                                            \
        n                                                                               \
        ngs.pipe.somatic.mutect.ge.sh                                                   \
          $SAMPL                                                                        \
          $BAM_N                                                                        \
          $BAM_T                                                                        \
          $RESOURC                                                                      \
          $OUT_DIR                                                                      \
          $TARGETI
done
