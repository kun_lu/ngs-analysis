#!/bin/bash
## 
## DESCRIPTION:   Run Hiseq base calling from within BaseCalls directory
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2014.03.27
## LAST MODIFIED: 
##
## USAGE:         ngs.pipe.bcl2fastq.sh
##                                        output_dir
##                                        path/to/SampleSheet.csv
##                                        [num_threads (default:20)]
##
## OUTPUT:        directory containing fastq files for each project/sample
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 2 $# $0

# Process input params
OUTPUT_DIR=$1
SAMPLESHEET=$2
NUM_THREADS=$3
NUM_THREADS=${NUM_THREADS:=20}

# Samplesheet sanity check
$PYTHON $NGS_ANALYSIS_DIR/modules/seq/illumina_samplesheet.py sanitycheck $SAMPLESHEET

# Check if tool ran successfully
assert_normal_exit_status $? "Invalid samplesheet"

# Run basecall
  casava.bcl2fastq.hiseq.sh                        \
    $OUTPUT_DIR                                    \
    $SAMPLESHEET                                   \
    $NUM_THREADS

# Run md5sum
  md5sum.sh                            \
    $OUTPUT_DIR/Project_*/Sample_*/*fastq.gz

# Run fastqc
  ngs.pipe.fastqc.sh                            \
    2                                              \
    $OUTPUT_DIR/Project_*/Sample_*/*fastq.gz

# Generate sequence summary table
  python_ngs.sh illumina_demultstat_parser.py      \
    $OUTPUT_DIR                                    \
    -t all                                         \
    -o $OUTPUT_DIR.sequence.summary.csv