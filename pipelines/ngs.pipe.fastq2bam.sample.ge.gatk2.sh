#!/bin/bash
## 
## DESCRIPTION:   Given a sample directory in a project directory of an illumina basecall output,
##                generate bam files from all the paired end reads within the sample directory
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2014.01.21
## MODIFIED BY:   Jin Kim jjinking(at)gmail(dot)com
##
## USAGE:         ngs.pipe.fastq2bam.sample.ge.gatk2.sh
##                                                      Sample_X(Sample directory)
##                                                      ref.fa
##                                                      dbsnp.vcf
##                                                      mills.indel.vcf
##                                                      1000G.indel.vcf
##                                                      (WGS|target_region.bed)       # If not WGS, then target region bed or intervals file
##
## OUTPUT:        sample.mergelanes.dedup.realign.recal.reduce.bam
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 6 $# $0

# Process input params
SAMPLEDIR=$1; shift
REFERENCE=$1; shift
DBSNP_VCF=$1; shift
MILLS_INDEL_VCF=$1; shift
INDEL_1000G_VCF=$1; shift
TGT=$1

# Set up pipeline variables
SAMPLE=`echo $SAMPLEDIR | cut -f2- -d'_'`
FASTQ_PE_LIST_FILE=$SAMPLEDIR/list.fastq.pe

# Get list of all paired-end fastq files in the sample directory
$PYTHON $NGS_ANALYSIS_DIR/modules/seq/detect_fastq_pe_file_pairs.py $SAMPLEDIR > $FASTQ_PE_LIST_FILE

# Fastq files QC and statistics
ngs.pipe.qc.fastq.ge.sh $SAMPLEDIR/*fastq.gz

# Generate raw bam files for each pair of PE reads
for pe in `sed 's/\t/:/' $FASTQ_PE_LIST_FILE`; do
  FASTQ_R1=`echo $pe | cut -f1 -d :`
  FASTQ_R2=`echo $pe | cut -f2 -d :`
  qsub_wrapper.sh                                                            \
    $SAMPLE.fastq2rawbam.pe                                                  \
    $Q_HIGH                                                                  \
    4                                                                        \
    none                                                                     \
    n                                                                        \
    $NGS_ANALYSIS_DIR/pipelines/ngs.pipe.fastq2rawbam.pe.sh                  \
      $FASTQ_R1                                                              \
      $FASTQ_R2                                                              \
      $REFERENCE
done

# Merge all bam files within the sample directory into a single sample bam file
qsub_wrapper.sh                                                              \
  $SAMPLE.mergelanes                                                         \
  $Q_HIGH                                                                    \
  1                                                                          \
  $SAMPLE.fastq2rawbam.pe                                                    \
  n                                                                          \
  $NGS_ANALYSIS_DIR/modules/align/picard.mergesamfiles.sh                    \
    $SAMPLEDIR/$SAMPLE.mergelanes.bam                                        \
    $SAMPLEDIR/*rg.bam

# Dedup, realign, recalibrate, reduce
qsub_wrapper.sh                                                              \
  $SAMPLE.processbam                                                         \
  $Q_MID                                                                     \
  1                                                                          \
  $SAMPLE.mergelanes                                                         \
  y                                                                          \
  $NGS_ANALYSIS_DIR/pipelines/ngs.pipe.dedup.realign.recal.ge.gatk2.sh       \
    $SAMPLE                                                                  \
    $SAMPLEDIR/$SAMPLE.mergelanes.bam                                        \
    $REFERENCE                                                               \
    $DBSNP_VCF                                                               \
    $MILLS_INDEL_VCF                                                         \
    $INDEL_1000G_VCF                                                         \
    4

# Bam QC
qsub_wrapper.sh                                                              \
  $SAMPLE.bam.QC                                                             \
  $Q_MID                                                                     \
  1                                                                          \
  $SAMPLE.BQSR.apply                                                         \
  n                                                                          \
  ngs.pipe.qc.bam.ge.gatk2.sh                                                \
    SINGLE                                                                   \
    $SAMPLE                                                                  \
    $REFERENCE                                                               \
    $TGT                                                                     \
    $SAMPLEDIR/$SAMPLE.mergelanes.dedup.realign.recal.bam
