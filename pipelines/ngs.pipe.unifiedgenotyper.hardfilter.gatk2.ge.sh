#!/bin/bash
##
## DESCRIPTION:   Variant filtration pipeline, based on best practices for applying hard filters to a call set
##                http://www.broadinstitute.org/gatk/guide/best-practices#variant-discovery
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2013.11.12
## LAST MODIFIED: 2014.01.21
## MODIFIED BY:   Jin Kim jjinking(at)gmail(dot)com
##
## USAGE:         ngs.pipe.unifiedgenotyper.hardfilter.gatk2.ge.sh
##                                                                 outprefix                   # Output prefix, i.e. samples.haplocall
##                                                                 ref.fa
##                                                                 in1.bam                     # Input bam file
##                                                                 [in2.bam [...]]             # Additional bam files
##
## OUTPUT:        
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 3 $# $0

# Process input params
OUT=$1; shift
REF=$1; shift
BAM=$@

QSUB=qsub_wrapper.sh

# Variant Call
$QSUB $OUT.UnifiedGT                                                   \
      $Q_HIGH                                                          \
      6                                                                \
      none                                                             \
      n                                                                \
      $NGS_ANALYSIS_DIR/modules/variant/gatk2.unifiedgenotyper.sh      \
        $REF                                                           \
        $OUT                                                           \
        "-U ALLOW_N_CIGAR_READS"                                       \
        $BAM

# Extract snps
$QSUB $OUT.extract.SNP                                                 \
      $Q_HIGH                                                          \
      1                                                                \
      $OUT.UnifiedGT                                                   \
      n                                                                \
      gatk2.selectvariants.sh                                          \
        $REF                                                           \
        $OUT.vcf                                                       \
        $OUT.snp.vcf                                                   \
        "-selectType SNP"

# Extract indels
$QSUB $OUT.extract.INDEL                                               \
      $Q_HIGH                                                          \
      1                                                                \
      $OUT.UnifiedGT                                                   \
      n                                                                \
      gatk2.selectvariants.sh                                          \
        $REF                                                           \
        $OUT.vcf                                                       \
        $OUT.indel.vcf                                                 \
        "-selectType INDEL"

# Filter snps
$QSUB $OUT.filter.SNP                                                  \
      $Q_HIGH                                                          \
      1                                                                \
      $OUT.extract.SNP                                                 \
      n                                                                \
      gatk2.variantfiltration.snp.sh                                   \
        $REF                                                           \
        $OUT.snp.vcf                                                   \
        $OUT.snp.filtr.vcf

# Filter indel
$QSUB $OUT.filter.INDEL                                                \
      $Q_HIGH                                                          \
      1                                                                \
      $OUT.extract.INDEL                                               \
      n                                                                \
      gatk2.variantfiltration.indel.sh                                 \
        $REF                                                           \
        $OUT.indel.vcf                                                 \
        $OUT.indel.filtr.vcf

# Dummy processes to wait for both snp and indel filter results
$QSUB $OUT.wait2merge                                                  \
      $Q_LOW                                                           \
      1                                                                \
      $OUT.filter.SNP                                                  \
      n                                                                \
      hello_world.sh

$QSUB $OUT.wait2merge                                                  \
      $Q_LOW                                                           \
      1                                                                \
      $OUT.filter.INDEL                                                \
      n                                                                \
      hello_world.sh

# Merge filtered vcf files into a single vcf file
$QSUB $OUT.merge                                                       \
      $Q_MID                                                           \
      1                                                                \
      $OUT.wait2merge                                                  \
      n                                                                \
      vcf_merge.sh                                                     \
        $REF                                                           \
        $OUT.analysisready.vcf                                         \
        $OUT.snp.filtr.vcf                                             \
        $OUT.indel.filtr.vcf
