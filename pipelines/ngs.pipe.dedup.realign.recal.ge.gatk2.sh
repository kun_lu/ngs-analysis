#!/bin/bash
## 
## DESCRIPTION:   Dedup, realign, and recalibrate a raw bam file, and also create reduced bam file
##                Parallelism primer:    http://www.broadinstitute.org/gatk/guide/article?id=1988
##                Parallelism reference: http://gatkforums.broadinstitute.org/discussion/1975/how-can-i-use-parallelism-to-make-gatk-tools-run-faster
##                Known variant sites:   http://gatkforums.broadinstitute.org/discussion/1247/what-should-i-use-as-known-variantssites-for-running-tool-x
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2014.01.20
## MODIFIED BY:   Jin Kim jjinking(at)gmail(dot)com
##
## USAGE:         ngs.pipe.dedup.realign.recal.ge.gatk2.sh
##                                                         sample             # Name of sample
##                                                         sample.bam
##                                                         ref.fa
##                                                         dbsnp.vcf
##                                                         mills.indels.vcf
##                                                         1000G.indels.vcf
##                                                         max_threads
##                                                         [target_interval]
##
## OUTPUT:        sample.dedup.realign.recal.reduce.bam
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 7 $# $0

# Process input params
SAMPLE=$1;          shift
SAMPLEBAM=$1;       shift
REFERENCE=$1;       shift
DBSNP_VCF=$1;       shift
MILLS_INDEL_VCF=$1; shift
INDEL_1000G_VCF=$1; shift
MAX_NUM_THREADS=$1; shift
TARGET_INTERVAL=$1; shift

# Make sure input files exist and are not empty
assert_file_exists_w_content $SAMPLEBAM
assert_file_exists_w_content $REFERENCE
assert_file_exists_w_content $DBSNP_VCF
assert_file_exists_w_content $MILLS_INDEL_VCF
assert_file_exists_w_content $INDEL_1000G_VCF

# Set up pipeline variables
SAMPLE_PREFIX=`filter_ext $SAMPLEBAM 1`
TARGET_OPTION=''
if [ ! -z $TARGET_INTERVAL ]; then
  assert_file_exists_w_content $TARGET_INTERVAL
  TARGET_OPTION="-L $TARGET_INTERVAL"
fi

QSUB=qsub_wrapper.sh

# Mark duplicates ----------------------------------------------#
$QSUB $SAMPLE.dedup                                                                                                      \
      $Q_HIGH                                                                                                            \
      6                                                                                                                  \
      $SAMPLE.mergelanes                                                                                                 \
      n                                                                                                                  \
      picard.markduplicates.sh                                                                                           \
        $SAMPLE_PREFIX.bam


# Indel realignment --------------------------------------------#

KNOWN_OPTION="-known $MILLS_INDEL_VCF -known $INDEL_1000G_VCF"

$QSUB $SAMPLE.Realign.TC                                                                                                 \
      $Q_HIGH                                                                                                            \
      $MAX_NUM_THREADS                                                                                                   \
      $SAMPLE.dedup                                                                                                      \
      n                                                                                                                  \
      gatk2.realignertargetcreator.sh                                                                                    \
        $REFERENCE                                                                                                       \
        $SAMPLE_PREFIX.dedup                                                                                             \
        "-nt $MAX_NUM_THREADS $KNOWN_OPTION"                                                                             \
        $SAMPLE_PREFIX.dedup.bam

$QSUB $SAMPLE.Realign                                                                                                    \
      $Q_HIGH                                                                                                            \
      1                                                                                                                  \
      $SAMPLE.Realign.TC                                                                                                 \
      n                                                                                                                  \
      gatk2.indelrealigner.sh                                                                                            \
        $REFERENCE                                                                                                       \
        $SAMPLE_PREFIX.dedup.bam                                                                                         \
        $SAMPLE_PREFIX.dedup.realign.intervals                                                                           \
        "$KNOWN_OPTION"
      
# BQSR ---------------------------------------------------------#

KNOWN_OPTION="-knownSites $DBSNP_VCF -knownSites $MILLS_INDEL_VCF -knownSites $INDEL_1000G_VCF"

# Before
$QSUB $SAMPLE.BQSR.pre                                                                                                   \
      $Q_HIGH                                                                                                            \
      $MAX_NUM_THREADS                                                                                                   \
      $SAMPLE.Realign                                                                                                    \
      n                                                                                                                  \
      gatk2.baserecalibrator.sh                                                                                          \
        $REFERENCE                                                                                                       \
        $SAMPLE_PREFIX.dedup.realign.bam                                                                                 \
        $SAMPLE_PREFIX.dedup.realign                                                                                     \
        "-nct $MAX_NUM_THREADS $KNOWN_OPTION $TARGET_OPTION"

# After
$QSUB $SAMPLE.BQSR.post                                                                                                  \
      $Q_HIGH                                                                                                            \
      $MAX_NUM_THREADS                                                                                                   \
      $SAMPLE.BQSR.pre                                                                                                   \
      n                                                                                                                  \
      gatk2.baserecalibrator.sh                                                                                          \
        $REFERENCE                                                                                                       \
        $SAMPLE_PREFIX.dedup.realign.bam                                                                                 \
        $SAMPLE_PREFIX.dedup.realign.recal.post                                                                          \
        "-nct $MAX_NUM_THREADS -BQSR $SAMPLE_PREFIX.dedup.realign.recal.grp $KNOWN_OPTION $TARGET_OPTION"

# Apply
$QSUB $SAMPLE.BQSR.apply                                                                                                 \
      $Q_HIGH                                                                                                            \
      $MAX_NUM_THREADS                                                                                                   \
      $SAMPLE.BQSR.pre                                                                                                   \
      y                                                                                                                  \
      gatk2.printreads.sh                                                                                                \
        $REFERENCE                                                                                                       \
        $SAMPLE_PREFIX.dedup.realign.recal                                                                               \
        "-nct $MAX_NUM_THREADS -baq RECALCULATE -BQSR $SAMPLE_PREFIX.dedup.realign.recal.grp"                            \
        $SAMPLE_PREFIX.dedup.realign.bam
      

# Reduce -------------------------------------------------------#

# $QSUB $SAMPLE.reduce                                           \
#       $Q_HIGH                                                  \
#       1                                                        \
#       $SAMPLE.BQSR.apply                                       \
#       y                                                        \
#       gatk2.reducereads.sh                                     \
#         $REFERENCE                                             \
#         $SAMPLE_PREFIX.dedup.realign.recal.bam
