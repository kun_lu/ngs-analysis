#!/bin/bash
## 
## DESCRIPTION:   Run bam-readcount on a list of bamfiles
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         ngs.pipe.bam_readcount.sh
##                                             num_parallel          # Number of processes to run in parallel
##                                             bamfileslist          # Single-column list of paths to bamfiles
##                                             ref.fa
##                                             chrom_type            # chr | num ('chr' if chr1, chr2,..,chrY; 'num' if 1,2,..,chrY)
##                                             min_base_quality
##                                             min_mapping_quality
##                                             [target_region]
##
## OUTPUT:        bam.readcount files for each of the bam files in bamfileslist
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 6 $# $0

# Process input parameters
NUM_PAR=$1; shift
BAMLIST=$1; shift
REFEREN=$1; shift
CHROM_T=$1; shift
MIN_B_Q=$1; shift
MIN_M_Q=$1; shift
TARGETI=$1; shift

# Run tool
P=0
for bamfile in `cat $BAMLIST`; do
  $NGS_ANALYSIS_DIR/modules/align/bam_readcount.sh        \
    $bamfile                                              \
    $REFEREN                                              \
    $CHROM_T                                              \
    $MIN_B_Q                                              \
    $MIN_M_Q                                              \
    $TARGETI &
  P=$((P + 1))
  if [ $P -ge $NUM_PAR ]; then
    wait
    P=0
  fi  
done
wait
