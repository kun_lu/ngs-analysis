#!/bin/bash
## 
## DESCRIPTION:   Run Hiseq base calling from within BaseCalls directory
##                Call only read 1, to be used mainly for troubleshooting
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         ngs.pipe.bcl2fastq.R1Only.ge.sh
##                                                output_dir
##                                                path/to/SampleSheet.csv
##                                                [num_threads (default:20)]
##
## OUTPUT:        directory containing fastq files for each project/sample
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 2 $# $0

# Process input params
OUTPUT_DIR=$1
SAMPLESHEET=$2
NUM_THREADS=$3
NUM_THREADS=${NUM_THREADS:=20}

# Samplesheet sanity check
$PYTHON $NGS_ANALYSIS_DIR/modules/seq/illumina_samplesheet.py sanitycheck $SAMPLESHEET

# Check if tool ran successfully
assert_normal_exit_status $? "Invalid samplesheet"

# Run basecall
qsub_wrapper.sh                                    \
  casava.basecall.$$                               \
  $Q_LOW                                           \
  $NUM_THREADS                                     \
  none                                             \
  y                                                \
  casava.bcl2fastq.hiseq.R1Only.sh                 \
    $OUTPUT_DIR                                    \
    $SAMPLESHEET                                   \
    $NUM_THREADS

# Run fastqc
qsub_wrapper.sh                                    \
  fastqc.$$                                        \
  $Q_LOW                                           \
  2                                                \
  casava.basecall.$$                               \
  y                                                \
  ngs.pipe.fastqc.ge.sh                            \
    2                                              \
    $OUTPUT_DIR/Project_*/Sample_*/*fastq.gz

# Generate sequence summary table
qsub_wrapper.sh                                    \
  summarytable.$$                                  \
  $Q_LOW                                           \
  2                                                \
  fastqc                                           \
  y                                                \
  python_ngs.sh illumina_demultstat_parser.py      \
    $OUTPUT_DIR                                    \
    -t all                                         \
    -r                                             \
    -o $OUTPUT_DIR.sequence.summary.R1.csv
