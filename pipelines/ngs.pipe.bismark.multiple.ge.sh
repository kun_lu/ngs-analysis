#!/bin/bash
##
## DESCRIPTION:   DNA Methylation analysis on multiple sample directories
## AUTHOR:        Jihye Kim kjhye1211(at)gmail(dot)com
## CREATED:       2013.11.26
## LAST MODIFIED: 2014.01.21
## MODIFIED BY:   Jin Kim jjinking(at)gmail(dot)com
##
## USAGE:         ngs.pipe.bismark.multiple.sh Sample_X [Sample_Y, ...]
##
## OUTPUT:        bismark result files
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 1 $# $0

# Submit each sample directory to the grid engine
SAMPLEDIRS=$@
for sampledir in $SAMPLEDIRS; do
    sampledir=`echo $sampledir | sed 's/\/$//'`
    SAMPLENAME=`echo $sampledir | cut -f2- -d'_'`
    qsub_wrapper.sh                                                \
         $SAMPLENAME.bismark                                       \
         $Q_LOW                                                    \
         1                                                         \
         none                                                      \
         n                                                         \
	 ngs.pipe.bismark.sample.ge.sh                             \
           $sampledir
done

