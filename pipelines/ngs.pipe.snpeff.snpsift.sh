#!/bin/bash
##
## DESCRIPTION:   Run SNPEff and SNPSift tools on a vcf file
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.12.16
## MODIFIED BY:   Jin Kim jjinking(at)gmail(dot)com
##
## USAGE:         ngs.pipe.snpeff.snpsift.sh
##                                           in.vcf
##                                           ["snpeff options"]     # SNPEff options, i.e. "-cancer -canceSample samples_cancer.txt" in quotes
##
## OUTPUT:        Various annotated vcf and report files
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 1 $# $0

# Process input params
VCF=$1; shift
OPT=$1; shift

# Make sure input file is not empty
assert_file_exists_w_content $VCF

# Set up env vars
NSAMPLES=`python_ngs.sh vcf_tool.py num_samples $VCF`
OUT=`filter_ext $VCF 1`

# Annotate variants with snpEff
snpeff.eff.sh $VCF "$OPT"

# Generate a counts pivot table for variant effects vs novel variants
FIN=$OUT.snpeff.vcf
FOU=$OUT.snpeff.vcf.effect_counts.txt
$PYTHON -c "from ngs import vcfframe;
infile  = '$FIN'
outfile = '$FOU'
vcfframe.SNPEffVcfFrame.load_vcf(infile).effect_pivot().to_csv(outfile, sep='\t', float_format='%.4f');"

# Annotate with snpsift dbNSFP
VIN=$OUT.snpeff.vcf
VOU=$OUT.snpeff.dbNSFP.vcf
`javajar 2g` $SNPSIFT dbnsfp -v $SNPSIFT_DBNSFP $VIN > $VOU

# Add variant type
VIN=$OUT.snpeff.dbNSFP.vcf
VOU=$OUT.snpeff.dbNSFP.vartype.vcf
`javajar 2g` $SNPSIFT varType $VIN > $VOU

# Add GWAS Catalog
VIN=$OUT.snpeff.dbNSFP.vartype.vcf
VOU=$OUT.snpeff.dbNSFP.vartype.gwas.vcf
GWASCAT=$SNPSIFT_DATA/gwascatalog.txt
`javajar 2g` $SNPSIFT gwasCat $GWASCAT $VIN > $VOU

# Generate one effect per line
VIN=$OUT.snpeff.dbNSFP.vartype.gwas.vcf
VOU=$OUT.snpeff.dbNSFP.vartype.gwas.opl.vcf
cat $VIN | $ONEPERLINE > $VOU

# Generate report files
ngs.pipe.vcf.report.sh $OUT.snpeff.dbNSFP.vartype.gwas.opl.vcf