#!/bin/bash
## 
## DESCRIPTION:   Given a pair of PE fastq files, generate raw (unprocessed by GATK) bam
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         ngs.pipe.fastq2rawbam.trimming.pe.sh
##                                                     Sample_AAAAAA_L00N_R1_NNN.fastq.gz
##                                                     Sample_AAAAAA_L00N_R2_NNN.fastq.gz
##                                                     ref.fa
##
## OUTPUT:        Sample_AAAAAA_L00N_R1_NNN.fastq.gz.trim.fastq.sai.sort.mergepe.sort.rg.bam
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 3 $# $0

# Process input params
FASTQ_R1=$1
FASTQ_R2=$2
REFERENCE=$3

# Set up pipeline variables
SAMPLE=`$PYTHON $NGS_ANALYSIS_DIR/modules/util/illumina_fastq_extract_samplename.py $FASTQ_R1`
FASTQ_PE=`echo $FASTQ_R1 | sed 's/R1/PE/'`
FASTQ_SE=`echo $FASTQ_R1 | sed 's/R1/SE/'`
FASTQ_ME=`echo $FASTQ_R1 | sed 's/R1/ME/'`

#==[ Trim ]=========================================================================#

$NGS_ANALYSIS_DIR/modules/seq/sickle.pe.sh                   \
  $FASTQ_R1                                                  \
  $FASTQ_R2

#==[ Align ]========================================================================#

# Create sam
bwa.mem.sh $REFERENCE                                        \
           "-t 4 -M"                                         \
           $FASTQ_PE.trim.fastq.gz                           \
           $FASTQ_R1.trim.fastq.gz                           \
           $FASTQ_R2.trim.fastq.gz &
bwa.mem.sh $REFERENCE                                        \
           "-t 2 -M"                                         \
           $FASTQ_SE.trim.fastq.gz                           \
           $FASTQ_SE.trim.fastq.gz &
wait

# Sort
picard.sortsam.sh $FASTQ_PE.trim.fastq.gz.sam &
picard.sortsam.sh $FASTQ_SE.trim.fastq.gz.sam &
wait

#==[ Merge, sort, and add read group ]==============================================#

# Merge paired and single end bam files
picard.mergesamfiles.sh                                      \
  $FASTQ_ME.trim.fastq.gz.sort.mergepe.bam                   \
  $FASTQ_PE.trim.fastq.gz.sort.bam                           \
  $FASTQ_SE.trim.fastq.gz.sort.bam

# Add read group to bam file
picard.addreadgroup.sh                                       \
  $FASTQ_ME.trim.fastq.gz.sort.mergepe.bam                   \
  $SAMPLE
