#!/bin/bash
##
## DESCRIPTION:   DNA Methylation analysis on each Sample directory
## AUTHOR:        Jihye Kim kjhye1211(at)gmail(dot)com
## CREATED:       2013.11.28
## LAST MODIFIED: 2014.01.24
## MODIFIED BY:   Jin Kim jjinking(at)gmail(dot)com
##
## USAGE:	  ngs.pipe.bismark.sample.sh Sample_X
##
## OUTPUT:  	  bismark result files
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 1 $# $0

# Process input args
SAMPLEDIR=$1

# Get sample name
SAMPLE=`echo $SAMPLEDIR | sed 's/Sample_//'`

# Set up
cd $SAMPLEDIR
FASTQ_PE_LIST_FILE=list.fastq.pe
python_ngs.sh detect_fastq_pe_file_pairs.py . > $FASTQ_PE_LIST_FILE
R1=`awk '{print $1}' $FASTQ_PE_LIST_FILE`
R2=`awk '{print $2}' $FASTQ_PE_LIST_FILE`

# Fastq files QC and statistics --------------------------------------------------------
ngs.pipe.qc.fastq.ge.sh $R1 $R2

# Run alignments -----------------------------------------------------------------------
qsub_wrapper.sh                                                   \
    bismark.alignment.$SAMPLE                                     \
    $Q_HIGH                                                       \
    15                                                            \
    none                                                          \
    n                                                             \
    bismark.pe.sh                                                 \
        $HG_BISMARK_REF_DIR                                       \
        8                                                         \
        $R1                                                       \
        $R2

# Run dedup ----------------------------------------------------------------------------
samfile=`basename $R1`'_bismark_bt2_pe.sam'
qsub_wrapper.sh                                                   \
    bismark.dedup.$SAMPLE                                         \
    $Q_HIGH                                                       \
    2                                                             \
    bismark.alignment.$SAMPLE                                     \
    n                                                             \
    bismark.dedup.pe.sh                                           \
        $samfile


# Extract methylations -----------------------------------------------------------------
bamfile=`basename $R1`'_bismark_bt2_pe.deduplicated.bam'
qsub_wrapper.sh                                                   \
    bismark.extractmethyl.$SAMPLE                                 \
    $Q_HIGH                                                       \
    1                                                             \
    bismark.dedup.$SAMPLE                                         \
    n                                                             \
    bismark.methyl_extract.pe.sh                                  \
        $bamfile

# Run bismark2bedgraph -----------------------------------------------------------------
OUT=`basename $R1`'.bedgraph'
qsub_wrapper.sh                                                   \
    bismark.bedgraph.$SAMPLE                                      \
    $Q_HIGH                                                       \
    1                                                             \
    bismark.extractmethyl.$SAMPLE                                 \
    n                                                             \
    bismark.bedgraph.sh                                           \
        $OUT                                                      \
        C??_context*deduplicated.txt


# Generate report-----------------------------------------------------------------------
OUD=`basename $R1`'.bismark.report'
qsub_wrapper.sh                                                   \
    bismark.report.$SAMPLE                                        \
    $Q_HIGH                                                       \
    1                                                             \
    bismark.bedgraph.$SAMPLE                                      \
    n                                                             \
    bismark.report.sh                                             \
        $OUD

#---------------------------------------------------------------------------------------
# Process bam for methylkit

# Sort deduplicated bam file
bamfile=`basename $R1`'_bismark_bt2_pe.deduplicated.bam'
qsub_wrapper.sh                                                   \
    bismark.sort.$SAMPLE                                          \
    $Q_HIGH                                                       \
    1                                                             \
    bismark.dedup.$SAMPLE                                         \
    n                                                             \
    picard.sortsam.sh $bamfile

# Convert bam to sam
bamfile=`basename $R1`'_bismark_bt2_pe.deduplicated.sort.bam'
samfile=`basename $R1`'_bismark_bt2_pe.deduplicated.sort.sam'
qsub_wrapper.sh                                                   \
    bismark.bam2sam.$SAMPLE                                       \
    $Q_HIGH                                                       \
    1                                                             \
    bismark.sort.$SAMPLE                                          \
    n                                                             \
    picard.samformatconvert.sh $bamfile $samfile

#---------------------------------------------------------------------------------------
# QC bam file

# Add read group to bam
bamfile=`basename $R1`'_bismark_bt2_pe.deduplicated.sort.bam'
qsub_wrapper.sh                                                   \
    bismark.rg.$SAMPLE                                            \
    $Q_HIGH                                                       \
    1                                                             \
    bismark.sort.$SAMPLE                                          \
    n                                                             \
    picard.addreadgroup.sh                                        \
      $bamfile                                                    \
      $SAMPLE

# Reorder bam file according to reference file using during QC
bamfile=`basename $R1`'_bismark_bt2_pe.deduplicated.sort.rg.bam'
qsub_wrapper.sh                                                   \
    bismark.reorder.$SAMPLE                                       \
    $Q_HIGH                                                       \
    1                                                             \
    bismark.rg.$SAMPLE                                            \
    n                                                             \
    picard.reordersam.sh                                          \
      $HG_REF_GATK2                                               \
      $bamfile

# Run QC pipe
bamfile=`basename $R1`'_bismark_bt2_pe.deduplicated.sort.rg.reorder.bam'
qsub_wrapper.sh                                                   \
    bismark.bamQC.$SAMPLE                                         \
    $Q_MID                                                        \
    1                                                             \
    bismark.reorder.$SAMPLE                                       \
    n                                                             \
    ngs.pipe.qc.bam.ge.gatk2.sh                                   \
      SINGLE                                                      \
      bamQC.$SAMPLE                                               \
      $HG_REF_GATK2                                               \
      $HG_SURESELECT_METHYLKIT_BED                                \
      $bamfile

cd ..
