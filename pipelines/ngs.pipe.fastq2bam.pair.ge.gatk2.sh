#!/bin/bash
## 
## DESCRIPTION:   Given two sample directories in a project directory structure of an Illumina basecall output (CASAVA),
##                generate bam files from all the paired end fastq reads within the sample directories
##                Go through the GATK recommended pipeline by merging the bamfiles for the two samples
##                Recommended pipeline for somatic mutation detection between different tissue from the same person, i.e.
##                Normal/Tumor samples
##
##                http://gatkforums.broadinstitute.org/discussion/2268/tumor-and-normal-in-a-merged-bam-file
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2014.01.21
## MODIFIED BY:   Jin Kim jjinking(at)gmail(dot)com
##
## USAGE:         ngs.pipe.fastq2bam.pair.ge.gatk2.sh
##                                                    Sample_X_N                   # normal sample directory
##                                                    Sample_X_T                   # tumor  sample directory
##                                                    ref.fa
##                                                    dbsnp.vcf
##                                                    mills.indel.vcf
##                                                    1000G.indel.vcf
##                                                    (WGS|target_region.bed)      # If not WGS, then target region bed or intervals file
##
## OUTPUT:        sample.mergelanes.dedup.realign.recal.reduce.bam
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 7 $# $0

# Process input params
SAMPLEDIR_N=$1;     shift
SAMPLEDIR_T=$1;     shift
REFERENCE=$1;       shift
DBSNP_VCF=$1;       shift
MILLS_INDEL_VCF=$1; shift
INDEL_1000G_VCF=$1; shift
TGT=$1

# Set up pipeline variables
SAMPLE_N=`echo $SAMPLEDIR_N | cut -f2- -d'_'`
SAMPLE_T=`echo $SAMPLEDIR_T | cut -f2- -d'_'`
FASTQ_PE_LIST_FILE_N=$SAMPLEDIR_N/list.fastq.pe
FASTQ_PE_LIST_FILE_T=$SAMPLEDIR_T/list.fastq.pe

# Directory for outputs of sample pairs
SAMPLE_PAIR=$SAMPLE_N'_'$SAMPLE_T
PAIRDIR=Pair_$SAMPLE_PAIR
mkdir $PAIRDIR

# Get list of all paired-end fastq files in the sample directory
python_ngs.sh detect_fastq_pe_file_pairs.py $SAMPLEDIR_N > $FASTQ_PE_LIST_FILE_N
python_ngs.sh detect_fastq_pe_file_pairs.py $SAMPLEDIR_T > $FASTQ_PE_LIST_FILE_T

# Fastq files QC and statistics
ngs.pipe.qc.fastq.ge.sh $SAMPLEDIR_N/*fastq.gz $SAMPLEDIR_T/*fastq.gz

# Generate raw bam files for each pair of PE reads
for pe in `sed 's/\t/:/' $FASTQ_PE_LIST_FILE_N`; do
  FASTQ_R1=`echo $pe | cut -f1 -d :`
  FASTQ_R2=`echo $pe | cut -f2 -d :`
  qsub_wrapper.sh                                                   \
    $SAMPLE_PAIR.fastq2rawbam.pe                                    \
    $Q_HIGH                                                         \
    6                                                               \
    none                                                            \
    n                                                               \
    $NGS_ANALYSIS_DIR/pipelines/ngs.pipe.fastq2rawbam.pe.sh         \
      $FASTQ_R1                                                     \
      $FASTQ_R2                                                     \
      $REFERENCE
done
for pe in `sed 's/\t/:/' $FASTQ_PE_LIST_FILE_T`; do
  FASTQ_R1=`echo $pe | cut -f1 -d :`
  FASTQ_R2=`echo $pe | cut -f2 -d :`
  qsub_wrapper.sh                                                   \
    $SAMPLE_PAIR.fastq2rawbam.pe                                    \
    $Q_HIGH                                                         \
    6                                                               \
    none                                                            \
    n                                                               \
    $NGS_ANALYSIS_DIR/pipelines/ngs.pipe.fastq2rawbam.pe.sh         \
      $FASTQ_R1                                                     \
      $FASTQ_R2                                                     \
      $REFERENCE
done

# Merge all bam in the samples directories into a single bam
qsub_wrapper.sh                                                     \
  $SAMPLE_PAIR.mergelanes                                           \
  $Q_HIGH                                                           \
  1                                                                 \
  $SAMPLE_PAIR.fastq2rawbam.pe                                      \
  n                                                                 \
  $NGS_ANALYSIS_DIR/modules/align/picard.mergesamfiles.sh           \
    $PAIRDIR/$SAMPLE_PAIR.mergelanes.bam                            \
    $SAMPLEDIR_N/*rg.bam                                            \
    $SAMPLEDIR_T/*rg.bam

# Dedup, realign, recalibrate, reduce
qsub_wrapper.sh                                                     \
  $SAMPLE_PAIR.processbam                                           \
  $Q_MID                                                            \
  1                                                                 \
  $SAMPLE_PAIR.mergelanes                                           \
  y                                                                 \
  ngs.pipe.dedup.realign.recal.ge.gatk2.sh                          \
    $SAMPLE_PAIR                                                    \
    $PAIRDIR/$SAMPLE_PAIR.mergelanes.bam                            \
    $REFERENCE                                                      \
    $DBSNP_VCF                                                      \
    $MILLS_INDEL_VCF                                                \
    $INDEL_1000G_VCF                                                \
    4

# Separate out the bam files
# qsub_wrapper.sh                                                         \
#   $SAMPLE_N.extractbam                                                  \
#   $Q_HIGH                                                               \
#   1                                                                     \
#   $SAMPLE_PAIR.BQSR.apply                                               \
#   n                                                                     \
#   $NGS_ANALYSIS_DIR/modules/align/samtools.view.rg.sh                   \
#     $PAIRDIR/$SAMPLE_PAIR.mergelanes.dedup.realign.recal.bam            \
#     $SAMPLE_N                                                           \
#     $PAIRDIR/$SAMPLE_PAIR.mergelanes.dedup.realign.recal.$SAMPLE_N.bam
# qsub_wrapper.sh                                                         \
#   $SAMPLE_T.extractbam                                                  \
#   $Q_HIGH                                                               \
#   1                                                                     \
#   $SAMPLE_PAIR.BQSR.apply                                               \
#   n                                                                     \
#   $NGS_ANALYSIS_DIR/modules/align/samtools.view.rg.sh                   \
#     $PAIRDIR/$SAMPLE_PAIR.mergelanes.dedup.realign.recal.bam            \
#     $SAMPLE_T                                                           \
#     $PAIRDIR/$SAMPLE_PAIR.mergelanes.dedup.realign.recal.$SAMPLE_T.bam

# Separate out the bam files
qsub_wrapper.sh                                                         \
  $SAMPLE_PAIR.splitbam                                                 \
  $Q_HIGH                                                               \
  1                                                                     \
  $SAMPLE_PAIR.BQSR.apply                                               \
  n                                                                     \
  gatk2.splitsamfile.sh                                                 \
    $REFERENCE                                                          \
    $PAIRDIR/$SAMPLE_PAIR.mergelanes.dedup.realign.recal.bam            \
 
# Sort and index
qsub_wrapper.sh                                                         \
  $SAMPLE_N.sort_extracted                                              \
  $Q_HIGH                                                               \
  1                                                                     \
  $SAMPLE_PAIR.splitbam                                                 \
  n                                                                     \
  $NGS_ANALYSIS_DIR/modules/align/picard.sortsam.sh                     \
    $PAIRDIR/$SAMPLE_PAIR.mergelanes.dedup.realign.recal.$SAMPLE_N.bam
qsub_wrapper.sh                                                         \
  $SAMPLE_T.sort_extracted                                              \
  $Q_HIGH                                                               \
  1                                                                     \
  $SAMPLE_PAIR.splitbam                                                 \
  n                                                                     \
  $NGS_ANALYSIS_DIR/modules/align/picard.sortsam.sh                     \
    $PAIRDIR/$SAMPLE_PAIR.mergelanes.dedup.realign.recal.$SAMPLE_T.bam

# Symlink to original sample directories
qsub_wrapper.sh                                                                \
  $SAMPLE_N.symlink                                                            \
  $Q_MID                                                                       \
  1                                                                            \
  $SAMPLE_N.sort_extracted                                                     \
  n                                                                            \
  $NGS_ANALYSIS_DIR/modules/util/symlink.abspath.sh                            \
    $PAIRDIR/$SAMPLE_PAIR.mergelanes.dedup.realign.recal.$SAMPLE_N.sort.bam    \
    $SAMPLEDIR_N/$SAMPLE_N.mergelanes.dedup.realign.recal.bam
qsub_wrapper.sh                                                                \
  $SAMPLE_T.symlink                                                            \
  $Q_MID                                                                       \
  1                                                                            \
  $SAMPLE_T.sort_extracted                                                     \
  n                                                                            \
  $NGS_ANALYSIS_DIR/modules/util/symlink.abspath.sh                            \
    $PAIRDIR/$SAMPLE_PAIR.mergelanes.dedup.realign.recal.$SAMPLE_T.sort.bam    \
    $SAMPLEDIR_T/$SAMPLE_T.mergelanes.dedup.realign.recal.bam
qsub_wrapper.sh                                                                \
  $SAMPLE_N.symlink                                                            \
  $Q_MID                                                                       \
  1                                                                            \
  $SAMPLE_N.sort_extracted                                                     \
  n                                                                            \
  $NGS_ANALYSIS_DIR/modules/util/symlink.abspath.sh                            \
    $PAIRDIR/$SAMPLE_PAIR.mergelanes.dedup.realign.recal.$SAMPLE_N.sort.bai    \
    $SAMPLEDIR_N/$SAMPLE_N.mergelanes.dedup.realign.recal.bai
qsub_wrapper.sh                                                                \
  $SAMPLE_T.symlink                                                            \
  $Q_MID                                                                       \
  1                                                                            \
  $SAMPLE_T.sort_extracted                                                     \
  n                                                                            \
  $NGS_ANALYSIS_DIR/modules/util/symlink.abspath.sh                            \
    $PAIRDIR/$SAMPLE_PAIR.mergelanes.dedup.realign.recal.$SAMPLE_T.sort.bai    \
    $SAMPLEDIR_T/$SAMPLE_T.mergelanes.dedup.realign.recal.bai

# Bam QC
qsub_wrapper.sh                                                                \
  $SAMPLE_N.bam.QC                                                             \
  $Q_MID                                                                       \
  1                                                                            \
  $SAMPLE_N.symlink                                                            \
  n                                                                            \
  ngs.pipe.qc.bam.ge.gatk2.sh                                                  \
    SINGLE                                                                     \
    $SAMPLE_N                                                                  \
    $REFERENCE                                                                 \
    $TGT                                                                       \
    $SAMPLEDIR_N/$SAMPLE_N.mergelanes.dedup.realign.recal.bam

qsub_wrapper.sh                                                                \
  $SAMPLE_T.bam.QC                                                             \
  $Q_MID                                                                       \
  1                                                                            \
  $SAMPLE_T.symlink                                                            \
  n                                                                            \
  ngs.pipe.qc.bam.ge.gatk2.sh                                                  \
    SINGLE                                                                     \
    $SAMPLE_T                                                                  \
    $REFERENCE                                                                 \
    $TGT                                                                       \
    $SAMPLEDIR_T/$SAMPLE_T.mergelanes.dedup.realign.recal.bam
