#!/bin/bash
##
## DESCRIPTION:   Convert a sam to bam, or bam to sam
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2013.12.08
## LAST MODIFIED: 2013.12.08
## MODIFIED BY:   Jin Kim jjinking(at)gmail(dot)com
##
## USAGE:         picard.samformatconvert.sh
##                                           in.[bs]am
##                                           out.[bs]am
##                                           [max_records_in_ram]
##
## OUTPUT:        out.[bs]am
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 2 $# $0

# Process input params
IN_FILE=$1; shift
OUTFILE=$1; shift
MAX_RECORDS_IN_RAM=$2
MAX_RECORDS_IN_RAM=${MAX_RECORDS_IN_RAM:=1000000}

# Run tool
`javajar 8g` $PICARD_PATH/SamFormatConverter.jar \
  INPUT=$IN_FILE                                 \
  OUTPUT=$OUTFILE                                \
  MAX_RECORDS_IN_RAM=$MAX_RECORDS_IN_RAM         \
  VALIDATION_STRINGENCY=LENIENT                  \
  &> $OUTFILE.log
