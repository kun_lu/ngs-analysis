#!/bin/bash
## 
## DESCRIPTION:   Count number of aligned reads in a bam file
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
## 
## USAGE:         samtools.view.count.sh
##                                       in.bam    # Input bam file
##
## OUTPUT:        in.bam.numreads
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 1 $# $0

# Process parameters
IBAM=$1

# Merge multiple bam files
$SAMTOOLS                   \
  view                      \
  -c                        \
  $IBAM                     \
  1> $IBAM.numreads         \
  2> $IBAM.numreads.err
