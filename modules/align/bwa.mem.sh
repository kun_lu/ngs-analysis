#!/bin/bash
##
## DESCRIPTION:   Align fastq sequences to a reference
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         bwa.mem.sh
##                           ref.fa            # Reference to align to
##                           "options"         # Additional bwa options, i.e. "-t 2 -M"
##                           outprefix         # Prefix for the sam file output
##                           in1.fastq.gz      # Input 1 or 2 fastq files
##                           [in2.fastq.gz]
##
## OUTPUT:        outprefix.sam
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 4 $# $0

# Process input params
REF=$1; shift
OPT=$1; shift
OUT=$1; shift
FA1=$1; shift
FA2=$1

# Format output filename
OUT=$OUT.sam

# If output exists, don't run
assert_file_not_exists_w_content $OUT

# Run tool
$BWA mem         \
     $REF        \
     $OPT        \
     $FA1 $FA2   \
     1> $OUT     \
     2> $OUT.err


# Usage: bwa mem [options] <idxbase> <in1.fq> [in2.fq]
# 
# Algorithm options:
# 
#        -t INT     number of threads [1]
#        -k INT     minimum seed length [19]
#        -w INT     band width for banded alignment [100]
#        -d INT     off-diagonal X-dropoff [100]
#        -r FLOAT   look for internal seeds inside a seed longer than {-k} * FLOAT [1.5]
#        -c INT     skip seeds with more than INT occurrences [10000]
#        -S         skip mate rescue
#        -P         skip pairing; mate rescue performed unless -S also in use
#        -A INT     score for a sequence match [1]
#        -B INT     penalty for a mismatch [4]
#        -O INT     gap open penalty [6]
#        -E INT     gap extension penalty; a gap of size k cost {-O} + {-E}*k [1]
#        -L INT     penalty for clipping [5]
#        -U INT     penalty for an unpaired read pair [17]
# 
# Input/output options:
# 
#        -p         first query file consists of interleaved paired-end sequences
#        -R STR     read group header line such as '@RG\tID:foo\tSM:bar' [null]
# 
#        -v INT     verbose level: 1=error, 2=warning, 3=message, 4+=debugging [3]
#        -T INT     minimum score to output [30]
#        -a         output all alignments for SE or unpaired PE
#        -C         append FASTA/FASTQ comment to SAM output
#        -M         mark shorter split hits as secondary (for Picard/GATK compatibility)
# 
# Note: Please read the man page for detailed description of the command line and options.
