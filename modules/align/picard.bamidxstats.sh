#!/bin/bash
##
## DESCRIPTION:   Bam index statistics
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         picard.bamidxstats.sh sample.bam [max_records_in_ram]
##
## OUTPUT:        
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 1 $# $0

BAMFILE=$1
MAX_RECORDS_IN_RAM=$2
MAX_RECORDS_IN_RAM=${MAX_RECORDS_IN_RAM:=1000000}

# Format output filenames
OUTPUTPREFIX=$BAMFILE
OUTPUTFILE=$OUTPUTPREFIX.idxstats
OUTPUTLOG=$OUTPUTFILE.err

# Run tool
`javajar 16g` $PICARD_PATH/BamIndexStats.jar          \
  INPUT=$BAMFILE                                      \
  MAX_RECORDS_IN_RAM=$MAX_RECORDS_IN_RAM              \
  VALIDATION_STRINGENCY=LENIENT                       \
  1> $OUTPUTFILE                                      \
  2> $OUTPUTLOG


# 
# USAGE: BamIndexStats [options]
# 
# Generates BAM index statistics. Input BAM file must have a corresponding index file.
# 
# 
# Options:
# 
# --help
# -h                            Displays options specific to this tool.
# 
# --stdhelp
# -H                            Displays options specific to this tool AND options common to all Picard command line 
#                               tools.
# 
# INPUT=File
# I=File                        A BAM file to process.  Required. 
# 