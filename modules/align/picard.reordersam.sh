#!/bin/bash
##
## DESCRIPTION:   Reorder sam file according to reference file
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         picard.reordersam.sh
##                                     ref.fa
##                                     sample.bam
##                                     [max_records_in_ram]
##
## OUTPUT:        sample.reorder.bam
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 2 $# $0

# Process input args
REFEREN=$1; shift
BAMFILE=$1; shift
MAX_RECORDS_IN_RAM=$1
MAX_RECORDS_IN_RAM=${MAX_RECORDS_IN_RAM:=1000000}

# Format output filenames
OUTPUTPREFIX=`filter_ext $BAMFILE 1`
OUTPUTFILE=$OUTPUTPREFIX.reorder.bam
OUTPUTLOG=$OUTPUTFILE.log

# If output exists, don't run
assert_file_not_exists_w_content $OUTPUTFILE

# Run tool
`javajar 16g` $PICARD_PATH/ReorderSam.jar             \
  INPUT=$BAMFILE                                      \
  OUTPUT=$OUTPUTFILE                                  \
  REFERENCE=$REFEREN                                  \
  ALLOW_INCOMPLETE_DICT_CONCORDANCE=true              \
  ALLOW_CONTIG_LENGTH_DISCORDANCE=false               \
  MAX_RECORDS_IN_RAM=$MAX_RECORDS_IN_RAM              \
  CREATE_INDEX=true                                   \
  VALIDATION_STRINGENCY=LENIENT                       \
  &> $OUTPUTLOG
