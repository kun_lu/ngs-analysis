#!/bin/bash
## 
## DESCRIPTION:   Split a bam file by chromosomes, uses grid engine
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2013.12.18
## LAST MODIFIED: 
## MODIFIED BY:
## 
## USAGE:         samtools.splitbychrom.ge.sh
##                                            in.bam          # Input bam file
##                                            (hg|b3x)        # Type of reference genome used
##                                            (sam|bam)       # Output format
##
## OUTPUT:        bam files for each chromosome
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 3 $# $0

# Process parameters
BAM=$1; shift
RCT=$1; shift
FMT=$1; shift

# Set resource vars
if [ $RCT = 'hg' ]; then
  CHROMS='chr1 chr2 chr3 chr4 chr5 chr6 chr7 chr8 chr9 chr10 chr11 chr12 chr13 chr14 chr15 chr16 chr17 chr18 chr19 chr20 chr21 chr22 chrX chrY'
else
  CHROMS='1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 X Y'
fi

# Set output options
if [ $FMT = 'sam' ]; then
  OPT=' -h '
else
  OPT=' -bh '
fi

# Run tool
for chrom in $CHROMS; do
  qsub_wrapper.sh                                                                 \
    split.`basename $BAM | cut -f1 -d'.'`.$chrom                                  \
    $Q_HIGH                                                                       \
    1                                                                             \
    none                                                                          \
    n                                                                             \
    bash_wrapper.sh $SAMTOOLS view $OPT $BAM $chrom -o `filter_ext $BAM 1`.$chrom.$FMT
done
