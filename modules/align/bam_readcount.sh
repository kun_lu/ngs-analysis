#!/bin/bash
##
## DESCRIPTION:   Count reads for each base position
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         bam_readcount.sh
##                                 input.bam
##                                 ref.fa
##                                 [chrom_type           # chr | num ('chr' if chr1, chr2,..,chrY; 'num' if 1,2,..,Y)
##                                 [min_base_quality
##                                 [min_mapping_quality
##                                 [target_region]]]]
##
## OUTPUT:        input.bam.readcount
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 2 $# $0

# Process input params
BAM=$1
REF=$2
CHT=$3
BQT=$4
MQT=$5
TGT=$6

# Make sure that the input files exist
assert_file_exists_w_content $BAM
assert_file_exists_w_content $REF

# Format output filenames
OUTPUT=$BAM.readcount
OUTERR=$OUTPUT.log

# If output file already exists and has content, then don't run
assert_file_not_exists_w_content $OUTPUT

# Set tool parameters
CHT=${CHT:='chr'}
if [ ! -z "$BQT" ]; then
  BQT_PARAM='-b '$BQT
fi
if [ ! -z "$MQT" ]; then
  MQT_PARAM='-q '$MQT
fi
if [ ! -z "$TGT" ]; then
  TGT_PARAM='-l '$TGT
fi
OPTIONAL_PARAMS=$BQT_PARAM' '$MQT_PARAM' '$TGT_PARAM

# Set up chromosomes
CHROMS='1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 X Y'
if [ "$CHT" = "chr" ]; then
  CHROMS='chr1 chr2 chr3 chr4 chr5 chr6 chr7 chr8 chr9 chr10 chr11 chr12 chr13
          chr14 chr15 chr16 chr17 chr18 chr19 chr20 chr21 chr22 chrX chrY'
fi

# Set up bam index file
bam.linkindex.sh $BAM

# Run tool for each chromosome
for chrom in $CHROMS; do
  $BAM_READCOUNT             \
    $BAM                     \
    $chrom                   \
    -f $REF                  \
    $OPTIONAL_PARAMS         \
    1>> $OUTPUT              \
    2>> $OUTERR
done


# Usage: bam-readcount <bam_file> [region]
#         -q INT    filtering reads with mapping quality less than INT [0]
#         -b INT    don't include reads where the base quality is less than INT [0]
#         -f FILE   reference sequence in the FASTA format
#         -l FILE   list of regions to report readcounts within.
#         -d        report the mapping qualities as a comma separated list

# This program reports readcounts for each base at each position requested.

# Positions should be requested via the -l option as chromosome, start, stop
# where the coordinates are 1-based and each field is separated by whitespace.

# A single region may be requested on the command-line similarly to samtools view
# (i.e. bam-readcount -f ref.fa some.bam 1:150-150).

# It also reports the average base quality of these bases and mapping qualities of
# the reads containing each base.

# The format is as follows:
# chr     position        reference_base  base:count:avg_mapping_quality:avg_basequality:avg_se_mapping_quality:num_plus_strand:num_minus_strand:avg_pos_as_fraction:avg_num_mismatches_as_fraction:avg_sum_mismatch_qualitiest:num_q2_containing_reads:avg_distance_to_q2_start_in_q2_reads:avg_clipped_length:avg_distance_to_effective_3p_end...
