#!/bin/bash
##
## DESCRIPTION:   Merge sam or bam files, but do not sort resulting merged bam file
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2013.11.18
## LAST MODIFIED: 2013.11.18
## MODIFIED BY:   Jin Kim jjinking(at)gmail(dot)com
##
## USAGE:         picard.mergesamfiles.unsorted.sh
##                                                 out.bam
##                                                 in1.bam
##                                                 in2.bam
##                                                 [in3.bam [...]]
##
## OUTPUT:        out.bam
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 2 $# $0

# Process input params
OUT=$1; shift
BAM=$@
MAX_RECORDS_IN_RAM=1000000

# Make sure that the output file does not exist
assert_file_not_exists $OUT

# If the input only contains a single bamfile,
# create symbolic link to out.bam
BAM_ARRAY=($BAM)
if [ ${#BAM_ARRAY[@]} -eq 1 ]; then
  ln -s `readlink -f $BAM` $OUT
  exit
fi

# Format input options
INPUTBAM=''
for bamfile in $BAM; do
  # Check if file exists
  assert_file_exists_w_content $bamfile
  INPUTBAM=$INPUTBAM' INPUT='$bamfile
done

# Run tool
`javajar 12g` $PICARD_PATH/MergeSamFiles.jar          \
  $INPUTBAM                                           \
  OUTPUT=$OUT                                         \
  SORT_ORDER=unsorted                                 \
  MERGE_SEQUENCE_DICTIONARIES=true                    \
  USE_THREADING=true                                  \
  MAX_RECORDS_IN_RAM=$MAX_RECORDS_IN_RAM              \
  CREATE_INDEX=false                                  \
  VALIDATION_STRINGENCY=LENIENT                       \
  &> $OUT.log
