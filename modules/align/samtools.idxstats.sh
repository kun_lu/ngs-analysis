#!/bin/bash
##
## DESCRIPTION:   Generate idxstats
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         samtools.idxstats.sh sample.bam
##
## OUTPUT:        sample.bam.idxstats
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 1 $# $0

# Process input params
BAM=$1

# Run tool
$SAMTOOLS              \
  idxstats             \
  $BAM                 \
  1> $BAM.idxstats     \
  2> $BAM.idxstats.err \
