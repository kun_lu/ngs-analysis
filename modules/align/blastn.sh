#!/bin/bash
##
## DESCRIPTION:   Run blast
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         blastn.sh
##                          input.fa
##                          db
##                          num_threads
##                          ["options"]  # Other blastn options in quotes, i.e. "-outfmt 5 -out input.fa.blast.xml"
##
## OUTPUT:        blast results to standard output, unless output option is specified within ["options"]
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 3 $# $0

# Process input params
INPUTFA=$1
DATABSE=$2
THREADS=$3
OPTIONS=$4

# Format output filenames
OUT_PRE=$INPUTFA

# Run tool
blastn                         \
  -db $DATABSE                 \
  -num_threads $THREADS        \
  -query $INPUTFA              \
  $OPTIONS                     \
  &> $OUT_PRE.blast.log
