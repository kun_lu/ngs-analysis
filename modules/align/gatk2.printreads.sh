#!/bin/bash
##
## DESCRIPTION:   Run GATK PrintReads tool
##                http://www.broadinstitute.org/gatk/gatkdocs/org_broadinstitute_sting_gatk_walkers_PrintReads.html
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         gatk2.printreads.sh
##                                    ref.fa            # Reference fasta file used in alignment
##                                    outprefix         # Output prefix
##                                    "options"         # Options for this tool in quotes i.e. "-nct 12 -BQSR recalibration_report.grp"
##                                    in1.bam           # Input bam file
##                                    [in2.bam [...]]   # Additional bam files
##
## OUTPUT:        outprefix.bam
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 4 $# $0

# Process input params
PAR=($@)                                    # Parameter array
P_N=${#PAR[@]}                              # Number of words in parameter array (counts words in quotes too)
REF=$1
OUT=$2
OPT=$3
OPA=($OPT)                                  # Convert quoted options string into array
OPN=${#OPA[@]}                              # Number of words in options string
BAM=${PAR[@]:$((2+$OPN)):$(($P_N-2-$OPN))}  # Bam files

# Format output filenames
OUTFILE=$OUT.bam

# If output exists, don't run
assert_file_not_exists_w_content $OUTFILE

# Format list of input bam files
INPUTBAM=''
for bamfile in $BAM; do
  # Check if file exists
  assert_file_exists_w_content $bamfile
  INPUTBAM=$INPUTBAM' -I '$bamfile
done

# Run tool
`javajar 8g` $GATK2     \
  -T PrintReads         \
  -R $REF               \
     $OPT               \
     $INPUTBAM          \
  -o $OUTFILE           \
  &> $OUTFILE.log


# Arguments for PrintReads:
#  -o,--out <out>                                    Write output to this BAM filename instead of STDOUT
#  -compress,--bam_compression <bam_compression>     Compression level to use for writing BAM files
#  --disable_bam_indexing                            Turn off on-the-fly creation of indices for output BAM files.
#  --generate_md5                                    Enable on-the-fly creation of md5s for output BAM files.
#  -simplifyBAM,--simplifyBAM                        If provided, output BAM files will be simplified to include just key 
#                                                    reads for downstream variation discovery analyses (removing 
#                                                    duplicates, PF-, non-primary reads), as well stripping all extended 
#                                                    tags from the kept reads except the read group identifier
#  -readGroup,--readGroup <readGroup>                Exclude all reads with this read group from the output
#  -platform,--platform <platform>                   Exclude all reads with this platform from the output
#  -n,--number <number>                              Print the first n reads from the file, discarding the rest
#  -ds,--downsample_coverage <downsample_coverage>   Downsample BAM to desired coverage
#  -sf,--sample_file <sample_file>                   File containing a list of samples (one per line). Can be specified 
#                                                    multiple times
#  -sn,--sample_name <sample_name>                   Sample name to be included in the analysis. Can be specified multiple 
#                                                    times.
#  -s,--simplify                                     Simplify all reads.
