#!/bin/bash
##
## DESCRIPTION:   Run pipeline version of blastgo
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
## 
## USAGE:         blast2go4pipe.sh in.blast.xml
##
## OUTPUT:        
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 1 $# $0

# Process input params
INPUT_XML=$1

# Format output filenames
OUTPUT_PRE=$INPUT_XML.b2g
OUTPUT_LOG=$OUTPUT_PRE.log

# Run tool
CLASSPATH=$BLAST2GO4PIPE_PATH/ext
$JAVA                                                                                           \
  -cp $BLAST2GO4PIPE_PATH/blast2go.jar:$BLAST2GO4PIPE_PATH/ext/*:                               \
  es.blast2go.prog.B2GAnnotPipe                                                                 \
  -in  $INPUT_XML                                                                               \
  -out $OUTPUT_PRE                                                                              \
  -prop $BLAST2GO4PIPE_PATH/b2gPipe.properties                                                  \
  -v                                                                                            \
  -annot                                                                                        \
  -dat                                                                                          \
  -img                                                                                          \
  -annex                                                                                        \
  -goslim                                                                                       \
  -wiki $BLAST2GO4PIPE_PATH/html_template.html &> $OUTPUT_LOG

# Blast2GO Pipeline Version 2.5.0
# --------------------------------------
#
# -in <input-file>        XML file with (ncbi-style) blast results
# -out <outputfile>       Name of output file without extension (optional: default=input-file + extension)
# -prop <property-file>   file containing the application settings (optional: default=./b2gPipe.properties)
# -v                      show application messages (verbose)
# -annot                  generate annotation (.annot)
# -dat                    generate blast2go project file (NOT recomended with more than 30000 Seqs. or less than 2 GB of memory) (.dat)
# -fas <fasta-file>       import a fasta file first
# -img                    generate images (statistic charts). Image generation works only in combination with the -dat option)
# -ips <path to interpro-folder>  parse InterProScan result XML files (provide dir path) and merge GO terms to annotation
# -wiki <wiki template>   generate a wiki file based on a given template
# -annex  runs the Annex step
# -goslim         converts the annotations into the GoSlim version

