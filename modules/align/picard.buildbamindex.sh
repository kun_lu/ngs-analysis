#!/bin/bash
##
## DESCRIPTION:   Generate bai
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.12.25
## MODIFIED BY:   Jin Kim jjinking(at)gmail(dot)com
##
## USAGE:         picard.buildbamindex.sh sample.bam [max_records_in_ram]
##
## OUTPUT:        sample.bai
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 1 $# $0

BAMFILE=$1
MAX_RECORDS_IN_RAM=$2
MAX_RECORDS_IN_RAM=${MAX_RECORDS_IN_RAM:=1000000}

# Format output filenames
OUTPREFIX=`filter_ext $BAMFILE 1`
OUTPUTFILE=$OUTPREFIX.bai
OUTPUTLOG=$OUTPUTFILE.log

assert_file_not_exists_w_content $OUTPUTFILE

# Run tool
`javajar 8g` $PICARD_PATH/BuildBamIndex.jar          \
  INPUT=$BAMFILE                                     \
  MAX_RECORDS_IN_RAM=$MAX_RECORDS_IN_RAM             \
  VALIDATION_STRINGENCY=LENIENT                      \
  &> $OUTPUTLOG

