#!/bin/bash
##
## DESCRIPTION:   Convert reads in a bam file to fastq format
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         picard.sam2fastq.sh sample.bam [max_records_in_ram]
##
## OUTPUT:        sample.bam.R1.fastq sample.bam.R2.fastq
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 1 $# $0

BAMFILE=$1
MAX_RECORDS_IN_RAM=$2
MAX_RECORDS_IN_RAM=${MAX_RECORDS_IN_RAM:=1000000}

# Format output filenames
OUT_PRE=$BAMFILE

# Run tool
`javajar 8g` $PICARD_PATH/SamToFastq.jar         \
  INPUT=$BAMFILE                                 \
  FASTQ=$OUT_PRE.R1.fastq                        \
  SECOND_END_FASTQ=$OUT_PRE.R2.fastq             \
  MAX_RECORDS_IN_RAM=$MAX_RECORDS_IN_RAM         \
  VALIDATION_STRINGENCY=LENIENT                  \
  &> $OUT_PRE.sam2fastq.log
