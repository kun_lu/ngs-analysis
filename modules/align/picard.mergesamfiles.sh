#!/bin/bash
##
## DESCRIPTION:   Merge sam or bam files
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         picard.mergesamfiles.sh
##                                        out.bam
##                                        in1.bam
##                                        in2.bam
##                                        [in3.bam [...]]
##
## OUTPUT:        out.bam
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 2 $# $0

# Process input params
OUT=$1; shift
BAM=$@
MAX_RECORDS_IN_RAM=1000000

# Make sure that the output file does not exist
assert_file_not_exists $OUT

# If the input only contains a single bamfile,
# create symbolic link to out.bam
BAM_ARRAY=($BAM)
if [ ${#BAM_ARRAY[@]} -eq 1 ]; then
  ln -s `readlink -f $BAM` $OUT
  exit
fi

# Format input options
INPUTBAM=''
for bamfile in $BAM; do
  # Check if file exists
  assert_file_exists_w_content $bamfile
  INPUTBAM=$INPUTBAM' INPUT='$bamfile
done

# Run tool
`javajar 12g` $PICARD_PATH/MergeSamFiles.jar          \
  $INPUTBAM                                           \
  OUTPUT=$OUT                                         \
  SORT_ORDER=coordinate                               \
  MERGE_SEQUENCE_DICTIONARIES=true                    \
  USE_THREADING=true                                  \
  MAX_RECORDS_IN_RAM=$MAX_RECORDS_IN_RAM              \
  CREATE_INDEX=true                                   \
  VALIDATION_STRINGENCY=LENIENT                       \
  &> $OUT.log
