#!/bin/bash
##
## DESCRIPTION:   Run GATK CountReads tool
##                http://www.broadinstitute.org/gatk/gatkdocs/org_broadinstitute_sting_gatk_walkers_qc_CountReads.html
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         gatk2.countreads.sh
##                                    ref.fa            # Reference fasta file used in alignment
##                                    outprefix         # Output prefix
##                                    "options"         # Options for this tool in quotes i.e. "-nct 12 -L input.intervals"
##                                    in1.bam           # Input bam file
##                                    [in2.bam [...]]   # Additional bam files
##
## OUTPUT:        outprefix.bam
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 4 $# $0

# Process input params
PAR=($@)                                    # Parameter array
P_N=${#PAR[@]}                              # Number of words in parameter array (counts words in quotes too)
REF=$1
OUT=$2
OPT=$3
OPA=($OPT)                                  # Convert quoted options string into array
OPN=${#OPA[@]}                              # Number of words in options string
BAM=${PAR[@]:$((2+$OPN)):$(($P_N-2-$OPN))}  # Bam files

# Format output filenames
OUTFILE=$OUT.countreads

# If output exists, don't run
assert_file_not_exists_w_content $OUTFILE

# Format list of input bam files
INPUTBAM=''
for bamfile in $BAM; do
  # Check if file exists
  assert_file_exists_w_content $bamfile
  INPUTBAM=$INPUTBAM' -I '$bamfile
done

# Run tool
`javajar 8g` $GATK2     \
  -T CountReads         \
  -R $REF               \
     $OPT               \
     $INPUTBAM          \
  1> $OUTFILE           \
  2> $OUTFILE.err
