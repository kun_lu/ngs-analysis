#!/usr/bin/env python
'''
Description     : Tool to handle bam file related stuff
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.09.27
'''

import argparse
import csv
import pysam
import sys
from collections import defaultdict

def subcommand_extractpe(args):
    '''
    Extract paired reads from a bam file
    Note: During testing, counts were a little bit off... not sure what is wrong
    '''

    bam_in = pysam.Samfile(args.bamfile, "rb")
    bam_out_pe = pysam.Samfile(args.outprefix + '.bam', "wb", template=bam_in)
    if args.single_prefix:
        bam_out_se = pysam.Samfile(args.single_prefix + '.bam', "wb", template=bam_in)
    
    for read in bam_in.fetch():
        if read.is_paired:
            bam_out_pe.write(read)
        elif args.single_prefix:
            bam_out_se.write(read)

    bam_in.close()
    bam_out_pe.close()
    if args.single_prefix:
        bam_out_se.close()

def subcommand_inserts(args):
    '''
    Process a bam file and get information about insert sizes
    '''
    bam_in = pysam.Samfile(args.bamfile, 'rb')
    size2counts = defaultdict(int) # histogram
    size2quals = defaultdict(list) # map insert size to list of quality values
    for read in bam_in.fetch():
        if read.is_paired:
            size2counts[read.isize] += 1
            size2quals[read.isize] += [ord(q) - 33 for q in read.qqual] # Convert to numeric quality scores

    # Output the insert information
    with args.out as fout:
        writer = csv.writer(fout, delimiter='\t', lineterminator='\n')
        writer.writerow(['size',
                         'count',
                         'meanqual'])
        for size in sorted(size2counts.keys()):
            quals = size2quals[size]
            avgqual = reduce(lambda x, y: x + y, quals) / float(len(quals))
            writer.writerow([size,
                             size2counts[size],
                             avgqual])

def main():
    parser = argparse.ArgumentParser(description="Tool to handle bam file related stuff")
    
    subparsers = parser.add_subparsers(title='subcommands',
                                       description='Available tools',
                                       dest='subcommand')
    # Subcommand: extract paired end reads
    parser_extractpe = subparsers.add_parser('extract_pe',
                                             help='Extract paired reads from a bam file')
    parser_extractpe.add_argument('bamfile',
                                  help='Input bam file',
                                  type=str)
    parser_extractpe.add_argument('outprefix',
                                  help='Output filename prefix',
                                  type=str)
    parser_extractpe.add_argument('-s','--single-prefix',
                                  help='Output filename prefix for single reads',
                                  type=str)
    parser_extractpe.set_defaults(func=subcommand_extractpe)

    # Subcommand: insert fragment information
    parser_inserts = subparsers.add_parser('inserts',
                                           help='Get information about insert fragments')
    parser_inserts.add_argument('bamfile',
                                help='Input bam file',
                                type=str)
    parser_inserts.add_argument('-o', '--out',
                                help='Output filename (default stdout)',
                                type=argparse.FileType('wb'),
                                default=sys.stdout)
    parser_inserts.set_defaults(func=subcommand_inserts)

    # Parse the arguments and call the corresponding function
    args = parser.parse_args()
    args.func(args)
                        

if __name__ == '__main__':
    main()
