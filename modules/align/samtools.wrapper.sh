#!/bin/bash
##
## DESCRIPTION:   Run samtools
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         samtools.wrapper.sh
##                                    subcommand        # Subcommand, i.e. flagstat
##                                    "options"         # Options for this tool in quotes i.e. "-nt 20 -L target.bed"
##                                    in.bam            # Input bam file
##                                    outfile           # Output filename
##
## OUTPUT:        outfile
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 4 $# $0

# Process input params
SUB=$1; shift
OPT=$1; shift
BAM=$1; shift
OUT=$1

# Make sure input file exists
assert_file_exists_w_content $BAM

# Check to make sure output file doesn't already exist
assert_file_not_exists_w_content $OUT

# Run tool
$SAMTOOLS      \
  $SUB         \
  $OPT         \
  $BAM         \
  1> $OUT      \
  2> $OUT.err
