#!/bin/bash
##
## DESCRIPTION:   Count number of reads for each strand
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2014.02.05
## LAST MODIFIED: 
## MODIFIED BY:
##
## USAGE:         bam_strandcount.sh input.bam region.bed
##
## OUTPUT:        
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 2 $# $0

# Process input params
BAM=$1; shift
BED=$1; shift

# Make sure that the input files exist
assert_file_exists_w_content $BAM

# Format output filenames
OUT=$BAM.strandcount

# If output file already exists and has content, then don't run
assert_file_not_exists_w_content $OUT

# Count positive strand
$SAMTOOLS view -L $BED -F 0x10 -b $BAM -c 1> $OUT.pos 2> $OUT.pos.err

# Count negative strand
$SAMTOOLS view -L $BED -f 0x10 -b $BAM -c 1> $OUT.neg 2> $OUT.neg.err
