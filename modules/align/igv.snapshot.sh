#!/bin/bash
##
## DESCRIPTION:   Generate IGV batch script file
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         igv.snapshot.sh
##                                bamfile
##                                chrom
##                                pos
##                                flanksize     # Will take snapshot of range (pos-flanksize, pos+flanksize), in base pairs
##                                outprefix
##
## OUTPUT:        outprefix.chrom.pos
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 5 $# $0

# Process input params
BAM_F=$1
CHROM=$2
COORD=$3
FLANK=$4
OUTPR=$5

# Format output filenames
OUTPUTFILE=$OUTPR.$CHROM.$COORD.txt
OUTPUTERROR=$OUTPUTFILE.err

assert_file_not_exists_w_content $OUTPUTFILE

# Generate
echo "new"                                                >> $OUTPUTFILE
echo "load" `readlink -f $BAM_F`                          >> $OUTPUTFILE
echo "snapshotDirectory" $PWD                             >> $OUTPUTFILE
echo "genome b37"                                         >> $OUTPUTFILE
echo "goto" $CHROM:$((COORD-$FLANK))-$((COORD + $FLANK))  >> $OUTPUTFILE
echo "sort base"                                          >> $OUTPUTFILE
echo "collapse"                                           >> $OUTPUTFILE
echo "snapshot" $OUTPUTFILE.png                           >> $OUTPUTFILE