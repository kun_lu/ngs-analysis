#!/usr/bin/env python
'''
Description     : Tool to work with blast xml files
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.09.27
'''

import argparse
import csv
import sys
from Bio.Blast import NCBIXML

def subcommand_count_hits(args):
    '''
    Count blast hits per query
    '''
    E_VALUE_THRESHS = (1E-2, 1E-3, 1E-4, 1E-5, 1E-6, 1E-7, 1E-8, 1E-9)
    writer = csv.writer(args.outfile, delimiter='\t', lineterminator='\n')
    writer.writerow(['Sequence'] + list(E_VALUE_THRESHS))

    # Iterate through all queries
    blast_records = NCBIXML.parse(args.result_xml)
    for blast_record in blast_records:

        # Count up the number of hits for each query
        num_hits = [0 for t in E_VALUE_THRESHS]
        for alignment in blast_record.alignments:
            hsp = alignment.hsps[0]
            eval = hsp.expect
            # Compare the e-value to all the thresholds
            for i,t in enumerate(E_VALUE_THRESHS):
                if eval <= t:
                    num_hits[i] += 1

        writer.writerow([blast_record.query.strip()] + num_hits)
                
def main():
    parser = argparse.ArgumentParser(description="Tool to work with blast xml files")
    subparsers = parser.add_subparsers(title='subcommands',
                                       description='Available tools',
                                       dest='subcommand')
    # Subcommand: Count hits for each query
    parser_count_hits = subparsers.add_parser('count_hits',
                                             help='Count hits per query')
    parser_count_hits.add_argument('result_xml',
                                   help='Blast result xml file',
                                   nargs='?',
                                   type=argparse.FileType('r'),
                                   default=sys.stdin)
    parser_count_hits.add_argument('-o', '--outfile',
                                   help='Output file',
                                   type=argparse.FileType('w'),
                                   default=sys.stdout)
    parser_count_hits.set_defaults(func=subcommand_count_hits)

    # Parse the arguments and call the corresponding function
    args = parser.parse_args()
    args.func(args)
                        

if __name__ == '__main__':
    main()
