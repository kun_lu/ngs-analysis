#!/bin/bash
##
## DESCRIPTION:   Run GATK RealignerTargetCreator tool
##                http://gatkforums.broadinstitute.org/discussion/38/local-realignment-around-indels
##                http://www.broadinstitute.org/gatk/gatkdocs/org_broadinstitute_sting_gatk_walkers_indels_RealignerTargetCreator.html
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         gatk2.realignertargetcreator.sh
##                                                ref.fa
##                                                outprefix
##                                                ["options"]      # Options for this tool in quotes i.e. "-nt 12 -L target_region.intervals -known indels.vcf"
##                                                in1.bam
##                                                [in2.bam [...]]
##
## OUTPUT:        sample.realign.intervals
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 4 $# $0

# Process input params
PAR=($@)                                    # Parameter array
P_N=${#PAR[@]}                              # Number of words in parameter array (counts words in quotes too)
REF=$1
OUT=$2
OPT=$3
OPA=($OPT)                                  # Convert quoted options string into array
OPN=${#OPA[@]}                              # Number of words in options string
BAM=${PAR[@]:$((2+$OPN)):$(($P_N-2-$OPN))}  # Bam files

# Format output filenames
OUTFILE=$OUT.realign.intervals

# If output exists, don't run
assert_file_not_exists_w_content $OUTFILE

# Format list of input bam files
INPUTBAM=''
for bamfile in $BAM; do
  # Check if file exists
  assert_file_exists_w_content $bamfile
  INPUTBAM=$INPUTBAM' -I '$bamfile
done

# Run tool
`javajar 8g` $GATK2                    \
  -T RealignerTargetCreator            \
  -R $REF                              \
     $OPT                              \
     $INPUTBAM                         \
  -o $OUTFILE                          \
  &> $OUTFILE.log


# Arguments for RealignerTargetCreator:
#  -o,--out <out>                                     An output file created by the walker.  Will overwrite contents if 
#                                                     file exists
#  -known,--known <known>                             Input VCF file with known indels
#  -window,--windowSize <windowSize>                  window size for calculating entropy or SNP clusters
#  -mismatch,--mismatchFraction <mismatchFraction>    fraction of base qualities needing to mismatch for a position to 
#                                                     have high entropy
#  -minReads,--minReadsAtLocus <minReadsAtLocus>      minimum reads at a locus to enable using the entropy calculation
#  -maxInterval,--maxIntervalSize <maxIntervalSize>   maximum interval size; any intervals larger than this value will be 
#                                                     dropped
