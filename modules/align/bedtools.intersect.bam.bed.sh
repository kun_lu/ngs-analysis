#!/bin/bash
## 
## DESCRIPTION:   Intersect a bam file with a bed file, and output compressed bam file
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         bedtools.intersect.bam.bed.sh
##                                              a.bam
##                                              b.bed
##                                              output.bam
##
## OUTPUT:        output.bed
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Usage check:
usage 3 $# $0

LEFTFILE=$1
RIGHTFILE=$2
OUTPUTFILE=$3

$BEDTOOLS_PATH/intersectBed     \
  -abam $LEFTFILE               \
  -b $RIGHTFILE                 \
  > $OUTPUTFILE
