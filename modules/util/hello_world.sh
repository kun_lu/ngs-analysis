#!/bin/bash
## 
## DESCRIPTION:   Example script template
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         hello_world.sh
##
## OUTPUT:        None
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Usage check:
# 1st parameter is the desired number of parameters: 1 in this case, i.e. foo
# Second parameter is the actual number of parameters passed in
# Third parameter is the path to this script
#usage 0 $# $0

echo 'hello world!'