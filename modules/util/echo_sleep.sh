#!/bin/bash
## 
## DESCRIPTION:   Example script template
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         echo_sleep.sh
##                              "text"        # Text to echo
##                              sleeptime     # Number of seconds to sleep before running echo
##                              outfile       # Output filename
##
## OUTPUT:        outfile
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Usage check
usage 3 $# $0

# Process args
TXT=$1; shift
SLP=$1; shift
OUT=$1

# Sleep
sleep $SLP

# Run command
echo $TXT > $OUT
