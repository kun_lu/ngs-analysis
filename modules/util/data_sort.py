#!/usr/bin/env python
'''
Description     : Sort the rows of a data file in tsv format
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.10.02
'''

import argparse
import contextlib
import csv
import sys
from itertools import ifilterfalse

def unique_everseen(iterable, key=None):
    """
    Taken from http://docs.python.org/2/library/itertools.html recipes

    Generate unique elements, preserving order. Remember all elements ever seen."
    unique_everseen('AAAABBBCCDAABBB') --> A B C D
    unique_everseen('ABBCcAD', str.lower) --> A B C D
    """
    seen = set()
    seen_add = seen.add
    if key is None:
        for element in ifilterfalse(seen.__contains__, iterable):
            seen_add(element)
            yield element
    else:
        for element in iterable:
            k = key(element)
            if k not in seen:
                seen_add(k)
                yield element

def data_sort(args):
    '''
    Sort data file
    '''
    with contextlib.nested(args.file, args.output) as (fin,fout):
        reader = csv.reader(fin, delimiter='\t')
        writer = csv.writer(fout, delimiter='\t', lineterminator='\n')

        # Output header
        if args.header:
            writer.writerow(reader.next())
            
        # Sort rows
        if args.column:
            sortedrows = sorted(reader, key=lambda row_i: row_i[args.column])
        else:
            sortedrows = sorted(reader)

        # Unique
        if args.unique:
            sortedrows = unique_everseen(sortedrows, tuple)

        # Output
        writer.writerows(sortedrows)

def main():
    # Set up parameter options
    parser = argparse.ArgumentParser(description="Sort the rows of a data file in tsv format")
    parser.add_argument('file',
                        help='Input file',
                        nargs='?',
                        type=argparse.FileType('rU'),
                        default=sys.stdin)
    parser.add_argument('-k', '--column',
                        help='Column number, with column count starting with 0',
                        type=int)
    parser.add_argument('-o', '--output',
                        help='Output file name (default standard output)',
                        type=argparse.FileType('wb'),
                        default=sys.stdout)
    parser.add_argument('-u', '--unique',
                        help='Output unique rows only',
                        action='store_true')
    parser.add_argument('--header',
                        help='Data file contains header row, which must be outputted and ignored',
                        action='store_true')
    args = parser.parse_args()
    data_sort(args)


if __name__ == '__main__':
    main()
