#!/usr/bin/env python
'''
Description     : Tool to create plots from the command line
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.10.08
'''

import argparse
import csv
import matplotlib as mpl; mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import sys

def subcommand_histogram(args):
    '''
    Generate histogram
    '''
    values = []
    with args.file as fin:

        # Skip header row
        if args.header:
            fin.next()

        reader = csv.reader(fin, delimiter='\t')
        for row in reader:
            value = row[args.col]
            try:
                value = float(value)
            except ValueError:
                sys.stderr.write('Data column must contain numeric values. Value found: %s\nExiting\n\n' % value)
                sys.exit(1)
            
            # If frequency column exists, must add value that many times
            if args.freq_col:
                freq = row[args.freq_col]
                try:
                    freq = int(freq)
                except ValueError:
                    sys.stderr.write('Frequency column must contain integer values. Value found: %s\nExiting\n\n' % freq)
                    sys.exit(1)
                for k in range(freq):
                    values.append(value)
            else:
                # No frequency column, just add the value
                values.append(value)

    # Generate histogram
    plt.figure()
    plt.hist(values, args.bins, facecolor='green', alpha=0.75)
    plt.title(args.title)
    plt.xlabel(args.xlabel)
    plt.ylabel(args.ylabel)
    plt.grid(True)
    plt.savefig(args.out + '.png', format='png')
    plt.close()

def main():
    parser = argparse.ArgumentParser(description="Tool to create plots from the command line")
    
    subparsers = parser.add_subparsers(title='subcommands',
                                       description='Available tools',
                                       dest='subcommand')
    # Subcommand: histogram
    parser_histogram = subparsers.add_parser('histogram',
                                             help='Generate histogram from data')
    parser_histogram.add_argument('file',
                                  help='Input data file',
                                  nargs='?',
                                  type=argparse.FileType('rU'),
                                  default=sys.stdin)
    parser_histogram.add_argument('out',
                                  help='Output file prefix',
                                  type=str)
    parser_histogram.add_argument('-b', '--bins',
                                  help='Number of bins',
                                  type=int,
                                  default=50)
    parser_histogram.add_argument('-k', '--col',
                                  help='Data column to generate histogram from (0-based)',
                                  type=int,
                                  default=0)
    parser_histogram.add_argument('-f', '--freq-col',
                                  help='Frequency column, if already counted (0-based)',
                                  type=int)
    parser_histogram.add_argument('--header',
                                  help='Set flag to indicate data contains header row',
                                  action='store_true')
    parser_histogram.add_argument('--title',
                                  help='Plot title',
                                  type=str)
    parser_histogram.add_argument('--xlabel',
                                  help='Label for the x-axis',
                                  type=str)
    parser_histogram.add_argument('--ylabel',
                                  help='Label for the y-axis',
                                  type=str)
    parser_histogram.set_defaults(func=subcommand_histogram)

    # Parse the arguments and call the corresponding function
    args = parser.parse_args()
    args.func(args)
                        

if __name__ == '__main__':
    main()
