#!/usr/bin/env python
'''
Description     : Read in from standard stream, and output to file.  I don't know why I made this tool.
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.10.08
'''

import argparse
import contextlib
import sys

def main():
    # Set up argument options
    parser = argparse.ArgumentParser(description="Read in from standard stream, and output to file.  I don't know why I made this tool.")
    parser.add_argument('input', 
                        help='Input file',
                        nargs='?',
                        type=argparse.FileType('rU'),
                        default=sys.stdin)
    parser.add_argument('-o','--out',
                        help='Output file',
                        type=argparse.FileType('wb'),
                        default=sys.stdout)
    args = parser.parse_args()

    with contextlib.nested(args.input, args.out) as (fin, fout):
        map(fout.write, fin)

if __name__ == '__main__':
    main()
