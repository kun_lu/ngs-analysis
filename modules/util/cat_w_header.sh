#!/bin/bash
## 
## DESCRIPTION:   Concatenate multiple files with headers
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2013.12.19
## LAST MODIFIED: 
## MODIFIED BY:
##
## USAGE:         cat_w_header.sh outfile in1 in2 [in3 [...]]
##
## OUTPUT:        outfile
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Usage check
usage_min 3 $# $0

# Process input params
OUT=$1; shift
IN1=$1; shift
INS=$@

# Format output
TMP=$OUT.tmp

# Merge maf files
cp $IN1 $OUT
for i in $INS; do 
  cat $OUT <(sed 1d $i) > $TMP
  mv $TMP $OUT
done
