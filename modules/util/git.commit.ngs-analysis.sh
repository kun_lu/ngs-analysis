#!/bin/bash
##
## DESCRIPTION:   Git commit ngs-analysis changes
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         git.commit.ngs-analysis.sh
##                                           version(i.e. v1.0.1)
##                                           "Commit string"
##
## OUTPUT:        
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Usage check:
usage 2 $# $0

# Process input args
VERSION=$1; shift
MSTRING=$1; shift

GIT=`which git`

# Remove analysis version info in current directory, since probably in one of the ngs-analysis subdirs
# and I don't need version info when committing anyway
rm -f $NGS_VERSION_FILE

# Go into ngs-analysis source dir
cd $NGS_ANALYSIS_DIR
cd docs
# Generate new doc
generate_doc.sh tools_doc
$GIT add tools_doc
# Generate new version number
$GIT tag -a $VERSION -m 'Version $VERSION'
$GIT describe --tags > $NGS_VERSION_FILE
$GIT add $NGS_VERSION_FILE
cd ..

# Commit
$GIT commit -m "$MSTRING"

