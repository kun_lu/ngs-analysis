#!/usr/bin/env python
'''
Description     : Tool to handle web related stuff
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.10.08
'''

import argparse
import base64
import mechanize
import os
import sys
import threading
import urllib2
from functools import partial
from Queue import Queue

# urllib2 request headers
req_headers = {'Authorization': 'Basic ' + base64.encodestring('[username]:[password]')}

def downloadlink(l, br):
    '''
    Download data from link l using browser br
    Link l is returned from br.links()
    '''
    # If output file exists already, output warning to standard error
    outfilename = l.text.split('/')[-1]
    if os.path.exists(outfilename):
        sys.stderr.write('Warning: Overwriting existing file %s\n' % outfilename)

    # Download data from url
    # http://stackoverflow.com/questions/7042867/sensing-password-protected-sites-when-using-urllib-in-python
    try:
        req = urllib2.Request(l.url, "", req_headers)
        data = urllib2.urlopen(req).read()
    except urllib2.HTTPError:
        sys.stderr.write('Warning: Skipping password-protected file %s\n' % outfilename)
        return None

    # Output data to file
    with open(outfilename, 'wb') as fout:
        fout.write(data)

def subcommand_getlinks(args):
    '''
    Download files hyperlinked in a web page
    
    Downloading links in a html file based on:
    http://stackoverflow.com/questions/5974595/download-all-the-linksrelated-documents-on-a-webpage-using-python
    http://stockrt.github.io/p/emulating-a-browser-in-python-with-mechanize/

    Multithreading based on:
    http://www.artfulcode.net/articles/multi-threading-python/
    '''

    def producer(q, links, br):
        '''
        q is a Queue, links is from br.links(), and br is a machanize browser object
        '''
        # Download individual files
        for l in links:
            thread = threading.Thread(target=partial(downloadlink, l,br))
            thread.start()
            q.put(thread, True)

    def consumer(q, total_num_links, finished):
        '''
        q is a Queue, num_links_total is the total number of links to download, and finished is a list of finished threads
        '''
        while len(finished) < total_num_links:
            thread = q.get(True)
            thread.join()
            finished.append(thread)
    
    # Create output directory and go into it
    if not os.path.exists(args.outdir):
        os.makedirs(args.outdir)
    os.chdir(args.outdir)

    # Initialize browser
    br = mechanize.Browser()
    br.open(args.url)

    # Output the source webpage for debugging purposes
    with open('source.html', 'wb') as fout:
        fout.write(br.response().read())

    # Filter links
    links = []
    exts_include = set() if not args.include else set(args.include)
    exts_exclude = set() if not args.exclude else set(args.exclude)
    for l in br.links():
        # If link is a jump to another section of the same html file, skip
        if l.url.find('#') >= 0:
            continue

        # Filter file extension
        file_ext = l.url.split('.')[-1]
        if exts_exclude and file_ext in exts_exclude:
            continue
        if exts_include and file_ext not in exts_include:
            continue
        links.append(l)

    # Use producer-consumer multithreading design pattern to download the files
    q = Queue(args.threads)
    finished = []
    producer_thread = threading.Thread(target=producer, args=(q, links, br))
    consumer_thread = threading.Thread(target=consumer, args=(q, len(links), finished))
    producer_thread.start()
    consumer_thread.start()
    producer_thread.join()
    consumer_thread.join()
        
def main():
    parser = argparse.ArgumentParser(description="Tool to handle web related stuff")
    subparsers = parser.add_subparsers(title='subcommands',
                                       description='Available tools',
                                       dest='subcommand')
    
    # Subcommand: Fetch all linked files in a web page
    parser_getlinks = subparsers.add_parser('get_links',
                                            help='Download files hyperlinked in a web page')
    parser_getlinks.add_argument('url',
                                 help='Web page url containing hyperlinks to files',
                                 type=str)
    parser_getlinks.add_argument('outdir',
                                 help='Name of directory where downloaded files will be',
                                 type=str)
    parser_getlinks.add_argument('-t','--threads',
                                 help='Number of threads',
                                 type=int,
                                 default=1)
    parser_getlinks.add_argument('--include',
                                 help='Only download files with these extensions (do not include .), i.e. txt, png',
                                 nargs='*',
                                 type=str)
    parser_getlinks.add_argument('--exclude',
                                 help='File extensions to exclude when downloading (do not include .), i.e. htm',
                                 nargs='*',
                                 type=str)
    parser_getlinks.set_defaults(func=subcommand_getlinks)
    
    # Parse the arguments and call the corresponding function
    args = parser.parse_args()
    args.func(args)


if __name__ == '__main__':
    main()
