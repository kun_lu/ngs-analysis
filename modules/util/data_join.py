#!/usr/bin/env python
'''
Description     : Read 2 files, and join them.  The files should be tab-delimited.
                  The order of the output rows will be based on file1 (left file).
                  The output file will be in the following format:
                  (file1row,file2row) separated by tabs
                  For optimal performance, the input files should be unique with respect to their rows.
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.10.01
'''

import argparse
import contextlib
import csv
import sys
from collections import defaultdict

def data_join(args):
    '''
    Join the two files based on the arguments in args
    '''
    reader1 = csv.reader(args.in1, delimiter='\t')
    reader2 = csv.reader(args.in2, delimiter='\t')
    writer  = csv.writer(args.outfile, delimiter='\t', lineterminator='\n')

    # Read file2 into memory ============================================================#
    file2_key2rows = defaultdict(set)
    # Header line for file 2
    if args.header2:
        file2_header = reader2.next()
    # Read rest of file 2
    file2_numcols = None
    for row in reader2:
        # Extract key
        key = tuple(sorted([row[k2] for k2 in args.f2_key_col]))

        # Store row values
        file2_key2rows[key].add(tuple(row))
        
        # Store the number of columns in file 2
        if file2_numcols is None:
            file2_numcols = len(row)

    # Read file 1, and output results as matching keys are found=========================#
    # Header line for file 1
    if args.header1:
        file1_header = reader1.next()

    # If both files have headers, output the combined header
    if args.header1 and args.header2:
        writer.writerow(file1_header + file2_header)

    # Read rest of file 1
    file2_unmatched_cols = ["" for k in range(file2_numcols)]
    for row in reader1:
        # Extract key
        key = tuple(sorted([row[k1] for k1 in args.f1_key_col]))

        # Output matching row
        file2_rows = file2_key2rows[key]

        # If file 2 contains at least 1 matching row, output all the matches
        if len(file2_rows) > 0:
            for file2_row in file2_rows:
                writer.writerow(row + list(file2_row))
        else: # File 2 has no matches
            # Only output file 1 rows if it's a left join
            if args.type == 'left':
                writer.writerow(row + file2_unmatched_cols)
        
def main():
    # Set up argument options
    parser = argparse.ArgumentParser(description="Read 2 files, and join them.  The files should be tab-delimited.  The order of the output rows will be based on file1 (left file).  The output file will be in the following format: (file1row,file2row) separated by tabs. For optimal performance, the input files should be unique with respect to their rows.")
    parser.add_argument('in1', 
                        help='Input file 1',
                        nargs='?',
                        type=argparse.FileType('r'),
                        default=sys.stdin)
    parser.add_argument('in2', 
                        help='Input file 2',
                        nargs='?',
                        type=argparse.FileType('r'),
                        default=sys.stdin)
    # Optional arguments
    parser.add_argument('-t', '--type',
                        help='Type of join to perform, (default inner)',
                        type=str,
                        choices=['inner','left'],
                        default='inner')
    parser.add_argument('-a', '--f1-key-col',
                        help='Key column(s) of file 1, 0-based.  Should match the number of columns provided in --f2-key-col',
                        nargs='*',
                        type=int,
                        default=[0])
    parser.add_argument('-b', '--f2-key-col',
                        help='Key column(s) of file 2, 0-based.  Should match the number of columns provided in --f1-key-col',
                        nargs='*',
                        type=int,
                        default=[0])
    parser.add_argument('--header1',
                        help='Header line exists for input 1',
                        action='store_true')
    parser.add_argument('--header2',
                        help='Header line exists for input 2',
                        action='store_true')
    parser.add_argument('-o', '--outfile',
                        help='Output file',
                        nargs='?',
                        type=argparse.FileType('w'),
                        default=sys.stdout)
    args = parser.parse_args()

    # Join the files
    with contextlib.nested(args.in1, args.in2, args.outfile):
        data_join(args)
        

if __name__ == '__main__':
    main()
