#!/usr/bin/env python
'''
Description     : Read in UCSC\'s dbsnp flat file and generate a dictionary mapping chromosome, position(1-based), ref allele, alt allele tuple to rsid.  Chromosome contains only the numbers, not the \'chr\' prefix.  All tuple elements are strings.
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.10.02
'''

import argparse
import cPickle
import sys
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC

def convert_fwd(strand, alleles):
    '''
    Given a list of alleles and strand,
    if the strand is -, then convert the alleles to their reverse complements
    Otherwise, just return the alleles as is
    '''
    if strand == '-':
        alleles = [str(Seq(a, IUPAC.unambiguous_dna).reverse_complement()) for a in alleles]
    return alleles

def main():
    ap = argparse.ArgumentParser(description="Read in UCSC\'s dbsnp flat file and generate a dictionary mapping chromosome, position(1-based), ref allele, alt allele tuple to rsid.  Chromosome contains only the numbers, not the \'chr\' prefix.  All tuple elements are strings.")
    ap.add_argument('infile',
                    help='dbsnp text flat file, i.e. snp135.txt',
                    nargs='?',
                    type=argparse.FileType('r'),
                    default=sys.stdin)
    ap.add_argument('outfile',
                    help='Output .pkl file',
                    type=argparse.FileType('wb'))
    params = ap.parse_args()

    # Build dictionary
    d = {}
    with params.infile as fin:        
        for line in fin:
            # parse each line
            cols = line.strip('\n').split('\t')
            chrom = cols[1].replace('chr','')
            pos = str(int(cols[2]) + 1)
            rsid = cols[4]
            strand = cols[6]
            ref = cols[7]
            observed = convert_fwd(strand, cols[9].split('/'))
            if ref in observed:
                observed.remove(ref)
            for alt in observed:
                d[(chrom, pos, ref, alt)] = rsid

    # Pickle the dict
    with params.outfile as fo:
        cPickle.dump(d, fo, cPickle.HIGHEST_PROTOCOL)
    

if __name__ == '__main__':
    main()
