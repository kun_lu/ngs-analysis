#!/bin/bash
## 
## DESCRIPTION:   Create a bed file from an fai file (Create a bed file for a given fasta file that has been indexed)
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         fai2bed.sh seq.fa.fai
##
## OUTPUT:        seq.fa.bed
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Usage check
usage 1 $# $0

# Process input args
FAI=$1

# Format output prefix
OUT=`filter_ext $FAI 1`

# Run the command described by the parameter
cut -f1,2 $FAI | $PYTHON -c "from ngs import noawk; noawk.ex(lambda row: noawk.writer.writerow([row[0], 1,row[1]]))" > $OUT.bed
