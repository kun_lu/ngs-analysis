#!/bin/bash
## 
## DESCRIPTION:   Wrap a command with bash, useful for wrapping binary tool commands
##                in order to submit to grid engine using qsub
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         R_wrapper.sh script.r [parameters [...]]
##
## OUTPUT:        
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Usage check:
usage_min 1 $# $0

# Run the command described by the parameter
TOOL=$1; shift
TOOL_DIR=$(dirname `which $TOOL`)
TOOL_NAME=$(basename $TOOL)
$R_SCRIPT $TOOL_DIR/$TOOL_NAME "$@"
