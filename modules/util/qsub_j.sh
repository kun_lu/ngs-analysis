#!/bin/bash
## 
## DESCRIPTION:   Wrap a command for a single slot job to submit to qsub with a specified name
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2013.10.17
## LAST MODIFIED: 2013.10.17
##
## USAGE:         qsub_j.sh
##                          jobid
##                          command [param1 [param2 [...]]]
##
## OUTPUT:        None
##

# Set shell env
source $HOME/.bashrc
if [ -s $HOME/.bash_profile ]; then
  source $HOME/.bash_profile
fi

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Usage check
usage_min 2 $# $0

# Process input params
JOBID=$1; shift

# Submit job to qsub
$NGS_ANALYSIS_DIR/modules/util/qsub_wrapper.sh $JOBID   \
                                               $Q_HIGH  \
                                               1        \
                                               none     \
                                               n        \
                                               "$@"
