#!/bin/bash
## 
## DESCRIPTION:   Wrap a command for quick submission to qsub
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         qsub_quick.sh command [param1 [param2 [...]]]
##
## OUTPUT:        None
##

# Set shell env
source $HOME/.bashrc
if [ -s $HOME/.bash_profile ]; then
  source $HOME/.bash_profile
fi

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Usage check
usage_min 1 $# $0

# Submit job to qsub
$NGS_ANALYSIS_DIR/modules/util/qsub_wrapper.sh quicksub \
                                               $Q_HIGH  \
                                               1        \
                                               none     \
                                               n        \
                                               "$@"
