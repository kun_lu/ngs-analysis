#!/bin/bash
## 
## DESCRIPTION:   Create a symlink from a source file or directory to destination file or directory using
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##                the source file's absolute path
##
##                NOTE: Works with only one file at a time
## 
## USAGE:         symlink.abspath.sh
##                                   source
##                                   destination
##
## OUTPUT:        symbolic link from destination to source
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 2 $# $0

# Process parameters
SF=$1; shift
DF=$1; shift

# Make sure that the source file exists
assert_file_exists_w_content $SF

# Create link
ln -s `readlink -f $SF` $DF
