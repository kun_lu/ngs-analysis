#!/bin/bash
## 
## DESCRIPTION:   Generate md5sum for each input file
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         md5sum.sh in1 [in2 [...]]
##
## OUTPUT:        in1.md5
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Usage check:
usage_min 1 $# $0

# Run the command described by the parameter
for file in $@; do
  md5sum $file > $file.md5
done