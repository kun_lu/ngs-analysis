#!/usr/bin/env python
'''
Description     : Merge rows of a given data file, grouping based on all the columns except the one provided by user, which are aggregated.
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.10.01
'''

import argparse
import contextlib
import csv
import sys
from collections import defaultdict

def comma_join(items):
    '''
    Comma-join the items as a single string and return it
    '''
    return ','.join(sorted(items))

def merge_rows(args):
    '''
    Merge rows and aggregate the column provided by the user
    '''
    aggfunc = comma_join
    
    reader = csv.reader(args.infile, delimiter='\t')
    writer = csv.writer(args.outfile, delimiter='\t', lineterminator='\n')

    # Header row
    if args.header_row:
        writer.writerow(reader.next())
    
    keycols2agg = defaultdict(set)
    for row in reader:
        aggval = row[args.aggregate_col]
        keycols = row[:]
        del keycols[args.aggregate_col]
        keycols2agg[tuple(keycols)].add(aggval)

    # Output results
    for keycols in sorted(keycols2agg.keys()):
        agg = keycols2agg[keycols]
        outcols = list(keycols)
        outcols.insert(args.aggregate_col, aggfunc(agg))
        writer.writerow(outcols)

def main():
    # Set up parameter options
    parser = argparse.ArgumentParser(description="Merge rows of a given data file, grouping based on all the columns except the one provided by user, which are aggregated.")
    parser.add_argument('infile',
                        help='Input file',
                        nargs='?',
                        type=argparse.FileType('r'),
                        default=sys.stdin)
    parser.add_argument('-k', '--aggregate-col',
                        help='Column to be aggregrated, zero-based',
                        type=int,
                        default=0)
    parser.add_argument('-d', '--header-row',
                        help='File includes header row, which needs to be ignored and outputted',
                        action='store_true')
    parser.add_argument('-o', '--outfile',
                        help='Output file',
                        type=argparse.FileType('w'),
                        default=sys.stdout)
    args = parser.parse_args()

    # Merge
    with contextlib.nested(args.infile, args.outfile):
        merge_rows(args)
    

if __name__ == '__main__':
    main()
