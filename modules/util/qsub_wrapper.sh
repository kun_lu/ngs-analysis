#!/bin/bash
## 
## DESCRIPTION:   Wrap a command with qsub submitting parameters
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         qsub_wrapper.sh 
##                                job_id
##                                queue_id
##                                num_parallel
##                                dependent_job_id
##                                sync(wait to finish, y|n)
##                                command [param1 [param2 [...]]]
##
## OUTPUT:        None
##

# Set shell env
source $HOME/.bashrc
if [ -s $HOME/.bash_profile ]; then
  source $HOME/.bash_profile
fi

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Usage check
usage_min 6 $# $0

# Process input parameters 
JOBID=$1; shift
QUEUE=$1; shift
NUMPP=$1; shift
WAIT4=$1; shift
SYNC=$1;  shift

# If job id starts with a number, put J_ in front
JOB_PREFIX='J_'
if is_numeric ${JOBID:0:1} ; then
  JOBID=$JOB_PREFIX$JOBID
fi
if is_numeric ${WAIT4:0:1} ; then
  WAIT4=$JOB_PREFIX$WAIT4
fi

# Make temporary files folder
create_dir tmp
TMP=tmp/qsub_wrapper.$JOBID.$$
mkdir $TMP

# Record qsub parameters
echo JOBID $JOBID >> $TMP/qsub.params
echo QUEUE $QUEUE >> $TMP/qsub.params
echo NUMPP $NUMPP >> $TMP/qsub.params
echo WAIT4 $WAIT4 >> $TMP/qsub.params
echo SYNC  $SYNC  >> $TMP/qsub.params
echo COMMD "$@"   >> $TMP/qsub.params

# Get absolute path of tool
TOOL=$1; shift
TOOL_DIR=$(dirname `which $TOOL`)
TOOL_NAME=$(basename $TOOL)

# Submit command
qsub                                             \
  -cwd                                           \
  -hold_jid $WAIT4                               \
  -N $JOBID                                      \
  -V                                             \
  -S /bin/bash                                   \
  -j y                                           \
  -o $TMP                                        \
  -e $TMP                                        \
  -pe orte $NUMPP                                \
  -q $QUEUE                                      \
  -sync $SYNC                                    \
  $TOOL_DIR/$TOOL_NAME "$@"

sleep $(( $RANDOM % 3 + 1 ))

#
# #$ -cwd
# #$ -N jobid
# #$ -v PATH
# #$ -S /bin/bash
# #$ -j y
# #$ -o .
# #$ -e .
# #$ -pe orte 2
# #$ -l h_vmem=8G
# #$ -q all.q
#
