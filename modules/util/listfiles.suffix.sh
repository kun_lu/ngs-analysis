#!/bin/bash
## 
## DESCRIPTION:   List all files within a directory that ends with the given suffix
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         listfiles.suffix.sh dirname suffix outfile
##
## OUTPUT:        outfile
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Usage check
usage_min 2 $# $0

DIRNAME=$1
SUFFIX=$2
OUTFILE=$3

find $DIRNAME -name "*$SUFFIX" > $OUTFILE
