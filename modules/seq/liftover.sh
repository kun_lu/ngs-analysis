#!/bin/bash
##
## DESCRIPTION:   Run UCSC liftOver tool
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         liftover.sh
##                           input.bed
##                           map.chain     (i.e. hg18ToHg19.over.chain)
##
## OUTPUT:        
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 2 $# $0

# Process input params
INPUTBED=$1
MAPCHAIN=$2

# Format output filenames
OUTBED=$INPUTBED.liftover.bed

# Run tool
$LIFTOVER           \
  $INPUTBED         \
  $MAPCHAIN         \
  $OUTBED           \
  $OUTBED.unmapped  \
  2> $OUTBED.err