#!/bin/bash
##
## DESCRIPTION:   Run FastQC
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
## 
## USAGE:         fastqc.sh input.fastq.gz [num_threads]
##
## OUTPUT:        FastQC output folder containing fastq qc info
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 1 $# $0

# Process input params
INPUT_FASTQ=$1
NUM_THREADS=$2
NUM_THREADS=${NUM_THREADS:=2}

# Format output directory name
OUTPUT_DIR=`echo $INPUT_FASTQ | sed 's/\.fastq.gz/_fastqc/'`

# If output exists, don't run
assert_file_not_exists_w_content $OUTPUT_DIR

# Run tool
$FASTQC                      \
  -t $NUM_THREADS            \
  $INPUT_FASTQ               \
  &> $INPUT_FASTQ.fastqc.log
