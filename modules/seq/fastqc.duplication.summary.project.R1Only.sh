#!/bin/bash
##
## DESCRIPTION:   Parse fastqc results to extract duplicate percentage for samples within a project directory
##                This tool assumes that the Illumina basecall results directory structure is maintained, and that fastqc was run on the fastq.gz files within each Sample_X directory
##                with default output naming conventions
##                Read 1 only
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
## 
## USAGE:         fastqc.duplication.summary.project.R1Only.sh
##                                                      Project_Dir               # Illumina basecall result "Project_X" containing the sample directories, which contain the fastq.gz results
##                                                      [Project_Dir2 [...]]
##
## OUTPUT:        duprates.R1.Project_Dir
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 1 $# $0

# Process input params
PROJECT_DIRS=$@

# Run tool
for PROJECT_DIR in $PROJECT_DIRS; do
  PROJECT_DIR=`echo $PROJECT_DIR | sed 's/\/$//'`
  `which grep` "Total Duplicate Percentage" $PROJECT_DIR/Sample_*/*_L???_R?_???_fastqc/fastqc_data.txt > duprates.$PROJECT_DIR
  `which paste` <(cut -f1,2 -d '/' duprates.$PROJECT_DIR) <(cut -f2 duprates.$PROJECT_DIR) | sed 's/\//\t/' > duprates.$PROJECT_DIR.2
  `which mv` -f duprates.$PROJECT_DIR.2 duprates.$PROJECT_DIR
done
