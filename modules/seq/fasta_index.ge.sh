#!/bin/bash
##
## DESCRIPTION:   Create index and dictionary for a fasta file
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
## 
## USAGE:         fasta_index.ge.sh
##                                  input.fa
##                                  (L|S)   # Large or small genome
##
## OUTPUT:        index files
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 2 $# $0

# Process input params
FASTA=$1; shift
GSIZE=$1;

# Make sure input fasta file exists
assert_file_exists_w_content $FASTA

BASH_WRAPPER=$NGS_ANALYSIS_DIR/modules/util/bash_wrapper.sh

# Create bwa index
if [ $GSIZE == L ]; then
  BWA_IDX_ALG=bwtsw
else
  BWA_IDX_ALG=is
fi
qsub_jn.sh bwa.index 1 $BASH_WRAPPER $BWA index -a $BWA_IDX_ALG $FASTA

# Create faidx
qsub_jn.sh samtools.faidx 1 $BASH_WRAPPER $SAMTOOLS faidx $FASTA

# Create dictionary
qsub_jn.sh picard.dict 1 $BASH_WRAPPER `javajar 2g` $PICARD_PATH/CreateSequenceDictionary.jar R=$FASTA O=`filter_ext $FASTA 1`.dict
