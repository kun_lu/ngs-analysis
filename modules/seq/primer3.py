#!/usr/bin/env python
'''
Description     : Tool to work with primer3
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.10.01
'''

import argparse
import contextlib
import csv
import sys
from ngs import vcf
from ngs.fasta import RandomAccessRecords

def subcommand_input(args):
    '''
    Generate primer3 input
    '''
    # Load variant positions to memory
    with args.vcf_file:
        # Load all variants to memory
        vcffile = vcf.VcfFile(args.vcf_file)
        variant_chrom2pos, variant_coord2alleles = vcffile.load_pos2memory_dict()

    # Set up fasta sequence loader
    seq_getter = RandomAccessRecords(args.fasta_file)

    # Generate primer3 input
    for chrom in sorted(variant_chrom2pos.keys()):

        # Get list of positions
        positions = variant_chrom2pos[chrom]

        # Get chromosome sequence
        if args.nomask:
            seq = seq_getter.get(chrom).seq
        else:
            ref_allele_lens = [len(variant_coord2alleles[(chrom, pos)][0]) for pos in positions]
            seq = seq_getter.get_n_masked_seq(chrom, tuple(positions), tuple(ref_allele_lens))

        # Write
        input_field2val = {}
        for pos in positions:
            ref_allele_length = len(variant_coord2alleles[(chrom, pos)][0])
            args.out_file.write('SEQUENCE_ID=%s\n' % chrom)
            args.out_file.write('SEQUENCE_TEMPLATE=%s\n' % seq)
            args.out_file.write('SEQUENCE_TARGET=%i,%i\n' % (pos, ref_allele_length))
            args.out_file.write('PRIMER_TASK=pick_sequencing_primers\n')
            args.out_file.write('PRIMER_PICK_LEFT_PRIMER=1\n')
            args.out_file.write('PRIMER_PICK_RIGHT_PRIMER=1\n')
            args.out_file.write('PRIMER_THERMODYNAMIC_PARAMETERS_PATH=%s\n' % args.configdir)
            args.out_file.write('PRIMER_EXPLAIN_FLAG=1\n')
            args.out_file.write('=\n')

def primer3_output_reader(fin):
    '''
    Generator for reading each record in a primer3 output file into a dictionary
    and yielding the dictionary
    '''
    primer_field2val = {}
    for line in fin:
        line = line.strip()
        if line == '=':
            yield primer_field2val
            primer_field2val = {}
        field, value = line.split('=')
        primer_field2val[field] = value

def subcommand_output(args):
    '''
    Parse primer3 output file and create a resulting table
    '''
    colnames = ['SEQUENCE_ID',
                'SEQUENCE_TARGET_POSITION',
                'SEQUENCE_TARGET_LENGTH',
                'PRIMER_LEFT_0',
                'PRIMER_RIGHT_0',
                'PRIMER_LEFT_0_SEQUENCE',
                'PRIMER_RIGHT_0_SEQUENCE',
                'PRIMER_LEFT_EXPLAIN',
                'PRIMER_RIGHT_EXPLAIN',
                'PRIMER_LEFT_0_PENALTY',
                'PRIMER_RIGHT_0_PENALTY',
                'PRIMER_LEFT_0_TM',
                'PRIMER_RIGHT_0_TM',
                'PRIMER_LEFT_0_GC_PERCENT',
                'PRIMER_RIGHT_0_GC_PERCENT',
                'PRIMER_LEFT_0_SELF_ANY_TH',
                'PRIMER_RIGHT_0_SELF_ANY_TH',
                'PRIMER_LEFT_0_SELF_END_TH',
                'PRIMER_RIGHT_0_SELF_END_TH',
                'PRIMER_LEFT_0_HAIRPIN_TH',
                'PRIMER_RIGHT_0_HAIRPIN_TH',
                'PRIMER_LEFT_0_END_STABILITY',
                'PRIMER_RIGHT_0_END_STABILITY']

    with contextlib.nested(args.primer3_file, args.out_file) as (fin, fout):
        writer = csv.writer(fout, delimiter='\t', lineterminator='\n')
        # Output header row
        writer.writerow(colnames)

        # Loop through each record
        for primer_field2val in primer3_output_reader(fin):
            outrow = []
            for cn in colnames:
                # Parse special columns
                if cn == 'SEQUENCE_TARGET_POSITION':
                    sequence_target = primer_field2val.get('SEQUENCE_TARGET', '')
                    if sequence_target:
                        sequence_target_pos_len = sequence_target.split(',')
                        outrow.append(sequence_target_pos_len[0])
                elif cn == 'SEQUENCE_TARGET_LENGTH':
                    if sequence_target:
                        outrow.append(sequence_target_pos_len[1])
                else:
                    # No special columns, just append values
                    value = primer_field2val.get(cn, '')
                    outrow.append(value)
            writer.writerow(outrow)

def main():
    parser = argparse.ArgumentParser(description="Tool to work with primer3")
    subparsers = parser.add_subparsers(title='subcommands',
                                       description='Available tools',
                                       dest='subcommand')
    # Subcommand: Create primer3 input
    parser_input = subparsers.add_parser('create_input',
                                          help='Create primer3 input file')
    parser_input.add_argument('vcf_file',
                              help='Vcf file from which to get the variants for generating the primers',
                              nargs='?',
                              type=argparse.FileType('r'),
                              default=sys.stdin)
    parser_input.add_argument('fasta_file',
                              help='Fasta file from which to generate the input from',
                              type=str)
    parser_input.add_argument('configdir',
                              help='Path to \'primer3_config\' directory. Better to use absolute path',
                              type=str)
    parser_input.add_argument('--nomask',
                              help='Don\'t mask variant positions in the input sequences with Ns (will do by default)',
                              action='store_true')
    parser_input.add_argument('-o', '--out_file',
                              help='Output file',
                              type=argparse.FileType('w'),
                              default=sys.stdout)
    parser_input.set_defaults(func=subcommand_input)

    # Subcommand: Parse primer3 output
    parser_output = subparsers.add_parser('parse_output',
                                          help='Parse primer3 output and generate a table')
    parser_output.add_argument('primer3_file',
                               help='Output file from primer3',
                               nargs='?',
                               type=argparse.FileType('r'),
                               default=sys.stdin)
    parser_output.add_argument('-o', '--out_file',
                               help='Output file',
                               type=argparse.FileType('w'),
                               default=sys.stdout)
    parser_output.set_defaults(func=subcommand_output)

    # Parse the arguments and call the corresponding function
    args = parser.parse_args()
    args.func(args)
                        

if __name__ == '__main__':
    main()
