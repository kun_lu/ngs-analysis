#!/bin/bash
##
## DESCRIPTION:   Generate PCR primers for variants using primer3
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         primer3.sh
##                           vcf_file
##                           ref.fa
##
## OUTPUT:        
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 2 $# $0

# Process input params
VCFIN=$1
REFER=$2

# Format output filenames
OUTPRE=$VCFIN

# Generate primer3 input
python_ngs.sh primer3.py create_input $VCFIN $REFER $PRIMER3_CONFIG_DIR > $OUTPRE.primer3.in

assert_normal_exit_status $? "primer3.py create_input exited with error"

# Run tool
$PRIMER3 < $OUTPRE.primer3.in             \
         -output=$OUTPRE.primer3.out      \
         -error=$OUTPRE.primer3.err

assert_normal_exit_status $? "primer3 tool exited with error"

# Parse output and generate table
python_ngs.sh primer3.py parse_output $OUTPRE.primer3.out > $OUTPRE.primer3.out.parsed

# Generate unique table
sed 1d $OUTPRE.primer3.out.parsed                 \
  | sort -u                                       \
  | cat <(head -n 1 $OUTPRE.primer3.out.parsed) - \
  > $OUTPRE.primer3.out.parsed.uniq

# Merge with vcf data
python_ngs.sh data_join.py                               \
              $OUTPRE.primer3.out.parsed.uniq            \
              <(python_ngs.sh vcf2tsv.py $VCFIN)         \
              -a 0 1                                     \
              -b 0 1                                     \
              --header1                                  \
              --header2                                  \
              -o $OUTPRE.primer3.out.parsed.uniq.vcfjoin
