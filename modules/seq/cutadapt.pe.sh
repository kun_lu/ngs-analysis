#!/bin/bash
##
## DESCRIPTION:   Cut adapter sequences from paired-end fastq files
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2013.11.08
## LAST MODIFIED: 2013.11.08
## MODIFIED BY:   Jin Kim jjinking(at)gmail(dot)com
## 
## USAGE:         cutadapt.pe.sh
##                               r1.fastq.gz
##                               r2.fastq.gz
##                               (mRNA|bisulfite)   # Type of adapters
##
## OUTPUT:        
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 3 $# $0

# Process input args
FASTQR1=$1; shift
FASTQR2=$1; shift
SEQTYPE=$1; shift

# By default, set adapter seqs to Truseq mRNA
ADAPTER_FWD=AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
ADAPTER_REV=AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT
ADAPTER_FWD_OPT="-a truseq_mRNA_fwd=$ADAPTER_FWD"
ADAPTER_REV_OPT="-a truseq_mRNA_rev=$ADAPTER_REV"
if [ $SEQTYPE = 'bisulfite' ]; then
  # http://www.bioinformatics.babraham.ac.uk/projects/trim_galore/
  ADAPTER_FWD=AGATCGGAAGAGC
  ADAPTER_REV=AGATCGGAAGAGC
  ADAPTER_FWD_OPT="-a bisulfite_fwd=$ADAPTER_FWD"
  ADAPTER_REV_OPT="-a bisulfite_rev=$ADAPTER_REV"
fi

# Format output filenames
OUTPRE1=`filter_ext $FASTQR1 2`.cutadapt
OUTPRE2=`filter_ext $FASTQR2 2`.cutadapt
OUTTMP1=$OUTPRE1.tmp.fastq.gz
OUTTMP2=$OUTPRE2.tmp.fastq.gz
OUTPUT1=$OUTPRE1.fastq.gz
OUTPUT2=$OUTPRE2.fastq.gz
OUTLOG1=$OUTPUT1.1.log
OUTLOG2=$OUTPUT1.2.log

# Make sure output file doesn't already exist
assert_file_not_exists_w_content $OUTPUT1

# Run tool
$CUTADAPT $ADAPTER_FWD_OPT           \
          --minimum-length 20        \
          -e 0.1                     \
          -O 5                       \
          -q 20                      \
          -z                         \
          --paired-output $OUTTMP2   \
          -o              $OUTTMP1   \
          $FASTQR1                   \
          $FASTQR2                   \
          &> $OUTLOG1

$CUTADAPT $ADAPTER_REV_OPT           \
          --minimum-length 20        \
          -e 0.1                     \
          -O 5                       \
          -q 20                      \
          -z                         \
          --paired-output $OUTPUT1   \
          -o              $OUTPUT2   \
          $OUTTMP2                   \
          $OUTTMP1                   \
          &> $OUTLOG2

rm -f $OUTTMP1 $OUTTMP2

# Options:
#   --version             show program's version number and exit
#   -h, --help            show this help message and exit
#   -f FORMAT, --format=FORMAT
#                         Input file format; can be either 'fasta', 'fastq' or
#                         'sra-fastq'. Ignored when reading csfasta/qual files
#                         (default: auto-detect from file name extension).

#   Options that influence how the adapters are found:
#     Each of the following three parameters (-a, -b, -g) can be used
#     multiple times and in any combination to search for an entire set of
#     adapters of possibly different types. All of the given adapters will
#     be searched for in each read, but only the best matching one will be
#     trimmed (but see the --times option).

#     -a ADAPTER, --adapter=ADAPTER
#                         Sequence of an adapter that was ligated to the 3' end.
#                         The adapter itself and anything that follows is
#                         trimmed.
#     -b ADAPTER, --anywhere=ADAPTER
#                         Sequence of an adapter that was ligated to the 5' or
#                         3' end. If the adapter is found within the read or
#                         overlapping the 3' end of the read, the behavior is
#                         the same as for the -a option. If the adapter overlaps
#                         the 5' end (beginning of the read), the initial
#                         portion of the read matching the adapter is trimmed,
#                         but anything that follows is kept.
#     -g ADAPTER, --front=ADAPTER
#                         Sequence of an adapter that was ligated to the 5' end.
#                         If the adapter sequence starts with the character '^',
#                         the adapter is 'anchored'. An anchored adapter must
#                         appear in its entirety at the 5' end of the read (it
#                         is a prefix of the read). A non-anchored adapter may
#                         appear partially at the 5' end, or it may occur within
#                         the read. If it is found within a read, the sequence
#                         preceding the adapter is also trimmed. In all cases,
#                         the adapter itself is trimmed.
#     -e ERROR_RATE, --error-rate=ERROR_RATE
#                         Maximum allowed error rate (no. of errors divided by
#                         the length of the matching region) (default: 0.1)
#     -n COUNT, --times=COUNT
#                         Try to remove adapters at most COUNT times. Useful
#                         when an adapter gets appended multiple times (default:
#                         1).
#     -O LENGTH, --overlap=LENGTH
#                         Minimum overlap length. If the overlap between the
#                         read and the adapter is shorter than LENGTH, the read
#                         is not modified.This reduces the no. of bases trimmed
#                         purely due to short random adapter matches (default:
#                         3).
#     --match-read-wildcards
#                         Allow 'N's in the read as matches to the adapter
#                         (default: False).
#     -N, --no-match-adapter-wildcards
#                         Do not treat 'N' in the adapter sequence as wildcards.
#                         This is needed when you want to search for literal 'N'
#                         characters.

#   Options for filtering of processed reads:
#     --discard-trimmed, --discard
#                         Discard reads that contain the adapter instead of
#                         trimming them. Also use -O in order to avoid throwing
#                         away too many randomly matching reads!
#     --discard-untrimmed, --trimmed-only
#                         Discard reads that do not contain the adapter.
#     -m LENGTH, --minimum-length=LENGTH
#                         Discard trimmed reads that are shorter than LENGTH.
#                         Reads that are too short even before adapter removal
#                         are also discarded. In colorspace, an initial primer
#                         is not counted (default: 0).
#     -M LENGTH, --maximum-length=LENGTH
#                         Discard trimmed reads that are longer than LENGTH.
#                         Reads that are too long even before adapter removal
#                         are also discarded. In colorspace, an initial primer
#                         is not counted (default: no limit).
#     --no-trim           Match and redirect reads to output/untrimmed-output as
#                         usual, but don't remove the adapters. (default: False.
#                         Remove the adapters)

#   Options that influence what gets output to where:
#     -o FILE, --output=FILE
#                         Write the modified sequences to this file instead of
#                         standard output and send the summary report to
#                         standard output. The format is FASTQ if qualities are
#                         available, FASTA otherwise. (default: standard output)
#     --info-file=FILE    Write information about each read and its adapter
#                         matches into FILE. Currently experimental: Expect the
#                         file format to change!
#     -r FILE, --rest-file=FILE
#                         When the adapter matches in the middle of a read,
#                         write the rest (after the adapter) into a file. Use -
#                         for standard output.
#     --wildcard-file=FILE
#                         When the adapter has wildcard bases ('N's) write
#                         adapter bases matching wildcard positions to FILE. Use
#                         - for standard output. When there are indels in the
#                         alignment, this may occasionally not be quite
#                         accurate.
#     --too-short-output=FILE
#                         Write reads that are too short (according to length
#                         specified by -m) to FILE. (default: discard reads)
#     --too-long-output=FILE
#                         Write reads that are too long (according to length
#                         specified by -M) to FILE. (default: discard reads)
#     --untrimmed-output=FILE
#                         Write reads that do not contain the adapter to FILE,
#                         instead of writing them to the regular output file.
#                         (default: output to same file as trimmed)
#     -p FILE, --paired-output=FILE
#                         Write reads from the paired end input to FILE.

#   Additional modifications to the reads:
#     -q CUTOFF, --quality-cutoff=CUTOFF
#                         Trim low-quality ends from reads before adapter
#                         removal. The algorithm is the same as the one used by
#                         BWA (Subtract CUTOFF from all qualities; compute
#                         partial sums from all indices to the end of the
#                         sequence; cut sequence at the index at which the sum
#                         is minimal) (default: 0)
#     --quality-base=QUALITY_BASE
#                         Assume that quality values are encoded as
#                         ascii(quality + QUALITY_BASE). The default (33) is
#                         usually correct, except for reads produced by some
#                         versions of the Illumina pipeline, where this should
#                         be set to 64. (default: 33)
#     -x PREFIX, --prefix=PREFIX
#                         Add this prefix to read names
#     -y SUFFIX, --suffix=SUFFIX
#                         Add this suffix to read names
#     --strip-suffix=STRIP_SUFFIX
#                         Remove this suffix from read names if present. Can be
#                         given multiple times.
#     -c, --colorspace    Colorspace mode: Also trim the color that is adjacent
#                         to the found adapter.
#     -d, --double-encode
#                         When in color space, double-encode colors (map
#                         0,1,2,3,4 to A,C,G,T,N).
#     -t, --trim-primer   When in color space, trim primer base and the first
#                         color (which is the transition to the first
#                         nucleotide)
#     --strip-f3          For color space: Strip the _F3 suffix of read names
#     --maq, --bwa        MAQ- and BWA-compatible color space output. This
#                         enables -c, -d, -t, --strip-f3, -y '/1' and -z.
#     --length-tag=TAG    Search for TAG followed by a decimal number in the
#                         name of the read (description/comment field of the
#                         FASTA or FASTQ file). Replace the decimal number with
#                         the correct length of the trimmed read. For example,
#                         use --length-tag 'length=' to correct fields like
#                         'length=123'.
#     -z, --zero-cap      Change negative quality values to zero (workaround to
#                         avoid segmentation faults in BWA)
