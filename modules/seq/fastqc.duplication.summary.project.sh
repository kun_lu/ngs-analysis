#!/bin/bash
##
## DESCRIPTION:   Parse fastqc results to extract duplicate percentage for samples within a project directory
##                This tool assumes that the Illumina basecall results directory structure is maintained, and that fastqc was run on the fastq.gz files within each Sample_X directory
##                with default output naming conventions
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
## 
## USAGE:         fastqc.duplication.summary.project.sh
##                                                      Project_Dir               # Illumina basecall result "Project_X" containing the sample directories, which contain the fastq.gz results
##                                                      [Project_Dir2 [...]]
##
## OUTPUT:        duprates.Project_Dir
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 1 $# $0

# Process input params
PROJECT_DIRS=$@

# Run tool
for PROJECT_DIR in $PROJECT_DIRS; do
  PROJECT_DIR=`echo $PROJECT_DIR | sed 's/\/$//'`
  `which grep` "Total Duplicate Percentage" $PROJECT_DIR/Sample_*/*_L???_R?_???_fastqc/fastqc_data.txt > duprates.$PROJECT_DIR
  `which paste` <(cut -f1,2 -d '/' duprates.$PROJECT_DIR) <(cut -f2 duprates.$PROJECT_DIR) | sed 's/\//\t/' > duprates.$PROJECT_DIR.2
  `which cat` duprates.$PROJECT_DIR.2 | $PYTHON -c "import sys,csv; from collections import defaultdict;  rows = csv.reader(sys.stdin,delimiter='\t'); d=defaultdict(list); [d[(row[0],row[1])].append(row[2]) for i,row in enumerate(rows)]; [sys.stdout.write('%s\t%s\t%s\t%s\n' % (p,s,d[(p,s)][0], d[(p,s)][1])) for p,s in sorted(d.keys())]" > duprates.$PROJECT_DIR
  `which rm` -f duprates.$PROJECT_DIR.2
done
