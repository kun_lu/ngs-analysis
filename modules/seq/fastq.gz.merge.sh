#!/bin/bash
##
## DESCRIPTION:   Merge fastq.gz files into a single fastq.gz file
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
## 
## USAGE:         fastq.gz.merge.sh
##                                  outfile.fastq.gz           # Output fastq.gz file name
##                                  in1.fastq.gz               # fastq.gz files to merge
##                                  in2.fastq.gz
##                                  [in3.fastq.gz [...]]
##
## OUTPUT:        outfile.fastq.gz
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 3 $# $0

# Process input params
OUT=$1; shift
INS=$@;

# If output exists, don't run
assert_file_not_exists_w_content $OUT

# Merge
zcat $INS | gzip 1> $OUT 2> $OUT.err
