#!/usr/bin/env python
'''
Description     : This tool parses demultiplex html file and generates a csv file
Author          : Kim jihye  kjhye1211(at)gmail(dot)com
Creation date   : 2013.09.30
Last Modified   : 2013.09.30
'''

import argparse
import csv
import os
import re
import sys
from bs4 import BeautifulSoup
from urllib2 import urlopen

COLNAMES = ["Lane","Sample ID", "Sample Ref", "Index", "Description",
	    "Control", "Project", "Yield (Mbases)", "% PF", "# Reads", 
	    "% of raw clusters per lane", "% Perfect Index Reads", 
	    "% One Mismatch Reads (Index)", "% of >= Q30 Bases (PF)",
	    "Mean Quality Score (PF)"]

COLNAMES_INTERNAL = ["Lane","Sample ID", "Sample Ref", "Index", "Description",
		     "Control", "Project", "Yield (Mbases)", "# Reads", 
		     "% of raw clusters per lane", "% Perfect Index Reads", 
		     "% One Mismatch Reads (Index)", "% of >= Q30 Bases (PF)",
		     "Mean Quality Score (PF)"]

COLNAMES_EXTERNAL = ["Lane","Sample ID", "Index","Yield (Mbases)", "# Reads", 
		     "% of >= Q30 Bases (PF)", "Mean Quality Score (PF)"]

def extract_cols(data, column_names):
	'''
	Extract desired columns listed in column_names from data
	Parameters:
	  data:         2-D list
	  column_names: desired column names
	'''
	indices = [COLNAMES.index(cn) for cn in column_names]
	result = []
	for row in data:
		new_row = [row[ix] for ix in indices]
		result.append(new_row)
	return result

def extract_cols_with_dup(data, column_names, fastqdir, r1only=False):
	'''
	Extract desired columns listed in column_names from data & add duplicate information
	Parameters:
	  data:         2-D list
	  column_names: desired column names
	'''
	indices = [COLNAMES.index(cn) for cn in column_names]
	result = []
	for row in data:
		if 'Sample ID' in row :
			if r1only:
				result.append(column_names + ["R1 Duprate"])
			else:
				result.append(column_names + ["R1 Duprate", "R2 Duprate"])
		else:
			new_row = [row[ix] for ix in indices]
			Lane =  row[COLNAMES.index('Lane')]
			Project = row[COLNAMES.index('Project')]
			SampleID = row[COLNAMES.index('Sample ID')]
			Index = row[COLNAMES.index('Index')]
			if Project == 'Undetermined_indices':
				pass
			else:
				file1 = os.path.join(fastqdir,
						     'Project_'+Project,
						     'Sample_'+SampleID,
						     SampleID+'_'+Index+'_L00'+Lane+'_R1_001_fastqc',
						     'fastqc_data.txt')
				with open(file1, 'r') as fa:
					for line1 in fa:
						if line1.find('Total Duplicate Percentag') != -1:
							a,b=line1.strip().split('\t')
							new_row.append(b)
							break

				if not r1only:
					file2 = os.path.join(fastqdir,
							     'Project_'+Project,
							     'Sample_'+SampleID,
							     SampleID+'_'+Index+'_L00'+Lane+'_R2_001_fastqc',
							     'fastqc_data.txt')
					with open(file2, 'r') as fb:
						for line1 in fb:
							if line1.find('Total Duplicate Percentag') != -1:
								a,b=line1.strip().split('\t')
								new_row.append(b)
								break
			result.append(new_row)
	return result

def html_csv(args):
	'''
	Parse Illumina demultiplex stats html file and generate summary table, output to a csv file	
	'''
	# Locate BasecallStats html file
	for i in [name for name in os.listdir(args.fastqdir) if os.path.isdir(os.path.join(args.fastqdir, name))]:
		match = re.match(r'Basecall_Stats_.*',i)
		if match:
			a=match.group(0)
	htmlfile = os.path.join(args.fastqdir, a, 'Demultiplex_Stats.htm')

	# Read in HTML file
	with open(htmlfile, 'rU') as f:
		soup = BeautifulSoup(f.read())

	# Parse the html string
	table = soup.find("div", attrs={ "id" : "ScrollableTableBodyDiv"}).find("table")
	head = soup.find("div", attrs={ "id" : "ScrollableTableHeaderDiv"}).find("table")
	headers = [header.text for header in head.find_all('th')]
	rows = [headers]

	# Parse all columns and store the data as a 2D list
	for row in table.find_all('tr'):
		rows.append([val.text.encode('utf8') for val in row.find_all('td')])

	# Output results
	w = csv.writer(args.outfile, delimiter=',', lineterminator='\n')
	if args.type == "internal":
		result = extract_cols_with_dup(rows, COLNAMES_INTERNAL, args.fastqdir, r1only=args.r1)
	elif args.type == 'external':
		result = extract_cols(rows, COLNAMES_EXTERNAL)
	else: # all columns
		result = extract_cols_with_dup(rows, COLNAMES, args.fastqdir, r1only=args.r1)
	w.writerows(result)

def main():
	# Set up parameter options
	parser = argparse.ArgumentParser(description="Parse Illumina demultiplex stats html file and generate summary table, output to a csv file")
	parser.add_argument('fastqdir',
			    help='Directory where basecalling output fastq files are stored',
			    type=str)
	parser.add_argument('-t', '--type',
			    help="Type of output desired, default all",
			    type=str,
			    choices=['all','internal','external'],
			    default='all')
	parser.add_argument('-r', '--r1-only',
			    dest='r1',
			    action='store_true')
	parser.add_argument('-o', '--outfile',
			    help='Output file',
			    type=argparse.FileType('wb'),
			    default=sys.stdout)
	args = parser.parse_args()
	html_csv(args)


if __name__ == '__main__':
	main()
