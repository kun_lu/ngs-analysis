#!/usr/bin/env python
'''
Description     : Manipulate pairs of fastq files
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.12.14
Modified By     : Jin Kim jjinking(at)gmail(dot)com
'''

import argparse
import contextlib
import csv
import matplotlib as mpl; mpl.use('Agg')
import matplotlib.pyplot as plt
import os
import sys
from collections import defaultdict
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from ngs import fasta

def subcommand_count(args):
    '''
    Count number of records in file
    '''
    # Determine the type of comparison to make
    compare_fun = {'l': lambda seqlen: seqlen < args.length,
                   'g': lambda seqlen: seqlen > args.length,
                   'e': lambda seqlen: seqlen == args.length,
                   'le': lambda seqlen: seqlen <= args.length,
                   'ge': lambda seqlen: seqlen >= args.length}[args.type]

    with args.fastafile as fin:
        seqcount = 0
        basecount = 0
        for rec in SeqIO.parse(fin, "fasta"):
            seq_len = len(rec.seq)
            if compare_fun(seq_len):
                seqcount += 1
                basecount += seq_len
    sys.stdout.write('Num Seqs\t%i\nNum Bases\t%i\n' % (seqcount, basecount))

def subcommand_split(args):
    '''
    Read through fasta file and output records to different files
    '''
    def write2file(sequences, outfilename):
        with open(outfilename, 'w') as fo:
            SeqIO.write(sequences, fo, 'fasta')

    with args.fastafile as fin:
        outfile_idx = 1
        rec_q = []
        for rec in SeqIO.parse(fin, "fasta"):
            rec_q.append(rec)
            
            # If record queue reached the threshold, output to a new file
            if len(rec_q) >= args.num_seqs:
                write2file(rec_q,'%s%i.fa' % (args.out_prefix, outfile_idx))
                rec_q = []
                outfile_idx += 1
        # Take care of remaining sequences
        if len(rec_q) > 0:
            write2file(rec_q, '%s%i.fa' % (args.out_prefix, outfile_idx))

def subcommand_hist(args):
    '''
    Generate histgram
    '''
    # Count up the length distribution
    lencounts = defaultdict(int)
    lengths = []
    with args.fastafile as fin:
        for rec in SeqIO.parse(fin, "fasta"):
            seq_len = len(rec.seq)
            lencounts[seq_len] += 1
            lengths.append(seq_len)
            
    # Output counts to text
    with args.outfile as fout:
        writer = csv.writer(fout, delimiter='\t', lineterminator='\n')
        writer.writerow(['length','counts'])
        for l_c in sorted(lencounts.iteritems()):
            writer.writerow(l_c)

    # Generate histogram plot
    fig = plt.figure()
    n, bins, patches = plt.hist(lengths, args.bins, facecolor='green', alpha=0.75)
    plt.xlabel('Sequence Length Bins')
    plt.ylabel('Counts')
    plt.suptitle('Sequence Length Histogram')
    # Save image
    plt.savefig(args.image_prefix + '.png', format='png')
    plt.close()

def subcommand_length(args):
    '''
    Compute and output the length of each sequence
    '''
    with contextlib.nested(args.fastafile,args.outfile) as (fin,fout):
        writer = csv.writer(fout, delimiter='\t', lineterminator='\n')
        for rec in SeqIO.parse(fin, 'fasta'):
            if args.short_id:
                seq_name = rec.id
            else:
                seq_name = rec.description
            seq_len = len(rec.seq)
            writer.writerow([seq_name, seq_len])

def subcommand_pickle(args):
    '''
    Generate pickle of a fasta dictionary and output as pickle
    '''
    # Create dictionary of the fasta records
    with args.fastafile as fin:
        ff = fasta.FastaFile(fin)
        recordid2sequence = ff.load2dict(shortid=args.short_id)

    # Output pickle of the dictionary to file
    import cPickle
    with open(args.outprefix + '.pkl', 'wb') as fout:
        cPickle.dump(recordid2sequence, fout, cPickle.HIGHEST_PROTOCOL)

def subcommand_combine(args):
    '''
    Combine multiple fasta files into a single file, basically concatenating them
    '''
    with contextlib.nested(*(args.fastafile + [args.outfile,])) as fs:
        fins = fs[:-1]
        fout = fs[-1]
        for i,fin in enumerate(fins):
            SeqIO.write(SeqIO.parse(fin, "fasta"), fout, "fasta")

def subcommand_prefixid(args):
    '''
    Append a prefix to each sequence ID in a fasta file
    '''
    with contextlib.nested(args.fastafile, args.outfile) as (fin, fout):
        sequences = (SeqRecord(rec.seq,
                               id=(args.prefix + rec.id),
                               name=rec.name,
                               description=rec.description) for rec in SeqIO.parse(fin, "fasta"))
        SeqIO.write(sequences, fout, "fasta")
    

def main():
    parser = argparse.ArgumentParser(description="Manipulate pairs of fastq files")
    
    subparsers = parser.add_subparsers(title='subcommands',
                                       description='Available tools',
                                       dest='subcommand')
    # Subcommand: Count sequences
    parser_count = subparsers.add_parser('count',
                                          help='Count number of sequences')    
    parser_count.add_argument('fastafile',
                               help='Input fasta file',
                               nargs='?',
                               type=argparse.FileType('r'),
                               default=sys.stdin)
    parser_count.add_argument('-l', '--length',
                               help='Length of sequence.  Default 1',
                               type=int,
                               default=1)
    parser_count.add_argument('-t', '--type',
                              help='Type of thresholding. I.e. ge will count sequences with length greater than or equal to the provided length value.  l=less_than, g=greater_than, e=equal_to, le=less_than_or_equal_to, ge=greather_than_or_equal_to. Default=ge',
                              type=str,
                              choices=['l','g','e','le','ge'],
                              default='ge')
    parser_count.set_defaults(func=subcommand_count)

    # Subcommand: Split sequences
    parser_split = subparsers.add_parser('split',
                                         help='Split up sequences into multiple files')
    parser_split.add_argument('fastafile',
                              help='Input fasta file',
                              nargs='?',
                              type=argparse.FileType('r'),
                              default=sys.stdin)
    parser_split.add_argument('-o','--out-prefix',
                              help='Output prefix of split files',
                              type=str,
                              default='fasta_split')
    parser_split.add_argument('-n','--num-seqs',
                              help='Number of sequence records to output per file',
                              type=int,
                              default=1)
    parser_split.set_defaults(func=subcommand_split)

    # Subcommand: Histogram
    parser_hist = subparsers.add_parser('histogram',
                                        help='Generate histogram about the sequences')
    parser_hist.add_argument('fastafile',
                             help='Input fasta file',
                             nargs='?',
                             type=argparse.FileType('r'),
                             default=sys.stdin)
    parser_hist.add_argument('-o','--outfile',
                             help='Output file',
                             type=argparse.FileType('w'),
                             default=sys.stdout)
    parser_hist.add_argument('-b','--bins',
                             help='Number of bins for the histogram',
                             type=int,
                             default=100)
    parser_hist.add_argument('-g','--image-prefix',
                             help='Prefix for the name of output histogram image',
                             type=str,
                             default='fasta_histogram')
    parser_hist.set_defaults(func=subcommand_hist)

    # Subcommand: Sequence lengths
    parser_seqlen = subparsers.add_parser('length',
                                          help='Output the lengths of each sequence')
    parser_seqlen.add_argument('fastafile',
                               help='Input fasta file',
                               nargs='?',
                               type=argparse.FileType('r'),
                               default=sys.stdin)
    parser_seqlen.add_argument('-o','--outfile',
                               help='Output file',
                               type=argparse.FileType('w'),
                               default=sys.stdout)
    parser_seqlen.add_argument('-s', '--short-id',
                               help='Output short sequence ids instead of the full description for each record in the fasta file',
                               action='store_true')
    parser_seqlen.set_defaults(func=subcommand_length)

    # Subcommand: Create fasta pickle
    parser_pickle = subparsers.add_parser('pickle',
                                          help='Generate pickle of fasta record dictionary mapping record ids to sequences')
    parser_pickle.add_argument('fastafile',
                               help='Input fasta file',
                               nargs='?',
                               type=argparse.FileType('r'),
                               default=sys.stdin)
    parser_pickle.add_argument('-s', '--short-id',
                               help='Use short sequence IDs as keys for the dictionary',
                               action='store_true')
    parser_pickle.add_argument('-o', '--outprefix',
                               help='Output file prefix',
                               type=str,
                               default='fasta')
    parser_pickle.set_defaults(func=subcommand_pickle)

    # Subcommand: Combine multiple fasta files
    parser_combine = subparsers.add_parser('combine',
                                           help='Combine multiple fasta files into a single file, basically concatenating them')
    parser_combine.add_argument('fastafile',
                                help='Input fasta files',
                                nargs='+',
                                type=argparse.FileType('r'),
                                default=sys.stdin)
    parser_combine.add_argument('-o', '--outfile',
                                help='Output filename',
                                type=argparse.FileType('w'),
                                default=sys.stdout)
    parser_combine.set_defaults(func=subcommand_combine)

    # Subcommand: Prefix sequence IDs
    parser_prefixid = subparsers.add_parser('prefix_id',
                                            help='Prefix each sequence ID in a fasta file, i.e. in order to distinguish it from multiple organisms with the same chromosome IDs when combining reference genomes')
    parser_prefixid.add_argument('fastafile',
                                 help='Input fasta fie',
                                 nargs='?',
                                 type=argparse.FileType('r'),
                                 default=sys.stdin)
    parser_prefixid.add_argument('prefix',
                                 help='Prefix string to add to the front of each sequence ID in the fasta file',
                                 type=str)
    parser_prefixid.add_argument('-o', '--outfile',
                                  help='Output filename',
                                  type=argparse.FileType('w'),
                                  default=sys.stdout)
    parser_prefixid.set_defaults(func=subcommand_prefixid)
    
    # Parse the arguments and call the corresponding function
    args = parser.parse_args()
    args.func(args)
                        

if __name__ == '__main__':
    main()
