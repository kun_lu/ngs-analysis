#!/bin/bash
##
## DESCRIPTION:   Run MuTect
##                http://www.broadinstitute.org/cancer/cga/mutect
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         mutect.sh
##                          bam_normal
##                          bam_tumor
##                          outprefix
##                          (hg|b3x)
##                          [target_interval]
##
## OUTPUT:        outprefix.mutect.calls
##                outprefix.mutect.coverage
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 4 $# $0

# Process input params
NORMAL=$1
TUMOR=$2
OUTPRE=$3
RESOURCE_T=$4
TARGET=$5

# Check if input files exist
assert_file_exists_w_content $NORMAL
assert_file_exists_w_content $TUMOR

# Format output filenames
OUTPRE=$OUTPRE.mutect
OUTFILE=$OUTPRE.calls

# If output exists, don't run
assert_file_not_exists_w_content $OUTFILE

# Set resource variables
if [ $RESOURCE_T = 'hg' ]; then
  REF_FA=$HG_REF_GATK2
  DBSNP_LEFT=$HG_DBSNP_LEFT_ALIGNED
  COSMIC_VCF=$HG_COSMIC_VCF
else
  REF_FA=$B3x_REF_GATK2
  DBSNP_LEFT=$B3x_DBSNP_LEFT_ALIGNED
  COSMIC_VCF=$B3x_COSMIC_VCF
fi

# Set target variable if provided
if [ ! -z $TARGET ]; then
  assert_file_exists_w_content $TARGET
  TARGET_OPT="--intervals $TARGET"
fi

# Run tool
`javajar6 8g` $MUTECT                  \
  --analysis_type MuTect               \
  --reference_sequence $REF_FA         \
  --cosmic $COSMIC_VCF                 \
  --dbsnp  $DBSNP_LEFT                 \
    $TARGET_OPT                        \
  --input_file:normal $NORMAL          \
  --input_file:tumor  $TUMOR           \
  --out $OUTFILE                       \
  --vcf $OUTFILE.vcf                   \
  --coverage_file $OUTPRE.coverage     \
  &> $OUTPRE.log


# Arguments for MuTect:
#  --noop                                                                                   used for debugging, basically 
#                                                                                           exit as soon as we get the 
#                                                                                           reads
#  --tumor_sample_name <tumor_sample_name>                                                  name to use for tumor in 
#                                                                                           output files
#  --bam_tumor_sample_name <bam_tumor_sample_name>                                          if the tumor bam contains 
#                                                                                           multiple samples, only use 
#                                                                                           read groups with SM equal to 
#                                                                                           this value
#  --normal_sample_name <normal_sample_name>                                                name to use for normal in 
#                                                                                           output files
#  --force_output                                                                           force output for each site
#  --force_alleles                                                                          force output for all alleles 
#                                                                                           at each site
#  --only_passing_calls                                                                     only emit passing calls
#  --initial_tumor_lod <initial_tumor_lod>                                                  Initial LOD threshold for 
#                                                                                           calling tumor variant
#  --tumor_lod <tumor_lod>                                                                  LOD threshold for calling 
#                                                                                           tumor variant
#  --fraction_contamination <fraction_contamination>                                        estimate of fraction (0-1) of 
#                                                                                           physical contamination with 
#                                                                                           other unrelated samples
#  --minimum_mutation_cell_fraction <minimum_mutation_cell_fraction>                        minimum fraction of cells 
#                                                                                           which are presumed to have a 
#                                                                                           mutation, used to handle 
#                                                                                           non-clonality and 
#                                                                                           contamination
#  --normal_lod <normal_lod>                                                                LOD threshold for calling 
#                                                                                           normal non-germline
#  --dbsnp_normal_lod <dbsnp_normal_lod>                                                    LOD threshold for calling 
#                                                                                           normal non-variant at dbsnp 
#                                                                                           sites
#  --somatic_classification_normal_power_threshold                                          Power threshold for normal to 
# <somatic_classification_normal_power_threshold>                                           determine germline vs variant
#  --minimum_normal_allele_fraction <minimum_normal_allele_fraction>                        minimum allele fraction to be 
#                                                                                           considered in normal, useful 
#                                                                                           for normal sample contaminated 
#                                                                                           with tumor
#  --tumor_f_pretest <tumor_f_pretest>                                                      for computational efficiency, 
#                                                                                           reject sites with allelic 
#                                                                                           fraction below this threshold
#  --min_qscore <min_qscore>                                                                threshold for minimum base 
#                                                                                           quality score
#  --gap_events_threshold <gap_events_threshold>                                            how many gapped events 
#                                                                                           (ins/del) are allowed in 
#                                                                                           proximity to this candidate
#  --heavily_clipped_read_fraction <heavily_clipped_read_fraction>                          if this fraction or more of 
#                                                                                           the bases in a read are 
#                                                                                           soft/hard clipped, do not use 
#                                                                                           this read for mutation calling
#  --clipping_bias_pvalue_threshold <clipping_bias_pvalue_threshold>                        pvalue threshold for fishers 
#                                                                                           exact test of clipping bias in 
#                                                                                           mutant reads vs ref reads
#  --fraction_mapq0_threshold <fraction_mapq0_threshold>                                    threshold for determining if 
#                                                                                           there is relatedness between 
#                                                                                           the alt and ref allele read 
#                                                                                           piles
#  --pir_median_threshold <pir_median_threshold>                                            threshold for clustered read 
#                                                                                           position artifact median
#  --pir_mad_threshold <pir_mad_threshold>                                                  threshold for clustered read 
#                                                                                           position artifact MAD
#  --required_maximum_alt_allele_mapping_quality_score                                      required minimum value for 
# <required_maximum_alt_allele_mapping_quality_score>                                       tumor alt allele maximum 
#                                                                                           mapping quality score
#  --max_alt_alleles_in_normal_count <max_alt_alleles_in_normal_count>                      threshold for maximum 
#                                                                                           alternate allele counts in 
#                                                                                           normal
#  --max_alt_alleles_in_normal_qscore_sum <max_alt_alleles_in_normal_qscore_sum>            threshold for maximum 
#                                                                                           alternate allele quality score 
#                                                                                           sum in normal
#  --max_alt_allele_in_normal_fraction <max_alt_allele_in_normal_fraction>                  threshold for maximum 
#                                                                                           alternate allele fraction in 
#                                                                                           normal
#  --power_constant_qscore <power_constant_qscore>                                          Phred scale quality score 
#                                                                                           constant to use in power 
#                                                                                           calculations
#  --absolute_copy_number_data <absolute_copy_number_data>                                  Absolute Copy Number Data, as 
#                                                                                           defined by Absolute, to use in 
#                                                                                           power calculations
#  --power_constant_af <power_constant_af>                                                  Allelic fraction constant to 
#                                                                                           use in power calculations
#  -o,--out <out>                                                                           Call-stats output
#  -vcf,--vcf <vcf>                                                                         VCF output of mutation 
#                                                                                           candidates
#  -dbsnp,--dbsnp <dbsnp>                                                                   VCF file of DBSNP information
#  -cosmic,--cosmic <cosmic>                                                                VCF file of COSMIC sites
#  -cov,--coverage_file <coverage_file>                                                     write out coverage in WIGGLE 
#                                                                                           format to this file
#  -cov_q20,--coverage_20_q20_file <coverage_20_q20_file>                                   write out 20x of Q20 coverage 
#                                                                                           in WIGGLE format to this file
#  -pow,--power_file <power_file>                                                           write out power in WIGGLE 
#                                                                                           format to this file
#  -tdf,--tumor_depth_file <tumor_depth_file>                                               write out tumor read depth in 
#                                                                                           WIGGLE format to this file
#  -ndf,--normal_depth_file <normal_depth_file>                                             write out normal read depth in 
#                                                                                           WIGGLE format to this file
