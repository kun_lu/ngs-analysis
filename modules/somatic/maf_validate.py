#!/usr/bin/env python
'''
Description     : Read in an maf file and validate it
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2013.12.14
Last Modified   : 2013.12.14
Modified By     : Jin Kim jjinking(at)gmail(dot)com
'''

import argparse
import contextlib
import csv
import sys
from ngs import fasta

MAF_HEADERS = ['Hugo_Symbol',
               'Entrez_Gene_Id',
               'Center',
               'NCBI_Build',
               'Chromosome',
               'Start_position',
               'End_position',
               'Strand',
               'Variant_Classification',
               'Variant_Type',
               'Reference_Allele',
               'Tumor_Seq_Allele1',
               'Tumor_Seq_Allele2',
               'dbSNP_RS',
               'dbSNP_Val_Status',
               'Tumor_Sample_Barcode',
               'Matched_Norm_Sample_Barcode',
               'Match_Norm_Seq_Allele1',
               'Match_Norm_Seq_Allele2',
               'Tumor_Validation_Allele1',
               'Tumor_Validation_Allele2',
               'Match_Norm_Validation_Allele1',
               'Match_Norm_Validation_Allele2',
               'Verification_Status',
               'Validation_Status',
               'Mutation_Status',
               'Sequencing_Phase',
               'Sequence_Source',
               'Validation_Method',
               'Score',
               'BAM_File',
               'Sequencer',
               'transcript_name',
               'amino_acid_change']

def validate(args):
    '''
    Validate and output fixed maf file
    '''
    # Load reference fasta file
    record_dict = fasta.FastaFile(args.ref).load2dict()
    
    # Create input and output file handles
    reader = csv.DictReader(args.maf, delimiter='\t', fieldnames=MAF_HEADERS)
    writer = csv.DictWriter(args.out, delimiter='\t', lineterminator='\n', fieldnames=MAF_HEADERS)
    
    # Output header
    writer.writerow(reader.next())

    # Read in each row and validate it
    for row in reader:
        chrom = row['Chromosome']
        start = int(row['Start_position'])
        end = int(row['End_position'])
        ref_allele = row['Reference_Allele']
        ref_allele_real = record_dict[chrom][start-1:end]
        # Check reference allele column versus genomic reference allele
        if ref_allele != ref_allele_real:
            # Check capitalization issues
            if ref_allele.lower() == ref_allele_real.lower():
                row['Reference_Allele'] = ref_allele_real
                for ac in ('Tumor_Seq_Allele1',
                           'Tumor_Seq_Allele2',
                           'Match_Norm_Seq_Allele1',
                           'Match_Norm_Seq_Allele2'):
                    if row[ac].lower() == ref_allele_real.lower():
                        row[ac] = ref_allele_real
            else:
                raise ValueError, "Reference allele does not match refenence genome build\n"
        writer.writerow(row)

def main():
    parser = argparse.ArgumentParser(description='Validate an maf file')
    parser.add_argument('maf',
                        help='Input maf file',
                        nargs='?',
                        type=argparse.FileType('rU'),
                        default=sys.stdin)
    parser.add_argument('ref',
                        help='Reference genome fasta file',
                        type=str)
    parser.add_argument('-o','--out',
                        help='Output fixed maf file',
                        type=argparse.FileType('wb'),
                        default=sys.stdout)
    args = parser.parse_args()

    with contextlib.nested(args.maf, args.out):
        validate(args)


if __name__ == '__main__':
    main()
