#!/bin/bash
##
## DESCRIPTION:   Run Mutsig
##                http://www.broadinstitute.org/cancer/cga/mutsig_run
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2013.12.13
## LAST MODIFIED: 2014.01.09
## MODIFIED BY:   Jin Kim jjinking(at)gmail(dot)com
##
## USAGE:         mutsig.sh
##                          input.maf
##
## OUTPUT:        outprefix.mutect.calls
##                outprefix.mutect.coverage
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 1 $# $0

# Process input params
MAF=$1

# Set output prefix
OUT=`filter_ext $MAF 1`.mutsig

# Check if input file exists
assert_file_exists_w_content $MAF

# Run tool
EFC=$MUTSIG_REF_DIR/exome_full192.coverage.txt
GCT=$MUTSIG_REF_DIR/gene.covariates.txt
MDF=$MUTSIG_REF_DIR/mutation_type_dictionary_file.txt
CFD=$MUTSIG_REF_DIR/chr_files_hg19
$MUTSIG $MCR $MAF $EFC $GCT $OUT $MDF $CFD &> $OUT.log
