#!/bin/bash
##
## DESCRIPTION:   Run VarScan processSomatic on varscan outputs
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         varscan.procsomatic.sh
##                                       infile              # snp or indel output from varscan
##                                       min-tumor-freq      # Minimum variant allele frequency in tumor
##                                       max-normal-freq     # Maximum variant allele frequency in normal
##                                       p-value             # P-value for high-confidence calling
##
## OUTPUT:        processSomatic output files
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 4 $# $0

# Process input params
INPUT=$1
MIN_T=$2
MAX_N=$3
P_VAL=$4

# Check to make sure that input files exist
assert_file_exists_w_content $INPUT

# Set up output filenames
O_PRE=$INPUT.procsomatic
O_LOG=$O_PRE.log

# Run tool
`javajar 16g` $VARSCAN          \
  processSomatic                \
  $INPUT                        \
  --min-tumor-freq  $MIN_T      \
  --max-normal-freq $MAX_N      \
  --p-value         $P_VAL      \
  &> $O_LOG


# USAGE: java -jar VarScan.jar process [status-file] OPTIONS
#         status-file - The VarScan output file for SNPs or Indels
#         OPTIONS
#         --min-tumor-freq - Minimum variant allele frequency in tumor [0.10]
#         --max-normal-freq - Maximum variant allele frequency in normal [0.05]
#         --p-value - P-value for high-confidence calling [0.07]
