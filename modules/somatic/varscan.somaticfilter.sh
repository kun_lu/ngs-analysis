#!/bin/bash
##
## DESCRIPTION:   Run VarScan somaticFilter on varscan outputs
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         varscan.somaticfilter.sh
##                                             sample.varscan.snp
##                                             sample.varscan.indel
##                                             p-value                 # Default p-value threshold for calling variants [5e-02]
##                                             min-coverage            # Minimum read depth at a position to make a call [10]
##                                             min-var-freq            # Minimum variant allele frequency threshold [0.20]
##                                             min-reads2              # Minimum supporting reads at a position to call variants [4]
##                                             min-strands2            # Minimum # of strands on which variant observed (1 or 2) [1]
##
## OUTPUT:        varscan_somatic_output_file.somaticfilter
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 7 $# $0

# Process input params
IN_SNP=$1; shift
IN_IND=$1; shift
S_PVAL=$1; shift
MINCOV=$1; shift
MINVAR=$1; shift
MINRDS=$1; shift
MINSTR=$1; shift

# Check to make sure that input files exist
assert_file_exists_w_content $IN_SNP
assert_file_exists_w_content $IN_IND

# Set up output filenames
OUT_PRE=$IN_SNP
OUTFILE=$OUT_PRE.somaticfilter
OUT_LOG=$OUTFILE.log

# Run tool
`javajar 8g` $VARSCAN                   \
  somaticFilter                         \
  $IN_SNP                               \
  --min-coverage  $MINCOV               \
  --min-reads2    $MINRDS               \
  --min-strands2  $MINSTR               \
  --min-var-freq  $MINVAR               \
  --p-value       $S_PVAL               \
  --indel-file    $IN_IND               \
  --output-file   $OUTFILE              \
  &> $OUT_LOG
