#!/usr/bin/env python
'''
Description     : From MAF formatted file, generate CHASM input.
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.10.01
'''

'''
MAF Format
0       Hugo_Symbol
1       Entrez_Gene_Id
2       Center
3       NCBI_Build
4       Chromosome
5       Start_position
6       End_position
7       Strand
8       Variant_Classification
9       Variant_Type
10      Reference_Allele
11      Tumor_Seq_Allele1
12      Tumor_Seq_Allele2
13      dbSNP_RS
14      dbSNP_Val_Status
15      Tumor_Sample_Barcode
16      Matched_Norm_Sample_Barcode
17      Match_Norm_Seq_Allele1
18      Match_Norm_Seq_Allele2
19      Tumor_Validation_Allele1
20      Tumor_Validation_Allele2
21      Match_Norm_Validation_Allele1
22      Match_Norm_Validation_Allele2
23      Verification_Status
24      Validation_Status
25      Mutation_Status
26      Sequencing_Phase
27      Sequence_Source
28      Validation_Method
29      Score
30      BAM_File
31      Sequencer
32      transcript_name
33      amino_acid_change
'''

import argparse
import sys

def get_alt_allele(ref, *all_alleles):
    '''
    Given ref allele, and other alleles, deduce the alternate allele and return it
    '''
    # Check if length of reference is not 1
    if len(ref) != 1:
        sys.stdout.write('Reference length is not 1\n%s\t%s\n' % (ref, '\t'.join(all_alleles)))
        sys.exit(1)

    alleles = set(all_alleles)
    # Exit with error if there are more than 2 alleles
    if len(alleles) > 2:
        sys.stdout.write('More than 2 alleles exist!\n%s\n\n' % '\t'.join(all_alleles))
        sys.exit(1)
    
    # Find the non-reference allele
    for a in alleles:
        if a != ref:
            # Check length of alternate allele
            if len(a) != 1:
                sys.stdout.write('Alternate length is not 1\n%s\t%s\n' % (ref, '\t'.join(all_alleles)))
                sys.exit(1)
            # No error, return the alternate allele
            return a

def generate_maf(maf_in):
    n = 1
    for line in maf_in:
        cols = line.strip('\n').split('\t')

        # Missense mutations only
        if cols[8] != 'Missense_Mutation':
            continue
        
        # Parse data
        chrom = cols[4]
        start = int(cols[5]) - 1
        end = start + 1
        strand = cols[7]
        ref = cols[10]
        tumor_allele1 = cols[11]
        tumor_allele2 = cols[12]
        norm_allele1 = cols[17]
        norm_allele2 = cols[18]
        alt = get_alt_allele(ref, tumor_allele1, tumor_allele2, norm_allele1, norm_allele2)
        sys.stdout.write('%i\t%s\t%i\t%i\t%s\t%s\t%s\n' % (n, chrom, start, end, strand, ref, alt))
        n += 1

def main():
    ap = argparse.ArgumentParser(description="From MAF formatted file, generate CHASM input.")
    ap.add_argument('maf_file',
                    help='MAF File',
                    nargs='?',
                    type=argparse.FileType('r'),
                    default=sys.stdin)
    params = ap.parse_args()

    with params.maf_file:
        generate_maf(params.maf_file)

if __name__ == '__main__':
    main()
