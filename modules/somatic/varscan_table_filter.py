#!/usr/bin/env python
'''
Description     : Filter the records in a varscan output file in table format.
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.10.01
'''

import argparse
import sys
from ngs import varscan

def filter_variants(args):
    '''
    Filter the variants
    '''
    vfile = varscan.VarScanFile(args.inputfile)
    # Print header
    args.outfile.write('%s\n' % '\t'.join(vfile.COLNAMES))
    # Print filtered variants
    for row in vfile.filtered_variants(somatic_status=args.somatic_status,
                                       min_somatic_pval=args.spv,
                                       min_dp_normal=args.normal_dp,
                                       min_dp_tumor=args.tumor_dp):
        args.outfile.write('%s\n' % '\t'.join(row))
        
def main():
    parser = argparse.ArgumentParser(description="Filter the records in a varscan output file in table format.")
    parser.add_argument('inputfile',
                        help='Input varscan output file, i.e. sample.snp, sample.indel',
                        nargs='?',
                        type=argparse.FileType('r'),
                        default=sys.stdin)
    parser.add_argument('-s', '--somatic-status',
                        help='Somatic status of variant',
                        choices=['Germline','Somatic','LOH', 'Unknown'])
    parser.add_argument('--spv',
                        help='Filter out variants with somatic p-values in range (spv, 1.0]',
                        type=float)
    parser.add_argument('--normal-dp',
                        help='Minimum normal depth',
                        type=int)
    parser.add_argument('--tumor-dp',
                        help='Minimum tumor depth',
                        type=int)
    parser.add_argument('-o','--outfile',
                        help='Output filename',
                        type=argparse.FileType('w'),
                        default=sys.stdout)
    args = parser.parse_args()

    # Filter variants
    with args.inputfile:
        filter_variants(args)


if __name__ == '__main__':
    main()
