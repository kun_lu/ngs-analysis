#!/bin/bash
##
## DESCRIPTION:   Run snpeff to annotate the variants in a vcf file
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.11.29
## MODIFIED BY:   Jin Kim jjinking(at)gmail(dot)com
##
## USAGE:         vcf_add_rsid.sh
##                                sample.vcf
##                                dbSNP.vcf
##                                out.vcf
##
## OUTPUT:        out.vcf
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 3 $# $0

# Process input parameters
VCF=$1; shift
DBS=$1; shift
OUT=$1; shift

# Make sure input files exist
assert_file_exists_w_content $VCF

# Run tool
`javajar 4g` $SNPSIFT                   \
  annotate                              \
  $DBS                                  \
  $VCF                                  \
  1> $OUT                               \
  2> $OUT.err
