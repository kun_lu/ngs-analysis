#!/bin/bash
##
## DESCRIPTION:   Use variant tools (vtools) to annotate variant positions with cosmic ids
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         vtools.cosmic.sh infile
##
## OUTPUT:        
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 1 $# $0

# Process input parameters
INPUT=$1

# Format outputs
O_PRE=$INPUT
OUT_F=$O_PRE.cosmic
O_LOG=$OUT_F.err

# Delete existing vtools projects
PROJECT=annot_cosmic
rm -rf $PROJECT'_genotype.DB' 
rm -rf $PROJECT.log
rm -rf $PROJECT.proj 

# Run tool
$VTOOLS init $PROJECT
$VTOOLS import --build hg19 $INPUT --format basic --sample_name $PROJECT'_sample'
$VTOOLS use CosmicCodingMuts
$VTOOLS output variant     chr pos ref alt COSMIC_ID    1> $OUT_F   2> $O_LOG

#
# # Output CosmicMutantExport
# $VTOOLS use CosmicMutantExport --linked_by CosmicCodingMuts.COSMIC_ID
# $VTOOLS output variant CosmicMutantExport.COSMIC_ID
#
