#!/bin/bash
##
## DESCRIPTION:   Run ANNOVAR
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         annovar.summarize.sh sample.annovar.in
##
## OUTPUT:        sample.annovar.summarize.*
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 1 $# $0

# Process input parameters
INPUT=$1

# Format outputs
OUTPRE=`filter_ext $INPUT 1`.summarize
OUTLOG=$OUTPRE.log

# Don't run if output file already exists
assert_file_not_exists_w_content $OUTPRE.genome_summary.csv

# Run tool
$ANNOVAR_PATH/summarize_annovar.pl   \
  --outfile $OUTPRE                  \
  --buildver hg19                    \
  --verdbsnp 137                     \
  --ver1000g 1000g2012apr            \
  --veresp 6500                      \
  --genetype refgene                 \
  --alltranscript                    \
  --remove                           \
  $INPUT                             \
  $ANNOVAR_HUMANDB                   \
  &> $OUTLOG



#
# Usage:
#      summarize_annovar.pl [arguments] <query-file> <database-location>
# 
#      Optional arguments:
#                     -h, --help                      print help message
#                     -m, --man                       print complete documentation
#                     -v, --verbose                   use verbose output
#                         --outfile <string>          output file name prefix
#                         --buildver <string>         genome build version (default: hg18)
#                         --remove                    remove all temporary files
#                         --verdbsnp <int>            dbSNP version to use (default: 130)
#                         --ver1000g <string>         1000G version (default: 1000g2010nov)
#                         --veresp <int>              ESP version (default: 5400)
#                         --genetype <string>         gene definition can be refgene (default), knowngene, ensgene
#                         --checkfile                 check existence of database file (default: ON)
#                         --(no)alltranscript         output all transcript names for exonic variants (default: OFF)
# 
#      Function: automatically run a pipeline on a list of variants and summarize 
#      their functional effects in a comma-delimited file, to be opened by Excel for 
#      manual filtering
#  
#      Example: summarize_annovar.pl ex2.human humandb/
#  
#      Version: $LastChangedDate: 2012-11-01 10:38:05 -0700 (Thu, 01 Nov 2012) $
# 
