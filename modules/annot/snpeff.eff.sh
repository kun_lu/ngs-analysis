#!/bin/bash
##
## DESCRIPTION:   Run snpeff to annotate the variants in a vcf file.  Also takes bed files.
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.12.18
## MODIFIED BY:   Jin Kim jjinking(at)gmail(dot)com
##
## USAGE:         snpeff.eff.sh
##                              sample.vcf
##                              ["other_snpeff_options"                #  SNPEff options, i.e. "-cancer -cancerSample samples_cancer.txt" in quotes
##                              [genome_version(default GRCh37.69)]]
##
## OUTPUT:        sample.snpeff.vcf
##                sample.snpeff.snpEff_summary.html
##                sample.snpeff.snpEff_summary.genes.txt
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 1 $# $0

# Process input parameters
VCFFILE=$1; shift
SNPEFF_OPTIONS=$1; shift
GENOME_VERSION=$1; shift

# Make sure input files exist
assert_file_exists_w_content $VCFFILE

# Set default genome version
GENOME_VERSION=${GENOME_VERSION:=$SNPEFF_DEFAULT_VERSION}

# If only genome version is provided, prefix GRCh
if [[ $GENOME_VERSION =~ '^[0-9]{2}\.[0-9]{2}$' ]]; then
  GENOME_VERSION='GRCh'$GENOME_VERSION
fi

# Format outputs
OUTPREFIX=`filter_ext $VCFFILE 1`
OUTVCF=$OUTPREFIX.snpeff.vcf
OUTSTATS=$OUTPREFIX.snpeff.snpEff_summary.html
OUTERR=$OUTVCF.err
#OUTDIR=$OUTPREFIX.snpeff

# Format options
filename=$(basename $VCFFILE)
extension="${filename##*.}"
if [ $extension = 'bed' ]; then
  OUTVCF=$OUTPREFIX.snpeff.bed
  OUTERR=$OUTVCF.err
  SNPEFF_OPTIONS=$SNPEFF_OPTIONS" -i bed -o bed "
else # vcf
  SNPEFF_OPTIONS=$SNPEFF_OPTIONS" -i vcf -o vcf -lof "
fi

# Run tool
`javajar 4g` $SNPEFF                    \
  eff                                   \
  $GENOME_VERSION                       \
  -c $SNPEFF_CONFIG                     \
  -s $OUTSTATS                          \
  -v                                    \
  -hgvs                                 \
  $SNPEFF_OPTIONS                       \
  $VCFFILE                              \
  1> $OUTVCF                            \
  2> $OUTERR

# GATK recommends this option, but it no longer exists
#  -onlyCoding true                      \

# snpEff version SnpEff 3.3h (build 2013-08-11), by Pablo Cingolani
# Usage: snpEff [eff] [options] genome_version [input_file]
# 
# 
# 
# variants_file           : Default is STDIN
# 
# 
# 
# Options:
# 	-a , -around            : Show N codons and amino acids around change (only in coding regions). Default is 0 codons.
# 	-i <format>             : Input format [ vcf, txt, pileup, bed ]. Default: VCF.
# 	-o <format>             : Ouput format [ txt, vcf, gatk, bed, bedAnn ]. Default: VCF.
# 	-interval               : Use a custom interval BED file (you may use this option many times)
# 	-chr <string>           : Prepend 'string' to chromosome name (e.g. 'chr1' instead of '1'). Only on TXT output.
# 	-s,  -stats             : Name of stats file (summary). Default is 'snpEff_summary.html'
# 	-t                      : Use multiple threads (implies '-noStats'). Default 'off'
# 	-noStats                : Do not create stats (summary) file
# 	-csvStats               : Create CSV summary file instead of HTML
# 
# Sequence change filter options:
# 	-del                    : Analyze deletions only
# 	-ins                    : Analyze insertions only
# 	-hom                    : Analyze homozygous variants only
# 	-het                    : Analyze heterozygous variants only
# 	-minQ X, -minQuality X  : Filter out variants with quality lower than X
# 	-maxQ X, -maxQuality X  : Filter out variants with quality higher than X
# 	-minC X, -minCoverage X : Filter out variants with coverage lower than X
# 	-maxC X, -maxCoverage X : Filter out variants with coverage higher than X
# 	-nmp                    : Only MNPs (multiple nucleotide polymorphisms)
# 	-snp                    : Only SNPs (single nucleotide polymorphisms)
# 
# Results filter options:
# 	-fi  <bedFile>                  : Only analyze changes that intersect with the intervals specified in this file (you may use this option many times)
# 	-no-downstream                  : Do not show DOWNSTREAM changes
# 	-no-intergenic                  : Do not show INTERGENIC changes
# 	-no-intron                      : Do not show INTRON changes
# 	-no-upstream                    : Do not show UPSTREAM changes
# 	-no-utr                         : Do not show 5_PRIME_UTR or 3_PRIME_UTR changes
# 
# Annotations options:
# 	-cancer                         : Perform 'cancer' comparissons (Somatic vs Germline). Default: false
# 	-canon                          : Only use canonical transcripts.
# 	-geneId                         : Use gene ID instead of gene name (VCF output). Default: false
# 	-hgvs                           : Use HGVS annotations for amino acid sub-field. Default: false
# 	-lof                            : Add loss of function (LOF) and Nonsense mediated decay (NMD) tags.
# 	-motif                          : Annotate using motifs (requires Motif database).
# 	-nextProt                       : Annotate using NextProt (requires NextProt database).
# 	-reg <name>                     : Regulation track to use (this option can be used add several times).
# 	-oicr                           : Add OICR tag in VCF file. Default: false
# 	-onlyReg                        : Only use regulation tracks.
# 	-onlyTr <file.txt>              : Only use the transcripts in this file. Format: One transcript ID per line.
# 	-sequenceOntolgy                : Use Sequence Ontolgy terms. Default: false
# 	-ss, -spliceSiteSize <int>      : Set size for splice sites (donor and acceptor) in bases. Default: 2
# 	-ud, -upDownStreamLen <int>     : Set upstream downstream interval length (in bases)
# 
# Generic options:
# 	-0                      : File positions are zero-based (same as '-inOffset 0 -outOffset 0')
# 	-1                      : File positions are one-based (same as '-inOffset 1 -outOffset 1')
# 	-c , -config            : Specify config file
# 	-h , -help              : Show this help and exit
# 	-if, -inOffset          : Offset input by a number of bases. E.g. '-inOffset 1' for one-based input files
# 	-of, -outOffset         : Offset output by a number of bases. E.g. '-outOffset 1' for one-based output files
# 	-noLog                  : Do not report usage statistics to server
# 	-q , -quiet             : Quiet mode (do not show any messages or errors)
# 	-v , -verbose           : Verbose mode
