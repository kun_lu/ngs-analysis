#!/bin/bash
##
## DESCRIPTION:   Run bismark alignment
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2014.02.17
## LAST MODIFIED: 
## MODIFIED BY:   
##
## USAGE:         biseq.bychrom.ge.sh
##                                   outprefix         # Output prefix for each biseq run's results
##                                   samples_group     # Tab-delimited file containing 3 column (sample, CPG.bedgraph.bismark.cov, 0/1(case:1 vs control:0))
##                                   cluster.min.sites # Minimum number of methylated sites per cluster
##                                   cluster.max.dist  # Maximum distance between DMRs in same cluster
##                                   threads           # Max number of threads per chromosome run
##
## OUTPUT:        
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 5 $# $0

# Process input params
OUT=$1; shift
GRO=$1; shift
MIN=$1; shift
MAX=$1; shift
THR=$1; shift

# Make sure that the groups file exists
assert_file_exists_w_content $GRO

# Divide up the contents of the groups file by chromosome
while read s c g; do
  python_ngs.sh bismark_cov_splitbychrom.py $c $c
done < $GRO

# Create groups files for each chromosome
chroms="chr1 chr10 chr11 chr12 chr13 chr14 chr15 chr16 chr17 chr18 chr19 chr2 chr20 chr21 chr22 chr3 chr4 chr5 chr6 chr7 chr8 chr9 chrM chrX chrY"
for chrom in $chroms; do
  create_dir biseq.$chrom
  cd biseq.$chrom
  # Attach chrom suffix to the coverage files in the grouping file
  $PYTHON -c "import sys,csv;
r = csv.reader(open('../$GRO', 'rU'), delimiter='\t');
w = csv.writer(open('../$GRO.$chrom.cov', 'wb'), delimiter='\t');
append_suffix=lambda row: [row[0], row[1]+'.$chrom.cov', row[2]];
[w.writerow(append_suffix(row)) for row in r];"
  qsub_wrapper.sh                    \
    $OUT.bismark.$chrom              \
    $Q_HIGH                          \
    $THR                             \
    none                             \
    n                                \
    bash_wrapper.sh biseq.bismark.bedgraph.cov.R $OUT.$chrom ../$GRO.$chrom.cov $MIN $MAX $THR
  cd ..
done
