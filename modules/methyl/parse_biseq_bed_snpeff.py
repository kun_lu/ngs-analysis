#!/usr/bin/env python
'''
Description     : Parse BiSeq bed file and the corresponding SNPEff annotated file
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation Date   : 2014.03.10
Last Modified   : 2014.03.11
Modified By     : Jin Kim jjinking(at)gmail(dot)com
'''

import argparse
import csv
import numpy as np
import pandas as pd
import sys
from ngs import vcfframe

def get_effect(row):
    valmap = {'UTR5PRIME': 'UTR_5_PRIME', 'UTR3PRIME':'UTR_3_PRIME', 'SPLICESITEDONOR':'SPLICE_SITE_DONOR', 'SPLICESITEACCEPTOR':'SPLICE_SITE_ACCEPTOR', 'RAREAMINOACID':'RARE_AMINO_ACID'}
    sv = lambda val: valmap[val] if val in valmap else val
    try:
        return sorted(row['info'].split(';')[1:], key=lambda eff: vcfframe.SNPEffVcfFrame.EFFECT2PRIORITY[sv(eff.split('|')[0].upper())])[0]
    except IndexError:
        return "N/A"

def get_up_down_position(row):
    if row['Effect.effect'] in set(['Upstream','Downstream']):
        return row['Effect'].split('|')[1]
    return np.nan

def extract_gene(row):
    extract_gene_intergenic = lambda row: ','.join(set(row['Effect'].split('|')[1].split('...'))) if row['Effect.effect'] == 'Intergenic' else None
    if row['Effect.effect'] in set(['Upstream','Downstream','Intron','Exon','Utr5prime','Utr3prime','SpliceSiteDonor']):
        return row['Effect'].split('|')[4]
    elif row['Effect.effect'] == 'Intergenic':
        return extract_gene_intergenic(row)
    else:
        return 'N/A'

def parse_bed(args):
    '''
    Parse the bed files
    '''
    # Read bed files
    with args.dmr_bed as dmr_bed:
        reader = csv.reader(dmr_bed, delimiter='\t')
        header = reader.next()
        header[0] = 'chrom'
        d = [row for row in reader]
    df_dmr = pd.DataFrame(d, columns=header)[['chrom', 'start', 'end', 'width', 'median.meth.group1', 'median.meth.group2', 'median.meth.diff', 'Promoter']]
    df_dmr['chrom'] = df_dmr['chrom'].apply(lambda row: row.replace('chr',''))

    with args.snpeff_bed as snpeff_bed:
        # Skip snpeff bed file headers
        for i in range(4):
            snpeff_bed.next()
        reader = csv.reader(snpeff_bed, delimiter='\t')
        d = [row for row in reader]

    # Create snpeff bed data frame
    df_snpeff = pd.DataFrame(d, columns=['chrom','start','end','info', 'score'])

    # Parse effect annotation string
    df_snpeff['Effect'] = df_snpeff.apply(get_effect, axis=1)

    # Extract effect
    df_snpeff['Effect.effect'] = df_snpeff['Effect'].apply(lambda s: s.split('|')[0])

    # Extract gene symbol
    df_snpeff['Effect.gene'] = df_snpeff.apply(extract_gene, axis=1)

    # Get upstream/downstream position
    df_snpeff['Effect.Upstream_Downstream_Pos'] = df_snpeff.apply(get_up_down_position, axis=1)

    df_snpeff = df_snpeff[['chrom','start','end','Effect.effect','Effect.gene','Effect.Upstream_Downstream_Pos']]

    results = pd.merge(df_dmr, df_snpeff, how='left', on=['chrom','start','end'])
    results.to_csv(args.output, sep='\t', index=False)

def main():
    # Set up parameter options
    parser = argparse.ArgumentParser(description="Parse BiSeq bed file and the corresponding SNPEff annotated file")
    parser.add_argument('dmr_bed',
                        help='Input BiSeq DMR bed file',
                        nargs='?',
                        type=argparse.FileType('rU'),
                        default=sys.stdin)
    parser.add_argument('snpeff_bed',
                        help='Input snpeff annotated bed file',
                        nargs='?',
                        type=argparse.FileType('rU'),
                        default=sys.stdin)
    parser.add_argument('-o', '--output',
                        help='Output file name (default standard output)',
                        type=argparse.FileType('wb'),
                        default=sys.stdout)
    args = parser.parse_args()
    parse_bed(args)


if __name__ == '__main__':
    main()
