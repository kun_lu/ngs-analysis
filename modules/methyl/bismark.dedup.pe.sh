#!/bin/bash
##
## DESCRIPTION:   Run deduplicate_bismark
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2013.10.16
## LAST MODIFIED: 2013.12.17
## MODIFIED BY:   Jin Kim jjinking(at)gmail(dot)com
##
## USAGE:         bismark.dedup.pe.sh
##                                    input.sam
##
## OUTPUT:        
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 1 $# $0

# Process input params
SAM=$1; shift

# If output file already exists and has content, then don't run
assert_file_not_exists_w_content `filter_ext $SAM 1`.deduplicated.bam

# Run tool
$BISMARKDIR/deduplicate_bismark        \
  --paired                             \
  --bam                                \
  --samtools_path `dirname $SAMTOOLS`  \
  $SAM                                 \
  &> `filter_ext $SAM 1`.dedup.log
