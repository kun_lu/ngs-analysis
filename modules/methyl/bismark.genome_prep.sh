#!/bin/bash
##
## DESCRIPTION:   Prep reference genome file for bismark
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2013.10.14
## LAST MODIFIED: 2013.10.16
##
## USAGE:         bismark.genome_prep.sh
##                                       genome_dir        # Directory containing a single .fa file
##
## OUTPUT:        
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 1 $# $0

# Process input params
REFDIR=`echo $1 | sed 's/\/$//'`; shift

# Run tool
$BISMARKDIR/bismark_genome_preparation \
  --path_to_bowtie `dirname $BOWTIE`   \
  --verbose                            \
  --bowtie2                            \
  $REFDIR                              \
  &> $REFDIR.bismark.prep.log
