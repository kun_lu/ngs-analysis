#!/bin/bash
##
## DESCRIPTION:   Run GATK SelectVariants tool
##                http://www.broadinstitute.org/gatk/gatkdocs/org_broadinstitute_sting_gatk_walkers_variantutils_SelectVariants.html
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.11.13
## MODIFIED BY:   Jin Kim jjinking(at)gmail(dot)com
##
## USAGE:         gatk2.selectvariants.sh
##                                        ref.fa            # Reference fasta file used in alignment
##                                        in.vcf            # Input vcf file
##                                        outfile           # Output file name
##                                        ["options"]       # Options for this tool in quotes i.e. "-sn sample_x"
##
## OUTPUT:        outfile
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 3 $# $0

# Process input params
REF=$1; shift
VCF=$1; shift
OUT=$1; shift
OPT="$@"

# If input files don't exist, don't run
assert_file_exists_w_content $REF
assert_file_exists_w_content $VCF

# If output file exists, don't run
assert_file_not_exists_w_content $OUT

# Remove previous log files
rm -f $OUT.log
rm -f $OUT.err

# Output additional options to log
echo "---------------"    >> $OUT.log
echo "Additional options" >> $OUT.log
echo $OPT                 >> $OUT.log
echo "---------------"    >> $OUT.log

# Run tool
`javajar 4g` $GATK2     \
  -T SelectVariants     \
  -R $REF               \
  --variant $VCF        \
     $OPT               \
  -o $OUT               \
  1>> $OUT.log          \
  2>> $OUT.err


# Arguments for SelectVariants:
#  -V,--variant <variant>                                                       Input VCF file
#  -disc,--discordance <discordance>                                            Output variants that were not called in 
#                                                                               this comparison track
#  -conc,--concordance <concordance>                                            Output variants that were also called in 
#                                                                               this comparison track
#  -o,--out <out>                                                               File to which variants should be written
#  -sn,--sample_name <sample_name>                                              Include genotypes from this sample. Can be 
#                                                                               specified multiple times
#  -se,--sample_expressions <sample_expressions>                                Regular expression to select many samples 
#                                                                               from the ROD tracks provided. Can be 
#                                                                               specified multiple times
#  -sf,--sample_file <sample_file>                                              File containing a list of samples (one per 
#                                                                               line) to include. Can be specified 
#                                                                               multiple times
#  -xl_sn,--exclude_sample_name <exclude_sample_name>                           Exclude genotypes from this sample. Can be 
#                                                                               specified multiple times
#  -xl_sf,--exclude_sample_file <exclude_sample_file>                           File containing a list of samples (one per 
#                                                                               line) to exclude. Can be specified 
#                                                                               multiple times
#  -select,--select_expressions <select_expressions>                            One or more criteria to use when selecting 
#                                                                               the data
#  -env,--excludeNonVariants                                                    Don't include loci found to be non-variant 
#                                                                               after the subsetting procedure
#  -ef,--excludeFiltered                                                        Don't include filtered loci in the 
#                                                                               analysis
#  -regenotype,--regenotype                                                     re-genotype the selected samples based on 
#                                                                               their GLs (or PLs)
#  -restrictAllelesTo,--restrictAllelesTo <restrictAllelesTo>                   Select only variants of a particular 
#                                                                               allelicity. Valid options are ALL 
#                                                                               (default), MULTIALLELIC or BIALLELIC (ALL|
#                                                                               BIALLELIC|MULTIALLELIC)
#  -keepOriginalAC,--keepOriginalAC                                             Store the original AC, AF, and AN values 
#                                                                               in the INFO field after selecting (using 
#                                                                               keys AC_Orig, AF_Orig, and AN_Orig)
#  -mv,--mendelianViolation                                                     output mendelian violation sites only
#  -mvq,--mendelianViolationQualThreshold <mendelianViolationQualThreshold>     Minimum genotype QUAL score for each trio 
#                                                                               member required to accept a site as a 
#                                                                               violation
#  -fraction,--select_random_fraction <select_random_fraction>                  Selects a fraction (a number between 0 and 
#                                                                               1) of the total variants at random from 
#                                                                               the variant track
#  -fractionGenotypes,--remove_fraction_genotypes <remove_fraction_genotypes>   Selects a fraction (a number between 0 and 
#                                                                               1) of the total genotypes at random from 
#                                                                               the variant track and sets them to nocall
#  -selectType,--selectTypeToInclude <selectTypeToInclude>                      Select only a certain type of variants 
#                                                                               from the input file. Valid types are 
#                                                                               INDEL, SNP, MIXED, MNP, SYMBOLIC, 
#                                                                               NO_VARIATION. Can be specified multiple 
#                                                                               times
#  -IDs,--keepIDs <keepIDs>                                                     Only emit sites whose ID is found in this 
#                                                                               file (one ID per line)
#  --maxIndelSize <maxIndelSize>                                                indel size select
#  --ALLOW_NONOVERLAPPING_COMMAND_LINE_SAMPLES                                  Allow a samples other than those in the 
#                                                                               VCF to be specified on the command line. 
#                                                                               These samples will be ignored.
