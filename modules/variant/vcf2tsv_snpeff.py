#!/usr/bin/env python
'''
Description     : Read in a vcf file, parse and output the data in tsv format. Select the highest priority effect from snpeff annotations.
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.11.29
'''

import argparse
import sys
from ngs import vcf

def parse_vcf(vcffile, fout):
    '''
    Read through the vcf file, and parse it.
    Output results in tsv format
    '''
    # Skip to the variants section of the vcf file
    vcffile.jump2variants()
    sample_names = vcffile.get_sample_names()
    sample_names_al = [sn + '_al' for sn in sample_names]
    sample_names_gt = [sn + '_gt' for sn in sample_names]
    sample_names_gq = [sn + '_gq' for sn in sample_names]
    sample_names_dp = [sn + '_dp' for sn in sample_names]
    sample_names_ad = [sn + '_ad' for sn in sample_names]

    # Output header line
    fout.write('%s\n' % '\t'.join(['Chrom',
                                   'Pos',
                                   'Ref',
                                   'Alt',
                                   'Type',
                                   'Rsid',
                                   'Qual',
                                   'Filter',
                                   'AF',
                                   'TotalDP',
                                   'RMS_MQ'] +
                                  ['NoCalls',
                                   'NoCallSamples',
                                   'HomoRef',
                                   'HomoRefSamples',
                                   'Hetero',
                                   'HeteroSamples',
                                   'HomoAffected',
                                   'HomoAffectedSamples'] +
                                  ['Effect',
                                   'EffectImpact',
                                   'FuncClass',
                                   'CodonChange',
                                   'AAChange',
                                   'GeneSymbol',
                                   'Biotype',
                                   'Coding',
                                   'Transcript',
                                   'Exon'] +
                                  sample_names_al +
                                  sample_names_gt +
                                  sample_names_gq +
                                  sample_names_dp +
                                  sample_names_ad))

    # Read in the variant lines
    for line in vcffile:

        # Get parsed variant data
        variant = vcffile.parse_line(line)
        # Record columns
        chrom = variant['CHROM']
        pos = variant['POS']
        ref = variant['REF']
        alt = variant['ALT']
        rsid = variant['ID']
        qual = variant['QUAL']
        filter_ = variant['FILTER']
        
        # Parse the info column
        info_map, info_single = vcffile.parse_info(variant)
        af = info_map.get('AF','')
        dp_total = info_map.get('DP', '')
        rms_mq = info_map.get('MQ', '')

        # Variant type
        variant_type = 'snp'
        len_ref = len(ref)
        len_alt = len(alt)
        if len_ref > 1 or len_alt > 1:
            if len_ref > len_alt:
                variant_type = 'del'
            elif len_ref < len_alt:
                variant_type = 'ins'
            else: # len_ref == len_alt
                if len_ref == 2:
                    variant_type = 'dnp'
                elif len_ref == 3:
                    variant_type = 'tnp'
                else:
                    variant_type = 'onp'

        # Select highest priority effect
        effect = vcffile.select_highest_priority_effect(variant)
        if effect:
            effect_cols = [effect.effect,
                           effect.impact,
                           effect.functional_class,
                           effect.codon_change,
                           effect.aa_change,
                           effect.gene,
                           effect.gene_biotype,
                           effect.coding,
                           effect.transcript,
                           effect.exon]
        else:
            effect_cols = ['',
                           '',
                           '',
                           '',
                           '',
                           '',
                           '',
                           '',
                           '',
                           '']
            
        # Sample genotypes
        samples2field2val = vcffile.parse_samples(variant)
        samples_als = []
        samples_gts = []
        samples_gqs = []
        samples_dps = []
        samples_ads = []
        for sample in sample_names:
            sample_al = samples2field2val[sample].get('GT','NA')
            sample_gt = vcffile.get_sample_gt(variant, sample)
            sample_gq = samples2field2val[sample].get('GQ','NA')
            sample_dp = samples2field2val[sample].get('DP','NA')
            sample_ad = samples2field2val[sample].get('AD','NA')
            samples_als.append(sample_al)
            samples_gts.append(sample_gt)
            samples_gqs.append(sample_gq)
            samples_dps.append(sample_dp)
            samples_ads.append(sample_ad)

        # Sample genotype categories
        uncalled, homo_ref, heterozy, homo_alt = vcffile.samples_gt_categories(samples2field2val)

        # Output to standard output
        fout.write('%s\n' % '\t'.join([chrom,
                                       pos,
                                       ref,
                                       alt,
                                       variant_type,
                                       rsid,
                                       qual,
                                       filter_,
                                       af,
                                       dp_total,
                                       rms_mq] +
                                      [str(len(uncalled)),
                                       ';'.join(uncalled),
                                       str(len(homo_ref)),
                                       ';'.join(homo_ref),
                                       str(len(heterozy)),
                                       ';'.join(heterozy),
                                       str(len(homo_alt)),
                                       ';'.join(homo_alt)] +
                                      effect_cols +
                                      samples_als +
                                      samples_gts +
                                      samples_gqs +
                                      samples_dps +
                                      samples_ads))

def main():
    ap = argparse.ArgumentParser(description="Read in a vcf file, parse and output the data in tsv format. Select the highest priority effect from snpeff annotations.")
    ap.add_argument('vcf_file',
                    help='Input vcf file',
                    nargs='?',
                    type=argparse.FileType('r'),
                    default=sys.stdin)
    ap.add_argument('-o', '--outfile',
                    help='Output result file',
                    type=argparse.FileType('w'),
                    default=sys.stdout)
    params = ap.parse_args()

    # Parse
    with params.vcf_file as fin:
        vcffile = vcf.SnpEffVcfFile(fin)
        parse_vcf(vcffile, params.outfile)


if __name__ == '__main__':
    main()
