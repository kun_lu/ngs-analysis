#!/usr/bin/env python
'''
Description     : Read a Haploview tag output and extract pairwise r^2 values or tag association groups info
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2011-2012
Last Modified   : 2013.10.02
'''

import argparse
import contextlib
import sys

def extract(tagfile_handle, r2file_handle, assocfile_handle):
    MODES = {'r2':0, 'assoc':1}
    MODE = -1
    for line in tagfile_handle:
        # Ignore comment lines
        line_strip = line.strip()
        if len(line_strip) == 0 or line[0] == '#':
            continue

        la = line_strip.split('\t')
        len_la = len(la)

        # Change mode based on section of haploview tag file
        if len_la == 3 and la[0] == 'Allele' and la[1] == 'Best Test' and la[2] == 'r^2 w/test':
            MODE = MODES['r2']
            continue
        if len_la == 2 and la[0] == 'Test' and la[1] == 'Alleles Captured':
            MODE = MODES['assoc']
            continue

        # Output data
        if MODE == MODES['r2']:
            r2file_handle.write(line)
        elif MODE == MODES['assoc']:
            assocfile_handle.write(line)

def main():
    # Set up parameter options
    ap = argparse.ArgumentParser(description="Read a Haploview tag output and extract pairwise r^2 values or tag association groups info")
    ap.add_argument('tagfile', 
                    help='Haploview tagging output file',
                    nargs='?',
                    type=argparse.FileType('r'), 
                    default=sys.stdin)
    ap.add_argument('-o', '--out-prefix', 
                    help='Output files prefix',
                    type=str,
                    default='haploview.output.extract')
    params = ap.parse_args()

    # Extract the data
    with contextlib.nested(params.tagfile,
                           open('.'.join([params.out_prefix, 'r2.txt']), 'w'),
                           open('.'.join([params.out_prefix, 'assoc.txt']), 'w')) as (tagfile, rsqfo, assocfo):
        extract(tagfile, rsqfo, assocfo)

if __name__ == '__main__':
    main()
