#!/bin/bash
##
## DESCRIPTION:   Run GATK SelectVariants tool to extract samples from a list of sample names
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2013.10.17
## LAST MODIFIED: 2013.10.17
##
## USAGE:         gatk2.selectvariants.extractsamples.sh
##                                                       ref.fa            # Reference fasta file used in alignment
##                                                       in.vcf            # Input vcf file
##                                                       out.vcf           # Output file name
##                                                       (y|n)             # y: exclude non variants
##                                                       sample            # Name of sample(s) to extract
##                                                       [sample2 [...]]
##
## OUTPUT:        outfile
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 5 $# $0

# Process input params
REF=$1; shift
VCF=$1; shift
OUT=$1; shift
EXC=$1; shift
SMS=$@

# If input files don't exist, don't run
assert_file_exists_w_content $REF
assert_file_exists_w_content $VCF

# Extract sample(s)
OPTION_CASE=''
for sample in $SMS; do
  OPTION_CASE=$OPTION_CASE' -sn '$sample
done

# Check if tool should exclude non variants or not
if [ $EXC = 'y' ]; then
  OPTION_CASE=$OPTION_CASE' --excludeNonVariants'
fi

# Run tool
gatk2.selectvariants.sh $REF $VCF $OUT "$OPTION_CASE"
