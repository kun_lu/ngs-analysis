#!/bin/bash
## 
## DESCRIPTION:   Merge multiple vcf files into a single vcf file
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         vcf_merge.sh
##                             ref.fa            # Reference used in vcf
##                             output.vcf        # Output vcf filename
##                             in1.vcf           # Vcf files to merge
##                             in2.vcf
##                             [in3.vcf [...]]
##
## OUTPUT:        output.vcf
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Usage check:
usage_min 4 $# $0

# Process input params
REF=$1; shift
OUT=$1; shift
VCF1=$1; shift
VCFS=$@

# Delete log file
rm -f $OUT.log

# Merge all vcf files
/bin/cp -f $VCF1 $OUT
for file in $VCFS; do
  gatk2.combinevariants.sh $REF $OUT.tmp "--variant:vcf1 $OUT --variant:vcf2 $file -genotypeMergeOptions PRIORITIZE -priority vcf1,vcf2" &>> $OUT.log
  /bin/mv -f $OUT.tmp $OUT 2>> $OUT.log
done

mv $OUT.tmp.idx $OUT.idx
mv $OUT.tmp.err $OUT.err
mv $OUT.tmp.log $OUT.log
