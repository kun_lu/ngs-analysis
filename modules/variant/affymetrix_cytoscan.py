#!/usr/bin/env python
'''
Description     : Tool to handle Affymetrix CytoScanHD related things
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.10.08
'''

import argparse
import cPickle
import contextlib
import csv
import sys
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC

CHROMS = set(['1','2','3','4','5','6','7','8','9','10',
                  '11','12','13','14','15','16','17','18',
                  '19','20','21','22','X','Y','M','MT'])

def convert_fwd(strand, alleles):
    '''
    Given a list of alleles and strand,
    if the strand is -, then convert the alleles to their reverse complements
    Otherwise, just return the alleles as is
    '''
    if strand == '-':
        alleles = [str(Seq(a, IUPAC.unambiguous_dna).reverse_complement()) for a in alleles]
    return alleles
    
def subcommand_probe_info(args):
    '''
    Create probe information resource file from annotation file
    '''
    # Load the reference genome pickle
    with args.ref_genome_pkl as f:
        chrom2seq = cPickle.load(f)
    
    with contextlib.nested(args.annotfile, args.out) as (fin, fout):

        # Skip header lines in the input file
        while True:
            line = fin.next()
            if line[0] != '#':
                break
        
        reader = csv.reader(fin, dialect='excel')
        writer = csv.writer(fout, delimiter='\t', lineterminator='\n')

        # Output header line
        writer.writerow(['probe_id',
                         'rsid',
                         'chrom',
                         'pos',
                         'ref',
                         'alt'])

        for row in reader:

            # Extract columns needed
            probe_id = row[0]
            rsid = row[1]
            chrom = row[2].replace('chr','')
            if chrom not in CHROMS:
                continue
            pos = int(row[3])
            strand = row[4]
            al_A, al_B = convert_fwd(strand, row[11:13])

            # Set ref and alt alleles
            ref = chrom2seq[chrom][pos-1]            
            if al_A == ref:
                alt = al_B
            elif al_B == ref:
                alt = al_A
            else:
                alt = ','.join(sorted([al_A, al_B]))

            writer.writerow([probe_id,
                             rsid,
                             chrom,
                             pos,
                             ref,
                             alt])

def main():
    parser = argparse.ArgumentParser(description="Tool to handle Affymetrix CytoScanHD related things")
    
    subparsers = parser.add_subparsers(title='subcommands',
                                       description='Available tools',
                                       dest='subcommand')
    # Subcommand: Filter variants
    parser_probeinfo = subparsers.add_parser('probe_info',
                                             help='Create probe information resource file from an annotation file.  Allele information will be matched to the forward (+) strand.')
    
    parser_probeinfo.add_argument('annotfile',
                                  help='Annotation file, i.e. CytoScanHD_Array.na32.3.annot.csv',
                                  type=argparse.FileType('r'))
    parser_probeinfo.add_argument('ref_genome_pkl',
                                  help='Python pickled reference genome file from which to get the reference allele from',
                                  type=argparse.FileType('rb'))
    parser_probeinfo.add_argument('-r','--ref-type',
                                  help='Reference type (default b3x)',
                                  choices=['b3x','hg'],
                                  default='b3x')
    parser_probeinfo.add_argument('-o','--out',
                                  help='Output probe information file',
                                  type=argparse.FileType('w'),
                                  default=sys.stdout)
    parser_probeinfo.set_defaults(func=subcommand_probe_info)

    # Parse the arguments and call the corresponding function
    args = parser.parse_args()
    args.func(args)
                        

if __name__ == '__main__':
    main()
