#!/bin/bash
##
## DESCRIPTION:   Run GATK VariantRecalibrator for variant filtering, based on GATK best practices
##                http://www.broadinstitute.org/gatk/gatkdocs/org_broadinstitute_sting_gatk_walkers_filters_VariantFiltration.html
##                http://www.broadinstitute.org/gatk/guide/best-practices#variant-discovery
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2013.11.13
## LAST MODIFIED: 2013.11.13
## MODIFIED BY:   Jin Kim jjinking(at)gmail(dot)com
##
## USAGE:         gatk2.variantfiltration.indel.sh
##                                                 ref.fa                         # Reference fasta file used in alignment
##                                                 input.vcf                      # Input vcf
##                                                 output.vcf                     # Ouput vcf
##
## OUTPUT:        output.vcf
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 3 $# $0

# Process input params
REF=$1; shift
VCF=$1; shift
OUT=$1; shift

# If output exists, don't run
assert_file_not_exists_w_content $OUT

# Run tool
`javajar 8g` $GATK2                                                      \
  -T VariantFiltration                                                   \
  -R $REF                                                                \
  --variant $VCF                                                         \
  -o $OUT                                                                \
  --filterExpression 'QD < 2.0 || FS > 200.0 || ReadPosRankSum < -20.0'  \
  --filterName indel_filter                                              \
  &> $OUT.log


# Arguments for VariantFiltration:
#  -V,--variant <variant>                                            Input VCF file
#  -mask,--mask <mask>                                               Input ROD mask
#  -o,--out <out>                                                    File to which variants should be written
#  -filter,--filterExpression <filterExpression>                     One or more expression used with INFO fields to 
#                                                                    filter
#  -filterName,--filterName <filterName>                             Names to use for the list of filters
#  -G_filter,--genotypeFilterExpression <genotypeFilterExpression>   One or more expression used with FORMAT 
#                                                                    (sample/genotype-level) fields to filter (see 
#                                                                    documentation guide for more info)
#  -G_filterName,--genotypeFilterName <genotypeFilterName>           Names to use for the list of sample/genotype filters 
#                                                                    (must be a 1-to-1 mapping); this name is put in the 
#                                                                    FILTER field for variants that get filtered
#  -cluster,--clusterSize <clusterSize>                              The number of SNPs which make up a cluster
#  -window,--clusterWindowSize <clusterWindowSize>                   The window size (in bases) in which to evaluate 
#                                                                    clustered SNPs
#  -maskExtend,--maskExtension <maskExtension>                       How many bases beyond records from a provided 'mask' 
#                                                                    rod should variants be filtered
#  -maskName,--maskName <maskName>                                   The text to put in the FILTER field if a 'mask' rod 
#                                                                    is provided and overlaps with a variant call
#  -filterNotInMask,--filterNotInMask                                Filter records NOT in given input mask.
#  --missingValuesInExpressionsShouldEvaluateAsFailing               When evaluating the JEXL expressions, missing values 
#                                                                    should be considered failing the expression
#  --invalidatePreviousFilters                                       Remove previous filters applied to the VCF
