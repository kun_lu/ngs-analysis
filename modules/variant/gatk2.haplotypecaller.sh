#!/bin/bash
##
## DESCRIPTION:   Run GATK HaplotypeCaller
##                http://www.broadinstitute.org/gatk/gatkdocs/org_broadinstitute_sting_gatk_walkers_haplotypecaller_HaplotypeCaller.html
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         gatk2.haplotypecaller.sh
##                                         ref.fa            # Reference fasta file used in alignment
##                                         outprefix         # Output prefix
##                                         "options"         # Options for this tool in quotes i.e. "--dbsnp dbSNP137.vcf -stand_call_conf [50.0] -stand_emit_conf 10.0 -L targets.interval_list"
##                                         in1.bam           # Input bam file
##                                         [in2.bam [...]]   # Additional bam files
##
## OUTPUT:        outprefix.vcf
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 4 $# $0

# Process input params
PAR=($@)                                    # Parameter array
P_N=${#PAR[@]}                              # Number of words in parameter array (counts words in quotes too)
REF=$1
OUT=$2
OPT=$3
OPA=($OPT)                                  # Convert quoted options string into array
OPN=${#OPA[@]}                              # Number of words in options string
BAM=${PAR[@]:$((2+$OPN)):$(($P_N-2-$OPN))}  # Bam files

# Format output filenames
OUTFILE=$OUT.vcf
OUT_LOG=$OUTFILE.log

# Format list of input bam files
INPUTBAM=''
for bamfile in $BAM; do
  # Check if file exists
  assert_file_exists_w_content $bamfile
  INPUTBAM=$INPUTBAM' -I '$bamfile
done

# Run tool
`javajar 64g` $GATK2           \
  -T HaplotypeCaller           \
  -R $REF                      \
  -baq CALCULATE_AS_NECESSARY  \
     $OPT                      \
     $INPUTBAM                 \
  -o $OUTFILE                  \
  &> $OUT_LOG


# Arguments for HaplotypeCaller:
#  -o,--out <out>                                                                           File to which variants should 
#                                                                                           be written
#  -graph,--graphOutput <graphOutput>                                                       File to which debug assembly 
#                                                                                           graph information should be 
#                                                                                           written
#  -pairHMM,--pair_hmm_implementation <pair_hmm_implementation>                             The PairHMM implementation to 
#                                                                                           use for genotype likelihood 
#                                                                                           calculations (EXACT|ORIGINAL|
#                                                                                           CACHING|LOGLESS_CACHING)
#  -minPruning,--minPruning <minPruning>                                                    The minimum allowed pruning 
#                                                                                           factor in assembly graph. 
#                                                                                           Paths with <= X supporting 
#                                                                                           kmers are pruned from the 
#                                                                                           graph
#  -gcpHMM,--gcpHMM <gcpHMM>                                                                Flat gap continuation penalty 
#                                                                                           for use in the Pair HMM
#  -dr,--downsampleRegion <downsampleRegion>                                                coverage, per-sample, to 
#                                                                                           downsample each active region 
#                                                                                           to
#  -allelesTrigger,--useAllelesTrigger                                                      If specified, use additional 
#                                                                                           trigger on variants found in 
#                                                                                           an external alleles file
#  -D,--dbsnp <dbsnp>                                                                       dbSNP file
#  -comp,--comp <comp>                                                                      comparison VCF file
#  -A,--annotation <annotation>                                                             One or more specific 
#                                                                                           annotations to apply to 
#                                                                                           variant calls
#  -XA,--excludeAnnotation <excludeAnnotation>                                              One or more specific 
#                                                                                           annotations to exclude
#  -G,--group <group>                                                                       One or more classes/groups of 
#                                                                                           annotations to apply to 
#                                                                                           variant calls
#  -hets,--heterozygosity <heterozygosity>                                                  Heterozygosity value used to 
#                                                                                           compute prior likelihoods for 
#                                                                                           any locus
#  -gt_mode,--genotyping_mode <genotyping_mode>                                             Specifies how to determine the 
#                                                                                           alternate alleles to use for 
#                                                                                           genotyping (DISCOVERY|
#                                                                                           GENOTYPE_GIVEN_ALLELES)
#  -out_mode,--output_mode <output_mode>                                                    Specifies which type of calls 
#                                                                                           we should output 
#                                                                                           (EMIT_VARIANTS_ONLY|
#                                                                                           EMIT_ALL_CONFIDENT_SITES|
#                                                                                           EMIT_ALL_SITES)
#  -stand_call_conf,--standard_min_confidence_threshold_for_calling                         The minimum phred-scaled 
# <standard_min_confidence_threshold_for_calling>                                           confidence threshold at which 
#                                                                                           variants should be called
#  -stand_emit_conf,--standard_min_confidence_threshold_for_emitting                        The minimum phred-scaled 
# <standard_min_confidence_threshold_for_emitting>                                          confidence threshold at which 
#                                                                                           variants should be emitted 
#                                                                                           (and filtered with LowQual if 
#                                                                                           less than the calling 
#                                                                                           threshold)
#  -alleles,--alleles <alleles>                                                             The set of alleles at which to 
#                                                                                           genotype when 
#                                                                                           --genotyping_mode is 
#                                                                                           GENOTYPE_GIVEN_ALLELES
#  -maxAltAlleles,--max_alternate_alleles <max_alternate_alleles>                           Maximum number of alternate 
#                                                                                           alleles to genotype
#  -pnrm,--p_nonref_model <p_nonref_model>                                                  Non-reference probability 
#                                                                                           calculation model to employ 
#                                                                                           (EXACT_INDEPENDENT|
#                                                                                           EXACT_REFERENCE|EXACT_ORIGINAL|
#                                                                                           EXACT_GENERAL_PLOIDY)
#  -contamination,--contamination_fraction_to_filter <contamination_fraction_to_filter>     Fraction of contamination in 
#                                                                                           sequencing data (for all 
#                                                                                           samples) to aggressively 
#                                                                                           remove
#  -debug,--debug                                                                           If specified, print out very 
#                                                                                           verbose debug information 
#                                                                                           about each triggering active 
#                                                                                           region
#  -ARO,--activeRegionOut <activeRegionOut>                                                 Output the active region to 
#                                                                                           this interval list file
#  -AR,--activeRegionIn <activeRegionIn>                                                    Use this interval list file as 
#                                                                                           the active regions to process
