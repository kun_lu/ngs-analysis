#!/bin/bash
## 
## DESCRIPTION:   Run haploview in no gui mode for tagging markers
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         haploview.sh
##                             vcf
##                             outprefix
##
## OUTPUT:        
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Usage check:
usage 2 $# $0

# Process input params
VCF=$1; shift
OUT=$1; shift

# Generate ped and info files
python_ngs.sh vcf_tool.py haploview $VCF -o $VCF

# Get chromosome from first line
CHROM=`grep -v ^# $VCF | head -n 1 | cut -f1`

# Merge all vcf files
$HAPLOVIEW                 \
  -nogui                   \
  -memory 8192             \
  -chromosome $CHROM       \
  -minMAF 0.05             \
  -tagrsqcutoff 0.8        \
  -minGeno 0.75            \
  -pairwiseTagging         \
  -svg                     \
  -pedfile $VCF.ped        \
  -info    $VCF.info       \
  -out $OUT                \
  1> $OUT.log              \
  2> $OUT.err

# Parse tag output
python_ngs.sh haploview_output_extract.py $OUT.TAGS -o $OUT.TAGS.parsed
