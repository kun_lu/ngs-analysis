#!/usr/bin/env python
'''
Description     : Read in 2 vcf files, and generate concordance report.
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.10.08
'''

import argparse
import contextlib
import csv
import itertools
import sys
from collections import defaultdict
from ngs import vcf


def main():
    parser = argparse.ArgumentParser(description="Read in 2 vcf files, and generate concordance report.")
    parser.add_argument('vcf1',
                        help='Input vcf file 1',
                        nargs='?',
                        type=argparse.FileType('r'),
                        default=sys.stdin)
    parser.add_argument('vcf2',
                        help='Input vcf file 2',
                        nargs='?',
                        type=argparse.FileType('r'),
                        default=sys.stdin)
    parser.add_argument('-d','--min-dp',
                        help='Minimum depth of position for comparison (otherwise, counted as nocall)',
                        type=int,
                        default=None)
    parser.add_argument('-q','--min-gq',
                        help='Minimum genotype quality for comparison (otherwise, counted as nocall)',
                        type=float,
                        default=None)
    parser.add_argument('-o', '--outfile',
                        help='Output result file',
                        type=argparse.FileType('w'),
                        default=sys.stdout)
    parser.add_argument('-p', '--positions-outfile',
                        help='If the vcf files are annotated by snpeff, generate additional report with each position, gene, and mismatch counts for each type',
                        type=argparse.FileType('w'))
    parser.add_argument('-s', '--match-samples',
                        help='Generate report only for matching sample names',
                        action='store_true')
    args = parser.parse_args()

    # Run concordance
    snpeff = True if args.positions_outfile else False
    with contextlib.nested(args.vcf1, args.vcf2) as (f1, f2):
        vcffile1 = vcf.SnpEffVcfFile(f1)
        vcffile2 = vcf.SnpEffVcfFile(f2)
        results = vcffile1.get_concordance(vcffile2,
                                           min_dp=args.min_dp,
                                           min_gq=args.min_gq,
                                           snpeff=snpeff,
                                           match_samples=args.match_samples)
        samples2counts = results[0]
        samples1 = results[1]
        pos1 = results[2]
        samples2 = results[3]
        pos2 = results[4]
        overlapping_positions = results[5]
        if snpeff:
            vpos2genes = results[6]
            samples2cat = results[7]

    if args.match_samples:
        sample_pairs = [(s1,s2) for s1 in samples1 for s2 in samples2 if s1 == s2]
    else:
        sample_pairs = [(s1,s2) for s1 in samples1 for s2 in samples2]

    # Output results
    with args.outfile as fout:
        fout.write('vcf1 total samples:\t%i\n' % len(samples1))
        fout.write('vcf2 total samples:\t%i\n' % len(samples2))
        fout.write('vcf1 total variants:\t%i\n' % len(pos1))
        fout.write('vcf2 total variants:\t%i\n' % len(pos2))
        fout.write('Overlapping positions:\t%i\n' % len(overlapping_positions))

        writer = csv.writer(fout, delimiter='\t', lineterminator='\n')
        count_categories = ['nocalls_both',
                            'nocalls_vcf1_only',
                            'nocalls_vcf2_only',
                            'nocalls_union',
                            'genotypes_compared',
                            'match_homo',
                            'match_hetero',
                            'mismatch_homo_homo',
                            'mismatch_hetero_hetero',
                            'mismatch_hetero_homo',
                            'mismatch_homo_hetero']
        writer.writerow(['vcf1_sample', 'vcf2_sample'] + count_categories)
        for s1,s2 in sample_pairs:
            outrow = [s1,s2]
            for counter in count_categories:
                outrow.append(samples2counts[(s1,s2)][counter])
            writer.writerow(outrow)

    # Output genes report
    if snpeff:
        gene_report_header = ['chrom','pos','ref','alt','gene']
        gene_report_header += ['vcf1_dp_'+s1 for s1 in samples1] + ['vcf2_dp_'+s2 for s2 in samples2]
        gene_report_header += ['vcf1_gq_'+s1 for s1 in samples1] + ['vcf2_gq_'+s2 for s2 in samples2]
        gene_report_header += [s1+'_vs_'+s2+'_'+cat
                               for s1,s2 in sample_pairs
                               for cat in count_categories]
        gene_report_header += ['allsamples_'+cat for cat in count_categories ]
        with args.positions_outfile as fout:
            writer = csv.writer(fout, delimiter='\t', lineterminator='\n')
            # Output header
            writer.writerow(gene_report_header)
            # Loop over every variant position
            for vpos in sorted(overlapping_positions):
                chrom, pos, ref, alt = vpos
                gene = vpos2genes[vpos]
                outrow = [chrom, pos, ref, alt, gene]

                # Add sample depths
                dps = []
                gqs = []
                for s1 in samples1:
                    dp = vcffile1.samples2dps[s1][vpos]
                    dps.append(dp if dp is not None else 0)
                    gq = vcffile1.samples2gqs[s1][vpos]
                    gqs.append(gq if gq is not None else 0)
                for s2 in samples2:
                    dp = vcffile2.samples2dps[s2][vpos]
                    dps.append(dp if dp is not None else 0)
                    gq = vcffile2.samples2gqs[s2][vpos]
                    gqs.append(gq if gq is not None else 0)
                outrow += dps
                outrow += gqs

                # Loop over every sample pairs
                allsamples_catsum = defaultdict(int)
                for s1,s2 in sample_pairs:
                    for cat in count_categories:
                        val = 1 if cat in samples2cat[(s1,s2)][vpos] else 0
                        outrow.append(val)
                        allsamples_catsum[cat] += val

                # Add all samples summation columns
                for cat in count_categories:
                    outrow.append(allsamples_catsum[cat])

                writer.writerow(outrow)
                

if __name__ == '__main__':
    main()
