#!/usr/bin/env python
'''
Description     : Tool to map old rsids to new rsids
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.10.08
'''

import argparse
import csv
import sys
from collections import defaultdict

VALID_CHROMOSOMES = set(['chr1','chr2','chr3','chr4','chr5','chr6',
                         'chr7','chr8','chr9','chr10','chr11','chr12',
                         'chr13','chr14','chr15','chr16','chr17','chr18',
                         'chr19','chr20','chr21','chr22','chrX','chrY','chrM'])

def map_rsids(args):

    # Load merged rsid mapping to memory
    old2new = {}
    with args.mapping_file as f:
        for row in csv.reader(f, delimiter='\t'):
            old = 'rs' + row[0]
            new = 'rs' + row[1]
            old2new[old] = new

    # Load dbsnp rsids to memory
    rsid2pos = defaultdict(list)
    with args.dbsnp_file as f:
        for row in csv.reader(f, delimiter='\t'):
            if row[1] not in VALID_CHROMOSOMES:
                continue
            chrom = row[1].replace('chr', '')
            pos_s = row[2]
            pos_e = row[3]
            rsid = row[4]
            rsid2pos[rsid].append([chrom, pos_s, pos_e])

    # Read through primers file
    with args.infile as f:
        reader = csv.reader(f, delimiter='\t')
        writer = csv.writer(args.output, delimiter='\t', lineterminator='\n')

        # Read and output header
        if args.header:
            writer.writerow(reader.next() + ['rsid_new', 'chr','start','end'])

        for row in reader:
            rsid = row[args.rsid_col]
            chrom = row[args.chr_col].replace('chr','')

            # Rsid exists in the newest dbSNP
            found = False
            if rsid in rsid2pos:
                for poss in rsid2pos[rsid]:
                    if poss[0] == chrom:
                        writer.writerow(row + [rsid] + rsid2pos[rsid][0])
                        found = True
                        break
            if found:
                continue

            # Map old rsid to new
            if rsid in old2new:
                new_rsid = old2new[rsid]
                try:
                    writer.writerow(row + [new_rsid] + rsid2pos[new_rsid][0])
                except IndexError:
                    if args.ignore_unfound:
                        continue
                    else:
                        raise ValueError('Could not find newly mapped rsid %s in dbsnp_file\n')
                continue

            # Not found
            if args.ignore_unfound:
                continue

            # If not found, raise errors
            if rsid in rsid2pos:
                raise ValueError('Rsid %s was found within rsid2pos but chromosome doesnt match\nSearch chr: %s, rsid2pos match: %s' % (rsid, chrom, str(rsid2pos[rsid])))
            else:
                raise ValueError('Could not figure out where rsid %s came from\n' % rsid)

def main():
    parser = argparse.ArgumentParser(description="Tool to map old rsids to new rsids")
    parser.add_argument('infile',
                        help='Input file',
                        nargs='?',
                        type=argparse.FileType('r'),
                        default=sys.stdin)
    parser.add_argument('mapping_file',
                        help='Rsid merge file, i.e. RsMergeArch.bcp from dbSNP',
                        nargs='?',
                        type=argparse.FileType('r'),
                        default=sys.stdin)
    parser.add_argument('dbsnp_file',
                        help='dbSNP text file, i.e. snp137.txt from UCSC',
                        nargs='?',
                        type=argparse.FileType('r'),
                        default=sys.stdin)
    parser.add_argument('-k','--rsid-col',
                        help='Column number of the old rsids in infile',
                        type=int,
                        default=0)
    parser.add_argument('-c','--chr-col',
                        help='Column number of chromosome',
                        type=int,
                        default=1)
    parser.add_argument('--header',
                        help='Flag to indicate infile has header row',
                        action='store_true')
    parser.add_argument('-i','--ignore-unfound',
                        help='Ignore rsids that were not found in mapping_file or dbsnp_file',
                        action='store_true')
    parser.add_argument('-o','--output',
                        help='Name of output file',
                        type=argparse.FileType('w'),
                        default=sys.stdout)
    args = parser.parse_args()

    map_rsids(args)


if __name__ == '__main__':
    main()
