#!/bin/bash
## 
## DESCRIPTION:   Split a vcf file, creating a separate vcf file for each sample
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2013.11.13
## LAST MODIFIED: 2013.11.13
## MODIFIED BY:   Jin Kim jjinking(at)gmail(dot)com
##
## USAGE:         vcf_extract_samples.sh
##                                       ref.fa       # Reference used in vcf
##                                       in.vcf       # Input vcf
##                                       out.vcf      # Output vcf
##                                       sample1
##                                       [sample2 [...]]
##
## OUTPUT:        out.vcf
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Usage check:
usage_min 4 $# $0

# Process input params
REF=$1; shift
VCF=$1; shift
OUT=$1; shift
SMP=$@

# Extract samples
OPT=""
for samplename in $SMP; do
  OPT=$OPT" -sn $samplename"
done
OPT="$OPT --excludeNonVariants"
gatk2.selectvariants.sh $REF $VCF $OUT "$OPT"
