#!/bin/bash
##
## DESCRIPTION:   Run GATK HaplotypeCaller using grid engine, using b3x resource files
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         gatk2.haplotypecaller.target.b3x.ge.sh 
##                                                       outprefix         # Output prefix, i.e. samples.haplocall
##                                                       target_region     # Target region bed or intervals file
##                                                       in1.bam           # Input bam file
##                                                       [in2.bam [...]]   # Additional bam files
##
## OUTPUT:        outprefix.vcf
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 3 $# $0

# Process input params
PAR=($@)                                    # Parameter array
P_N=${#PAR[@]}                              # Number of words in parameter array (counts words in quotes too)
OUT=$1                                      # Output prefix
TGT=$2                                      # Target region
B_N=$(($P_N - 2))                           # Number of bam files
BAM=${PAR[@]:2:$B_N}                        # Bam files

# Ensure that input file(s) exist
assert_file_exists_w_content $TGT
assert_file_exists_w_content $B3x_DBSNP_VCF_GATK2

# Submit job to grid engine
QSUB=$NGS_ANALYSIS_DIR/modules/util/qsub_wrapper.sh
TOOL=$NGS_ANALYSIS_DIR/modules/variant/gatk2.haplotypecaller.sh
$QSUB HaploCall.$$                                     \
      $Q_HIGH                                          \
      1                                                \
      none                                             \
      n                                                \
      $TOOL $B3x_REF_GATK2                             \
            $OUT                                       \
            "--dbsnp $B3x_DBSNP_VCF_GATK2 -L $TGT"     \
            $BAM
