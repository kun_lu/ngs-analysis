#!/bin/bash
## 
## DESCRIPTION:   Split a vcf file, creating a separate vcf file for each sample
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2014.01.02
## MODIFIED BY:   Jin Kim jjinking(at)gmail(dot)com
##
## USAGE:         vcf_split_by_sample.ge.sh
##                                          ref.fa       # Reference used in vcf
##                                          in.vcf       # Input vcf
##                                          outprefix    # Prefix for each output vcf, which will be postfixed by each samplename then the .vcf extension
##                                          (y|n)        # Remove non-variants
##
## OUTPUT:        vcf files for each sample
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Usage check:
usage 4 $# $0

# Process input params
REF=$1; shift
VCF=$1; shift
OUT=$1; shift
NON=$1;

# Split vcf files
SAMPLENAMES=`python_ngs.sh vcf_tool.py sample_names $VCF`
for samplename in $SAMPLENAMES; do
  OPT="-sn $samplename"
  if [ $NON = "y" ]; then
    OPT="$OPT --excludeNonVariants"
  fi
  qsub_jn.sh vcf_split.$samplename 1 gatk2.selectvariants.sh $REF $VCF $OUT$samplename.vcf "$OPT"
done
