#!/usr/bin/env python
'''
Description     : Tool to handle and manipulate vcf files
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.11.29
Modified by     : Jin Kim jjinking(at)gmail(dot)com
'''

import argparse
import contextlib
import csv
import itertools
import sys
from ngs import vcf

def subcommand_count(args):
    '''
    Count the number of variants
    '''
    # Count each vcf file and output
    vcf_counts = []
    for fin in args.vcf:
        with fin:
            vcffile = vcf.VcfFile(fin)
            vcf_counts.append([vcffile.count_variants(unique_only=args.unique),
                               fin.name])
        
    # Output only numeric values
    if args.numeric_only:
        for n_vars, vcfname in vcf_counts:
            args.outfile.write('%i\n' % n_vars)
        return

    # Output left-adjusted
    col1_maxlen = max(map(lambda row: len(str(row[0])), vcf_counts))
    col2_maxlen = max(map(lambda row: len(str(row[1])), vcf_counts))
    for n_vars, vcfname in vcf_counts:
        args.outfile.write('%s   %s\n' % (str(n_vars).rjust(col1_maxlen + 1),
                                          str(vcfname).ljust(col2_maxlen + 3)))

def subcommand_filter(args):
    with contextlib.nested(args.vcf_in, args.vcf_out) as (fin, fout):
        vcffile = vcf.SnpEffVcfFile(fin)
        vcffile.jump2variants(outfile=fout)

        # Add filters
        filter_factory = vcf.VariantFilterFactory()

        # Different genotype selector
        if args.select_diff_gt is not None:
            sample1,sample2 = args.select_diff_gt
            filter = filter_factory.create_diff_gt_selector(sample1,sample2)
            vcffile.add_variant_filter(filter)

        # All samples hetero
        if args.select_all_hetero:
            filter = filter_factory.create_all_hetero_selector()
            vcffile.add_variant_filter(filter)

        # All samples homo ref
        if args.select_all_homo_ref:
            filter = filter_factory.create_all_homo_ref_selector()
            vcffile.add_variant_filter(filter)

        # All samples homo alt
        if args.select_all_homo_alt:
            filter = filter_factory.create_all_homo_alt_selector()
            vcffile.add_variant_filter(filter)

        # All samples affected
        if args.select_all_affected:
            filter = filter_factory.create_all_samples_affected_selector()
            vcffile.add_variant_filter(filter)

        # Nocalls filters
        if args.nocalls_all:
            filter = filter_factory.create_nocalls_filter(type='ALL')
            vcffile.add_variant_filter(filter)
        if args.nocalls_any:
            filter = filter_factory.create_nocalls_filter(type='ANY')
            vcffile.add_variant_filter(filter)

        # Generate filtered variants and write output
        for line in vcffile.filtered_variants():
            fout.write(line)

def subcommand_sample(args):

    raise NotImplementedError('This subcommand has not been implemented yet.\n')
    
    with contextlib.nested(args.vcf_in, args.vcf_out) as (fin, fout):
        vcffile = vcf.VcfFile(fin)
        vcffile.jump2variants(outfile=fout)

def subcommand_createfromtsv(args):
    '''
    Create a vcf file from a tsv file
    '''
    with contextlib.nested(args.tsv_file, args.out) as (fin, fout):
        tsv2vcf_converter = vcf.Tsv2VcfConverter(fin,
                                                 variant_caller=args.variant_caller,
                                                 chrom_col=args.chrom_col,
                                                 pos_col=args.pos_col,
                                                 rsid_col=args.rsid_col,
                                                 ref_col=args.ref_col,
                                                 alt_col=args.alt_col,
                                                 sample_ids=args.samples,
                                                 sample_gt_cols=args.geno_cols,
                                                 is_header=args.header)
        fout.write('%s\n' % tsv2vcf_converter.get_header())
        for line in tsv2vcf_converter:
            fout.write('%s\n' % line)

def subcommand_gene_summary(args):
    '''
    Generate summary by gene
    '''
    with contextlib.nested(args.vcf, args.out) as (fin, fout):
        vcffile = vcf.SnpEffVcfFile(fin)
        gene_transcript2summary, effects = vcffile.summarize_gene_transcript()

        writer = csv.writer(fout, delimiter='\t', lineterminator='\n')
        writer.writerow(['gene',
                         'transcript',
                         'variant_sites_count',
                         'variant_count',
                         'variant_samplespos',
                         'samples_affected',
                         'samples_hetero',
                         'samples_homo_alt',
                         'samples_homo_ref'] + list(itertools.chain(*[(e,e+'_samplepos') for e in effects])))

        for gene_transcript,summary in gene_transcript2summary.iteritems():
            writer.writerow([summary.gene,
                             summary.transcript,
                             len(summary.get_unique_variant_sites()),
                             len(summary.variant_samplepos),
                             summary.variant_samplepos_as_str(),
                             len(summary.samples_affected),
                             len(summary.samples_hetero),
                             len(summary.samples_homo_alt),
                             len(summary.samples_homo_ref)] + list(itertools.chain(*[(len(summary.effects2samplepos[e]),
                                                                                     summary.effects2samplepos_str(e)) for e in effects])))

def subcommand_samplenames(args):
    '''
    Extract sample names from vcf file and output
    '''
    with args.vcf as f:
        vcffile = vcf.VcfFile(f)
        vcffile.jump2variants()
        samplenames = vcffile.get_sample_names()
    for sn in samplenames:
        sys.stdout.write('%s\n' % sn)

def subcommand_numsamples(args):
    '''
    Get the number of samples in the vcf file
    '''
    with args.vcf as f:
        vcffile = vcf.VcfFile(f)
        vcffile.jump2variants()
        sys.stdout.write('%i\n' % len(vcffile.get_sample_names()))

def subcommand_numoverlaps(args):
    '''
    Get the number of overlapping variants between two vcf files
    '''
    with contextlib.nested(args.vcf1, args.vcf2) as (f1,f2):
        vcffile1 = vcf.VcfFile(f1)
        vcffile2 = vcf.VcfFile(f2)
        vcf1_variants = vcffile1.load_pos2memory_set(include_alleles=True)
        vcf2_variants = vcffile2.load_pos2memory_set(include_alleles=True)
    sys.stdout.write('%i\n' % len(vcf1_variants.intersection(vcf2_variants)))

def subcommand_subtract(args):
    '''
    Set subtract vcf1 - vcf2
    '''
    with contextlib.nested(args.vcf1, args.vcf2, args.out) as (f1, f2, fout):
        vcffile1 = vcf.VcfFile(f1)
        vcffile2 = vcf.VcfFile(f2)
        
        # Load vcf2 variant positions and alleles to memory
        vcf2_variants = vcffile2.load_pos2memory_set(include_alleles=True)
        vcffile1.jump2variants(outfile=fout)
        
        # Check each variant in vcf1
        for line in vcffile1:
            variant = vcffile1.parse_line(line)
            
            # Output variant record if it's not in vcf2
            if (variant['CHROM'],
                int(variant['POS']),
                variant['REF'],
                variant['ALT']) not in vcf2_variants:
                fout.write(line)

def subcommand_removeid(args):
    '''
    Remove id column of vcf file, usually in order to replace outdated rsid values
    '''
    with contextlib.nested(args.vcf, args.out) as (fin, fout):
        vcffile = vcf.VcfFile(fin)
        vcffile.jump2variants(outfile=fout)        
        for line in vcffile:
            row = line.split('\t')
            row[2] = '.'
            fout.write('\t'.join(row))

def subcommand_fix(args):
    '''
    Fix vcf file formats
    '''
    def fix_mutect_row(variant, vcffile):
        '''
        Fix mutect vcf row
        '''
        sample2field2val = vcffile.parse_samples(variant)
        for s in vcffile.get_sample_names():
            if sample2field2val[s]['GT'] == '0':
                gt_idx = variant['FORMAT'].split(':').index('GT')
                s_list = variant[s].split(':')
                s_list[gt_idx] = '0/0'
                variant[s] = ':'.join(s_list)
        return '\t'.join([variant[c] for c in vcffile.column_names])
                
    with contextlib.nested(args.vcf, args.out) as (fin, fout):
        vcffile = vcf.VcfFile(fin)
        vcffile.jump2variants(outfile=fout)
        for line in vcffile:
            variant = vcffile.parse_line(line)
            if args.type == 'mutect':
                fout.write('%s\n' % fix_mutect_row(variant, vcffile))

def subcommand_haploview(args):
    '''
    Generate haploview input files, .ped and .info files
    '''
    base2code = {'A' : '1',
                 'C' : '2',
                 'G' : '3',
                 'T' : '4'}

    def convert_gt2code(gt):
        if gt in set(['./.', 'N/N', '.', '', 'N']):
            return '0 0'
        return ' '.join([base2code[a.upper()] for a in gt.split('/')])

    def position_already_seen(pos, marker_info):
        for i,row in enumerate(marker_info):
            if row[1] == pos:
                return i
        return -1
            
    marker_info = []
    pedigree = []
    with args.vcf as fin:
        vcffile = vcf.SnpEffVcfFile(fin)
        vcffile.jump2variants()
        samplenames = vcffile.get_sample_names()
        # Add fam column
        pedigree.append(['fam_' + s for s in samplenames])
        # Add individual id
        pedigree.append(samplenames)
        # Add father's id
        len_samplenames = len(samplenames)
        zeros_col = ['0'] * len_samplenames
        pedigree.append(zeros_col)
        # Add mother's id
        pedigree.append(zeros_col)
        # Add gender
        pedigree.append(zeros_col)
        # Add affection status
        pedigree.append(zeros_col)

        # Loop through variants and add marker and genotype info
        for i,line in enumerate(vcffile):
            variant = vcffile.parse_line(line)

            # Get marker info ---------------------------------------------
            marker_name_cols = ['CHROM','POS']
            # Append rsid if it exists
            if len(variant['ID']) > 1:
                marker_name_cols.append('ID')
            marker_name = '_'.join([variant[c] for c in marker_name_cols])

            # Get marker effect
            effect = vcffile.select_highest_priority_effect(variant)

            # For non-unique positions, replace existing position record if effect
            # of current variant has higher priority
            pos_idx = position_already_seen(variant['POS'], marker_info)
            if pos_idx >= 0:
                effect_prev = marker_info[pos_idx][2]
                effect_prev_rank = vcf.SnpEffVcfFile.effects_prioritized.index(effect_prev)
                effect_curr_rank = vcf.SnpEffVcfFile.effects_prioritized.index(effect.effect)
                if effect_curr_rank < effect_prev_rank:
                    marker_info[pos_idx][2] = effect.effect
                    marker_info[pos_idx][0] = marker_name + '_' + effect.effect
            else:
                # Add to info matrix
                _effect = effect.effect if effect else ''
                if _effect:
                    marker_name = marker_name + '_' + _effect
                marker_info.append([marker_name, variant['POS'], _effect])

                # Add genotype info to pedigree ------------------------------
                pedigree.append([convert_gt2code(vcffile.get_sample_gt(variant,
                                                                       s, 
                                                                       phased=False)) for s in samplenames])
    
    # Output info file
    with open(args.out + '.info', 'wb') as f:
        writer = csv.writer(f, delimiter='\t', lineterminator='\n')
        writer.writerows(marker_info)
        
    # Output transpose of pedigree data
    with open(args.out + '.ped', 'wb') as f:
        writer = csv.writer(f, delimiter='\t', lineterminator='\n')
        writer.writerows(zip(*pedigree))

def main():
    parser = argparse.ArgumentParser(description="Tool to handle and manipulate vcf files")
    
    subparsers = parser.add_subparsers(title='subcommands',
                                       description='Available tools',
                                       dest='subcommand')
    
    # Subcommand: Count number of variants
    parser_numvariants = subparsers.add_parser('count',
                                               help='Count the number of variants in vcf file')
    parser_numvariants.add_argument('vcf',
                                    help='Input vcf file(s)',
                                    nargs='*',
                                    type=argparse.FileType('rU'),
                                    default=sys.stdin)
    parser_numvariants.add_argument('-u','--unique',
                                    help='Count only unique variants using tuple (chromosome, position, reference allele, alternate allele) as the key',
                                    action='store_true')
    parser_numvariants.add_argument('-n','--numeric-only',
                                    help='Output only numeric values for the counts as a single column (exclude file names, formatting spaces, etc)',
                                    action='store_true')
    parser_numvariants.add_argument('-o','--outfile',
                                    help='Output filename (default stdout)',
                                    type=argparse.FileType('wb'),
                                    default=sys.stdout)
    parser_numvariants.set_defaults(func=subcommand_count)

    # Subcommand: Filter variants
    parser_filter = subparsers.add_parser('filter',
                                          help='Select for or filter out variants')
    
    parser_filter.add_argument('vcf_in',
                               help='Input vcf file',
                               nargs='?',
                               type=argparse.FileType('r'),
                               default=sys.stdin)
    parser_filter.add_argument('--select-diff-gt',
                               help='Sample names for two samples in the vcf file whose genotypes will be compared to check if they are different',
                               nargs=2,
                               type=str,
                               default=None)
    parser_filter.add_argument('--select-all-hetero',
                               help='Select variants where all samples have heterozygous genotype calls',
                               action='store_true')
    parser_filter.add_argument('--select-all-homo-ref',
                               help='Select variants where all samples have homozygous reference genotype calls',
                               action='store_true')
    parser_filter.add_argument('--select-all-homo-alt',
                               help='Select variants where all samples have homozygous alternate genotype calls',
                               action='store_true')
    parser_filter.add_argument('--select-all-affected',
                               help='Select variants where all samples have either homozygous alternate or heterozygous genotype calls',
                               action='store_true')
    parser_filter.add_argument('--nocalls-all',
                               help='Filter out variants where all samples have nocalls',
                               action='store_true')
    parser_filter.add_argument('--nocalls-any',
                               help='Filter out variants where at least one sample has a nocall',
                               action='store_true')
    parser_filter.add_argument('-o','--vcf_out',
                               help='Output vcf file',
                               type=argparse.FileType('w'),
                               default=sys.stdout)
    parser_filter.set_defaults(func=subcommand_filter)

    # Subcommand: Extract samples
    parser_samples = subparsers.add_parser('extract_samples',
                                           help='Select a list of sample columns')
    parser_samples.add_argument('vcf_in',
                                help='Input vcf file',
                                nargs='?',
                                type=argparse.FileType('r'),
                                default=sys.stdin)
    parser_samples.add_argument('sampleslist',
                                help='Input file with a single list of sample ids to extract',
                                nargs='?',
                                type=argparse.FileType('r'),
                                default=sys.stdin)
    parser_samples.add_argument('-o','--vcf_out',
                                help='Output vcf file',
                                type=argparse.FileType('w'),
                                default=sys.stdout)
    parser_samples.set_defaults(func=subcommand_sample)

    # Subcommand: Create from tsv
    parser_createfromtsv = subparsers.add_parser('create_from_tsv',
                                                 help='Create a vcf file from a tsv file')
    parser_createfromtsv.add_argument('tsv_file',
                                      help='Input tsv file',
                                      nargs='?',
                                      type=argparse.FileType('r'),
                                      default=sys.stdin)
    parser_createfromtsv.add_argument('-v', '--variant-caller',
                                      help='Tool or program used to call variants',
                                      type=str,
                                      default='FooCaller')
    parser_createfromtsv.add_argument('--header',
                                      help='Set this flag to indicate that the input tsv file contains a header row at the top',
                                      action='store_true')
    parser_createfromtsv.add_argument('-c', '--chrom-col',
                                      help='Chromosome column number, 0-based, default 0',
                                      type=int,
                                      default=0)
    parser_createfromtsv.add_argument('-p', '--pos-col',
                                      help='Position column number, 0-based, default 1',
                                      type=int,
                                      default=1)
    parser_createfromtsv.add_argument('-d', '--rsid-col',
                                      help='dbSNP rsid column number, 0-based, default 2',
                                      type=int,
                                      default=2)
    parser_createfromtsv.add_argument('-r', '--ref-col',
                                      help='Reference allele genotype column number, 0-based, default 3',
                                      type=int,
                                      default=3)
    parser_createfromtsv.add_argument('-a', '--alt-col',
                                      help='Alternate allele genotype column number, 0-based, default 4',
                                      type=int,
                                      default=4)
    parser_createfromtsv.add_argument('-s', '--samples',
                                      help='Sample ids',
                                      nargs='*',
                                      type=str,
                                      default=['sample'])
    parser_createfromtsv.add_argument('-g', '--geno-cols',
                                      help='Sample(s) genotype column number(s), 0-based, default 5.  Contains genotypes in the format A/C, T/T',
                                      nargs='*',
                                      type=int,
                                      default=[5])
    parser_createfromtsv.add_argument('-o', '--out',
                                      help='Output vcf file name',
                                      type=argparse.FileType('wb'),
                                      default=sys.stdout)
    parser_createfromtsv.set_defaults(func=subcommand_createfromtsv)

    # Subcommand: Gene summary
    parser_gene_summary = subparsers.add_parser('gene_summary',
                                                 help='Generate summary by gene')
    parser_gene_summary.add_argument('vcf',
                                     help='Input vcf file',
                                     nargs='?',
                                     type=argparse.FileType('r'),
                                     default=sys.stdin)
    parser_gene_summary.add_argument('-o', '--out',
                                     help='Output gene summary table',
                                     type=argparse.FileType('w'),
                                     default=sys.stdout)
    parser_gene_summary.set_defaults(func=subcommand_gene_summary)

    # Subcommand: Sample names
    parser_samplenames = subparsers.add_parser('sample_names',
                                               help='Extract sample names from vcf file')
    parser_samplenames.add_argument('vcf',
                                    help='Input vcf file',
                                    nargs='?',
                                    type=argparse.FileType('r'),
                                    default=sys.stdin)
    parser_samplenames.set_defaults(func=subcommand_samplenames)

    # Subcommand: Number of samples
    parser_numsamples = subparsers.add_parser('num_samples',
                                              help='Get the number of samples in the vcf file')
    parser_numsamples.add_argument('vcf',
                                   help='Input vcf file',
                                   nargs='?',
                                   type=argparse.FileType('r'),
                                   default=sys.stdin)
    parser_numsamples.set_defaults(func=subcommand_numsamples)

    # Subcommand: Number of overlapping positions
    parser_numoverlap = subparsers.add_parser('num_overlap',
                                              help='Get the number of variant positions that overlap between two vcf files')
    parser_numoverlap.add_argument('vcf1',
                                   help='Input vcf file 1',
                                   nargs='?',
                                   type=argparse.FileType('r'),
                                   default=sys.stdin)
    parser_numoverlap.add_argument('vcf2',
                                   help='Input vcf file 2',
                                   nargs='?',
                                   type=argparse.FileType('r'),
                                   default=sys.stdin)
    parser_numoverlap.set_defaults(func=subcommand_numoverlaps)

    # Subcommand: Subtract
    parser_subtract = subparsers.add_parser('subtract',
                                            help='From a vcf file with a set of variants, subtract variants that exist in another vcf file, i.e. set_subtract(vcf1, vcf2)')
    parser_subtract.add_argument('vcf1',
                                 help='Input original vcf file (vcf1)',
                                 nargs='?',
                                 type=argparse.FileType('rU'),
                                 default=sys.stdin)
    parser_subtract.add_argument('vcf2',
                                 help='Input vcf file containing variants to subtract (vcf2)',
                                 nargs='?',
                                 type=argparse.FileType('rU'),
                                 default=sys.stdin)
    parser_subtract.add_argument('-o','--out',
                                 help='Output vcf file',
                                 type=argparse.FileType('w'),
                                 default=sys.stdout)
    parser_subtract.set_defaults(func=subcommand_subtract)

    # Subcommand: fix
    parser_fix = subparsers.add_parser('fix',
                                       help='Fix a vcf files from tools that did not abide by vcf format specifications')
    parser_fix.add_argument('vcf',
                            help='Input vcf file',
                            nargs='?',
                            type=argparse.FileType('rU'),
                            default=sys.stdin)
    parser_fix.add_argument('-t','--type',
                            help='Tool that generated the vcf file (default: mutect)',
                            choices=['mutect','varscan'],
                            default='mutect')
    parser_fix.add_argument('-o', '--out',
                            help='Output vcf file',
                            type=argparse.FileType('wb'),
                            default=sys.stdout)
    parser_fix.set_defaults(func=subcommand_fix)

    # Subcommand: haploview input
    parser_haploview = subparsers.add_parser('haploview',
                                             help='Generate haploview input files from vcf')
    parser_haploview.add_argument('vcf',
                                  help='Input vcf file.  Vcf file should only contain same-chromosome variants, be annotated with SNPEff, and may contain duplicate rows for each position (output of one_per_line tool)',
                                  nargs='?',
                                  type=argparse.FileType('rU'),
                                  default=sys.stdin)
    parser_haploview.add_argument('-o', '--out',
                                  help='Output prefix',
                                  type=str,
                                  default='vcf2haploview')
    parser_haploview.set_defaults(func=subcommand_haploview)

    # Subcommand: remove id field
    parser_removeid = subparsers.add_parser('remove_id',
                                            help='Remove id field, usually to remove outdated rsids')
    parser_removeid.add_argument('vcf',
                                 help='Input vcf file',
                                 nargs='?',
                                 type=argparse.FileType('rU'),
                                 default=sys.stdin)
    parser_removeid.add_argument('-o', '--out',
                                 help='Output filename',
                                 type=argparse.FileType('wb'),
                                 default=sys.stdout)
    parser_removeid.set_defaults(func=subcommand_removeid)

    # Parse the arguments and call the corresponding function
    args = parser.parse_args()
    args.func(args)
                        

if __name__ == '__main__':
    main()
