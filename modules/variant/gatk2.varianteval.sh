#!/bin/bash
##
## DESCRIPTION:   Run GATK VariantEval tool
##                http://www.broadinstitute.org/gatk/gatkdocs/org_broadinstitute_sting_gatk_walkers_varianteval_VariantEval.html
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         gatk2.varianteval.sh
##                                     ref.fa
##                                     outprefix
##                                     "options"             # Example: "--dbsnp dbSNP.vcf -noST -noEV -ST Sample -ST FunctionalClass -EV CompOverlap -EV CountVariants -EV TiTvVariantEvaluator"
##                                     input1.vcf
##                                     [input2.vcf [...]]
##
## OUTPUT:        outprefix.vareval.report
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 4 $# $0

# Process input params
REF=$1; shift
OUT=$1; shift
OPT=$1; shift
VCF="$@"

# Format output
OUTPUT=$OUT.vareval.report
OUTLOG=$OUTPUT.log

# If output exists, don't run
assert_file_not_exists_w_content $OUTPUT

# Format list of input vcf files and sample ids
INPUTVCFS=''
for file in $VCF; do
  # Check if file exists
  assert_file_exists_w_content $file
  SET=`filter_ext $file 1`
  INPUTVCFS=$INPUTVCFS' --eval:'$SET' '$file
done

# Run tool
`javajar 8g` $GATK2    \
   -T VariantEval      \
   -R $REF             \
      $OPT             \
      $INPUTVCFS       \
   -o $OUTPUT          \
   &> $OUTLOG


# Arguments for VariantEval:
#  -eval,--eval <eval>                                                        Input evaluation file(s)
#  -o,--out <out>                                                             An output file created by the walker.  Will 
#                                                                             overwrite contents if file exists
#  -comp,--comp <comp>                                                        Input comparison file(s)
#  -D,--dbsnp <dbsnp>                                                         dbSNP file
#  -gold,--goldStandard <goldStandard>                                        Evaluations that count calls at sites of 
#                                                                             true variation (e.g., indel calls) will use 
#                                                                             this argument as their gold standard for 
#                                                                             comparison
#  -ls,--list                                                                 List the available eval modules and exit
#  -select,--select_exps <select_exps>                                        One or more stratifications to use when 
#                                                                             evaluating the data
#  -selectName,--select_names <select_names>                                  Names to use for the list of stratifications 
#                                                                             (must be a 1-to-1 mapping)
#  -sn,--sample <sample>                                                      Derive eval and comp contexts using only 
#                                                                             these sample genotypes, when genotypes are 
#                                                                             available in the original context
#  -knownName,--known_names <known_names>                                     Name of ROD bindings containing variant 
#                                                                             sites that should be treated as known when 
#                                                                             splitting eval rods into known and novel 
#                                                                             subsets
#  -ST,--stratificationModule <stratificationModule>                          One or more specific stratification modules 
#                                                                             to apply to the eval track(s) (in addition 
#                                                                             to the standard stratifications, unless -noS 
#                                                                             is specified)
#  -noST,--doNotUseAllStandardStratifications                                 Do not use the standard stratification 
#                                                                             modules by default (instead, only those that 
#                                                                             are specified with the -S option)
#  -EV,--evalModule <evalModule>                                              One or more specific eval modules to apply 
#                                                                             to the eval track(s) (in addition to the 
#                                                                             standard modules, unless -noEV is specified)
#  -noEV,--doNotUseAllStandardModules                                         Do not use the standard modules by default 
#                                                                             (instead, only those that are specified with 
#                                                                             the -EV option)
#  -mpq,--minPhaseQuality <minPhaseQuality>                                   Minimum phasing quality
#  -mvq,--mendelianViolationQualThreshold <mendelianViolationQualThreshold>   Minimum genotype QUAL score for each trio 
#                                                                             member required to accept a site as a 
#                                                                             violation. Default is 50.
#  -ploidy,--samplePloidy <samplePloidy>                                      Per-sample ploidy (number of chromosomes per 
#                                                                             sample)
#  -aa,--ancestralAlignments <ancestralAlignments>                            Fasta file with ancestral alleles
#  -strict,--requireStrictAlleleMatch                                         If provided only comp and eval tracks with 
#                                                                             exactly matching reference and alternate 
#                                                                             alleles will be counted as overlapping
#  -keepAC0,--keepAC0                                                         If provided, modules that track polymorphic 
#                                                                             sites will not require that a site have AC > 
#                                                                             0 when the input eval has genotypes
#  -mergeEvals,--mergeEvals                                                   If provided, all -eval tracks will be merged 
#                                                                             into a single eval track
#  -stratIntervals,--stratIntervals <stratIntervals>                          File containing tribble-readable features 
#                                                                             for the IntervalStratificiation
#  -knownCNVs,--knownCNVs <knownCNVs>                                         File containing tribble-readable features 
#                                                                             describing a known list of copy number 
#                                                                             variants
# 
# Available Reference Ordered Data types:
#          Name        FeatureType   Documentation
#          BCF2     VariantContext   http://www.broadinstitute.org/gatk/gatkdocs/org_broadinstitute_variant_bcf2_BCF2Codec.html
#        BEAGLE      BeagleFeature   http://www.broadinstitute.org/gatk/gatkdocs/org_broadinstitute_sting_utils_codecs_beagle_BeagleCodec.html
#           BED         BEDFeature   http://www.broadinstitute.org/gatk/gatkdocs/org_broad_tribble_bed_BEDCodec.html
#      BEDTABLE       TableFeature   http://www.broadinstitute.org/gatk/gatkdocs/org_broadinstitute_sting_utils_codecs_table_BedTableCodec.html
# EXAMPLEBINARY            Feature   http://www.broadinstitute.org/gatk/gatkdocs/org_broad_tribble_example_ExampleBinaryCodec.html
#      GELITEXT    GeliTextFeature   http://www.broadinstitute.org/gatk/gatkdocs/org_broad_tribble_gelitext_GeliTextCodec.html
#      OLDDBSNP    OldDbSNPFeature   http://www.broadinstitute.org/gatk/gatkdocs/org_broad_tribble_dbsnp_OldDbSNPCodec.html
#     RAWHAPMAP   RawHapMapFeature   http://www.broadinstitute.org/gatk/gatkdocs/org_broadinstitute_sting_utils_codecs_hapmap_RawHapMapCodec.html
#        REFSEQ      RefSeqFeature   http://www.broadinstitute.org/gatk/gatkdocs/org_broadinstitute_sting_utils_codecs_refseq_RefSeqCodec.html
#     SAMPILEUP   SAMPileupFeature   http://www.broadinstitute.org/gatk/gatkdocs/org_broadinstitute_sting_utils_codecs_sampileup_SAMPileupCodec.html
#       SAMREAD     SAMReadFeature   http://www.broadinstitute.org/gatk/gatkdocs/org_broadinstitute_sting_utils_codecs_samread_SAMReadCodec.html
#         TABLE       TableFeature   http://www.broadinstitute.org/gatk/gatkdocs/org_broadinstitute_sting_utils_codecs_table_TableCodec.html
#           VCF     VariantContext   http://www.broadinstitute.org/gatk/gatkdocs/org_broadinstitute_variant_vcf_VCFCodec.html
#          VCF3     VariantContext   http://www.broadinstitute.org/gatk/gatkdocs/org_broadinstitute_variant_vcf_VCF3Codec.html
# 
# For a full description of this walker, see its GATKdocs at:
# http://www.broadinstitute.org/gatk/gatkdocs/org_broadinstitute_sting_gatk_walkers_varianteval_VariantEval.html
# 
