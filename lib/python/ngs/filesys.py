#!/usr/bin/env python
'''
Description     : API for dealing with general files
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.10.08
'''

import gzip
import os
import sys
import zipfile

def get_file_handle(filename, mode):
    '''
    Given a filename, check the extension
    and return the proper file read handle
    '''
    file_ext = os.path.splitext(filename)[1]
    try:
        if file_ext == '.zip' and zipfile.is_zipfile(filename):
            f = zipfile.ZipFile(filename, mode)
        elif file_ext == '.gz':
            f = gzip.GzipFile(filename, mode)
        else:
            f = open(filename, mode)
        return f
    except IOError:
        return False

