#!/usr/bin/env python
'''
Description     : API for parsing varscan output files
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.10.08
'''

import csv
import sys

class VarScanFile(object):
    '''
    Class to handle varscan output files (non-vcf outputs, i.e. .snp and .indel files)
    '''
    # Column names described in header line
    COLNAMES = ['chrom',
                'position',
                'ref',
                'var',
                'normal_reads1',
                'normal_reads2',
                'normal_var_freq',
                'normal_gt',
                'tumor_reads1',
                'tumor_reads2',
                'tumor_var_freq',
                'tumor_gt',
                'somatic_status',
                'variant_p_value',
                'somatic_p_value',
                'tumor_reads1_plus',
                'tumor_reads1_minus',
                'tumor_reads2_plus',
                'tumor_reads2_minus',
                'normal_reads1_plus',
                'normal_reads1_minus',
                'normal_reads2_plus',
                'normal_reads2_minus']

    def __init__(self, filehandle):
        self.f = filehandle
        self.csvreader = csv.reader(self.f, delimiter='\t')

        # Check the header row, and start at the first line of variants
        header_row = self.csvreader.next()
        header_row_len = len(header_row)
        if header_row_len == 19: # VarScan bug in the header number of cols
            if header_row != self.COLNAMES[:19]:
                raise ValueError('Header row does not match')
        elif header_row_len == 23:
            if header_row != self.COLNAMES:
                raise ValueError('Header row does not match')
        else:
            raise ValueError('Incorrect number of columns in the header: %i columns found.' % header_row_len)

    def __iter__(self):
        return self

    def next(self):
        row = self.csvreader.next()
        return self.parse_row(row)

    def parse_row(self, row):
        '''
        Given a row list containig the columns of the row,
        return a dict object mapping the colnames to the values
        '''
        return dict(zip(self.COLNAMES, row))

    def filtered_variants(self,
                          somatic_status=None,
                          min_somatic_pval=None,
                          min_dp_normal=None,
                          min_dp_tumor=None):
        '''
        Return a generator that yields filtered variant rows as a list
        '''
        for row in self.csvreader:
            variant = self.parse_row(row)

            # Filter by somatic status
            if somatic_status is not None:
                if variant['somatic_status'].lower() != somatic_status.lower():
                    continue

            # Filter by somatic pvalue
            if min_somatic_pval is not None:
                if float(variant['somatic_p_value']) > min_somatic_pval:
                    continue

            # Filter by normal depth
            if min_dp_normal is not None:
                if int(variant['normal_reads1']) + int(variant['normal_reads2']) < min_dp_normal:
                    continue

            # Filter by tumor depth
            if min_dp_tumor is not None:
                if int(variant['tumor_reads1']) + int(variant['tumor_reads2']) < min_dp_tumor:
                    continue

            # Passed all filter requirements
            yield row

    def generate_annovar_input(self, fout):
        '''
        Given output file handle fout, convert varscan output to annovar input, and write to fout
        '''
        for row in self.csvreader:
            variant = self.parse_row(row)
            chrom = variant['chrom']
            start = int(variant['position'])
            end = start
            ref = variant['ref']
            alt = variant['var']
            # Indels
            if alt[0] == '+': # Insertion
                ref = '-'
                alt = alt.replace('+', '')
            elif alt[0] == '-': # Deletion
                ref = alt.replace('-', '')
                alt = '-'
                start += 1
                end = start + len(ref) - 1
            # Output row
            fout.write('%s\t%s\n' % ('\t'.join([chrom,
                                                str(start),
                                                str(end),
                                                ref,
                                                alt]),
                                     '\t'.join(row)))
    
#     def convert2vcf(self, fout):
#         '''
#         Convert file to vcf format, and write to output file handle object fout
#         '''
#         vcf_writer = VcfWriter(fout)
#         for variant in self:
#             n1 = variant['normal_reads1']
#             n2 = variant['normal_reads2']
#             t1 = variant['tumor_reads1']
#             t2 = variant['tumor_reads2']
#             dp = int(n1) + int(n2) + int(t1) + int(t2)
#             info = 'DP=%i;SOMATIC;SS=2;SSC=6;GPV=1E0;SPV=2.2386E-1' % (dp,)
#             vcf_writer.write_variant(variant['chrom'],
#                                      variant['position'],
#                                      '.',
#                                      variant['ref'],
#                                      variant['var'],
#                                      '.',
#                                      'PASS',
#                                      info,
#                                      'GT:GQ:DP:RD:AD:FREQ:DP4',
#                                      normal,
#                                      tumor)


# class VcfWriter(object):
#     '''
#     Class to handle outputting varscan vcf file
#     '''
         
#     VCF_HEADERS = ["##fileformat=VCFv4.1",
#                    "##source=VarScan2",
#                    "##INFO=<ID=DP,Number=1,Type=Integer,Description=\"Total depth of quality bases\">",
#                    "##INFO=<ID=SOMATIC,Number=0,Type=Flag,Description=\"Indicates if record is a somatic mutation\">",
#                    "##INFO=<ID=SS,Number=1,Type=String,Description=\"Somatic status of variant (0=Reference,1=Germline,2=Somatic,3=LOH, or 5=Unknown)\">",
#                    "##INFO=<ID=SSC,Number=1,Type=String,Description=\"Somatic score in Phred scale (0-255) derived from somatic p-value\">",
#                    "##INFO=<ID=GPV,Number=1,Type=Float,Description=\"Fisher\'s Exact Test P-value of tumor+normal versus no variant for Germline calls\">",
#                    "##INFO=<ID=SPV,Number=1,Type=Float,Description=\"Fisher\'s Exact Test P-value of tumor versus normal for Somatic/LOH calls\">",
#                    "##FILTER=<ID=str10,Description=\"Less than 10% or more than 90% of variant supporting reads on one strand\">",
#                    "##FILTER=<ID=indelError,Description=\"Likely artifact due to indel reads at this position\">",
#                    "##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">",
#                    "##FORMAT=<ID=GQ,Number=1,Type=Integer,Description=\"Genotype Quality\">",
#                    "##FORMAT=<ID=DP,Number=1,Type=Integer,Description=\"Read Depth\">",
#                    "##FORMAT=<ID=RD,Number=1,Type=Integer,Description=\"Depth of reference-supporting bases (reads1)\">",
#                    "##FORMAT=<ID=AD,Number=1,Type=Integer,Description=\"Depth of variant-supporting bases (reads2)\">",
#                    "##FORMAT=<ID=FREQ,Number=1,Type=String,Description=\"Variant allele frequency\">",
#                    "##FORMAT=<ID=DP4,Number=1,Type=String,Description=\"Strand read counts: ref/fwd, ref/rev, var/fwd, var/rev\">",
#                    "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\tNORMAL\tTUMOR"]

#     def __init__(self, fout):
#         self.fo = fout
#         writer = csv.writer(self.fo, delimiter='\t', lineterminator='\n')

#         # Output header
#         self.fo.write('%s\n' % '\n'.join(VCF_HEADERS))
    
#     def write_variant(self, chrom, pos, id, ref, alt, qual, filter, info, format, normal, tumor):
#         '''
#         Output variant record
#         '''
#         writer.writerow([chrom, pos, id, ref, alt, qual, filter, info, format, normal, tumor])




