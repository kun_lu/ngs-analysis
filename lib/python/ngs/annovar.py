#!/usr/bin/env python
'''
Description     : API for parsing annovar files
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.10.08
'''

import csv
import itertools
import os
import sys
from collections import defaultdict
from ngs import varscan

#import pandas
#import pandas.rpy.common as com

class AnnovarCsv(object):
    '''
    Class to handle Annovar output excel csv files
    '''
#     def __init__(self, filename=None):
#         self.colnames = None
#         self.data = None
#         self.fname = filename
#         if self.fname is not None:
#             self.load_data(self.fname)

#     def load_data(self, fname):
#         '''
#         Load data
#         '''
#         with open(fname, 'r') as f:
#             reader = csv.reader(f, dialect='excel')

#             # Read and store the first row of column names
#             header_row = reader.next()

#             # Find index of 'Otherinfo'
#             last_colnum = header_row.index('Otherinfo') + 1
#             self.colnames = header_row[:last_colnum]

#             # Load data
#             self.data = pandas.DataFrame([tuple(row[:last_colnum]) for row in reader],
#                                          columns=self.colnames)
    def __init__(self, filehandle):
        self._f = filehandle
        self.filters = []
        self.reader = csv.reader(filehandle, dialect='excel')
        # Read in column headers
        self.header = None
        self.load_header()

    def __iter__(self):
        return self

    def load_header(self):
        '''
        Load the header row from file
        '''
        if self.header is None:
            self.header = self.reader.next()

    def next(self):
        #return dict(zip(self.header, self.reader.next()))
        return self.reader.next()

    def parse_row(self, row):
        '''
        Return a dictionary mapping column names to row values
        row is a list containing the values of a given row
        '''
        return dict(zip(self.header, row))

    def add_filter(self, filter):
        '''
        Add a filter
        '''
        self.filters.append(filter)

    def filtered_variants(self):
        '''
        Given a list of filters, generate resulting variants that pass all the filters
        '''
        for row in self:
            tests_passed = True
            for filter in self.filters:
                tests_passed = tests_passed and filter.pass_test(dict(zip(self.header, row)))
            if tests_passed:
                yield row

    def count(self, column_name):
        '''
        Given a list of filters, count up all the variants with respect to the values
        of a given column
        '''
        genecolindex = self.header.index('Gene')
        refcolindex = self.header.index(column_name)
        colval2varcounts = defaultdict(int)
        colval2genes = defaultdict(set)
        for row in self:
            colval = row[refcolindex]
            colval2varcounts[colval] += 1

            # Get gene name
            gene = row[genecolindex]
            colval2genes[colval].add(gene)
        return colval2varcounts, colval2genes

class AnnovarCsvFilter(object):
    '''
    Abstract filter class
    '''
    def pass_test(self):
        pass


class AnnovarCsvFilterFactory(object):
    '''
    Create filters for variants in annovar csv files
    '''
    def create_genes_selector(self,genes):
        '''
        Return a filter that tests to see if the variant occurs in a set of genes.
        Filter will pass the test if variant occurs in the gene set.
        '''
        class GeneSelector(AnnovarCsvFilter):
            def pass_test(self, colname2val):
                return colname2val['Gene'] in genes
        return GeneSelector()

    def create_genes_filter(self, genes):
        '''
        Similar to create_genes_selector, but this time the filter will pass the test if variant
        does NOT occur within the gene set
        '''
        class GeneFilter(AnnovarCsvFilter):
            def pass_test(self, colname2val):
                return colname2val['Gene'] not in genes
        return GeneFilter()

    def create_dbSNP_selector(self):
        '''
        Return a filter that tests to see if the variant occurs in dbSNP.
        Filter will pass the test if variant does occur in dbSNP
        '''
        class DbSNPSelector(AnnovarCsvFilter):
            def pass_test(self, colname2val):
                return colname2val['dbSNP135'] and colname2val['dbSNP135'][:2] == 'rs'
        return DbSNPSelector()
        
    def create_dbSNP_filter(self):
        '''
        Return a filter that tests to see if the variant occurs in dbSNP.
        Filter will pass the test if variant does NOT occur in dbSNP
        '''
        class DbSNPFilter(AnnovarCsvFilter):
            def pass_test(self, colname2val):
                return not colname2val['dbSNP135']
        return DbSNPFilter()

    def create_polyphen_pred_selector(self, prediction_set):
        '''
        Return a selector that tests to see if the polyphen prediction is in the
        prediction_set
        '''
        prediction_set = set(prediction_set)
        class PolyphenPredSelector(AnnovarCsvFilter):
            def pass_test(self, colname2val):
                polyphenpred = colname2val['LJB_PolyPhen2_Pred']
                return polyphenpred == '' or polyphenpred in prediction_set
        return PolyphenPredSelector()

    def create_sift_pred_selector(self, prediction_set):
        '''
        Return a selector that tests to see if the sift prediction is in the prediction set
        '''
        prediction_set = set(prediction_set)
        class SIFTPredSelector(AnnovarCsvFilter):
            def pass_test(self, colname2val):
                siftpred = colname2val['LJB_SIFT_Pred']
                return siftpred == '' or siftpred in prediction_set
        return SIFTPredSelector()

    def create_1000Genomes_maf_selector(self, equalval=None, minval=None, maxval=None):
        '''
        Return a filter that tests to see if the variant maf in 1000Genomes is either
        equal to equalval, or is > minval and < maxval
        '''
        if equalval is not None:
            _equalval = float(equalval)
        if minval is not None:
            _minval = float(minval)
        if maxval is not None:
            _maxval = float(maxval)
        class G1000Selector(AnnovarCsvFilter):
            def pass_test(self, colname2val):
                mafval = 0.0
                if colname2val['1000g2010nov_ALL']:
                    mafval = float(colname2val['1000g2010nov_ALL'])
                # Compare equal
                if equalval is not None:
                    return mafval == _equalval

                else:
                    # Compare against minimal accepted value
                    if minval is not None:
                        if mafval < _minval:
                            return False

                    # Compare against maximum accepted value
                    if maxval is not None:
                        if mafval > _maxval:
                            return False
                
                    return True
        return G1000Selector()

class AnnovarVarScan(AnnovarCsv):
    '''
    Class to handle Annovar annotated output (csv) of VarScan
    '''
    
    def load_header(self):
        '''
        Load header row of annovar file, and append header of VarScan output
        '''
        if self.header is None:
            # Read header
            self.header = self.reader.next()
            other_info_idx = self.header.index('Otherinfo')

            # Attach header with varscan header
            varscan_header = varscan.VarScanFile.COLNAMES[:]
            self.header = self.header[:other_info_idx]
            self.header += varscan_header

IUPAC2GENO = {'A':'A/A',
              'C':'C/C',
              'G':'G/G',
              'T':'T/T',
              'R':'A/G',
              'Y':'C/T',
              'S':'C/G',
              'W':'A/T',
              'K':'G/T',
              'M':'A/C'}

class AnnovarVarScanFiles(object):
    '''
    Class to handle many Annovar-annotated VarScan files (AnnovarVarScan objects)
    '''

    def __init__(self, *files):
        '''
        Initialize the collection of file objects
        files is a list of file input handles
        '''
        self.annovar_varscans = [AnnovarVarScan(f) for f in files]

    def get_samplename(self, annovar_varscan):
        '''
        Given a AnnovarVarScan object, get the sample name and return it
        '''
        tmp_ = os.path.split(annovar_varscan._f.name)[-1].split('.')
        samplename = '.'.join(tmp_[:tmp_.index('varscan')])
        return samplename

    def write_pos_report(self, fout=sys.stdout, detailed=False):
        '''
        Output a positional variant report to output stream fout, which is stdout by default
        "detailed" option selects whether to use detailed annotated option or not
        If set to False(default), tool will simply count the sample frequency per mutation position,
        ignoring the variant allele type.
        '''
        # Simple version
        poskey_columns = ('Chr',
                          'Start',
                          'End')

        # Output file handle
        writer = csv.writer(fout, delimiter='\t', lineterminator='\n')

        # Process files
        poskey2samples = defaultdict(set)
        for annovar_varscan in self.annovar_varscans:

            # Get sample name
            sample = self.get_samplename(annovar_varscan)
            
            # Define the poskey for a detailed report
            if detailed:
                poskey_columns = annovar_varscan.header[:annovar_varscan.header.index('chrom')]
                poskey_columns.append(annovar_varscan.header[33]) # normal_gt
                poskey_columns.append(annovar_varscan.header[37]) # tumor_gt

            # Parse each file
            for row in annovar_varscan:
                # Generate key
                row_dict = annovar_varscan.parse_row(row)
                if len(row_dict['normal_gt']) == 1 and row_dict['normal_gt'] in IUPAC2GENO:
                    row_dict['normal_gt'] = IUPAC2GENO[row_dict['normal_gt']]
                if len(row_dict['tumor_gt']) == 1 and row_dict['tumor_gt'] in IUPAC2GENO:
                    row_dict['tumor_gt'] = IUPAC2GENO[row_dict['tumor_gt']]
                poskey = tuple([row_dict[k] for k in poskey_columns])

                # Update sample list for the given mutation
                poskey2samples[poskey].add(sample)

        # Output header row
        writer.writerow(list(poskey_columns) + ['Num_Samples','Samples'])
        # Sorted Output
        def sort_key(k):
            '''
            Sorting key for the output
            '''
            def chrom_priority(c):
                c = c.replace('chr','')
                try:
                    return int(c)
                except ValueError:
                    try:
                        return ord(c)
                    except TypeError:
                        return sys.maxint
            col1 = 0
            col2 = 1
            if detailed:
                col1 = 21
                col2 = 22
            return (chrom_priority(k[col1]),int(k[col2]))
        for poskey in sorted(poskey2samples.keys(), key=sort_key):
            samples = poskey2samples[poskey]
            writer.writerow(list(poskey) + [len(samples), ','.join(sorted(samples))])

    def write_gene_report(self, fout=sys.stdout):
        '''
        Output a gene variant report to output stream fout, which is stdout by default
        '''
        # Initialize counters
        g2func2samples = defaultdict(dict)
        g2func2samplepos = defaultdict(dict)
        funcs = set()
        g2exonicfunc2samples = defaultdict(dict)
        g2exonicfunc2samplepos = defaultdict(dict)
        exonicfuncs = set()

        # Output file handle
        writer = csv.writer(fout, delimiter='\t', lineterminator='\n')

        # Process files
        for annovar_varscan in self.annovar_varscans:

            # Get sample name
            sample = self.get_samplename(annovar_varscan)

            # Parse each file
            for row in annovar_varscan:
                # Extract fields
                row_dict = annovar_varscan.parse_row(row)
                chrom = row_dict['Chr'].replace('chr','')
                pos = row_dict['Start']
                gene = row_dict['Gene']
                func = row_dict['Func'].replace(' ','_')
                exonicfunc = row_dict['ExonicFunc'].replace(' ','_')

                # Update counts
                funcs.add(func)

                # Update samples
                g2func2samples[gene].setdefault(func, set()).add(sample)

                # Update samplepos
                samplepos = ':'.join([sample, chrom, pos])
                g2func2samplepos[gene].setdefault(func, set()).add(samplepos)
                
                # Exonic function counts
                if func == 'exonic':
                    exonicfuncs.add(exonicfunc)
                    g2exonicfunc2samples[gene].setdefault(exonicfunc, set()).add(sample)
                    g2exonicfunc2samplepos[gene].setdefault(exonicfunc, set()).add(samplepos)
                    
        # Output header row
        funcs = sorted(funcs)
        exonicfuncs = sorted(exonicfuncs)
        func_count_sample_cols1 = ['%s_Num_Samples' % ft for ft in funcs]
        func_count_sample_cols2 = ['%s_Sample_Chrom_Pos' % ft for ft in funcs]
        func_count_cols = list(itertools.chain(*zip(funcs,
                                                    func_count_sample_cols1,
                                                    func_count_sample_cols2)))
        exonicfunc_count_sample_cols1 = ['%s_Num_Samples' % ft for ft in exonicfuncs]
        exonicfunc_count_sample_cols2 = ['%s_Sample_Chrom_Pos' % ft for ft in exonicfuncs]
        exonicfunc_count_cols = list(itertools.chain(*zip(exonicfuncs,
                                                          exonicfunc_count_sample_cols1,
                                                          exonicfunc_count_sample_cols2)))
        writer.writerow(['Gene'] + func_count_cols + exonicfunc_count_cols + ['Total',
                                                                              'Total_Num_Samples',
                                                                              'Total_Sample_Chrom_Pos'])
        # Output counts per gene
        for g in sorted(g2func2samples.keys()):
            # Start with gene name
            output_line_items = [g]

            # Functional classes counts
            total = 0
            total_samples = set()
            total_samplepos = set()
            for func in funcs:
                # Output counts for each functional region
                samples = g2func2samples[g].setdefault(func, set())
                samplepos = g2func2samplepos[g].setdefault(func, set())
                varcounts = len(samplepos)
                samplecounts = len(samples)
                output_line_items.append(str(varcounts))
                output_line_items.append(str(samplecounts))
                output_line_items.append(','.join(sorted(samplepos)))

                # Update total counts
                total += varcounts
                total_samples.update(samples)
                total_samplepos.update(samplepos)

            # Exonic functional classes counts
            for exonicfunc in exonicfuncs:
                # Output counts for each exonicfunctional region
                samples = g2exonicfunc2samples[g].setdefault(exonicfunc, set())
                samplepos = g2exonicfunc2samplepos[g].setdefault(exonicfunc, set())
                varcounts = len(samplepos)
                samplecounts = len(samples)
                output_line_items.append(str(varcounts))
                output_line_items.append(str(samplecounts))
                output_line_items.append(','.join(sorted(samplepos)))

            # Total counts
            output_line_items.append(str(total))
            output_line_items.append(str(len(total_samples)))
            output_line_items.append(','.join(sorted(total_samplepos)))

            # Write output
            writer.writerow(output_line_items)
