#!/usr/bin/env python
'''
Description     : API for parsing vcf files
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.10.30
'''

import csv
import re
import sys
from collections import defaultdict,namedtuple

class VcfFile(object):
    '''
    Class to handle vcf formatted files
    '''
    
    # Column names described in header line
    column_names = None

    def __init__(self, filehandle):
        self._f = filehandle
        self.jumped2variants = False

    def __iter__(self):
        '''
        Iterator method
        '''
        return self

    def is_meta(self, line):
        '''
        Check to see if line is meta
        '''
        if line[0:2] == '##':
            return True
        return False

    def is_header(self, line):
        '''
        Check to see if line is header line
        '''
        if line[0:1] != '#':
            return False

        if line[0:2] == '##':
            return False
        
        la = line[1:].strip().split()
        # Just check the first 3 columns
        if la[0] != 'CHROM':
            return False
        if la[1] != 'POS':
            return False
        if la[2] != 'ID':
            return False
        return True

    def set_column_names(self, header_line):
        '''
        Parse the header line and set the column names
        '''
        # Check that the line is header line
        if not self.is_header(header_line):
            raise ValueError('This is not a header line:\n\t%s\n' % header_line)

        # Parse the line
        self.column_names = header_line[1:].strip().split()

    def jump2variants(self, outfile=None):
        '''
        Skip ahead to the variants section of the vcf file
        The column names in the header line will be set as an attribute
        The next readline() call will read the first variant line
        in the file
        Warning: should only be used once, at the beginning of the file
        '''
        if self.jumped2variants:
            return
        
        while True:
            line = self.readline()
            if outfile is not None:
                outfile.write(line)
            if not self.is_meta(line):
                self.set_column_names(line)
                break
        self.jumped2variants = True
        return

    def is_variant_line(self, line):
        '''
        Validate a variant line string
        '''
        if self.is_meta(line):
            return False
        if self.is_header(line):
            return False
        if len(line.strip().split()) != len(self.column_names):
            return False
        return True

    def next(self):
        '''
        Iterator method
        '''
        return self._f.next()
                
    def readline(self):
        '''
        Emulate the file object readline() method
        '''
        return self._f.readline()

    def read_variant(self):
        '''
        Parse the variant line, and call the parse_line method and return the
        resulting output
        NOTE:
          - This function will cause this iterator to move forward, since
            readline() will be called.
        '''
        # Read in the next variant line
        line = self.readline()

        # Check if end of file is reached
        if not line:
            raise StopIteration

        # Generate dictionary and return it
        return self.parse_line(line)

    def parse_line(self, line):
        '''
        Parse a line of vcf variant and return a dictionary mapping column names
        to variant values
        NOTE:
          - If the next line to read in is not a variant line, the function will
            raise an error.
        '''
        # Ensure that the line is a variant line
        if not self.is_variant_line(line):
            raise ValueError('This is not a variant line:\n\t%s\n' % line)

        return dict(zip(self.column_names, line.strip().split()))

    def parse_info(self, variant):
        '''
        For those field=val pairs in the info string that contain = sign,
        Generate a dictionary that maps the info column fields to their
        corresponding values and set it as an attribute of this object.
        Return the ones that do not have the = sign in a separate list.
        Input: variant dictionary returned by the read_variant() method
               or the parse_line(line) method
        '''
        # Separate the fields within the info string
        equal_sign = []
        no_equal_sign = []
        for val in variant['INFO'].split(';'):
            if re.search(r'=', val):
                equal_sign.append(val)
            else:
                no_equal_sign.append(val)

        # Check equal_sign empty
        if not equal_sign:
            return None, no_equal_sign

        # Generate the mapping dictionary
        return dict(map(lambda field_eq_val: field_eq_val.split('='),
                        equal_sign)), no_equal_sign

    def get_sample_names(self):
        '''
        Return the sample names as a list in the column order that they appear
        Note: The header line must have been read in.
        '''
        return self.column_names[9:]

    def parse_samples(self, variant):
        '''
        Generate a mapping from format fields to each sample\'s values
        Return a 2D dictionary mapping sample2formatfield2val

        Input: variant dictionary returned by the read_variant() method or the
               parse_line(line) method
        '''
        sample2field2val = {}
        formats = variant['FORMAT'].split(':')
        for sample in self.get_sample_names():
            sample2field2val[sample] = dict(zip(variant['FORMAT'].split(':'), variant[sample].split(':')))
        return sample2field2val

    def get_phase_separator(self, phased=False):
        '''
        Based on whether genotype data is phased or not return the correct separator
        '''
        return '|' if phased else '/'

    def samples_gt_categories(self, sample2field2val, phased=False):
        '''
        Return list of uncalled, affected, and unaffected samples
        '''
        # Set separator
        sep = self.get_phase_separator(phased)

        gt_uncalled = sep.join(['.','.'])
        gt_homo_ref = sep.join(['0','0'])

        # Initialize return lists
        uncalled = []
        homo_ref = []
        heterozy = []
        homo_alt = []

        # Loop through samples, and categorize each
        for sample in sample2field2val:
            alleles_str = sample2field2val[sample]['GT']
            # uncalled
            if alleles_str == gt_uncalled:
                uncalled.append(sample)
            # homo ref
            elif alleles_str == gt_homo_ref:
                homo_ref.append(sample)
            # affected 
            else:
                # heterozygous
                if '0' in alleles_str.split(sep):
                    heterozy.append(sample)
                # homo alt
                else:
                    homo_alt.append(sample)
                    
        return uncalled, homo_ref, heterozy, homo_alt

    def get_sample_gt(self, variant, sample, phased=False):
        '''
        Get a sample\'s genotype.
        The GT field shows alleles, not the actual nucleotide bases.
        Substitute the nucleotide bases into the numeric alleles.
        
        Given an allele string, convert to genotype string
        i.e.  0/0, 0/1, 1|1 => A/A, A/C, C|C
        Separator:
          /: unphased
          |: phased

        Input:
          variant: dictionary returned by the read_variant() or the parse_line(line) methods
          sample: sample name
          phased: True | False, default False
          
        '''
        # Set separator
        sep = self.get_phase_separator(phased)

        # Get sample GT value
        sample2field2val = self.parse_samples(variant)
        alleles_str = sample2field2val[sample]['GT']
        alleles = alleles_str.split(sep)

        possible_genotypes = [variant['REF']] +  variant['ALT'].split(',')

        # Get ref/alt alleles
        bases = []
        for a in alleles:
            # No Call
            if a == '.':
                bases.append('N')
            # Allele index
            else:
                bases.append(possible_genotypes[int(a)])

        # For unphased, sort the genotype bases
        if not phased:
            bases = sorted(bases)
        return sep.join(bases)

    def is_sample_hetero(self, variant, sample, phased=False):
        '''
        Return true of sample genotype is hetero
        '''
        sep = self.get_phase_separator(phased)
        gt_alleles = set(self.get_sample_gt(variant, sample, phased).split(sep))
        return len(gt_alleles) > 1 and variant['REF'] in gt_alleles

    def is_sample_homo_ref(self, variant, sample, phased=False):
        '''
        Return true if sample genotype is homo ref
        '''
        sep = self.get_phase_separator(phased)
        gt_alleles = set(self.get_sample_gt(variant, sample, phased).split(sep))
        return len(gt_alleles) == 1 and variant['REF'] in gt_alleles

    def is_sample_homo_alt(self, variant, sample, phased=False):
        '''
        Return true if sample genotype is homo alt
        '''
        sep = self.get_phase_separator(phased)
        gt_alleles = set(self.get_sample_gt(variant, sample, phased).split(sep))
        return variant['REF'] not in gt_alleles

    def is_sample_affected(self, variant, sample, phased=False):
        '''
        Return true if sample genotype is homo alt or hetero
        '''
        return self.is_sample_homo_alt(variant, sample, phased) or self.is_sample_hetero(variant, sample, phased)

    # def load_pos2memory_set2(self):
    #     '''
    #     Load all the variant positions to memory
    #     Returns a set of tuples (chrom, pos), where chrom is in string format
    #     and pos is in int format (1-based)
    #     '''
    #     variant_positions = set()
    #     self.jump2variants()
    #     for line in self:
    #         variant = self.parse_line(line)
    #         variant_positions.add((variant['CHROM'], int(variant['POS'])))
    #     return variant_positions

    def load_pos2memory_dict(self):
        '''
        Load all variant positions to memory
        Return a dict mapping chromosome to a list of positions
        Also return a dict mapping (chrom, pos) to (ref, alt)
        Chromosome is in string format
        Positions are a list of ints (1-based)
        '''
        chrom2pos = defaultdict(list)
        coord2alleles = {}
        self.jump2variants()
        for line in self:
            variant = self.parse_line(line)
            chrom = variant['CHROM']
            pos = int(variant['POS'])
            chrom2pos[chrom].append(pos)
            coord2alleles[(chrom,pos)] = (variant['REF'],variant['ALT'])
        return chrom2pos, coord2alleles

    def load_pos2memory_set(self, include_alleles=False):
        '''
        Load all variant positions as a set of tuples to memory
        '''
        variant_positions = set()
        self.jump2variants()
        for line in self:
            variant = self.parse_line(line)
            chrom = variant['CHROM']
            pos = int(variant['POS'])
            v = [chrom, pos]
            if include_alleles:
                ref = variant['REF']
                alt = variant['ALT']
                v += [ref, alt]
            variant_positions.add(tuple(v))
        return variant_positions

    def count_variants(self, unique_only=False):
        '''
        Count the number of variants
        '''
        self.jump2variants()

        # Count non-unique
        if not unique_only:
            n = 0
            for line in self:
                n += 1
            return n
        
        # Count only unique variants (chr,pos,ref,alt)
        unique_variants = set()
        for line in self:
            variant = self.parse_line(line)
            unique_variants.add((variant['CHROM'],
                                 variant['POS'],
                                 variant['REF'],
                                 variant['ALT']))
        return len(unique_variants)
    
class VarscanVcfFile(VcfFile):
    '''
    Extension of the VcfFile class for parsing VarScan output vcf files
    '''
    SOMATIC_STATUS_CODE2TEXT = {'0': 'Reference', 
                                '1': 'Germline',
                                '2': 'Somatic',
                                '3': 'LOH',
                                '5': 'Unknown'}


class SnpEffVcfFile(VarscanVcfFile):
    '''
    Extension of the VarscanVcfFile for parsing SNPEff output vcf files
    '''
    IMPACT_EFFECTS = [('High','SPLICE_SITE_ACCEPTOR'),
                      ('High','SPLICE_SITE_DONOR'),
                      ('High','START_LOST'),
                      ('High','EXON_DELETED'),
                      ('High','FRAME_SHIFT'),
                      ('High','STOP_GAINED'),
                      ('High','STOP_LOST'),
                      ('Moderate','NON_SYNONYMOUS_CODING'),
                      ('Moderate','CODON_CHANGE'),
                      ('Moderate','CODON_INSERTION'),
                      ('Moderate','CODON_CHANGE_PLUS_CODON_INSERTION'),
                      ('Moderate','CODON_DELETION'),
                      ('Moderate','CODON_CHANGE_PLUS_CODON_DELETION'),
                      ('Moderate','UTR_5_DELETED'),
                      ('Moderate','UTR_3_DELETED'),
                      ('Low','SYNONYMOUS_START'),
                      ('Low','NON_SYNONYMOUS_START'),
                      ('Low','START_GAINED'),
                      ('Low','SYNONYMOUS_CODING'),
                      ('Low','SYNONYMOUS_STOP'),
                      ('Low','NON_SYNONYMOUS_STOP'),
                      ('Modifier','UTR_5_PRIME'),
                      ('Modifier','UTR_3_PRIME'),
                      ('Modifier','REGULATION'),
                      ('Modifier','UPSTREAM'),
                      ('Modifier','DOWNSTREAM'),
                      ('Modifier','GENE'),
                      ('Modifier','TRANSCRIPT'),
                      ('Modifier','EXON'),
                      ('Modifier','INTRON_CONSERVED'),
                      ('Modifier','INTRON'),
                      ('Modifier','INTRAGENIC'),
                      ('Modifier','INTERGENIC'),
                      ('Modifier','INTERGENIC_CONSERVED'),
                      ('Modifier','NONE'),
                      ('Modifier','CHROMOSOME'),
                      ('Modifier','CUSTOM'),
                      ('Modifier','CDS')]
    effects_prioritized = ['RARE_AMINO_ACID',
                           'SPLICE_SITE_ACCEPTOR',
                           'SPLICE_SITE_DONOR',
                           'START_LOST',
                           'EXON_DELETED',
                           'FRAME_SHIFT',
                           'STOP_GAINED',
                           'STOP_LOST',
                           'NON_SYNONYMOUS_CODING',
                           'CODON_CHANGE',
                           'CODON_INSERTION',
                           'CODON_CHANGE_PLUS_CODON_INSERTION',
                           'CODON_DELETION',
                           'CODON_CHANGE_PLUS_CODON_DELETION',
                           'UTR_5_DELETED',
                           'UTR_3_DELETED',
                           'SYNONYMOUS_START',
                           'NON_SYNONYMOUS_START',
                           'START_GAINED',
                           'SYNONYMOUS_CODING',
                           'SYNONYMOUS_STOP',
                           'NON_SYNONYMOUS_STOP',
                           'UTR_5_PRIME',
                           'UTR_3_PRIME',
                           'REGULATION',
                           'UPSTREAM',
                           'DOWNSTREAM',
                           'GENE',
                           'TRANSCRIPT',
                           'EXON',
                           'INTRON_CONSERVED',
                           'INTRON',
                           'INTRAGENIC',
                           'INTERGENIC',
                           'INTERGENIC_CONSERVED',
                           'NONE',
                           'CHROMOSOME',
                           'CUSTOM',
                           'CDS',
                           '']
    effect2priority = None

    class GeneTranscriptSummary(object):
        def __init__(self, gene, transcript):
            self.gene = gene
            self.transcript = transcript
            self.variant_samplepos = set()
            self.samples_affected = set()
            self.samples_hetero = set()
            self.samples_homo_alt = set()
            self.samples_homo_ref = set()
            self.effects2samplepos = defaultdict(set)

        def get_alleles_set(self, alleles_str, delimiter='/'):
            return set(alleles_str.split(delimiter))

        def is_hetero(self, alleles_str):
            alleles = self.get_alleles_set(alleles_str)
            return len(alleles) > 1 and '0' in alleles

        def is_homo_ref(self, alleles_str):
            alleles = self.get_alleles_set(alleles_str)
            return len(alleles) == 1 and '0' in alleles

        def is_homo_alt(self, alleles_str):
            return '0' not in self.get_alleles_set(alleles_str) and '.' not in self.get_alleles_set(alleles_str)

        def is_affected(self, alleles_str):
            return self.is_homo_alt(alleles_str) or self.is_hetero(alleles_str)

        def get_unique_variant_sites(self):
            '''
            Based on variant_samplepos, return a list of unique chromosome coordinates of the variants
            '''
            return set(((chrom,pos) for sample,chrom,pos in self.variant_samplepos))

        def samplepos_as_str(self, samplepos_mult):
            '''
            Format samplepos tuple (sample, chrom, pos) into string sample:chrom:pos
            Multiple samplepos values are comma-delimited
            '''
            return ','.join((':'.join(samplepos) for samplepos in sorted(samplepos_mult)))
            
        def variant_samplepos_as_str(self):
            '''
            Return variant_samplepos formatted as a string
            '''
            return self.samplepos_as_str(self.variant_samplepos)

        def effects2samplepos_str(self, effect):
            '''
            Return the samplepos formatted as a string for the given effect in effects2samplepos
            '''
            return self.samplepos_as_str(self.effects2samplepos[effect])
        
    Effect = namedtuple('Effect', ['effect',
                                   'impact',
                                   'functional_class',
                                   'codon_change',
                                   'aa_change',
                                   'aa_len',
                                   'gene',
                                   'gene_biotype',
                                   'coding',
                                   'transcript',
                                   'exon',
                                   'genotype_num']) # Errors, Warnings

    NO_CALLS = set(['N/N','N|N','NN','./.','.|.','.'])

    def __init__(self, filehandle):
        self.samples2gts = None
        self.samples2dps = None
        self.samples2gqs = None
        self.variant2gene = None
        self.variant_filters = []
        super(SnpEffVcfFile, self).__init__(filehandle)

    def _set_effect2priority(self):
        '''
        Set the dictionary mapping effect to its priority number
        Priority number is based on the index of each effect in
        the effects_prioritized attribute
        '''
        self.effect2priority = dict(zip(self.effects_prioritized, range(len(self.effects_prioritized))))
    
    def set_prioritized_effects(self, effectslist):
        '''
        Setter for effects_prioritized attribute
        Will also set the effects2priority variable
        Input:
          effectslist: list of effects
        '''
        self.effects_prioritized = effectslist
        self._set_effect2priority()

    def get_effects(self, infostr):
        '''
        Parse the info string and extract the annotated effects
        '''
        # If effect2priority is not set, set it
        if self.effect2priority is None:
            self._set_effect2priority()

        effect_info_str = re.search(r';?EFF=([^;]+)', infostr).group(1)
        effect_strs = effect_info_str.split(',')
        effects = []
        for effect_str in effect_strs:
            effect_val = re.search('(.+)\(', effect_str).group(1)
            effect_attrs = [effect_val] + re.search('\((.+)\)', effect_str).group(1).split('|')
            
            # Skip annotations with errors and warnings
            if len(effect_attrs) > len(self.Effect._fields):
                continue

            # Create namedtuple object
            effect = self.Effect(*effect_attrs[:len(self.Effect._fields)])
            effects.append(effect)
        return sorted(effects, key=lambda eff: self.effect2priority[eff.effect])

    def get_effect(self, infostr):
        '''
        Return the first effect object from get_effects method
        '''
        effects = self.get_effects(infostr)
        if not effects:
            return None
        return effects[0]

    def parse_effects(self, variant):
        '''
        Parse the info column string in the vcf file, and extract
        a list of Effects objects sorted by their priority
        Inputs
          variant: dictionary returned by the read_variant() or
                   the parse_line(line) method
        '''
        return self.get_effects(variant['INFO'])

    def select_highest_priority_effect(self, variant):
        '''
        Parse the info column string in the vcf file, and extract the highest
        priority effect as defined by the effects_prioritized attribute

        Inputs
          variant: dictionary returned by the read_variant() or the
                   parse_line(line) method
        '''        
        # Parse the effects
        effects = self.parse_effects(variant)

        # If there were no valid effects, return None
        if not effects:
            return None

        # Return the first element, which has the highest priority
        return effects[0]

    def find_selected_transcript_effects(self, variant, g2t):
        '''
        Given a variant (output of parse_line) and a mapping from gene to selected transcripts (g2t)
        find all transcripts within the variant annotation that were selected for the annotated genes
        '''
        found_effects = []
        for effect in self.parse_effects(variant):
            if effect.gene in g2t:
                if effect.transcript == g2t[effect.gene]:
                    found_effects.append(effect)
        return found_effects

    def add_variant_filter(self, filter):
        '''
        Add a filter
        '''
        self.variant_filters.append(filter)

    def filtered_variants(self):
        '''
        Generate variants that are filtered
        '''
        for line in self:
            variant = self.parse_line(line)
            tests_passed = True
            for filter in self.variant_filters:
                tests_passed = tests_passed and filter.pass_test(self, variant)
            if tests_passed:
                yield line

    def load_samples2gts(self, no_chr=True, snpeff=False):
        '''
        Load and return a dictionary of sample genotypes for each variant position
        ex: geno[samplename][(chrom,pos,ref,alt)] = C/G
        '''
        # First, jump to variants
        self.jump2variants()

        # Get the sample names
        samples = self.get_sample_names()

        # Load the genotypes, dp, and gene symbol to memory
        self.samples2gts = defaultdict(dict)
        self.samples2dps = defaultdict(dict)
        self.samples2gqs = defaultdict(dict)
        if snpeff:
            self.variant2gene = {}
        for line in self:
            variant = self.parse_line(line)
            sample2field2val = self.parse_samples(variant)
            for s in samples:
                vpos = (variant['CHROM'].replace('chr','') if no_chr else variant['CHROM'],
                        variant['POS'],
                        variant['REF'],
                        variant['ALT'])
                self.samples2gts[s][vpos] = self.get_sample_gt(variant, s) # TODO: Phased parameter
                dp = sample2field2val[s].get('DP', None)
                gq = sample2field2val[s].get('GQ', None)
                self.samples2dps[s][vpos] = int(dp) if dp is not None else None
                try:
                    self.samples2gqs[s][vpos] = float(gq)
                except (ValueError,TypeError):
                    self.samples2gqs[s][vpos] = None
                if snpeff:
                    if vpos not in self.variant2gene:
                        effect = self.select_highest_priority_effect(variant)
                        self.variant2gene[vpos] = effect.gene if effect is not None else ''

        returnvals = [self.samples2gts, self.samples2dps, self.samples2gqs]
        if snpeff:
            returnvals.append(self.variant2gene)
        return returnvals

    def get_concordance(self, other_vcffile, phased=False, min_dp=None, min_gq=None, snpeff=False, match_samples=False):
        '''
        Generate concordance between the overlapping
        sample genotypes with another vcf file data.
        other_vcffile is an instance of this class
        '''
        # Set separator
        sep = self.get_phase_separator(phased)
        
        # If the genotypes are not loaded, load them
        if self.samples2gts is None:
            self.load_samples2gts(snpeff=snpeff)
        if other_vcffile.samples2gts is None:
            other_vcffile.load_samples2gts(snpeff=snpeff)

        # Get sample names and overlapping positions
        self_samples = self.get_sample_names()
        other_samples = other_vcffile.get_sample_names()
        self_vpos = self.samples2gts[self_samples[0]].keys()
        other_vpos = other_vcffile.samples2gts[other_samples[0]].keys()
        overlapping_positions = sorted(set(self_vpos) & set(other_vpos))

        # Count the matches and mismatches
        samples2counts = defaultdict(lambda : defaultdict(int))
        samples2cat = defaultdict(lambda : defaultdict(set))
        if match_samples:
            sample_pairs = [(s1,s2) for s1 in self_samples for s2 in other_samples if s1 == s2]
        else:
            sample_pairs = [(s1,s2) for s1 in self_samples for s2 in other_samples]
                
        for s1,s2 in sample_pairs:
            for vpos in overlapping_positions:
                # Get the genotypes
                self_geno = self.samples2gts[s1][vpos]
                other_geno = other_vcffile.samples2gts[s2][vpos]

                # No call ==================================================
                
                self_geno_nocall = self_geno in self.NO_CALLS
                other_geno_nocall = other_geno in self.NO_CALLS
                if self_geno_nocall or other_geno_nocall:
                    # Union
                    samples2counts[(s1,s2)]['nocalls_union'] += 1
                    samples2cat[(s1,s2)][vpos].add('nocalls_union')
                    # Both nocall
                    if self_geno_nocall and other_geno_nocall:
                        samples2counts[(s1,s2)]['nocalls_both'] += 1
                        samples2cat[(s1,s2)][vpos].add('nocalls_both')
                    # Only f1
                    elif self_geno_nocall:
                        samples2counts[(s1,s2)]['nocalls_vcf1_only'] += 1
                        samples2cat[(s1,s2)][vpos].add('nocalls_vcf1_only')
                    # Only f2
                    else:
                        samples2counts[(s1,s2)]['nocalls_vcf2_only'] += 1
                        samples2cat[(s1,s2)][vpos].add('nocalls_vcf2_only')
                    continue

                # Min DP ===================================================
                if min_dp is not None:
                    self_dp = self.samples2dps[s1][vpos]
                    other_dp = other_vcffile.samples2dps[s2][vpos]
                    self_dp_low = self_dp < min_dp
                    other_dp_low = other_dp < min_dp
                    if self_dp_low or other_dp_low:
                        # Union
                        samples2counts[(s1,s2)]['nocalls_union'] += 1
                        samples2cat[(s1,s2)][vpos].add('nocalls_union')
                        # Both nocall
                        if self_dp_low and other_dp_low:
                            samples2counts[(s1,s2)]['nocalls_both'] += 1
                            samples2cat[(s1,s2)][vpos].add('nocalls_both')
                        # Only f1
                        elif self_dp_low:
                            samples2counts[(s1,s2)]['nocalls_vcf1_only'] += 1
                            samples2cat[(s1,s2)][vpos].add('nocalls_vcf1_only')
                        # Only f2
                        else:
                            samples2counts[(s1,s2)]['nocalls_vcf2_only'] += 1
                            samples2cat[(s1,s2)][vpos].add('nocalls_vcf2_only')
                        continue
                    
                # Min GQ ===================================================
                if min_gq is not None:
                    self_gq = self.samples2gqs[s1][vpos]
                    other_gq = other_vcffile.samples2gqs[s2][vpos]
                    self_gq_low = self_gq < min_gq
                    other_gq_low = other_gq < min_gq
                    if self_gq_low or other_gq_low:
                        # Union
                        samples2counts[(s1,s2)]['nocalls_union'] += 1
                        samples2cat[(s1,s2)][vpos].add('nocalls_union')
                        # Both nocall
                        if self_gq_low and other_gq_low:
                            samples2counts[(s1,s2)]['nocalls_both'] += 1
                            samples2cat[(s1,s2)][vpos].add('nocalls_both')
                        # Only f1
                        elif self_gq_low:
                            samples2counts[(s1,s2)]['nocalls_vcf1_only'] += 1
                            samples2cat[(s1,s2)][vpos].add('nocalls_vcf1_only')
                        # Only f2
                        else:
                            samples2counts[(s1,s2)]['nocalls_vcf2_only'] += 1
                            samples2cat[(s1,s2)][vpos].add('nocalls_vcf2_only')
                        continue

                # COMPARE
                # Increment num variants compared
                samples2counts[(s1,s2)]['genotypes_compared'] += 1
                samples2cat[(s1,s2)][vpos].add('genotypes_compared')

                # Match ===================================================
                self_geno_alleles = self_geno.split(sep)
                other_geno_alleles = other_geno.split(sep)
                self_geno_homo = self_geno_alleles[0] == self_geno_alleles[1]
                other_geno_homo = other_geno_alleles[0] == other_geno_alleles[1]
                
                # If match
                if self_geno == other_geno:
                    # Homo match
                    if self_geno_homo:
                        samples2counts[(s1,s2)]['match_homo'] += 1
                        samples2cat[(s1,s2)][vpos].add('match_homo')
                    # Hetero match
                    else:
                        samples2counts[(s1,s2)]['match_hetero'] += 1
                        samples2cat[(s1,s2)][vpos].add('match_hetero')
                    continue

                # Mismatch ================================================
                if self_geno_homo and other_geno_homo:
                    samples2counts[(s1,s2)]['mismatch_homo_homo'] += 1
                    samples2cat[(s1,s2)][vpos].add('mismatch_homo_homo')
                elif not self_geno_homo and not other_geno_homo:
                    samples2counts[(s1,s2)]['mismatch_hetero_hetero'] += 1
                    samples2cat[(s1,s2)][vpos].add('mismatch_hetero_hetero')
                elif self_geno_homo and not other_geno_homo:
                    samples2counts[(s1,s2)]['mismatch_homo_hetero'] += 1
                    samples2cat[(s1,s2)][vpos].add('mismatch_homo_hetero')
                elif not self_geno_homo and other_geno_homo:
                    samples2counts[(s1,s2)]['mismatch_hetero_homo'] += 1
                    samples2cat[(s1,s2)][vpos].add('mismatch_hetero_homo')
                else:
                    raise ValueError('Genotype mismatch is neither homo_homo, hetero_hetero, homo_hetero, or hetero_homo\n')
        returnvals = [samples2counts,
                      self_samples,
                      self_vpos,
                      other_samples,
                      other_vpos,
                      overlapping_positions]
        if snpeff:
            returnvals.append(self.variant2gene)
            returnvals.append(samples2cat)
        return returnvals

    def summarize_gene_transcript(self):
        '''
        Summarize the variants by gene transcripts
        Return gene_transcript2summary, effects_all

        gene_transcript2summary maps (gene, transcript) tuples to GeneTranscriptSummary objects
        effects_all is a set of all the effects found within this vcf file

        samplepos are tuples of the form: (sample, chrom, pos)
        '''            
        # Keep track of all effects
        effects_all = set()

        # Parse vcf file
        gene_transcript2summary = {}
        self.jump2variants()
        samples = self.get_sample_names()        
        for line in self:
            variant = self.parse_line(line)
            sample2field2val = self.parse_samples(variant)
            effect = self.select_highest_priority_effect(variant)
            if effect is None or not effect:
                continue
            effects_all.add(effect.effect)

            gene_transcript = (effect.gene, effect.transcript)
            summary = gene_transcript2summary.setdefault(gene_transcript,
                                                         self.GeneTranscriptSummary(*gene_transcript))

            # Count up all the sample genotype information
            for sample in samples:
                alleles_str = sample2field2val[sample]['GT']
                samplepos = (sample, variant['CHROM'], variant['POS'])

                if summary.is_affected(alleles_str):
                    summary.samples_affected.add(sample)
                    summary.variant_samplepos.add(samplepos)
                    summary.effects2samplepos[effect.effect].add(samplepos)
                if summary.is_hetero(alleles_str):
                    summary.samples_hetero.add(sample)
                if summary.is_homo_alt(alleles_str):
                    summary.samples_homo_alt.add(sample)
                if summary.is_homo_ref(alleles_str):
                    summary.samples_homo_ref.add(sample)
        return gene_transcript2summary, effects_all
            
    def data_frame(self):
        '''
        Load vcf and return pandas dataframe
        '''
        import pandas as pd

        self.jump2variants()
        data = []
        for line in self:
            data.append(line.strip().split())
        return pd.DataFrame(data, columns=self.column_names)

class VariantFilterFactory(object):

    class VariantFilter(object):
        '''
        Abstract filter class
        '''
        def pass_test(self):
            pass

    def create_diff_gt_selector(self, sample1, sample2):
        '''
        Selects variants with different genotypes between 2 samples
        '''
        class DiffGTSelector(VariantFilterFactory.VariantFilter):
            def pass_test(self, vcffile, variant):
                sample2field2val = vcffile.parse_samples(variant)
                return sample2field2val[sample1]['GT'] != sample2field2val[sample2]['GT']
        return DiffGTSelector()

    def create_all_hetero_selector(self):
        '''
        Return a selector that
        selects variants where all samples have heterozygous genotype calls
        '''
        class AllHeteroGTSelector(VariantFilterFactory.VariantFilter):
            def pass_test(self, vcffile, variant):
                for sample in vcffile.get_sample_names():
                    if not vcffile.is_sample_hetero(variant, sample):
                        return False
                return True
        return AllHeteroGTSelector()

    def create_all_homo_ref_selector(self):
        '''
        Return a selector that
        selects variants where all samples have homo ref genotype calls
        '''
        class AllHomoRefGTSelector(VariantFilterFactory.VariantFilter):
            def pass_test(self, vcffile, variant):
                for sample in vcffile.get_sample_names():
                    if not vcffile.is_sample_homo_ref(variant, sample):
                        return False
                return True
        return AllHomoRefGTSelector()

    def create_all_homo_alt_selector(self):
        '''
        Return a selector that selects variants where all samples have homo alt genotype calls
        '''
        class AllHomoAltGTSelector(VariantFilterFactory.VariantFilter):
            def pass_test(self, vcffile, variant):
                for sample in vcffile.get_sample_names():
                    if not vcffile.is_sample_homo_alt(variant, sample):
                        return False
                return True
        return AllHomoAltGTSelector()

    def create_all_samples_affected_selector(self):
        '''
        Return a selector that selects variants where all samples are affected
        '''
        class AllSamplesAffectedSelector(VariantFilterFactory.VariantFilter):
            def pass_test(self, vcffile, variant):
                for sample in vcffile.get_sample_names():
                    if not vcffile.is_sample_affected(variant, sample):
                        return False
                return True
        return AllSamplesAffectedSelector()
            
    def create_nocalls_filter(self, type='ANY'):
        '''
        Filter out variants where there is a no call
        if type is ANY, then if any of the samples have a no call at this position,
        filter out the position.
        if type is ALL, then if all of the samples have a no call at this position,
        filter out the position.
        '''
        class NoCallsFilter(VariantFilterFactory.VariantFilter):
            def pass_test(self, vcffile, variant):
                genotypes = []
                for s in vcffile.get_sample_names():
                    genotypes.append(vcffile.get_sample_gt(variant, s))

                if type == 'ANY':
                    any_nc = False
                    for gt in genotypes:
                        if gt in vcffile.NO_CALLS:
                            any_nc = True
                    return not any_nc
                else: # ALL
                    all_nc = True
                    for gt in genotypes:
                        if gt not in vcffile.NO_CALLS:
                            all_nc = False
                    return not all_nc
        return NoCallsFilter()
                
    

# ------------------------------------------------------------------------------------- #
# Classes to handle a list of vcf files

class VcfFiles(list):
    '''
    Class to manage multiple VcfFile objects
    '''
    pass

class SnpEffVcfFiles(VcfFiles):
    '''
    Class to manage multiple SnpEffVcfFile objects
    Generate gene2transcript2effect2counts (g2t2e2c)
    Also updates a dictionary of effects to impacts
    '''
#     g2t2e2c = None
#     eff2imp = None
#     transcript2len = None
    
    def count_transcript_effects_single(self, vcffile, g2t2e2c, effect2impact, highest_priority=False):
        '''
        For a single vcf file, update the counts of the total number of transcripts and their effects
        g2t2e2c is a dictionary of counts for the transcript effects counts.
        effect22impact is a dictionary mapping effect to their corresponding impact.
        If highest_priority is set to True, count only the highest priority effect per variant.
        By default, will count all the transcript effects for each variant.
        '''
        vcffile.jump2variants()
        for line in vcffile:
            variant = vcffile.parse_line(line)

            # Use single highest priority, or all the transcript effects for the variant
            if highest_priority:
                effects = [vcffile.select_highest_priority_effect(variant)]
            else:
                effects = vcffile.parse_effects(variant)

            # Update the counts
            for eff in effects:
                a = eff.gene
                b = eff.transcript
                c = eff.effect
                g2t2e2c[a][b][c] = g2t2e2c.setdefault(a, {}).setdefault(b, {}).setdefault(c, 0) + 1

                # Update effect to impact mapping
                if eff.effect not in effect2impact:
                    effect2impact[eff.effect] = eff.impact

                # Impact already set for effect but does not match current impact
                elif effect2impact[eff.effect] != eff.impact:
                    exit_message = 'Multiple impacts for effect %s: %s, %s\nExiting.'
                    sys.stderr.write(exit_message % (eff.effect, eff.impact, effect2impact[eff.effect]))
                    sys.exit(1)
        return g2t2e2c, effect2impact
        
    def count_transcript_effects_all(self, highest_priority=False):
        '''
        For all vcf files in the list, count the transcript effects.
        '''
        g2t2e2c = {}
        effect2impact = {}
        for vcffile in self:
            self.count_transcript_effects_single(vcffile, g2t2e2c, effect2impact, highest_priority=highest_priority)

        #self.register_transcript_counts(g2t2e2c, effect2impact, None)
        return g2t2e2c, effect2impact

    def select_transcript_for_gene(self, g2t2e2c, effect2impact, transcript2len):
        '''
        For a gene, select transcript with highest mutation count with effects that have
        "HIGH" impact.
        If multiple transcripts result, select one with the longest transcript.
        If selected transcripts have the same length, then select randomly.
        '''
        def get_high_impact_count(_e2c):
            '''
            Given an effect to count mapping, sum up the counts of effects that have HIGH impact
            '''
            high_impact_count = 0
            for _e,_c in _e2c.iteritems():
                if effect2impact[_e] == 'HIGH':
                    high_impact_count += int(_c)
            return high_impact_count

        def get_transcript_w_most_high_impact(_t2e2c, _transcript2len):
            '''
            Given a set of transcript to effect to counts mapping, return transcript with the
            highest HIGH impact effects.  If more than 1 transcript results, select the longest.
            If still more than 1 transcript results, select randomly
            If no transcript results, return False
            '''

            # Filter by transcript2len mapping.  Those that do not have length data cannot be used
            usable_t2e2c = {}
            for _t,_e2c in _t2e2c.iteritems():
                if _t in _transcript2len:
                    usable_t2e2c[_t] = _e2c
            if len(usable_t2e2c) == 0:
                return False
            
            # Generate tuples (transcript, count) sorted from highest to lowest counts
            t_counts = sorted([(_t, get_high_impact_count(_e2c)) for _t,_e2c in usable_t2e2c.iteritems()],
                              key=lambda x: x[1], reverse=True)

            # Select the transcripts that have equal highest counts (could be one or more)
            highest_count = t_counts[0][1]
            transcripts_w_most_high_impact = [_t_c for _t_c in t_counts if _t_c[1] == highest_count]
            if len(transcripts_w_most_high_impact) == 1:
                return transcripts_w_most_high_impact[0][0]
            
            # Select longer transcript
            return sorted([(_t_c[0], _transcript2len[_t_c[0]]) for _t_c in transcripts_w_most_high_impact],
                          key=lambda x: x[1], reverse=True)[0][0]

        g2highestt = {}
        for g in g2t2e2c:
            # Find total HIGH impact mutation counts
            t = get_transcript_w_most_high_impact(g2t2e2c[g], transcript2len)
            if not t:
                continue
            g2highestt[g] = t
        
        return g2highestt            

#     def register_transcript_counts(self, g2t2e2c, eff2imp, transcript2len):
#         '''
#         Set the following as instance variables:
#         transcript effect counts
#         effect to impact mapping
#         transcript2length mapping
#         '''
#         self.g2t2e2c = g2t2e2c
#         self.eff2imp = eff2imp
#         self.transcript2len = transcript2len

#     def select_single_gene_transcript(self, effects):
#         '''
#         Select the gene transcript with the most variants using the transcript effect counts
#         g2t2e2c, eff2imp and transcript2len  must be set using the register_transcript_counts method.
#         Input must be the output of VcfFile.parse_effects method
#         '''
#         # If the necessary variables are not set, return 
#         if self.g2t2e2c is None or self.eff2imp is None or self.transcript2len is None:
#             raise Exception("g2t2e2c, eff2imp, and transcript2len are not set!")
        

# ------------------------------------------------------------------------------------- #
# Classes to handle converting vcf files

class Tsv2VcfConverter(object):
    '''
    Class to handle converting tsv files to vcf files
    This class assumes that the genotypes are provided in letters with a separating /, i.e. A/C, T/T
    '''
    def __init__(self,
                 f,
                 variant_caller='FooCaller',
                 chrom_col=0,
                 pos_col=1,
                 rsid_col=2,
                 ref_col=3,
                 alt_col=4,
                 sample_ids=['sample'],
                 sample_gt_cols=[5],
                 is_header=True):
        '''
        Initialize instance variables
        Note: col numbers are all zero-based
        '''
        # Check that the number of sample_ids and sample_gt_cols are equal
        if len(sample_ids) != len(sample_gt_cols):
            raise ValueError('Number of samples and genotype columns do not match:\nSamples: %s\nGenotype Columns: %s' % (str(sample_ids), str(sample_gt_cols)))

        # Set instance variables
        self._f = f
        self._variant_caller = variant_caller
        self._chrom_col = chrom_col
        self._pos_col = pos_col
        self._rsid_col = rsid_col
        self._ref_col = ref_col
        self._alt_col = alt_col
        self._sample_ids = sample_ids
        self._sample_gt_cols = sample_gt_cols

        # Skip input file has header when reading tsv file
        if is_header:
            f.next()
        self._reader = csv.reader(f, delimiter='\t')

    def __iter__(self):
        '''
        Iterator method
        '''
        return self
        
    def get_header(self):
        '''
        Return the header string including column descriptors
        '''
        column_names = ['CHROM',
                        'POS',
                        'ID',
                        'REF',
                        'ALT',
                        'QUAL',
                        'FILTER',
                        'INFO',
                        'FORMAT'] + self._sample_ids
        return '\n'.join(['##fileformat=VCFv4.1',
                          '##%s' % self._variant_caller,
                          '##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">',
                          '#%s' % '\t'.join(column_names)])

    def convert_to_numeric_alleles(self, gt, ref, alt):
        '''
        Given a genotype string in the format A/C, T/T, etc
        and ref, alt information
        Convert to 0/0, 0/1, or 1/1

        Alt alleles can contain multiple alternative alleles separated by a comma
        '''
        gts = gt.split('/')
        alts = sorted(alt.split(','))
        all_alleles_set = set(alts + [ref])
        
        # Check if genotype is in ref and alt alleles
        for a in gts:
            if a not in all_alleles_set:
                raise ValueError('Genotype does not match ref or alt alleles provided\ngenotype:%s\nalleles:%s\n\n' % (gts, all_alleles_set))
        
        als = []
        for a in gts:
            if a == ref:
                als.append('0')
            else:
                # Break up alternate alleles if multiple alleles exist
                for i,alt_a in enumerate(alts):
                    if a == alt_a:
                        als.append(str(i + 1))
                        break
        return '/'.join(sorted(als))
        
    def next(self):
        '''
        Iterator method
        '''
        irow = self._reader.next()
        rsid = irow[self._rsid_col]
        rsid = '.' if not re.search('rs', rsid) else rsid
        orow = [irow[self._chrom_col],
                irow[self._pos_col],
                rsid,
                irow[self._ref_col],
                irow[self._alt_col],
                '.',
                '.',
                '.',
                'GT'] + [self.convert_to_numeric_alleles(irow[c],
                                                         irow[self._ref_col],
                                                         irow[self._alt_col]) for c in self._sample_gt_cols]
        return '\t'.join(orow)
    
