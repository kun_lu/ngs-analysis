#!/usr/bin/env python
'''
Description     : API for parsing fasta files
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.12.14
Modified By     : Jin Kim jjinking(at)gmail(dot)com
'''

from Bio import SeqIO
from ngs.util import memoize

class FastaFile(object):
    '''
    Class to handle fasta files
    '''
    def __init__(self, f):
        if type(f) == str:
            self.name = f
            self.f = open(f, 'rU')
        elif type(f) == file:
            self.name = f.name
            self.f = f

    def load2dict(self, shortid=True):
        '''
        Load the records in fasta file to dictionary
        '''
        id2seq = {}
        for rec in SeqIO.parse(self.f, "fasta"):
            if shortid:
                k = rec.id
            else:
                k = rec.description
            id2seq[str(k)] = str(rec.seq)
        return id2seq

class RandomAccessRecords(object):
    '''
    Class to handle random access of records in a fasta file
    '''
    def __init__(self, filename):
        self.filename = filename
        self.record_dict = SeqIO.index(self.filename, 'fasta')

    def get(self, recordID):
        '''
        Redundant getter of sequence records
        '''
        return self.record_dict[recordID]

    @memoize
    def get_n_masked_seq(self, recordID, positions, lengths):
        '''
        Given positions, an immutable iterable containing integer positions (1-based),
        mask those positions in the sequence corresponding to recordID with N,
        and return the masked sequence
        The length of the masks for each position is given by lengths
        '''
        seq_list = list(self.record_dict[recordID].seq)
        for i,p in enumerate(positions):
            for j in range(lengths[i]):
                seq_list[p-1+j] = 'N'
        return ''.join(seq_list)
