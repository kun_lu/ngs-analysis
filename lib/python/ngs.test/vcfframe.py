#!/usr/bin/env python
'''
Description     : Unit test for vcfframe.py
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2014.01.06
Last Modified   : 2014.03.20
Modified By     : Jin Kim jjinking(at)gmail(dot)com
'''

import cStringIO
import os
import sys
import unittest
from ngs.vcfframe import *

class TestVcfFrame(unittest.TestCase):
    '''
    Unit tests for the VcfFrame class
    '''

    def test_load_vcf(self):
        '''
        Test vcf loading class method
        '''
        vf = VcfFrame.load_vcf('resources/example.vcf')
        
        # Check class of loaded vcf object
        self.assertEqual(type(vf), VcfFrame)

        # Check samples
        self.assertEqual(vf.samples, ['NA00001', 'NA00002', 'NA00003'])
        
        # Check headers
        self.assertEqual(len(vf.headers), 19)

        # Check size of data
        self.assertEqual(vf.shape[0], 7)
        self.assertEqual(vf.shape[1], 12)

        # Check class of loaded vcf object
        vf = SNPEffVcfFrame.load_vcf('resources/example.unphased2.vcf')
        self.assertEqual(type(vf), SNPEffVcfFrame)

    def test_parse_info_str(self):
        '''
        Test parsing info field string
        '''
        info_str = "AC=50;AF=0.189;AN=264;BaseQRankSum=9.761;DP=6734;Dels=0.00;FS=452.247;HaplotypeScore=0.9087;InbreedingCoeff=-0.2141;MLEAC=48;MLEAF=0.182;MQ=16.35;MQ0=4042;MQRankSum=-4.108;NEGATIVE_TRAIN_SITE;QD=0.69;ReadPosRankSum=11.267;VQSLOD=-4.635e+00;culprit=QD;"
        dd = VcfFrame.parse_info_str(info_str)
        self.assertEqual(dd['AC'], '50')
        self.assertEqual(dd['AF'], '0.189')
        self.assertEqual(dd['HaplotypeScore'], '0.9087')
        self.assertEqual(dd['MQRankSum'], '-4.108')
        self.assertEqual(dd['culprit'], 'QD')

    def test_update_info_str(self):
        '''
        Test updating info field string
        '''
        infostr = 'AC=50;AF=0.189;AN=264;DP=6734;Dels=0.00;X;Y;Z'
        self.assertEqual(VcfFrame.update_info_str(infostr, 'AC', '60'),
                         'AC=60;AF=0.189;AN=264;DP=6734;Dels=0.00;X;Y;Z')
        self.assertEqual(VcfFrame.update_info_str(infostr, 'AN', '100'),
                         'AC=50;AF=0.189;AN=100;DP=6734;Dels=0.00;X;Y;Z')
        self.assertEqual(VcfFrame.update_info_str(infostr, 'foo', 'bar', addnew=True),
                         'AC=50;AF=0.189;AN=264;DP=6734;Dels=0.00;X;Y;Z;foo=bar')
        self.assertEqual(VcfFrame.update_info_str(infostr, 'foo', 'bar', addnew=False),
                         'AC=50;AF=0.189;AN=264;DP=6734;Dels=0.00;X;Y;Z')

    def test_get_variant_type(self):
        '''
        Test classifying variants
        '''
        ref = 'A'
        alt = 'C'
        self.assertEqual(VcfFrame.get_variant_type(ref,alt), 'snp')
        ref = 'AG'
        alt = 'G'
        self.assertEqual(VcfFrame.get_variant_type(ref,alt), 'del')
        ref = 'C'
        alt = 'CTT'
        self.assertEqual(VcfFrame.get_variant_type(ref,alt), 'ins')
        ref = 'AC'
        alt = 'GA'
        self.assertEqual(VcfFrame.get_variant_type(ref,alt), 'dnp')
        ref = 'ACT'
        alt = 'GCA'
        self.assertEqual(VcfFrame.get_variant_type(ref,alt), 'tnp')
        ref = 'ACGA'
        alt = 'CGAT'
        self.assertEqual(VcfFrame.get_variant_type(ref,alt), 'onp')
        ref = 'A'
        alt = 'A'
        self.assertEqual(VcfFrame.get_variant_type(ref,alt), 'non-variant')
        ref = ''
        alt = ''
        self.assertEqual(VcfFrame.get_variant_type(ref,alt), 'non-variant')

    def test_convert_allele2base_gt(self):
        '''
        Test conversion of a sample's allele string to genotype
        '''
        row = {'REF':'A',
               'ALT':'C',
               'FORMAT':'GT:X:Y:Z',
               's1':'0/0:x:y:z',
               's2':'0|1:x:y:z',
               's3':'1/1:x:y:z'}
        self.assertEqual(VcfFrame.convert_allele2base_gt(row, 's1'), 'A/A')
        self.assertEqual(VcfFrame.convert_allele2base_gt(row, 's2'), 'A|C')
        self.assertEqual(VcfFrame.convert_allele2base_gt(row, 's3'), 'C/C')

        row2 = {'REF':'A',
                'ALT':'C,G',
                'FORMAT':'GT:X:Y:Z',
                's1':'2/0:x:y:z',
                's2':'0|2:x:y:z',
                's3':'1/2:x:y:z'}
        self.assertEqual(VcfFrame.convert_allele2base_gt(row2, 's1'), 'G/A')
        self.assertEqual(VcfFrame.convert_allele2base_gt(row2, 's2'), 'A|G')
        self.assertEqual(VcfFrame.convert_allele2base_gt(row2, 's3'), 'C/G')

    def test_count_alleles(self):
        '''
        Test counting alleles
        NA00001        NA00002        NA00004
        0/0:48:1:51,51 1/0:48:8:51,51 1/1:43:5:.,.
        0/1:49:3:58,50 0/1:3:5:65,3   0/0:41:3
        1/1:21:6:23,27 2/1:2:0:18,2   2/2:35:4
        0/0:54:7:56,60 0/0:48:4:51,51 0/0:61:2
        0/1:35:4       1/0:17:2       1/1:40:3
        0/0:10:3       1/1:10:3       ./.:10:3
        0/0:10:3       ./.            1/1:10:3
        0/0:10:3       ./.            1/1:10:3
        '''
        vf = VcfFrame.load_vcf('resources/example.unphased2.vcf')
        vf['allele_counts'] = vf.apply(vf.count_alleles, args=(vf.samples,), axis=1)
        self.assertEqual(vf['allele_counts'].shape[0], 8)
        self.assertEqual(len(vf['allele_counts'][0]), 2)
        self.assertEqual(vf['allele_counts'][0]['0'], 3)
        self.assertEqual(vf['allele_counts'][0]['1'], 3)
        self.assertEqual(len(vf['allele_counts'][2]), 3)
        self.assertEqual(vf['allele_counts'][2]['0'], 0)
        self.assertEqual(vf['allele_counts'][2]['1'], 3)
        self.assertEqual(vf['allele_counts'][2]['2'], 3)
        self.assertEqual(vf['allele_counts'][3]['0'], 6)
        self.assertEqual(vf['allele_counts'][3]['1'], 0)
        self.assertEqual(vf['allele_counts'][7]['1'], 2)
        self.assertEqual(vf['allele_counts'][7]['.'], 2)

        vf['AC'] = vf.apply(vf.count_alleles, args=(['NA00002'],), axis=1)
        self.assertEqual(vf['AC'][6]['0'], 0)
        self.assertEqual(vf['AC'][7]['1'], 0)

    def test_categorize_allele_counts(self):
        '''
        Test categorizing allele counts
        '''
        allele_cnts = {'0': 1,
                       '1': 3}
        cats = VcfFrame.categorize_allele_counts(allele_cnts)
        self.assertEqual(cats[0], 1)
        self.assertEqual(cats[1], 3)
        self.assertEqual(len(cats), 2)

        allele_cnts = {'0': 0,
                       '1': 3,
                       '2': 4}
        cats = VcfFrame.categorize_allele_counts(allele_cnts)
        self.assertEqual(cats[0], 0)
        self.assertEqual(cats[1], 3)
        self.assertEqual(cats[2], 4)

        allele_cnts = {'0': 4}
        cats = VcfFrame.categorize_allele_counts(allele_cnts)
        self.assertEqual(cats[0], 4)

    def test_compute_ac(self):
        '''
        Test counting the number of non-reference alleles
        NA00001        NA00002        NA00004
        0/0:48:1:51,51 1/0:48:8:51,51 1/1:43:5:.,.
        0/1:49:3:58,50 0/1:3:5:65,3   0/0:41:3
        1/1:21:6:23,27 2/1:2:0:18,2   2/2:35:4
        0/0:54:7:56,60 0/0:48:4:51,51 0/0:61:2
        0/1:35:4       1/0:17:2       1/1:40:3
        0/0:10:3       1/1:10:3       ./.:10:3
        0/0:10:3       ./.            1/1:10:3
        0/0:10:3       ./.            1/1:10:3
        '''
        vf = VcfFrame.load_vcf('resources/example.unphased2.vcf')
        vf['AC'] = vf.apply(vf.compute_ac, args=(vf.samples,), axis=1)
        self.assertEqual(vf['AC'][0], (3,))
        self.assertEqual(vf['AC'][1], (2,))
        self.assertEqual(vf['AC'][2], (3,3))
        self.assertEqual(vf['AC'][3], (0,))
        self.assertEqual(vf['AC'][7], (2,))

    def test_compute_af(self):
        '''
        Test alternate allele frequency computation
        '''
        vf = VcfFrame.load_vcf('resources/example.unphased2.vcf')
        vf['AF'] = vf.apply(vf.compute_af, args=(vf.samples,), axis=1)
        self.assertEqual(vf['AF'][0], (0.5,))
        self.assertEqual(vf['AF'][1], (1/3.0,))
        self.assertEqual(vf['AF'][2], (0.5, 0.5))
        self.assertEqual(vf['AF'][3], (0,))
        self.assertEqual(vf['AF'][7], (.5,))

        vf['AF2'] = vf.apply(vf.compute_af, args=(['NA00002'],), axis=1)
        self.assertEqual(vf['AF2'][6], (0.0,))
        self.assertEqual(vf['AF2'][7], (0.0,))

    def test_get_affected_samples(self):
        '''
        Test getting affected samples list
        '''
        vf = VcfFrame.load_vcf('resources/example.unphased2.vcf')
        vf['affected'] = vf.apply(vf.get_affected_samples, args=(vf.samples,), axis=1)
        self.assertEqual(vf['affected'][0], ['NA00002','NA00004'])
        self.assertEqual(vf['affected'][1], ['NA00001','NA00002'])
        self.assertEqual(vf['affected'][2], ['NA00001','NA00002','NA00004'])
        self.assertEqual(vf['affected'][3], [])
        self.assertEqual(vf['affected'][7], ['NA00004'])

    def test_get_samplepos(self):
        '''
        Test attaching variant information to each sample name
        CHROM POS        REF  ALT  Sample1 Sample2 Sample3 Sample4 Sample5
        2     167085495  A    G    0/0     0/0     0/0     0/0     0/0
        7     116778457  TA   T    0/0     0/0     0/0     0/0     0/0
        9     125438378  C    T    0/0     0/0     0/1     0/1     0/0
        16    58030634   C    T    0/1     0/1     0/0     0/0     0/1
        22    34022284   G    A    0/1     0/1     0/1     0/1     1/1
        X     49179755   G    A    0/1     0/0     0/0     0/0     0/0
        '''
        vf = SNPSiftVcfFrame.load_vcf('resources/example.germline.snpeff.vcf')
        vf.set_samples_affected(vf.samples, newcolname='Samples Affected')
        vf['foo'] = vf.apply(vf.get_samplepos, args=('Samples Affected',), axis=1)
        self.assertEqual(vf['foo'][0], '')

    def test_categorize_genotype(self):
        '''
        Test categorizing genotypes as NoCall, HomoRef, HomoAlt, Hetero, or HeteroAlt
        '''
        self.assertEqual(VcfFrame.categorize_genotype('./.'), 'NoCall')
        self.assertEqual(VcfFrame.categorize_genotype('.|.'), 'NoCall')
        self.assertEqual(VcfFrame.categorize_genotype('0/0'), 'HomoRef')
        self.assertEqual(VcfFrame.categorize_genotype('0|1'), 'Hetero')
        self.assertEqual(VcfFrame.categorize_genotype('2|0'), 'Hetero')
        self.assertEqual(VcfFrame.categorize_genotype('2|2'), 'HomoAlt')
        self.assertEqual(VcfFrame.categorize_genotype('1/1'), 'HomoAlt')
        self.assertEqual(VcfFrame.categorize_genotype('1|2'), 'HeteroAlt')

    def test_categorize_samples_by_geno(self):
        '''
        Test creating dictionary of genotype categories mapping to a list of samples
        NA00001        NA00002        NA00004
        0/0:48:1:51,51 1/0:48:8:51,51 1/1:43:5:.,.
        0/1:49:3:58,50 0/1:3:5:65,3   0/0:41:3
        1/1:21:6:23,27 2/1:2:0:18,2   2/2:35:4
        0/0:54:7:56,60 0/0:48:4:51,51 0/0:61:2
        0/1:35:4       1/0:17:2       1/1:40:3
        0/0:10:3       1/1:10:3       ./.:10:3
        0/0:10:3       ./.            1/1:10:3
        0/0:10:3       ./.            1/1:10:3
        '''
        vf = VcfFrame.load_vcf('resources/example.unphased2.vcf')
        vf['sample_geno_cats'] = vf.apply(vf.categorize_samples_by_geno, args=(vf.samples,), axis=1)
        self.assertEqual(len(vf['sample_geno_cats'][0]), 3)
        self.assertEqual(vf['sample_geno_cats'][0]['HomoRef'], ['NA00001'])
        self.assertEqual(vf['sample_geno_cats'][0]['Hetero'], ['NA00002'])
        self.assertEqual(vf['sample_geno_cats'][0]['HomoAlt'], ['NA00004'])
        self.assertEqual(len(vf['sample_geno_cats'][1]), 2)
        self.assertEqual(vf['sample_geno_cats'][1]['Hetero'], ['NA00001','NA00002'])
        self.assertEqual(vf['sample_geno_cats'][1]['HomoRef'], ['NA00004'])
        self.assertEqual(vf['sample_geno_cats'][2]['HeteroAlt'], ['NA00002'])
        self.assertEqual(vf['sample_geno_cats'][2]['HomoAlt'], ['NA00001','NA00004'])
        self.assertEqual(vf['sample_geno_cats'][7]['NoCall'], ['NA00002'])

    def test_count_allelic_geno(self):
        '''
        Test counting allelic genotypes
        '''
        vf = VcfFrame.load_vcf('resources/example.unphased2.vcf')
        vf['counts'] = vf.apply(vf.count_allelic_geno, args=(vf.samples,), axis=1)
        self.assertEqual(len(vf['counts'][0]), 3)
        self.assertEqual(vf['counts'][0][('0','0')], 1)
        self.assertEqual(vf['counts'][0][('0','1')], 1)
        self.assertEqual(len(vf['counts'][1]), 2)
        self.assertEqual(vf['counts'][1][('0','1')], 2)
        self.assertEqual(vf['counts'][1][('0','0')], 1)
        self.assertEqual(len(vf['counts'][2]), 3)
        self.assertEqual(vf['counts'][2][('1','1')], 1)
        self.assertEqual(vf['counts'][2][('1','2')], 1)
        self.assertEqual(vf['counts'][2][('2','2')], 1)
        self.assertEqual(vf['counts'][7][('0','0')], 1)
        self.assertEqual(vf['counts'][7][('.','.')], 1)

    def test_categorize_genotype_counts(self):
        '''
        Test categorizing allelic genotypes
        '''
        self.assertEqual(VcfFrame.categorize_genotype_counts(
                {('0','0'): 3,
                 ('1','1'): 3}),
                         (3,0,3,0))
        self.assertEqual(VcfFrame.categorize_genotype_counts(
                {('0','0'): 3,
                 ('0','1'): 1,
                 ('1','1'): 3,
                 ('2','2'): 2}),
                         (3,1,5,0))
        self.assertEqual(VcfFrame.categorize_genotype_counts(
                {('0','0'): 3,
                 ('1','0'): 7,
                 ('1','1'): 3,
                 ('2','2'): 1,
                 ('1','2'): 1,
                 ('2','3'): 2}),
                         (3,7,4,3))
                         
    def test_filter_rows(self):
        '''
        Test filtering rows of VcfFrame objects
        '''
        vf = SNPSiftVcfFrame.load_vcf('resources/example.unphased2.vcf')
        vf_filtered = vf.filter_rows(vf['REF'] == 'A')
        self.assertEqual(type(vf_filtered), SNPSiftVcfFrame)
        self.assertEqual(vf_filtered.shape[0], 3)
        
        vf = SNPEffVcfFrame.load_vcf('resources/example.unphased2.vcf')
        vf_filtered = vf.filter_rows(vf['FILTER'] == 'PASS')
        self.assertEqual(type(vf_filtered), SNPEffVcfFrame)
        self.assertEqual(vf_filtered.shape[0], 7)
        
    def test_remove_samples(self):
        '''
        Test removing samples from a vcf file
        '''
        vf = CaseControlVcfFrame.load_vcf('resources/example.unphased2.vcf')
        vf2 = vf.remove_samples(['NA00002'])
        
        # Write to temporary string buffer
        output = cStringIO.StringIO()
        vf2.to_vcf(output)
        newvcfstr = output.getvalue()
 
        with open('resources/example.unphased2.2samples.vcf', 'rU') as f:
            storedvcfstr = f.read()
        self.assertEqual(newvcfstr, storedvcfstr)

    def test_to_vcf(self):
        '''
        Test writing to vcf file
        '''
        vcf = 'resources/example.variant2.tsv.vcf'
        vf = VcfFrame.load_vcf(vcf)
        output = cStringIO.StringIO()
        vf.to_vcf(output)
        self.assertEqual(output.getvalue(), open(vcf, 'rU').read())
        
        vcf = 'resources/example.unphased2.2samples.vcf'
        vf = VcfFrame.load_vcf(vcf)
        output = cStringIO.StringIO()
        vf.to_vcf(output)
        self.assertEqual(output.getvalue(), open(vcf, 'rU').read())
        
    def test_create_ped(self):
        '''
        Test creating a ped file
        Family ID       Individual ID   Paternal ID Maternal ID Sex     Phenotype
        Fam_sample_1    sample_1        0           0           0       2
        Fam_sample_2    sample_2        0           0           0       1
        '''
        vf = VcfFrame.load_vcf('resources/example.variant2.tsv.vcf')
        ped_df = vf.create_ped(case_samples=['sample_1'])
        self.assertEqual(ped_df.shape[0], len(vf.samples))
        self.assertEqual(ped_df.ix[0]['Family ID'], 'Fam_sample_1')
        self.assertEqual(ped_df.ix[1]['Family ID'], 'Fam_sample_2')
        self.assertEqual(ped_df.ix[0]['Individual ID'], 'sample_1')
        self.assertEqual(ped_df.ix[1]['Paternal ID'], 0)
        self.assertEqual(ped_df.ix[0]['Maternal ID'], 0)
        self.assertEqual(np.sum(ped_df['Sex']), 0)
        self.assertTrue((ped_df['Phenotype'].values == np.array([2, 1])).all())

    def test_set_sample_numeric_gt(self):
        '''
        Test converting a sample's genotype to numeric format
        '''
        vf = VcfFrame.load_vcf('resources/example.unphased2.vcf')
        vf.set_sample_numeric_gt('NA00001', postfix=' Foo', overwrite=False)
        self.assertEqual(vf['NA00001 Foo'][0], 0)
        vf.set_sample_numeric_gt('NA00002', postfix=' Bar', overwrite=False)
        self.assertEqual(vf['NA00002 Bar'][0], 1)
        vf['NA00002'] = vf['NA00001']
        vf.set_sample_numeric_gt('NA00002', postfix=' Bar', overwrite=False)
        self.assertEqual(vf['NA00002 Bar'][0], 1)
        vf.set_sample_numeric_gt('NA00002', postfix=' Bar', overwrite=True)
        self.assertEqual(vf['NA00002 Bar'][0], 0)

    def test_set_samples_numeric_gt(self):
        '''
        Test converting sample genotype to numeric format
        NA00001        NA00002        NA00004
        0/0:48:1:51,51 1/0:48:8:51,51 1/1:43:5:.,.
        0/1:49:3:58,50 0/1:3:5:65,3   0/0:41:3
        1/1:21:6:23,27 2/1:2:0:18,2   2/2:35:4
        0/0:54:7:56,60 0/0:48:4:51,51 0/0:61:2
        0/1:35:4       1/0:17:2       1/1:40:3
        0/0:10:3       1/1:10:3       ./.:10:3
        0/0:10:3       ./.            1/1:10:3
        0/0:10:3       ./.            1/1:10:3
        '''
        vf = VcfFrame.load_vcf('resources/example.unphased2.vcf')
        vf.set_samples_numeric_gt(vf.samples)
        numeric_gt_cols = vf[map(lambda x: x + " NumGT", vf.samples)]
        self.assertEqual(numeric_gt_cols['NA00001 NumGT'][0], 0)
        self.assertEqual(numeric_gt_cols['NA00001 NumGT'][1], 1)
        self.assertEqual(numeric_gt_cols['NA00001 NumGT'][2], 2)
        self.assertEqual(numeric_gt_cols['NA00001 NumGT'][3], 0)
        self.assertEqual(numeric_gt_cols['NA00001 NumGT'][7], 0)
        self.assertEqual(numeric_gt_cols['NA00002 NumGT'][0], 1)
        self.assertEqual(numeric_gt_cols['NA00002 NumGT'][1], 1)
        self.assertEqual(numeric_gt_cols['NA00002 NumGT'][2], 2)
        self.assertEqual(numeric_gt_cols['NA00002 NumGT'][6], -1)
        self.assertEqual(numeric_gt_cols['NA00004 NumGT'][0], 2)
        self.assertEqual(numeric_gt_cols['NA00004 NumGT'][1], 0)
        self.assertEqual(numeric_gt_cols['NA00004 NumGT'][5], -1)
        self.assertEqual(numeric_gt_cols['NA00004 NumGT'][7], 2)

    def test_set_extracted_info_val(self):
        '''
        Test extracting values from the info column and setting it in a new column
        INFO                              FORMAT      NA00001        NA00002        NA00004
        NS=3;DP=14;AF=0.5;DB;H2           GT:GQ:DP:HQ 0/0:48:1:51,51 1/0:48:8:51,51 1/1:43:5:.,.
        NS=3;DP=11;AF=0.017               GT:GQ:DP:HQ 0/1:49:3:58,50 0/1:3:5:65,3   0/0:41:3
        NS=2;DP=10;AF=0.333,0.667;AA=T;DB GT:GQ:DP:HQ 1/1:21:6:23,27 2/1:2:0:18,2   2/2:35:4
        NS=3;DP=13;AA=T                   GT:GQ:DP:HQ 0/0:54:7:56,60 0/0:48:4:51,51 0/0:61:2
        NS=3;DP=9;AA=G                    GT:GQ:DP    0/1:35:4       1/0:17:2       1/1:40:3
        FOO                               GT:GQ:DP    0/0:10:3       1/1:10:3       ./.:10:3
        FOO                               GT:GQ:DP    0/0:10:3       ./.            1/1:10:3
        Foo                               GT:GQ:DP    0/0:10:3       ./.            1/1:10:3
        '''
        vf = VcfFrame.load_vcf('resources/example.unphased2.vcf')
        vf.set_extracted_info_val('NS', newcolname='NS', overwrite=True)
        self.assertEqual(vf['NS'][0], '3')
        self.assertEqual(vf['NS'][2], '2')
        self.assertEqual(vf['NS'][5], None)
        self.assertEqual(vf['NS'][7], None)
        vf.set_extracted_info_val('DP', overwrite=True)
        self.assertEqual(vf['DP'][0], '14')
        self.assertEqual(vf['DP'][4], '9')
        self.assertEqual(vf['DP'][5], None)
        vf.set_extracted_info_val('DB', overwrite=True)
        self.assertEqual(vf['DB'].isnull().sum(), 8)

    def test_set_variant_type_field(self):
        '''
        Test setting variant type field
        #CHROM POS     ID        REF    ALT     
        20     14370   rs6054257 G      A       
        20     17330   .         T      A       
        20     1110696 rs6040355 A      G,T     
        20     1230237 .         T      .       
        20     1234567 microsat1 GTC    G,GTCT  
        21     111     .         A      T       
        21     222     .         C      G       
        2      1       .         A      T       
        '''
        vf = VcfFrame.load_vcf('resources/example.unphased2.vcf')
        vf.set_variant_type_field(newcolname='vtype')
        self.assertEqual(vf['vtype'][0], 'snp')
        self.assertEqual(vf['vtype'][1], 'snp')
        self.assertEqual(vf['vtype'][2], 'snp')
        self.assertEqual(vf['vtype'][4], 'del,ins')

    def test_set_sample_allele_field(self):
        '''
        Test extracting a sample's allele string (i.e. 0/1) and
        setting it to a new field
        NA00001        NA00002        NA00004
        0/0:48:1:51,51 1/0:48:8:51,51 1/1:43:5:.,.
        0/1:49:3:58,50 0/1:3:5:65,3   0/0:41:3
        1/1:21:6:23,27 2/1:2:0:18,2   2/2:35:4
        0/0:54:7:56,60 0/0:48:4:51,51 0/0:61:2
        0/1:35:4       1/0:17:2       1/1:40:3
        0/0:10:3       1/1:10:3       ./.:10:3
        0/0:10:3       ./.            1/1:10:3
        0/0:10:3       ./.            1/1:10:3
        '''
        vf = VcfFrame.load_vcf('resources/example.unphased2.vcf')
        vf.set_sample_allele_field('NA00004', postfix=' Foo', overwrite=False)
        vf.set_sample_allele_field('NA00002', postfix=' Bar', overwrite=False)
        self.assertEqual(vf['NA00004 Foo'][0], '1/1')
        self.assertEqual(vf['NA00002 Bar'][0], '1/0')
        self.assertEqual(vf['NA00002 Bar'][6], './.')
        vf['NA00002'] = vf['NA00004']
        vf.set_sample_allele_field('NA00002', postfix=' Bar', overwrite=False)
        self.assertEqual(vf['NA00002 Bar'][0], '1/0')
        vf.set_sample_allele_field('NA00002', postfix=' Bar', overwrite=True)
        self.assertEqual(vf['NA00002 Bar'][0], '1/1')

    def test_set_samples_allele_fields(self):
        '''
        Test extracting alleles information from a set of samples
        '''
        vf = VcfFrame.load_vcf('resources/example.unphased2.vcf')
        vf.set_samples_allele_fields(['NA00001','NA00002','NA00004'], postfix=' Foo')
        self.assertEqual(vf['NA00001 Foo'][0], '0/0')
        self.assertEqual(vf['NA00001 Foo'][1], '0/1')
        self.assertEqual(vf['NA00001 Foo'][2], '1/1')
        self.assertEqual(vf['NA00001 Foo'][5], '0/0')
        self.assertEqual(vf['NA00001 Foo'][7], '0/0')
        self.assertEqual(vf['NA00002 Foo'][0], '1/0')
        self.assertEqual(vf['NA00004 Foo'][0], '1/1')
        self.assertEqual(vf['NA00004 Foo'][6], '1/1')
        
    def test_set_sample_dp_field(self):
        '''
        Test setting dp field for a given sample
        NA00001        NA00002        NA00004
        0/0:48:1:51,51 1/0:48:8:51,51 1/1:43:5:.,.
        0/1:49:3:58,50 0/1:3:5:65,3   0/0:41:3
        1/1:21:6:23,27 2/1:2:0:18,2   2/2:35:4
        0/0:54:7:56,60 0/0:48:4:51,51 0/0:61:2
        0/1:35:4       1/0:17:2       1/1:40:3
        0/0:10:3       1/1:10:3       ./.:10:3
        0/0:10:3       ./.            1/1:10:3
        0/0:10:3       ./.            1/1:10:3
        '''
        vf = VcfFrame.load_vcf('resources/example.unphased2.vcf')
        vf.set_sample_dp_field('NA00004', postfix=' Foo', overwrite=False)
        vf.set_sample_dp_field('NA00002', postfix=' Bar', overwrite=False)
        self.assertEqual(vf['NA00004 Foo'][0], 5)
        self.assertEqual(vf['NA00002 Bar'][0], 8)
        self.assertEqual(vf['NA00002 Bar'][6], 0)
        vf['NA00002'] = vf['NA00004']
        vf.set_sample_dp_field('NA00002', postfix=' Bar', overwrite=False)
        self.assertEqual(vf['NA00002 Bar'][0], 8)
        vf.set_sample_dp_field('NA00002', postfix=' Bar', overwrite=True)
        self.assertEqual(vf['NA00002 Bar'][0], 5)

    def test_set_samples_dp_fields(self):
        '''
        Test setting dp fields for a given list of samples
        '''
        vf = VcfFrame.load_vcf('resources/example.unphased2.vcf')
        vf.set_samples_dp_fields(['NA00001','NA00002','NA00004'], postfix=' Foo')
        self.assertEqual(vf['NA00001 Foo'][0], 1)
        self.assertEqual(vf['NA00001 Foo'][1], 3)
        self.assertEqual(vf['NA00001 Foo'][2], 6)
        self.assertEqual(vf['NA00001 Foo'][5], 3)
        self.assertEqual(vf['NA00001 Foo'][7], 3)
        self.assertEqual(vf['NA00002 Foo'][0], 8)
        self.assertEqual(vf['NA00002 Foo'][7], 0)
        self.assertEqual(vf['NA00004 Foo'][0], 5)
        self.assertEqual(vf['NA00004 Foo'][6], 3)
        
    def test_set_sample_gt_field(self):
        '''
        Test setting a sample's genotype field based on ref and alt alleles
        i.e. A/A or C/T
        REF    ALT     NA00001        NA00002        NA00004
        G      A       0/0:48:1:51,51 1/0:48:8:51,51 1/1:43:5:.,.
        T      A       0/1:49:3:58,50 0/1:3:5:65,3   0/0:41:3
        A      G,T     1/1:21:6:23,27 2/1:2:0:18,2   2/2:35:4
        T      .       0/0:54:7:56,60 0/0:48:4:51,51 0/0:61:2
        GTC    G,GTCT  0/1:35:4       1/0:17:2       1/1:40:3
        A      T       0/0:10:3       1/1:10:3       ./.:10:3
        C      G       0/0:10:3       ./.            1/1:10:3
        A      T       0/0:10:3       ./.            1/1:10:3
        '''
        vf = VcfFrame.load_vcf('resources/example.unphased2.vcf')
        vf.set_sample_gt_field('NA00004', postfix=' Foo', overwrite=False)
        vf.set_sample_gt_field('NA00002', postfix=' Bar', overwrite=False)
        self.assertEqual(vf['NA00004 Foo'][0], 'A/A')
        self.assertEqual(vf['NA00002 Bar'][0], 'A/G')
        self.assertEqual(vf['NA00002 Bar'][6], 'N/N')
        vf['NA00002'] = vf['NA00004']
        vf.set_sample_gt_field('NA00002', postfix=' Bar', overwrite=False)
        self.assertEqual(vf['NA00002 Bar'][0], 'A/G')
        vf.set_sample_gt_field('NA00002', postfix=' Bar', overwrite=True)
        self.assertEqual(vf['NA00002 Bar'][0], 'A/A')

    def test_set_samples_gt_fields(self):
        '''
        Test setting genotype fields for a set of samples
        '''
        vf = VcfFrame.load_vcf('resources/example.unphased2.vcf')
        vf.set_samples_gt_fields(['NA00001','NA00002','NA00004'], postfix=' Foo')
        self.assertEqual(vf['NA00001 Foo'][0], 'G/G')
        self.assertEqual(vf['NA00001 Foo'][1], 'T/A')
        self.assertEqual(vf['NA00001 Foo'][2], 'G/G')
        self.assertEqual(vf['NA00001 Foo'][5], 'A/A')
        self.assertEqual(vf['NA00001 Foo'][7], 'A/A')
        self.assertEqual(vf['NA00002 Foo'][0], 'A/G')
        self.assertEqual(vf['NA00002 Foo'][2], 'T/G')
        self.assertEqual(vf['NA00002 Foo'][4], 'G/GTC')
        self.assertEqual(vf['NA00004 Foo'][0], 'A/A')
        self.assertEqual(vf['NA00004 Foo'][4], 'G/G')
        self.assertEqual(vf['NA00004 Foo'][5], 'N/N')
        self.assertEqual(vf['NA00004 Foo'][6], 'G/G')

    def test_set_category_samples(self):
        '''
        Test setting a column containing samples whose genotypes are of the given category
        which is one of {'NoCall', 'HomoRef','HomoAlt','Hetero','HeteroAlt'}
        NA00001        NA00002        NA00004
        0/0:48:1:51,51 1/0:48:8:51,51 1/1:43:5:.,.
        0/1:49:3:58,50 0/1:3:5:65,3   0/0:41:3
        1/1:21:6:23,27 2/1:2:0:18,2   2/2:35:4
        0/0:54:7:56,60 0/0:48:4:51,51 0/0:61:2
        0/1:35:4       1/0:17:2       1/1:40:3
        0/0:10:3       1/1:10:3       ./.:10:3
        0/0:10:3       ./.            1/1:10:3
        0/0:10:3       ./.            1/1:10:3
        '''
        vf = VcfFrame.load_vcf('resources/example.unphased2.vcf')
        vf.set_category_samples(['NA00001','NA00002','NA00004'], 'NoCall', 'NoCall')
        vf.set_category_samples(['NA00001','NA00002','NA00004'], 'HomoRef', 'HomoRef')
        vf.set_category_samples(['NA00001','NA00002','NA00004'], 'HomoAlt', 'HomoAlt')
        vf.set_category_samples(['NA00001','NA00002','NA00004'], 'Hetero', 'Hetero')
        vf.set_category_samples(['NA00001','NA00002','NA00004'], 'HeteroAlt', 'HeteroAlt')
        self.assertEqual(vf['NoCall'][0], '')
        self.assertEqual(vf['NoCall'][5], 'NA00004')
        self.assertEqual(vf['NoCall'][6], 'NA00002')
        self.assertEqual(vf['HomoRef'][0], 'NA00001')
        self.assertEqual(vf['HomoRef'][3], ';'.join(vf.samples))
        self.assertEqual(vf['HomoAlt'][0], 'NA00004')
        self.assertEqual(vf['HomoAlt'][2], 'NA00001;NA00004')
        self.assertEqual(vf['Hetero'][0], 'NA00002')
        self.assertEqual(vf['Hetero'][4], 'NA00001;NA00002')
        self.assertEqual(vf['Hetero'][5], '')
        self.assertEqual(vf['HeteroAlt'][1], '')
        self.assertEqual(vf['HeteroAlt'][2], 'NA00002')

    def test_set_category_samples_count(self):
        '''
        Test setting a new column containing sample counts whose genotypes
        are of one of the following: {'NoCall', 'HomoRef','HomoAlt','Hetero','HeteroAlt'}
        '''
        vf = VcfFrame.load_vcf('resources/example.unphased2.vcf')
        vf.set_category_samples_count(['NA00001','NA00002','NA00004'], 'NoCall', 'NoCall')
        vf.set_category_samples_count(['NA00001','NA00002','NA00004'], 'HomoRef', 'HomoRef')
        vf.set_category_samples_count(['NA00001','NA00002','NA00004'], 'HomoAlt', 'HomoAlt')
        vf.set_category_samples_count(['NA00001','NA00002','NA00004'], 'Hetero', 'Hetero')
        vf.set_category_samples_count(['NA00001','NA00002','NA00004'], 'HeteroAlt', 'HeteroAlt')
        self.assertEqual(vf['NoCall'][0], 0)
        self.assertEqual(vf['NoCall'][5], 1)
        self.assertEqual(vf['NoCall'][6], 1)
        self.assertEqual(vf['HomoRef'][0], 1)
        self.assertEqual(vf['HomoRef'][3], 3)
        self.assertEqual(vf['HomoAlt'][0], 1)
        self.assertEqual(vf['HomoAlt'][2], 2)
        self.assertEqual(vf['Hetero'][0], 1)
        self.assertEqual(vf['Hetero'][4], 2)
        self.assertEqual(vf['Hetero'][5], 0)
        self.assertEqual(vf['HeteroAlt'][1], 0)
        self.assertEqual(vf['HeteroAlt'][2], 1)
        
    def test_set_variant_callrate(self):
        '''
        Test setting variant callrates
        NA00001        NA00002        NA00004
        0/0:48:1:51,51 1/0:48:8:51,51 1/1:43:5:.,.
        0/1:49:3:58,50 0/1:3:5:65,3   0/0:41:3
        1/1:21:6:23,27 2/1:2:0:18,2   2/2:35:4
        0/0:54:7:56,60 0/0:48:4:51,51 0/0:61:2
        0/1:35:4       1/0:17:2       1/1:40:3
        0/0:10:3       1/1:10:3       ./.:10:3
        0/0:10:3       ./.            1/1:10:3
        0/0:10:3       ./.            1/1:10:3
        '''
        vf = VcfFrame.load_vcf('resources/example.unphased2.vcf')
        vf.set_variant_callrate(vf.samples, newcolname='callrate')
        self.assertEqual(vf['callrate'][0], 1.0)
        self.assertEqual(vf['callrate'][1], 1.0)
        self.assertEqual(round(vf['callrate'][5], 5), round(2.0/3.0, 5))
        self.assertEqual(round(vf['callrate'][6], 5), round(2.0/3.0, 5))
        self.assertEqual(round(vf['callrate'][7], 5), round(2.0/3.0, 5))
        
    def test_set_samples_affected(self):
        '''
        Test setting the name of samples that are affected
        '''
        vf = VcfFrame.load_vcf('resources/example.unphased2.vcf')
        vf.set_samples_affected(vf.samples, newcolname='affected')
        self.assertEqual(vf['affected'][0], 'NA00002;NA00004')
        self.assertEqual(vf['affected'][1], 'NA00001;NA00002')
        self.assertEqual(vf['affected'][2], 'NA00001;NA00002;NA00004')
        self.assertEqual(vf['affected'][4], 'NA00001;NA00002;NA00004')
        self.assertEqual(vf['affected'][5], 'NA00002')
        self.assertEqual(vf['affected'][6], 'NA00004')
        self.assertEqual(vf['affected'][7], 'NA00004')

    def test_set_samples_affected_count(self):
        '''
        Test setting the number of samples that are affected
        '''
        vf = VcfFrame.load_vcf('resources/example.unphased2.vcf')
        vf.set_samples_affected_count(vf.samples, newcolname='affected')
        self.assertEqual(vf['affected'][0], 2)
        self.assertEqual(vf['affected'][1], 2)
        self.assertEqual(vf['affected'][2], 3)
        self.assertEqual(vf['affected'][4], 3)
        self.assertEqual(vf['affected'][5], 1)
        self.assertEqual(vf['affected'][6], 1)
        self.assertEqual(vf['affected'][7], 1)

    def test_remove_non_variant_records(self):
        '''
        Test removing records that do not contain any non-variants
        NA00001        NA00002        NA00004
        0/0:48:1:51,51 1/0:48:8:51,51 1/1:43:5:.,.
        0/1:49:3:58,50 0/1:3:5:65,3   0/0:41:3
        1/1:21:6:23,27 2/1:2:0:18,2   2/2:35:4
        0/0:54:7:56,60 0/0:48:4:51,51 0/0:61:2
        0/1:35:4       1/0:17:2       1/1:40:3
        0/0:10:3       1/1:10:3       ./.:10:3
        0/0:10:3       ./.            1/1:10:3
        0/0:10:3       ./.            1/1:10:3
        '''
        vf = VcfFrame.load_vcf('resources/example.unphased2.vcf')
        vf2 = vf.remove_non_variant_records(vf.samples)
        self.assertEqual(vf.shape[0], 8)
        self.assertEqual(vf2.shape[0], 7)

    def test_count_base_changes(self):
        '''
        Test counting each type of base change from sample 1 to sample 2 as a defaultdict
        REF    ALT     NA00001        NA00002        NA00004
        G      A       0/0:48:1:51,51 1/0:48:8:51,51 1/1:43:5:.,.
        T      A       0/1:49:3:58,50 0/1:3:5:65,3   0/0:41:3
        A      G,T     1/1:21:6:23,27 2/1:2:0:18,2   2/2:35:4
        T      .       0/0:54:7:56,60 0/0:48:4:51,51 0/0:61:2
        GTC    G,GTCT  0/1:35:4       1/0:17:2       1/1:40:3
        A      T       0/0:10:3       1/1:10:3       ./.:10:3
        C      G       0/0:10:3       ./.            1/1:10:3
        A      T       0/0:10:3       ./.            1/1:10:3
        NA00001 NA00004
        G/G     A/A
        T/A     T/T
        G/G     T/T
        T/T     T/T
        GTC/G   G/G
        A/A     N/N
        C/C     G/G
        A/A     T/T
        '''
        vf = VcfFrame.load_vcf('resources/example.unphased2.vcf')
        base_changes = vf.count_base_changes('NA00001', 'NA00004')
        self.assertEqual(len(base_changes), 8)
        self.assertEqual(base_changes[('G/G','A/A')], 1)
        self.assertEqual(base_changes[('G/G','T/T')], 1)
        self.assertEqual(base_changes[('GTC/G','G/G')], 1)
        self.assertEqual(base_changes[('A/A','N/N')], 1)

    def test_set_genotype_counts(self):
        '''
        Test setting a new column of genotype counts
        '''
        vf = VcfFrame.load_vcf('resources/example.unphased2.vcf')
        vf['gtcounts'] = vf.apply(lambda row: defaultdict(int), axis=1)
        vf.set_genotype_counts(vf.samples, newcolname='gtcounts', overwrite=True)
        self.assertEqual(len(vf['gtcounts'][0]), 3)
        self.assertEqual(vf['gtcounts'][0][('0','0')], 1)
        self.assertEqual(vf['gtcounts'][0][('.','.')], 0)
        self.assertEqual(vf['gtcounts'][3][('0','0')], 3)
        self.assertEqual(vf['gtcounts'][5][('.','.')], 1)

    def test_set_categorized_genotype_counts(self):
        '''
        Test setting a new column of categorized genotype counts
        REF    ALT     NA00001        NA00002        NA00004
        G      A       0/0:48:1:51,51 1/0:48:8:51,51 1/1:43:5:.,.
        T      A       0/1:49:3:58,50 0/1:3:5:65,3   0/0:41:3
        A      G,T     1/1:21:6:23,27 2/1:2:0:18,2   2/2:35:4
        T      .       0/0:54:7:56,60 0/0:48:4:51,51 0/0:61:2
        GTC    G,GTCT  0/1:35:4       1/0:17:2       1/1:40:3
        A      T       0/0:10:3       1/1:10:3       ./.:10:3
        C      G       0/0:10:3       ./.            1/1:10:3
        A      T       0/0:10:3       ./.            1/1:10:3
        '''
        vf = VcfFrame.load_vcf('resources/example.unphased2.vcf')
        vf['cat_gtcounts'] = vf.apply(lambda row: (1,1,1,1), axis=1)
        vf.set_categorized_genotype_counts(vf.samples, newcolname='cat_gtcounts', overwrite=True)
        self.assertEqual(len(vf['cat_gtcounts'][0]), 4)
        self.assertEqual(len(vf['cat_gtcounts'][1]), 4)
        self.assertEqual(vf['cat_gtcounts'][0], (1,1,1,0))
        self.assertEqual(vf['cat_gtcounts'][1], (1,2,0,0))
        self.assertEqual(vf['cat_gtcounts'][2], (0,0,2,1))
        self.assertEqual(vf['cat_gtcounts'][7], (1,0,1,0))


class TestCaseControlVcfFrame(unittest.TestCase):
    '''
    Unit tests for the CaseControlVcfFrame class
    '''

    def test_fisher_exact_dominant(self):
        '''
        Test fisher exact test under dominant model
                  Alt (A/a + a/a)   Ref (A/A)
        Cases     N11               N12
        Controls  N21               N22
        '''
        cases_homo_ref = 2
        cases_hetero = 4
        cases_homo_alt = 4
        controls_homo_ref = 5
        controls_hetero = 1
        controls_homo_alt = 0
        pval = CaseControlVcfFrame.fisher_exact_dominant(cases_homo_ref,
                                                         cases_hetero,
                                                         cases_homo_alt,
                                                         controls_homo_ref,
                                                         controls_hetero,
                                                         controls_homo_alt)
        self.assertEqual(sp.stats.fisher_exact([[8, 2], [1, 5]])[1], pval)

        cases_homo_ref1 = 0
        cases_hetero1 = 50
        cases_homo_alt1 = 50
        controls_homo_ref1 = 100
        controls_hetero1 = 0
        controls_homo_alt1 = 0

        cases_homo_ref2 = 50
        cases_hetero2 = 25
        cases_homo_alt2 = 25
        controls_homo_ref2 = 50
        controls_hetero2 = 25
        controls_homo_alt2 = 25
        self.assertTrue(CaseControlVcfFrame.fisher_exact_dominant(cases_homo_ref1,
                                                                  cases_hetero1,
                                                                  cases_homo_alt1,
                                                                  controls_homo_ref1,
                                                                  controls_hetero1,
                                                                  controls_homo_alt1) <
                        CaseControlVcfFrame.fisher_exact_dominant(cases_homo_ref2,
                                                                  cases_hetero2,
                                                                  cases_homo_alt2,
                                                                  controls_homo_ref2,
                                                                  controls_hetero2,
                                                                  controls_homo_alt2))
        
        cases_homo_ref1 = 0
        cases_hetero1 = 50
        cases_homo_alt1 = 50
        controls_homo_ref1 = 100
        controls_hetero1 = 50
        controls_homo_alt1 = 0

        cases_homo_ref2 = 0
        cases_hetero2 = 100
        cases_homo_alt2 = 0
        controls_homo_ref2 = 100
        controls_hetero2 = 25
        controls_homo_alt2 = 25
        self.assertEqual(CaseControlVcfFrame.fisher_exact_dominant(cases_homo_ref1,
                                                                   cases_hetero1,
                                                                   cases_homo_alt1,
                                                                   controls_homo_ref1,
                                                                   controls_hetero1,
                                                                   controls_homo_alt1),
                         CaseControlVcfFrame.fisher_exact_dominant(cases_homo_ref2,
                                                                   cases_hetero2,
                                                                   cases_homo_alt2,
                                                                   controls_homo_ref2,
                                                                   controls_hetero2,
                                                                   controls_homo_alt2))


    def test_fisher_exact_recessive(self):
        '''
        Test fisher exact test under dominant model
                  Alt (a/a)   Ref (A/A + A/a)
        Cases     N11         N12
        Controls  N21         N22
        '''
        cases_homo_ref = 1
        cases_hetero = 1
        cases_homo_alt = 8
        controls_homo_ref = 3
        controls_hetero = 2
        controls_homo_alt = 1
        pval = CaseControlVcfFrame.fisher_exact_recessive(cases_homo_ref,
                                                          cases_hetero,
                                                          cases_homo_alt,
                                                          controls_homo_ref,
                                                          controls_hetero,
                                                          controls_homo_alt)
        self.assertEqual(sp.stats.fisher_exact([[8, 2], [1, 5]])[1], pval)

        cases_homo_ref1 = 0
        cases_hetero1 = 0
        cases_homo_alt1 = 100
        controls_homo_ref1 = 100
        controls_hetero1 = 0
        controls_homo_alt1 = 0

        cases_homo_ref2 = 25
        cases_hetero2 = 25
        cases_homo_alt2 = 59
        controls_homo_ref2 = 25
        controls_hetero2 = 25
        controls_homo_alt2 = 50
        self.assertTrue(CaseControlVcfFrame.fisher_exact_recessive(cases_homo_ref1,
                                                                   cases_hetero1,
                                                                   cases_homo_alt1,
                                                                   controls_homo_ref1,
                                                                   controls_hetero1,
                                                                   controls_homo_alt1) <
                        CaseControlVcfFrame.fisher_exact_recessive(cases_homo_ref2,
                                                                   cases_hetero2,
                                                                   cases_homo_alt2,
                                                                   controls_homo_ref2,
                                                                   controls_hetero2,
                                                                   controls_homo_alt2))
        
        cases_homo_ref1 = 5
        cases_hetero1 = 5
        cases_homo_alt1 = 100
        controls_homo_ref1 = 10
        controls_hetero1 = 50
        controls_homo_alt1 = 10

        cases_homo_ref2 = 10
        cases_hetero2 = 0
        cases_homo_alt2 = 100
        controls_homo_ref2 = 60
        controls_hetero2 = 0
        controls_homo_alt2 = 10
        self.assertEqual(CaseControlVcfFrame.fisher_exact_recessive(cases_homo_ref1,
                                                                    cases_hetero1,
                                                                    cases_homo_alt1,
                                                                    controls_homo_ref1,
                                                                    controls_hetero1,
                                                                    controls_homo_alt1),
                         CaseControlVcfFrame.fisher_exact_recessive(cases_homo_ref2,
                                                                    cases_hetero2,
                                                                    cases_homo_alt2,
                                                                    controls_homo_ref2,
                                                                    controls_hetero2,
                                                                    controls_homo_alt2))
        
    def test_fisher_exact_allelic(self):
        '''
        Test fisher exact test under dominant model
                  count(a)    count(A)
        Cases     N11         N12
        Controls  N21         N22
        '''
        cases_alt = 8
        cases_ref = 2
        controls_alt = 1
        controls_ref = 5
        pval = CaseControlVcfFrame.fisher_exact_allelic(cases_ref,
                                                        cases_alt,
                                                        controls_ref,
                                                        controls_alt)
        self.assertEqual(sp.stats.fisher_exact([[8, 2], [1, 5]])[1], pval)

        cases_alt1 = 100
        cases_ref1 = 0
        controls_alt1 = 0
        controls_ref1 = 100

        cases_alt2 = 50
        cases_ref2 = 50
        controls_alt2 = 50
        controls_ref2 = 50
        self.assertTrue(CaseControlVcfFrame.fisher_exact_allelic(cases_ref1,
                                                                 cases_alt1,
                                                                 controls_ref1,
                                                                 controls_alt1) <
                        CaseControlVcfFrame.fisher_exact_allelic(cases_ref2,
                                                                 cases_alt2,
                                                                 controls_ref2,
                                                                 controls_alt2))
        
        cases_alt1 = 100
        cases_ref1 = 0
        controls_alt1 = 0
        controls_ref1 = 100

        cases_alt2 = 10
        cases_ref2 = 0
        controls_alt2 = 0
        controls_ref2 = 10
        self.assertTrue(CaseControlVcfFrame.fisher_exact_allelic(cases_ref1,
                                                                 cases_alt1,
                                                                 controls_ref1,
                                                                 controls_alt1) <
                        CaseControlVcfFrame.fisher_exact_allelic(cases_ref2,
                                                                 cases_alt2,
                                                                 controls_ref2,
                                                                 controls_alt2))

    def test_set_case_control_pvals_dominant(self):
        '''
        Test setting a new column containing p-values for the dominant model
        NA00001        NA00002        NA00004
        0/0:48:1:51,51 1/0:48:8:51,51 1/1:43:5:.,.
        0/1:49:3:58,50 0/1:3:5:65,3   0/0:41:3
        1/1:21:6:23,27 2/1:2:0:18,2   2/2:35:4
        0/0:54:7:56,60 0/0:48:4:51,51 0/0:61:2
        0/1:35:4       1/0:17:2       1/1:40:3
        0/0:10:3       1/1:10:3       ./.:10:3
        0/0:10:3       ./.            1/1:10:3
        0/0:10:3       ./.            1/1:10:3
        '''
        vf = CaseControlVcfFrame.load_vcf('resources/example.unphased2.vcf')
        vf.set_case_control_pvals_dominant(['NA00001'],
                                           ['NA00002','NA00004'],
                                           newcolname='pval')
        self.assertEqual(round(vf['pval'][0], 5),
                         round(sp.stats.fisher_exact([[0, 1],
                                                      [2, 0]])[1], 5))

        self.assertEqual(round(vf['pval'][2], 5),
                         round(sp.stats.fisher_exact([[1, 0],
                                                      [2, 0]])[1], 5))
        
        self.assertEqual(round(vf['pval'][3], 5),
                         round(sp.stats.fisher_exact([[0, 0],
                                                      [0, 0]])[1], 5))

    def test_set_case_control_pvals_recessive(self):
        '''
        Test setting a new column containing p-values for the dominant model
        NA00001        NA00002        NA00004
        0/0:48:1:51,51 1/0:48:8:51,51 1/1:43:5:.,.
        0/1:49:3:58,50 0/1:3:5:65,3   0/0:41:3
        1/1:21:6:23,27 2/1:2:0:18,2   2/2:35:4
        0/0:54:7:56,60 0/0:48:4:51,51 0/0:61:2
        0/1:35:4       1/0:17:2       1/1:40:3
        0/0:10:3       1/1:10:3       ./.:10:3
        0/0:10:3       ./.            1/1:10:3
        0/0:10:3       ./.            1/1:10:3
        '''
        vf = CaseControlVcfFrame.load_vcf('resources/example.unphased2.vcf')
        vf.set_case_control_pvals_recessive(['NA00001'],
                                            ['NA00002','NA00004'],
                                            newcolname='pval')
        self.assertEqual(round(vf['pval'][0], 5),
                         round(sp.stats.fisher_exact([[0, 1],
                                                      [1, 1]])[1], 5))

        self.assertEqual(round(vf['pval'][1], 5),
                         round(sp.stats.fisher_exact([[0, 1],
                                                      [0, 0]])[1], 5))

        self.assertEqual(round(vf['pval'][2], 5),
                         round(sp.stats.fisher_exact([[1, 0],
                                                      [2, 0]])[1], 5))

        self.assertEqual(round(vf['pval'][7], 5),
                         round(sp.stats.fisher_exact([[0, 1],
                                                      [1, 0]])[1], 5))

    def test_set_case_control_pvals_allelic(self):
        '''
        Test setting a new column containing p-values for the allelic model

        NA00001        NA00002        NA00004
        0/0:48:1:51,51 1/0:48:8:51,51 1/1:43:5:.,.
        0/1:49:3:58,50 0/1:3:5:65,3   0/0:41:3
        1/1:21:6:23,27 2/1:2:0:18,2   2/2:35:4
        0/0:54:7:56,60 0/0:48:4:51,51 0/0:61:2
        0/1:35:4       1/0:17:2       1/1:40:3
        0/0:10:3       1/1:10:3       ./.:10:3
        0/0:10:3       ./.            1/1:10:3
        0/0:10:3       ./.            1/1:10:3
        '''
        vf = CaseControlVcfFrame.load_vcf('resources/example.unphased2.vcf')
        vf.set_case_control_pvals_allelic(['NA00001'],
                                          ['NA00002','NA00004'],
                                          newcolname='pval')
        self.assertEqual(round(vf['pval'][0], 5),
                         round(sp.stats.fisher_exact([[0, 2],
                                                      [3, 1]])[1], 5))
        self.assertEqual(round(vf['pval'][2], 5),
                         round(sp.stats.fisher_exact([[2, 0],
                                                      [4, 0]])[1], 5))
        self.assertEqual(round(vf['pval'][3], 5),
                         round(sp.stats.fisher_exact([[0, 2],
                                                      [0, 4]])[1], 5))
        self.assertEqual(round(vf['pval'][7], 5),
                         round(sp.stats.fisher_exact([[0, 2],
                                                      [2, 0]])[1], 5))

    def test_case_control_pvals_all(self):
        '''
        Test setting p-value result columns for all available types of fisher exact tests
        '''
        vf = CaseControlVcfFrame.load_vcf('resources/example.unphased2.vcf')
        vf.set_case_control_pvals_all(['NA00001'],
                                      ['NA00002','NA00004'],
                                      newcolprefix='pval')

        self.assertTrue('pval_dominant' in vf.columns)
        self.assertTrue('pval_recessive' in vf.columns)
        self.assertTrue('pval_allelic' in vf.columns)


class TestSNPEffVcfFrame(unittest.TestCase):
    '''
    Unit tests for the SNPEffVcfFrame class
    '''
    
    def test_get_effects(self):
        '''
        Test parsing the info string to extract the annotated effects
        '''
        infostr = "AC=8;AF=0.043;AN=186;BaseQRankSum=-1.195;DB;DP=248;Dels=0.00;FS=1.695;HaplotypeScore=0.0884;InbreedingCoeff=-0.0548;MLEAC=8;MLEAF=0.043;MQ=60.00;MQ0=0;MQRankSum=2.031;QD=5.41;ReadPosRankSum=1.243;VQSLOD=6.10;culprit=FS;EFF=DOWNSTREAM(MODIFIER||493|||AGRN|processed_transcript|CODING|ENST00000477585||1),EXON(MODIFIER|||||AGRN|retained_intron|CODING|ENST00000469403|3|1),NON_SYNONYMOUS_CODING(MODERATE|MISSENSE|aCc/aTc|T258I|2045|AGRN|protein_coding|CODING|ENST00000379370|5|1),UPSTREAM(MODIFIER||4181|||AGRN|retained_intron|CODING|ENST00000479707||1);dbNSFP_GERP++_RS=3.76;dbNSFP_Ensembl_transcriptid=ENST00000379370;dbNSFP_Uniprot_acc=O00468;dbNSFP_29way_logOdds=9.5878;dbNSFP_GERP++_NR=4.68;dbNSFP_Polyphen2_HVAR_pred=D;dbNSFP_Interpro_domain=Follistatin-like,_N-terminal_(1),;dbNSFP_SIFT_score=0.520000;SNP"
        effects = SNPEffVcfFrame.get_effects(infostr)
        self.assertEqual(len(effects), 4)
        self.assertEqual(effects[0].effect, 'NON_SYNONYMOUS_CODING')
        self.assertEqual(effects[0].impact, 'MODERATE')
        self.assertEqual(effects[0].functional_class, 'MISSENSE')
        self.assertEqual(effects[0].codon_change, 'aCc/aTc')
        self.assertEqual(effects[0].aa_change, 'T258I')
        self.assertEqual(effects[0].aa_len, '2045')
        self.assertEqual(effects[0].gene, 'AGRN')
        self.assertEqual(effects[0].gene_biotype, 'protein_coding')
        self.assertEqual(effects[0].coding, 'CODING')
        self.assertEqual(effects[0].transcript, 'ENST00000379370')
        self.assertEqual(effects[0].exon, '5')
        self.assertEqual(effects[0].genotype_num, '1')
        self.assertEqual(effects[1].effect, 'UPSTREAM')

    def test_get_effect(self):
        '''
        Test getting the single highest priority effect
        '''
        infostr = "AC=8;AF=0.043;AN=186;BaseQRankSum=-1.195;DB;DP=248;Dels=0.00;FS=1.695;HaplotypeScore=0.0884;InbreedingCoeff=-0.0548;MLEAC=8;MLEAF=0.043;MQ=60.00;MQ0=0;MQRankSum=2.031;QD=5.41;ReadPosRankSum=1.243;VQSLOD=6.10;culprit=FS;EFF=DOWNSTREAM(MODIFIER||493|||AGRN|processed_transcript|CODING|ENST00000477585||1),EXON(MODIFIER|||||AGRN|retained_intron|CODING|ENST00000469403|3|1),NON_SYNONYMOUS_CODING(MODERATE|MISSENSE|aCc/aTc|T258I|2045|AGRN|protein_coding|CODING|ENST00000379370|5|1),UPSTREAM(MODIFIER||4181|||AGRN|retained_intron|CODING|ENST00000479707||1);dbNSFP_GERP++_RS=3.76;dbNSFP_Ensembl_transcriptid=ENST00000379370;dbNSFP_Uniprot_acc=O00468;dbNSFP_29way_logOdds=9.5878;dbNSFP_GERP++_NR=4.68;dbNSFP_Polyphen2_HVAR_pred=D;dbNSFP_Interpro_domain=Follistatin-like,_N-terminal_(1),;dbNSFP_SIFT_score=0.520000;SNP"
        effect = SNPEffVcfFrame.get_effect(infostr)
        self.assertEqual(effect.effect, 'NON_SYNONYMOUS_CODING')
        self.assertEqual(effect.impact, 'MODERATE')
        self.assertEqual(effect.functional_class, 'MISSENSE')
        self.assertEqual(effect.codon_change, 'aCc/aTc')
        self.assertEqual(effect.aa_change, 'T258I')
        self.assertEqual(effect.aa_len, '2045')
        self.assertEqual(effect.gene, 'AGRN')
        self.assertEqual(effect.gene_biotype, 'protein_coding')
        self.assertEqual(effect.coding, 'CODING')
        self.assertEqual(effect.transcript, 'ENST00000379370')
        self.assertEqual(effect.exon, '5')
        self.assertEqual(effect.genotype_num, '1')

    def test_set_effecttuple_field(self):
        '''
        Test parsing the INFO column string to extract annotated effects
        INFO    FORMAT  NORMAL  TUMOR
        DP=27;SOMATIC;SS=2;SSC=14;GPV=1E0;SPV=3.7681E-2;EFF=DOWNSTREAM(MODIFIER|||||MMP23B|protein_coding|CODING|ENST00000356026||),TRANSCRIPT(MODIFIER|||||AL691432.2|unprocessed_pseudogene|NON_CODING|ENST00000317673||),SPLICE_SITE_ACCEPTOR(HIGH|||||WASH2P|unprocessed_pseudogene|NON_CODING|ENST00000542901||),UTR_5_PRIME(MODIFIER|||||ARHGEF16|protein_coding|CODING|ENST00000378371|exon_1_3383535_3383901|),NON_SYNONYMOUS_CODING(MODERATE|MISSENSE|Cgg/Tgg|R10W||CYP4B1|processed_transcript|CODING|ENST00000468637|exon_1_47279154_47279278|)         GT:GQ:DP:RD:AD:FREQ     0/0:.:12:11:1:8.33%     0/1:.:15:8:7:46.67%
        DP=76;SOMATIC;SS=2;SSC=14;GPV=1E0;SPV=3.1816E-2;EFF=TRANSCRIPT(MODIFIER|||||AL691432.2|unprocessed_pseudogene|NON_CODING|ENST00000317673||),TRANSCRIPT(MODIFIER|||||AL691432.2|unprocessed_pseudogene|NON_CODING|ENST00000340677||),TRANSCRIPT(MODIFIER|||||AL691432.2|unprocessed_pseudogene|NON_CODING|ENST00000341832||),TRANSCRIPT(MODIFIER|||||AL691432.2|unprocessed_pseudogene|NON_CODING|ENST00000407249||),TRANSCRIPT(MODIFIER|||||AL691432.2|unprocessed_pseudogene|NON_CODING|ENST00000513088||)               GT:GQ:DP:RD:AD:FREQ     1/1:.:25:15:4:21.05%    0/1:.:51:25:24:48.98%
        '''
        vf = SNPEffVcfFrame.load_vcf('resources/example.varscan.snpeff.vcf')
        vf.set_effecttuple_field(newcolname='foo')
        self.assertEqual(type(vf['foo'][0]), SNPEffVcfFrame.Effect)
        self.assertEqual(vf['foo'][0].effect, 'SPLICE_SITE_ACCEPTOR')
        self.assertEqual(vf['foo'][0].gene, 'WASH2P')
        
    def test_set_effect_field(self):
        '''
        Test setting a specific effect field
        SPLICE_SITE_ACCEPTOR(HIGH|||||WASH2P|unprocessed_pseudogene|NON_CODING|ENST00000542901||)
        TRANSCRIPT(MODIFIER|||||AL691432.2|unprocessed_pseudogene|NON_CODING|ENST00000317673||)
        '''
        vf = SNPEffVcfFrame.load_vcf('resources/example.varscan.snpeff.vcf')
        vf.set_effect_field('gene', newcolname='Gene Symbol', overwrite=True)
        vf.set_effect_field('effect', newcolname='FooEffect', overwrite=True)
        self.assertEqual(vf['Gene Symbol'][0], 'WASH2P')
        self.assertEqual(vf['FooEffect'][0], 'SPLICE_SITE_ACCEPTOR')
        self.assertEqual(vf['Gene Symbol'][1], 'AL691432.2')
        self.assertEqual(vf['FooEffect'][1], 'TRANSCRIPT')

    def test_effect_pivot(self):
        '''
        Test generating a pivot table of effect counts
        #CHROM   POS         ID           REF
        2        167085495   .            A
        7        116778457   .            TA
        9        125438378   rs79636164   C
        16       58030634    rs3743556    C
        22       34022284    rs86487      G
        X        49179755    rs7064530    G

        Result:
        dbSNP_novel            dbSNP  dbSNP %  novel  novel %  Total
        effect                                                      
        DOWNSTREAM                 0      0.0      1      0.5      1
        INTRON                     0      0.0      1      0.5      1
        NON_SYNONYMOUS_CODING      2      0.5      0      0.0      2
        SYNONYMOUS_CODING          2      0.5      0      0.0      2
        TOTAL                      4      1.0      2      1.0      6
        '''
        vf = SNPEffVcfFrame.load_vcf('resources/example.germline.snpeff.vcf')
        pt = vf.effect_pivot()
        self.assertEqual(list(pt.columns), ['dbSNP', 'dbSNP %', 'novel', 'novel %', 'Total'])
        self.assertEqual(pt['dbSNP'][0], 0)
        self.assertEqual(pt.ix['NON_SYNONYMOUS_CODING','dbSNP'], 2)
        self.assertEqual(pt.ix['TOTAL', 'dbSNP'], 4)
        self.assertEqual(pt.ix['TOTAL', 'novel'], 2)
        self.assertEqual(pt.ix['DOWNSTREAM', 'novel %'], 0.5)
        self.assertEqual(pt.ix['INTRON', 'novel %'], 0.5)
        self.assertEqual(pt.ix['NON_SYNONYMOUS_CODING', 'dbSNP %'], 0.5)
        self.assertEqual(pt.ix['SYNONYMOUS_CODING', 'dbSNP %'], 0.5)
        self.assertEqual(pt.ix['NON_SYNONYMOUS_CODING', 'novel %'], 0)
        self.assertEqual(pt.ix['SYNONYMOUS_CODING', 'novel %'], 0)
        self.assertEqual(pt.ix['TOTAL', 'Total'], 6)


class TestSNPSiftVcfFrame(unittest.TestCase):
    '''
    Unit tests for the SNPSiftVcfFrame class
    '''
    
    def test_merge_delim_str(self):
        '''
        Test merging a bunch of semi-colon-delimited values into a single unique list
        '''
        self.assertEqual(SNPSiftVcfFrame.merge_delim_str(['d;e;b', 'a;b;c;a', 'e;a;b;c;f']),
                         ['a','b','c','d','e','f'])

        self.assertEqual(SNPSiftVcfFrame.merge_delim_str(['d|e|b', 'a|b|c|a', 'e|a|b|c|f'], sep='|'),
                         ['a','b','c','d','e','f'])


    def test_flatten_merge_delim_str(self):
        '''
        Test flattening the results of merge_delim_str into a single string with values
        delimited by the original delimiters
        '''
        self.assertEqual(SNPSiftVcfFrame.flatten_merge_delim_str(['d;e;b', 'a;b;c;a', 'e;a;b;c;f']),
                         'a;b;c;d;e;f')

        self.assertEqual(SNPSiftVcfFrame.flatten_merge_delim_str(['d|e|b', 'a|b|c|a', 'e|a|b|c|f'],
                                                                 sep='|'),
                         'a|b|c|d|e|f')
        
    def test_count_delim_str(self):
        '''
        Test counting the number of unique elements in a delimited string
        '''
        self.assertEqual(SNPSiftVcfFrame.count_delim_str('a;b;c;d;e;f'), 6)
        self.assertEqual(SNPSiftVcfFrame.count_delim_str('a|b|c|d|e', sep='|'), 5)
        

if __name__ == '__main__':
    unittest.main()
