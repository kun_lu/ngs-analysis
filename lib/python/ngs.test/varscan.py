#!/usr/bin/env python
'''
Description     : Unit test for varscan.py libary
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.10.11
'''

import os
import sys
import unittest
from ngs import varscan

RESOURCE_DIR = 'resources'
EXAMPLE_VARSCAN_SNP = 'example.varscan.snp'

class TestVarScanFileFunctions(unittest.TestCase):
    
    def setUp(self):
        self.example_varscan_snp = os.path.join(RESOURCE_DIR, EXAMPLE_VARSCAN_SNP)

    def test_parse_row(self):
        with open(self.example_varscan_snp, 'r') as f:
            vfile = varscan.VarScanFile(f)

            # First line
            col2val = vfile.next()
            self.assertEqual(col2val['chrom'], 'chr1')
            self.assertEqual(col2val['position'], '14907')
            self.assertEqual(col2val['somatic_status'], 'Germline')
            self.assertEqual(col2val['normal_reads2_minus'], '5')

            # Second line
            col2val = vfile.next()
            
            # Third line
            col2val = vfile.next()
            self.assertEqual(col2val['normal_var_freq'], '52.17%')

    def test_filtered_variants(self):
        # Filter germline
        with open(self.example_varscan_snp, 'r') as f:
            vfile = varscan.VarScanFile(f)

            num_germline = 0
            for variant in vfile.filtered_variants(somatic_status='Germline'):
                num_germline += 1
            self.assertEqual(num_germline, 27)

        # Filter somatic
        with open(self.example_varscan_snp, 'r') as f:
            vfile = varscan.VarScanFile(f)

            num_somatic = 0
            for variant in vfile.filtered_variants(somatic_status='Somatic'):
                num_somatic += 1
            self.assertEqual(num_somatic, 1)

        # Filter min_somatic_pval
        with open(self.example_varscan_snp, 'r') as f:
            vfile = varscan.VarScanFile(f)
            num_variants = 0
            for variant in vfile.filtered_variants(min_somatic_pval=0.05):
                num_variants += 1
            self.assertEqual(num_variants, 2)
        with open(self.example_varscan_snp, 'r') as f:
            vfile = varscan.VarScanFile(f)
            num_variants = 0
            for variant in vfile.filtered_variants(min_somatic_pval=0.95):
                num_variants += 1
            self.assertEqual(num_variants, 8)

        # Filter min_dp_normal
        with open(self.example_varscan_snp, 'r') as f:
            vfile = varscan.VarScanFile(f)
            num_variants = 0
            for variant in vfile.filtered_variants(min_dp_normal=10):
                num_variants += 1
            self.assertEqual(num_variants, 20)
        with open(self.example_varscan_snp, 'r') as f:
            vfile = varscan.VarScanFile(f)
            num_variants = 0
            for variant in vfile.filtered_variants(min_dp_normal=21):
                num_variants += 1
            self.assertEqual(num_variants, 9)

        # Filter min_dp_tumor
        with open(self.example_varscan_snp, 'r') as f:
            vfile = varscan.VarScanFile(f)
            num_variants = 0
            for variant in vfile.filtered_variants(min_dp_tumor=10):
                num_variants += 1
            self.assertEqual(num_variants, 28)
        with open(self.example_varscan_snp, 'r') as f:
            vfile = varscan.VarScanFile(f)
            num_variants = 0
            for variant in vfile.filtered_variants(min_dp_tumor=20):
                num_variants += 1
            self.assertEqual(num_variants, 16)

        # Filter somatic_status and min_dp_tumor
        with open(self.example_varscan_snp, 'r') as f:
            vfile = varscan.VarScanFile(f)
            num_variants = 0
            for variant in vfile.filtered_variants(min_dp_tumor=15, somatic_status='Germline'):
                num_variants += 1
            self.assertEqual(num_variants, 23)

if __name__ == '__main__':
    unittest.main()
