#!/usr/bin/env python
'''
Description     : Unit test for fasta.py libary
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.10.11
'''

import os
import sys
import unittest
from ngs.fasta import RandomAccessRecords

resource_dir = 'resources'
example_fasta = 'example.fasta'

class TestRandomAccessRecords(unittest.TestCase):

    def setUp(self):
        self.fastafile = os.path.join(resource_dir, example_fasta)

    def test_get(self):
        raccess = RandomAccessRecords(self.fastafile)
        record = raccess.get('record1')
        self.assertEqual(record.seq[0], 'A')
        self.assertEqual(record.seq[-1], 'A')
        self.assertEqual(len(record.seq), 100)

        record = raccess.get('record3')
        self.assertEqual(record.seq[0], 'G')
        self.assertEqual(record.seq[-1], 'G')
        self.assertEqual(len(record.seq), 100)

        record = raccess.get('record4')
        self.assertEqual(record.seq[0], 'T')
        self.assertEqual(record.seq[-1], 'T')
        self.assertEqual(len(record.seq), 100)

        record = raccess.get('record2')
        self.assertEqual(record.seq[0], 'C')
        self.assertEqual(record.seq[-1], 'C')
        self.assertEqual(len(record.seq), 100)

    def test_get_n_masked_seq(self):
        raccess = RandomAccessRecords(self.fastafile)

        sequence = raccess.get_n_masked_seq('record4', (50,51), (1,1))
        self.assertEqual(sequence, 'T'*49 + 'NN' + 'T'*49)
        
        sequence = raccess.get_n_masked_seq('record1', (1,2), (1,1))
        self.assertEqual(sequence, 'NN' + 'A'*98)

        sequence = raccess.get_n_masked_seq('record2', (1,), (11,))
        self.assertEqual(sequence, 'N'*11 + 'C'*89)

        sequence = raccess.get_n_masked_seq('record3', (25,100), (4, 1))
        self.assertEqual(sequence, 'G'*24 + 'NNNN' + 'G'*71 + 'N')
        
        

if __name__ == '__main__':
    unittest.main()
